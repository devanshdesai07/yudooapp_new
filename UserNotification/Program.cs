﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace ShopOwnerNotifications
{
    class Program
    {

        public static string ConnStr = ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString;//"Data Source=databaseyudoo.cdxog55h32lu.ap-south-1.rds.amazonaws.com;Initial Catalog=yudoo-dev;Persist Security Info=True;User ID=admin;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        static void Main(string[] args)
        {


            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnStr);

            while (true)
            {
                try
                {

                    string q = "select * from [dbo].[UserNotifications] where IsSent = 2 and CreatedDate >= DATEADD(minute, -1,  GETDATE())";
                    SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);


                    string qry = "select * from [dbo].[ShopOwnerNotifications] where IsSent = 2 and CreatedDate >= DATEADD(minute, -1,  GETDATE())";
                    SqlDataAdapter sda = new SqlDataAdapter(qry, ConnStr);
                    DataTable dtl = new DataTable();
                    sda.Fill(dtl);


                    if (dt.Rows.Count > 0)
                    {
                        List<string> DeviceT = new List<string>();
                        Console.WriteLine("Getting Connection ...");

                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["Id"].ToString() != "")
                            {
                                string getDT = "select DeviceToken from [dbo].[UserDevices] where UserId = " + row["UserId"].ToString() + " AND DeviceToken IS NOT NULL order by Id desc"; /*AND IsVerified=1*/
                                //string getDT = "select DeviceToken from [dbo].[UserDevices] where Id = 13  AND LastLogin IS NULL  order by Id desc"; /*AND IsVerified=1*/
                                SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                                DataTable DEDeviceToken = new DataTable();
                                DT.Fill(DEDeviceToken);
                                DeviceT = new List<string>();

                                if (DEDeviceToken.Rows.Count > 0)
                                {
                                    DeviceT = DEDeviceToken.AsEnumerable()
                                              .Select(r => r.Field<string>("DeviceToken"))
                                              .ToList();

                                    if (DeviceT.Count > 0)
                                    {
                                        SendUserNotification(DeviceT, row["description"].ToString(), row["id"].ToString());
                                    }
                                }
                            }
                        }
                    }

                    //ShopOwnerDevices
                    if (dtl.Rows.Count > 0)
                    {
                        List<string> DeviceT = new List<string>();
                        Console.WriteLine("Getting Connection ...");

                        foreach (DataRow row in dtl.Rows)
                        {
                            if (row["Id"].ToString() != "")
                            {
                                //string getDT = "select DeviceToken from [dbo].[UserDevices] where Id = " + row["Id"].ToString() + " AND LastLogin IS NULL  order by Id desc"; /*AND IsVerified=1*/
                                string getDT = "select DeviceToken from [dbo].[ShopOwnerDevices] where shopownerid = " + row["shopownerid"].ToString() + " AND DeviceToken IS NOT NULL  order by Id desc"; /*AND IsVerified=1*/
                                SqlDataAdapter DT = new SqlDataAdapter(getDT, ConnStr);
                                DataTable DEDeviceToken = new DataTable();
                                DT.Fill(DEDeviceToken);
                                DeviceT = new List<string>();

                                if (DEDeviceToken.Rows.Count > 0)
                                {
                                    DeviceT = DEDeviceToken.AsEnumerable()
                                              .Select(r => r.Field<string>("DeviceToken"))
                                              .ToList();

                                    if (DeviceT.Count > 0)
                                    {
                                        SendShopOwnerNotification(DeviceT, row["description"].ToString(), row["id"].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }

            Console.Read();
        }


        public static string SendUserNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = "AAAAxv5XDwg:APA91bFrEM6TjrK3qirMU3EIFmJ4wM2NyUkJRZCB4thI3-hpwllvQVxaHBmG5nqVElP_9j5vhOXTwTz_sXG9adMVcuRcFRVbVOXajnjL_O_FUAIFUOtN5iF1pV5Pbozm8HThMSH6P0o0"; // Something very long
            //string serverKey = "AAAA6tsjjhM:APA91bEbAdQr_VHZ5pHjtTUhspCHe_bN2GTQfYXbgu4skFl7n2_jGcAhw2A8Rox1qAf5UyFW7eSpVmREt-9VcWe_MENAnNSU9ZUDFw7WUzxFt06kcsB2p0mtor1mAp4IRQSspn0QnLNK"; // Something very long
            //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            //var data = new
            //{
            //    registration_ids = DeviceT,
            //    //to = deviceId,
            //    data = new
            //    {
            //        body = Description,//"Your job Status has been changed",                    
            //        title = "test",//"Fitting job has been completed.",
            //        sound = "default",
            //        mutable_contenct = true,
            //        badge = 4,
            //        data = new
            //        {

            //        }
            //    },
            //    mutable_contenct = true
            //};

            //dynamic data = new
            //{
            //    to = DeviceT, // Uncoment this if you want to test for single device
            //                             // registration_ids = singlebatch, // this is for multiple user 
            //    notification = new
            //    {
            //        title = "Test",         // Notification title
            //        body = Description,     // Notification body data
            //        link =  ""              // When click on notification user redirect to this link
            //    }
            //};

            var data = new
            {
                registration_ids = DeviceT,
                //notification = new
                //{
                //    body = Description,
                //    title = "Grettings from Yudoo !",
                //    sound = "Enabled"
                //},
                data = new
                {
                    payload = Description
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();
                            string updateIsSent = "UPDATE UserNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();

                            string updateIsSent2 = "update UserNotifications set IsSent = 1 where IsSent =2 and CreatedDate < DATEADD(minute, -1,  GETDATE())";
                            SqlCommand UpdateDENotificationcmd2 = new SqlCommand(updateIsSent2, Conn);
                            UpdateDENotificationcmd2.ExecuteNonQuery();

                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }

            return "";
        }

        public static string SendShopOwnerNotification(List<string> DeviceT, string Description, string Id)
        {

            string response = string.Empty;
            string serverKey = "AAAAxv5XDwg:APA91bFrEM6TjrK3qirMU3EIFmJ4wM2NyUkJRZCB4thI3-hpwllvQVxaHBmG5nqVElP_9j5vhOXTwTz_sXG9adMVcuRcFRVbVOXajnjL_O_FUAIFUOtN5iF1pV5Pbozm8HThMSH6P0o0"; // Something very long
                                                                                                                                                                                           //string senderId = "439168650109";
            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            //var data = new
            //{
            //    registration_ids = DeviceT,
            //    //to = deviceId,
            //    data = new
            //    {
            //        body = Description.Replace("'", "\""),//"Your job Status has been changed",                    
            //        title = "test",//"Fitting job has been completed.",
            //        sound = "default",
            //        mutable_contenct = true,
            //        badge = 4,
            //        data = new { }
            //    },
            //    mutable_contenct = true
            //};


            var data = new
            {
                registration_ids = DeviceT,
                //notification = new
                //{
                //    body = Description,
                //    title = "Grettings from Yudoo !",
                //    sound = "Enabled"
                //},
                data = new
                {
                    payload = Description
                }
            };


            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();
                            string updateIsSent = "UPDATE ShopOwnerNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();
                            
                            string updateIsSent2 = "update ShopOwnerNotifications set IsSent = 1 where IsSent =2 and CreatedDate < DATEADD(minute, -1,  GETDATE())";
                            SqlCommand UpdateDENotificationcmd2 = new SqlCommand(updateIsSent2, Conn);
                            UpdateDENotificationcmd2.ExecuteNonQuery();
                            
                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }

            return "";
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class BankMasterDao : AbstractBankMasterDao
    {

        public override SuccessResult<AbstractBankMaster> BankMaster_Upsert(AbstractBankMaster abstractBankMaster)
        {
            SuccessResult<AbstractBankMaster> BankMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractBankMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractBankMaster.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@BankName", abstractBankMaster.BankName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BankAccountNo", abstractBankMaster.BankAccountNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IFSCCode", abstractBankMaster.IFSCCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AccountHolderName", abstractBankMaster.AccountHolderName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractBankMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractBankMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BankMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                BankMaster = task.Read<SuccessResult<AbstractBankMaster>>().SingleOrDefault();
                BankMaster.Item = task.Read<BankMaster>().SingleOrDefault();
            }

            return BankMaster;
        }

        public override SuccessResult<AbstractBankMaster> BankMaster_ById(long Id)
        {
            SuccessResult<AbstractBankMaster> BankMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BankMaster_ById, param, commandType: CommandType.StoredProcedure);
                BankMaster = task.Read<SuccessResult<AbstractBankMaster>>().SingleOrDefault();
                BankMaster.Item = task.Read<BankMaster>().SingleOrDefault();
            }

            return BankMaster;
        }

        public override PagedList<AbstractBankMaster> BankMaster_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractBankMaster> BankMaster = new PagedList<AbstractBankMaster>();
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BankMaster_ByShopId, param, commandType: CommandType.StoredProcedure);
                BankMaster.Values.AddRange(task.Read<BankMaster>());
                BankMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return BankMaster;
        }

        public override SuccessResult<AbstractBankMaster> BankMaster_ActInAct(long Id)
        {
            SuccessResult<AbstractBankMaster> BankMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BankMaster_ActInAct, param, commandType: CommandType.StoredProcedure);
                BankMaster = task.Read<SuccessResult<AbstractBankMaster>>().SingleOrDefault();
                BankMaster.Item = task.Read<BankMaster>().SingleOrDefault();
            }

            return BankMaster;
        }
    }
}

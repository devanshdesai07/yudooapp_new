﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class TrendingSearchDao : AbstractTrendingSearchDao
    {
        public override PagedList<AbstractTrendingSearch> TrendingSearch_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractTrendingSearch> TrendingSearch = new PagedList<AbstractTrendingSearch>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrendingSearch_All, param, commandType: CommandType.StoredProcedure);
                TrendingSearch.Values.AddRange(task.Read<TrendingSearch>());
                TrendingSearch.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TrendingSearch;
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_ById(long Id)
        {
            SuccessResult<AbstractTrendingSearch> TrendingSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrendingSearch_ById, param, commandType: CommandType.StoredProcedure);
                TrendingSearch = task.Read<SuccessResult<AbstractTrendingSearch>>().SingleOrDefault();
                TrendingSearch.Item = task.Read<TrendingSearch>().SingleOrDefault();
            }

            return TrendingSearch;
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_Delete(long Id)
        {
            SuccessResult<AbstractTrendingSearch> TrendingSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrendingSearch_Delete, param, commandType: CommandType.StoredProcedure);
                TrendingSearch = task.Read<SuccessResult<AbstractTrendingSearch>>().SingleOrDefault();
                TrendingSearch.Item = task.Read<TrendingSearch>().SingleOrDefault();
            }
            return TrendingSearch;
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_ActInActive(long Id)
        {
            SuccessResult<AbstractTrendingSearch> TrendingSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrendingSearch_ActInActive, param, commandType: CommandType.StoredProcedure);
                TrendingSearch = task.Read<SuccessResult<AbstractTrendingSearch>>().SingleOrDefault();
                TrendingSearch.Item = task.Read<TrendingSearch>().SingleOrDefault();
            }
            return TrendingSearch;
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_Upsert(AbstractTrendingSearch abstractTrendingSearch)
        {
            SuccessResult<AbstractTrendingSearch> TrendingSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTrendingSearch.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Text", abstractTrendingSearch.Text, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractTrendingSearch.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractTrendingSearch.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTrendingSearch.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrendingSearch_Upsert, param, commandType: CommandType.StoredProcedure);
                TrendingSearch = task.Read<SuccessResult<AbstractTrendingSearch>>().SingleOrDefault();
                TrendingSearch.Item = task.Read<TrendingSearch>().SingleOrDefault();
            }

            return TrendingSearch;
        }
        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_Update_DisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractTrendingSearch> TrendingSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsUp", IsUp, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TrendingSearch_Update_DisplayOrder, param, commandType: CommandType.StoredProcedure);
                TrendingSearch = task.Read<SuccessResult<AbstractTrendingSearch>>().SingleOrDefault();
                TrendingSearch.Item = task.Read<TrendingSearch>().SingleOrDefault();
            }

            return TrendingSearch;
        }
    }
}

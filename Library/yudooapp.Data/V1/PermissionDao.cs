﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class PermissionDao : AbstractPermissionDao
    {
        public override PagedList<AbstractPermission> Permission_ByCustomerId(PageParam pageParam, string search, AbstractPermission abstractPermission)
        {
            PagedList<AbstractPermission> Permission = new PagedList<AbstractPermission>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractPermission.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Permission_ByCustomerId, param, commandType: CommandType.StoredProcedure);
                Permission.Values.AddRange(task.Read<Permission>());
                Permission.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Permission;
        }

       
      
        public override SuccessResult<AbstractPermission> Permission_Upsert(AbstractPermission abstractPermission)
        {
            SuccessResult<AbstractPermission> Permission = null;
            var param = new DynamicParameters();

             param.Add("@Id", abstractPermission.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PermissionName", abstractPermission.PermissionName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PermissionDescription", abstractPermission.PermissionDescription, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractPermission.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Permission_Upsert, param, commandType: CommandType.StoredProcedure);
                Permission = task.Read<SuccessResult<AbstractPermission>>().SingleOrDefault();
                Permission.Item = task.Read<Permission>().SingleOrDefault();
            }

            return Permission;
        }

     

      

    }
}

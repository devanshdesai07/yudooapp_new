﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopTimgingsDao : AbstractShopTimgingsDao
    {


        public override SuccessResult<AbstractShopTimingJson> ShopTimgings_Upsert(AbstractShopTimingJson abstractShopTimingJson)
        {
            SuccessResult<AbstractShopTimingJson> ShopTimgings = null;
            var param = new DynamicParameters();

            for (int i = 0; i < abstractShopTimingJson.data.Count; i++)
            {
                param.Add("@Id", abstractShopTimingJson.data[i].Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ShopId", abstractShopTimingJson.data[i].ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Day", abstractShopTimingJson.data[i].Day, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@FromTime", abstractShopTimingJson.data[i].FromTime, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@ToTime", abstractShopTimingJson.data[i].ToTime, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@BreaktimeStart ", abstractShopTimingJson.data[i].BreaktimeStart, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@BreakTimeEnd", abstractShopTimingJson.data[i].BreakTimeEnd, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractShopTimingJson.data[i].CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractShopTimingJson.data[i].UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                //param.Add("@IsClosed", abstractShopTimingJson.data[i].IsClosed, dbType: DbType.Boolean, direction: ParameterDirection.Input);


                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.ShopTimgings_Upsert, param, commandType: CommandType.StoredProcedure);
                    ShopTimgings = task.Read<SuccessResult<AbstractShopTimingJson>>().SingleOrDefault();
                    ShopTimgings.Item = task.Read<ShopTimingJson>().SingleOrDefault();
                }
            }
            

            return ShopTimgings;
        }


        public override PagedList<AbstractShopTimgings> ShopTimgings_All(PageParam pageParam, string search , long ShopId)
        {
            PagedList<AbstractShopTimgings> ShopTimgings = new PagedList<AbstractShopTimgings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopTimgings_All, param, commandType: CommandType.StoredProcedure);
                ShopTimgings.Values.AddRange(task.Read<ShopTimgings>());
                ShopTimgings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopTimgings;
        }


        public override PagedList<AbstractShopTimgings> ShopTimgings_ShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractShopTimgings> ShopTimgings = new PagedList<AbstractShopTimgings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);            
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopTimgings_ShopId, param, commandType: CommandType.StoredProcedure);
                ShopTimgings.Values.AddRange(task.Read<ShopTimgings>());
                ShopTimgings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopTimgings;
        }


        public override SuccessResult<AbstractShopTimgings> ShopTimgings_ById(long Id)
        {
            SuccessResult<AbstractShopTimgings> ShopTimgings = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopTimgings_ById, param, commandType: CommandType.StoredProcedure);
                ShopTimgings = task.Read<SuccessResult<AbstractShopTimgings>>().SingleOrDefault();
                ShopTimgings.Item = task.Read<ShopTimgings>().SingleOrDefault();
            }

            return ShopTimgings;
        }


        public override SuccessResult<AbstractShopTimgings> ShopTimgings_IsClosedIsopen(long Id)
        {
            SuccessResult<AbstractShopTimgings> ShopTimgings = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopTimgings_IsClosedIsopen, param, commandType: CommandType.StoredProcedure);
                ShopTimgings = task.Read<SuccessResult<AbstractShopTimgings>>().SingleOrDefault();
                ShopTimgings.Item = task.Read<ShopTimgings>().SingleOrDefault();
            }

            return ShopTimgings;
        }


        public override SuccessResult<AbstractShopTimgings> ShopTimgings_Delete(long Id)
        {
            SuccessResult<AbstractShopTimgings> ShopTimgings = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopTimgings_Delete, param, commandType: CommandType.StoredProcedure);
                ShopTimgings = task.Read<SuccessResult<AbstractShopTimgings>>().SingleOrDefault();
                ShopTimgings.Item = task.Read<ShopTimgings>().SingleOrDefault();
            }

            return ShopTimgings;
        }




    }
}

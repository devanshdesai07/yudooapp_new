﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class BannersDao : AbstractBannersDao
    {


        public override SuccessResult<AbstractBanners> Banners_Upsert(AbstractBanners abstractBanners)
        {
            SuccessResult<AbstractBanners> Banners = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractBanners.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@BannerName", abstractBanners.BannerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Descriptions", abstractBanners.Descriptions, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BannerUrl", abstractBanners.BannerUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BannerType", abstractBanners.BannerType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BannerShopId", abstractBanners.BannerShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractBanners.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractBanners.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractBanners.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NavigationURL", abstractBanners.NavigationURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CuratedShopList", abstractBanners.CuratedShopList, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsSpecialist", abstractBanners.IsSpecialist, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Priority", abstractBanners.Priority, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Banners_Upsert, param, commandType: CommandType.StoredProcedure);
                Banners = task.Read<SuccessResult<AbstractBanners>>().SingleOrDefault();
                Banners.Item = task.Read<Banners>().SingleOrDefault();
            }

            return Banners;
        }


        public override PagedList<AbstractBanners> Banners_All(PageParam pageParam, string search,int IsSpecialist)
        {
            PagedList<AbstractBanners> Banners = new PagedList<AbstractBanners>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsSpecialist", IsSpecialist, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Banners_All, param, commandType: CommandType.StoredProcedure);
                Banners.Values.AddRange(task.Read<Banners>());
                Banners.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Banners;
        }


        public override SuccessResult<AbstractBanners> Banners_ById(long Id)
        {
            SuccessResult<AbstractBanners> Banners = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Banners_ById, param, commandType: CommandType.StoredProcedure);
                Banners = task.Read<SuccessResult<AbstractBanners>>().SingleOrDefault();
                Banners.Item = task.Read<Banners>().SingleOrDefault();
            }

            return Banners;
        }


        public override SuccessResult<AbstractBanners> Banners_ActInAct(long Id)
        {
            SuccessResult<AbstractBanners> Banners = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Banners_ActInAct, param, commandType: CommandType.StoredProcedure);
                Banners = task.Read<SuccessResult<AbstractBanners>>().SingleOrDefault();
                Banners.Item = task.Read<Banners>().SingleOrDefault();
            }

            return Banners;
        }


        public override SuccessResult<AbstractBanners> Banners_Delete(long Id)
        {
            SuccessResult<AbstractBanners> Banners = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Banners_Delete, param, commandType: CommandType.StoredProcedure);
                Banners = task.Read<SuccessResult<AbstractBanners>>().SingleOrDefault();
                Banners.Item = task.Read<Banners>().SingleOrDefault();
            }

            return Banners;
        }

        public override SuccessResult<AbstractBanners> Banners_Update_DisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractBanners> Banners = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsUp", IsUp, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Banners_Update_DisplayOrder, param, commandType: CommandType.StoredProcedure);
                Banners = task.Read<SuccessResult<AbstractBanners>>().SingleOrDefault();
                Banners.Item = task.Read<Banners>().SingleOrDefault();
            }

            return Banners;
        }
    }
}

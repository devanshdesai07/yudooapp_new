﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class StateDao : AbstractStateDao
    {

        public override PagedList<AbstractState> State_All(PageParam pageParam, string search)
        {
            PagedList<AbstractState> State = new PagedList<AbstractState>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.State_All, param, commandType: CommandType.StoredProcedure);
                State.Values.AddRange(task.Read<State>());
                State.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return State;
        }

        public override SuccessResult<AbstractState> State_ById(long Id)
        {
            SuccessResult<AbstractState> State = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.State_ById, param, commandType: CommandType.StoredProcedure);
                State = task.Read<SuccessResult<AbstractState>>().SingleOrDefault();
                State.Item = task.Read<State>().SingleOrDefault();
            }

            return State;
        }


        public override PagedList<AbstractState> State_ByCountryId(PageParam pageParam, long CountryId)
        {
            PagedList<AbstractState> State = new PagedList<AbstractState>();
            var param = new DynamicParameters();

            param.Add("@CountryId", CountryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.State_ByCountryId, param, commandType: CommandType.StoredProcedure);
                State.Values.AddRange(task.Read<State>());
                State.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return State;
        }

        public override SuccessResult<AbstractState> State_Delete(long Id)
        {
            SuccessResult<AbstractState> State = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.State_Delete, param, commandType: CommandType.StoredProcedure);
                State = task.Read<SuccessResult<AbstractState>>().SingleOrDefault();
                State.Item = task.Read<State>().SingleOrDefault();
            }

            return State;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;
using Newtonsoft.Json;

namespace yudooapp.Data.V1
{
    public class OrderParcelDao : AbstractOrderParcelDao
    {


        public override SuccessResult<AbstractOrderParcel> OrderParcel_Upsert(AbstractOrderParcel abstractOrderParcel)
        {
            SuccessResult<AbstractOrderParcel> OrderParcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrderParcel.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractOrderParcel.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductIds", abstractOrderParcel.ProductIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Weight", abstractOrderParcel.Weight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@WeightType", abstractOrderParcel.WeightType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Length", abstractOrderParcel.Length, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@LengthType", abstractOrderParcel.LengthType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Breadth", abstractOrderParcel.Breadth, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@BreadthType", abstractOrderParcel.BreadthType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Height", abstractOrderParcel.Height, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@HeightType", abstractOrderParcel.HeightType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrderParcel.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrderParcel.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderParcel_Upsert, param, commandType: CommandType.StoredProcedure);
                OrderParcel = task.Read<SuccessResult<AbstractOrderParcel>>().SingleOrDefault();
                OrderParcel.Item = task.Read<OrderParcel>().SingleOrDefault();
            }

            return OrderParcel;
        }

        public override PagedList<AbstractOrderParcel> OrderParcel_ByOrderId(long OrderId)
        {
            PagedList<AbstractOrderParcel> OrderParcelProduct = new PagedList<AbstractOrderParcel>();

            var param = new DynamicParameters();
            param.Add("@Offset", 0, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", 100000, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", "", dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderParcel_ByOrderId, param, commandType: CommandType.StoredProcedure);
                OrderParcelProduct.Values.AddRange(task.Read<OrderParcel>());
                OrderParcelProduct.TotalRecords = task.Read<long>().SingleOrDefault();

                for (var i = 0; i < OrderParcelProduct.Values.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(OrderParcelProduct.Values[i].OrderProductObj)))
                    {
                        OrderParcelProduct.Values[i].OrderProductObj = JsonConvert.DeserializeObject(Convert.ToString(OrderParcelProduct.Values[i].OrderProductObj));
                    }
                }

            }
            return OrderParcelProduct;



            //var param = new DynamicParameters();

            //param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            //using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            //{
            //    var task = con.QueryMultiple(SQLConfig.OrderParcel_ByOrderId, param, commandType: CommandType.StoredProcedure);
            //    OrderParcel = task.Read<SuccessResult<AbstractOrderParcel>>().SingleOrDefault();
            //    OrderParcel.Item = task.Read<OrderParcel>().SingleOrDefault();

            //    if (!string.IsNullOrEmpty(Convert.ToString(OrderParcel.Item.OrderProductObj)))
            //    {
            //        OrderParcel.Item.OrderProductObj = JsonConvert.DeserializeObject(Convert.ToString(OrderParcel.Item.OrderProductObj));
            //    }
            //}
            //return OrderParcel;
        }

        public override SuccessResult<AbstractOrderParcel> OrderParcel_ById(long Id)
        {
            SuccessResult<AbstractOrderParcel> OrderParcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderParcel_ById, param, commandType: CommandType.StoredProcedure);
                OrderParcel = task.Read<SuccessResult<AbstractOrderParcel>>().SingleOrDefault();
                OrderParcel.Item = task.Read<OrderParcel>().SingleOrDefault();

                if (!string.IsNullOrEmpty(Convert.ToString(OrderParcel.Item.OrderProductObj)))
                {
                    OrderParcel.Item.OrderProductObj = JsonConvert.DeserializeObject(Convert.ToString(OrderParcel.Item.OrderProductObj));
                }
            }
            return OrderParcel;
        }

        public override SuccessResult<AbstractCheckCreateShipment> Check_CreateShipment(long OrderId)
        {
            SuccessResult<AbstractCheckCreateShipment> CheckCreateShipment = null;
            var param = new DynamicParameters();

            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Check_CreateShipment, param, commandType: CommandType.StoredProcedure);
                CheckCreateShipment = task.Read<SuccessResult<AbstractCheckCreateShipment>>().SingleOrDefault();
                CheckCreateShipment.Item = task.Read<CheckCreateShipment>().SingleOrDefault();

            }
            return CheckCreateShipment;
        }


        public override PagedList<AbstractOrderParcelProduct> OrderParcelProduct_All(PageParam pageParam, string search,long OrderId)
        {
            PagedList<AbstractOrderParcelProduct> OrderParcelProduct = new PagedList<AbstractOrderParcelProduct>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderParcelProduct_All, param, commandType: CommandType.StoredProcedure);
                OrderParcelProduct.Values.AddRange(task.Read<OrderParcelProduct>());
                OrderParcelProduct.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return OrderParcelProduct;
        }

        public override SuccessResult<AbstractOrderParcel> OrderParcels_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractOrderParcel> OrderParcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderParcel_Delete, param, commandType: CommandType.StoredProcedure);
                OrderParcel = task.Read<SuccessResult<AbstractOrderParcel>>().SingleOrDefault();
                OrderParcel.Item = task.Read<OrderParcel>().SingleOrDefault();
            }

            return OrderParcel;
        }


        //Shipment Method
        public override PagedList<AbstarctCreateShipment> OrderParcelShipment_ByOrderId(PageParam pageParam, string search, long OrderId)
        {
            PagedList<AbstarctCreateShipment> CreateShipment = new PagedList<AbstarctCreateShipment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderParcelShipment_ByOrderId, param, commandType: CommandType.StoredProcedure);
                CreateShipment.Values.AddRange(task.Read<CreateShipment>());
                CreateShipment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CreateShipment;
        }

    }
}

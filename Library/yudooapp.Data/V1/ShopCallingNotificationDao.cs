﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopCallingNotificationDao : AbstractShopCallingNotificationDao
    {
        public override SuccessResult<AbstractShopCallingNotification> ShopCallingNotification_Upsert(AbstractShopCallingNotification abstractShopCallingNotification)
        {
            SuccessResult<AbstractShopCallingNotification> ShopCallingNotification = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopCallingNotification.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopCallingNotification.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractShopCallingNotification.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", abstractShopCallingNotification.Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserShopCallLogsId", abstractShopCallingNotification.UserShopCallLogsId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopCallingNotification.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCallingNotification_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopCallingNotification = task.Read<SuccessResult<AbstractShopCallingNotification>>().SingleOrDefault();
                ShopCallingNotification.Item = task.Read<ShopCallingNotification>().SingleOrDefault();
            }

            return ShopCallingNotification;
        }

    }
}

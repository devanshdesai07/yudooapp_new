﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;
using Newtonsoft.Json;

namespace yudooapp.Data.V1
{
    public class UserOrderDao : AbstractUserOrderDao
    {


        public override SuccessResult<AbstractUserOrder> UserOrder_Upsert(AbstractUserOrder abstractUserOrder)
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserOrder.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserOrder.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractUserOrder.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentMode", abstractUserOrder.PaymentMode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressId", abstractUserOrder.AddressId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PayableAmount", abstractUserOrder.PayableAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@TaxAmount", abstractUserOrder.TaxAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@DeliveryAmount", abstractUserOrder.DeliveryAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@OtherAmount", abstractUserOrder.OtherAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Discount", abstractUserOrder.Discount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@TotalAmount", abstractUserOrder.TotalAmount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@AmountPaid", abstractUserOrder.AmountPaid, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@OrderStatus", abstractUserOrder.OrderStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", abstractUserOrder.VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderNumber", abstractUserOrder.OrderNumber, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserOrder.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserOrder.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_Upsert, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }


        public override SuccessResult<AbstractUserOrder> UserOrder_Upsert_paymentLink(long Id,string PaymentLink, string RazorPayOrderId = "")
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id",Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentLink", PaymentLink, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RazorPayOrderId", RazorPayOrderId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_Upsert_paymentLink, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }
        public override PagedList<AbstractUserOrder> UserOrder_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserOrder> UserOrder = new PagedList<AbstractUserOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_All, param, commandType: CommandType.StoredProcedure);
                UserOrder.Values.AddRange(task.Read<UserOrder>());
                UserOrder.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrder;
        }


        public override SuccessResult<AbstractUserOrder> UserOrder_ById(long Id)
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_ById, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }

        


        public override PagedList<AbstractUserOrder> UserOrder_ByShopId(PageParam pageParam, string search, long Id,string OrderStatus)
        {
            PagedList<AbstractUserOrder> UserOrder = new PagedList<AbstractUserOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderStatus", OrderStatus, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_ByShopId, param, commandType: CommandType.StoredProcedure);
                UserOrder.Values.AddRange(task.Read<UserOrder>());
                UserOrder.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrder;
        }

        public override PagedList<AbstractUserOrder> UserOrder_ByUserId(PageParam pageParam, long UserId,long IsPendingPayment,long IsPlaced)
        {
            PagedList<AbstractUserOrder> UserOrder = new PagedList<AbstractUserOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsPendingPayment", IsPendingPayment, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsPlaced", IsPlaced, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserOrder.Values.AddRange(task.Read<UserOrder>());
                UserOrder.TotalRecords = task.Read<long>().SingleOrDefault();

                for(int i = 0; i < UserOrder.Values.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(UserOrder.Values[i].ProductDetails)))
                    {
                        UserOrder.Values[i].ProductDetailsObj = JsonConvert.DeserializeObject(Convert.ToString(UserOrder.Values[i].ProductDetails));
                    }
                }
            }
            return UserOrder;
        }

        public override SuccessResult<AbstractUserOrder> UserOrder_Delete(long Id)
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_Delete, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }


            
        public override PagedList<AbstractUserOrder> OrderDetails_All(PageParam pageParam, string Search, long OrderStatus, string PaymentStatus, long CityId,decimal Amount)
        {
            PagedList<AbstractUserOrder> UserOrder = new PagedList<AbstractUserOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OrderStatus", OrderStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentStatus", PaymentStatus, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Amount", Amount, dbType: DbType.Decimal, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderDetails_All, param, commandType: CommandType.StoredProcedure);
                UserOrder.Values.AddRange(task.Read<UserOrder>());
                UserOrder.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrder;
        }
        public override SuccessResult<AbstractUserOrder> UserOrder_StatusChange(long Id, long OrderStatus)
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderStatus", OrderStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_StatusChange, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }
        public override SuccessResult<AbstractUserOrder> UserOrder_StatusChangeByShipmentId(long Id, long OrderStatus)
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderStatus", OrderStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_StatusChangeByShipmentId, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }

        public override SuccessResult<AbstractUserOrder> UserOrder_DeliveryRatingUpdate(long Id, long DeliveryRating)
        {
            SuccessResult<AbstractUserOrder> UserOrder = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryRating", DeliveryRating, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrder_DeliveryRatingUpdate, param, commandType: CommandType.StoredProcedure);
                UserOrder = task.Read<SuccessResult<AbstractUserOrder>>().SingleOrDefault();
                UserOrder.Item = task.Read<UserOrder>().SingleOrDefault();
            }

            return UserOrder;
        }

    }
}

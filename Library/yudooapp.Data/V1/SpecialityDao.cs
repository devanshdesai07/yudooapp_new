﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class SpecialityDao : AbstractSpecialityDao
    {
        public override PagedList<AbstractSpeciality> Speciality_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractSpeciality> Speciality = new PagedList<AbstractSpeciality>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Speciality_All, param, commandType: CommandType.StoredProcedure);
                Speciality.Values.AddRange(task.Read<Speciality>());
                Speciality.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Speciality;
        }

        public override SuccessResult<AbstractSpeciality> Speciality_ById(long Id)
        {
            SuccessResult<AbstractSpeciality> Speciality = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Speciality_ById, param, commandType: CommandType.StoredProcedure);
                Speciality = task.Read<SuccessResult<AbstractSpeciality>>().SingleOrDefault();
                Speciality.Item = task.Read<Speciality>().SingleOrDefault();
            }

            return Speciality;
        }

        public override SuccessResult<AbstractSpeciality> Speciality_Delete(long Id,long DeletedBy)
        {
            SuccessResult<AbstractSpeciality> Speciality = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Speciality_Delete, param, commandType: CommandType.StoredProcedure);
                Speciality = task.Read<SuccessResult<AbstractSpeciality>>().SingleOrDefault();
                Speciality.Item = task.Read<Speciality>().SingleOrDefault();
            }

            return Speciality;
        }

        public override SuccessResult<AbstractSpeciality> Speciality_ActInActive(long Id)
        {
            SuccessResult<AbstractSpeciality> Speciality = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Speciality_ActInActive, param, commandType: CommandType.StoredProcedure);
                Speciality = task.Read<SuccessResult<AbstractSpeciality>>().SingleOrDefault();
                Speciality.Item = task.Read<Speciality>().SingleOrDefault();
            }

            return Speciality;
        }


        public override SuccessResult<AbstractSpeciality> Speciality_Upsert(AbstractSpeciality abstractSpeciality)
        {
            SuccessResult<AbstractSpeciality> Speciality = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractSpeciality.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractSpeciality.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractSpeciality.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractSpeciality.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Speciality_Upsert, param, commandType: CommandType.StoredProcedure);
                Speciality = task.Read<SuccessResult<AbstractSpeciality>>().SingleOrDefault();
                Speciality.Item = task.Read<Speciality>().SingleOrDefault();
            }

            return Speciality;
        }

    }
}

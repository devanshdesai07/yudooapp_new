﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class AddressDao : AbstractAddressDao
    {


        public override SuccessResult<AbstractAddress> Address_Upsert(AbstractAddress abstractAddress)
        {
            SuccessResult<AbstractAddress> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAddress.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractAddress.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractAddress.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractAddress.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine3", abstractAddress.AddressLine3, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractAddress.PinCode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupCityId", abstractAddress.LookupCityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractAddress.LookupStateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupCountryId", abstractAddress.LookupCountryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressType", abstractAddress.AddressType, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractAddress.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractAddress.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Address_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                Address.Item = task.Read<Address>().SingleOrDefault();
            }

            return Address;
        }


        public override PagedList<AbstractAddress> Address_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAddress> Address = new PagedList<AbstractAddress>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Address_All, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<Address>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Address;
        }


        public override SuccessResult<AbstractAddress> Address_ById(long Id)
        {
            SuccessResult<AbstractAddress> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Address_ById, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                Address.Item = task.Read<Address>().SingleOrDefault();
            }

            return Address;
        }


        public override PagedList<AbstractAddress> Address_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractAddress> Address = new PagedList<AbstractAddress>();
            var param = new DynamicParameters();

            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Address_ByUserId, param, commandType: CommandType.StoredProcedure);
                Address.Values.AddRange(task.Read<Address>());
                Address.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Address;
        }


        public override SuccessResult<AbstractAddress> Address_ActInAct(long Id)
        {
            SuccessResult<AbstractAddress> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Address_ActInAct, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                Address.Item = task.Read<Address>().SingleOrDefault();
            }

            return Address;
        }

        public override SuccessResult<AbstractAddress> Address_Delete(long Id)
        {
            SuccessResult<AbstractAddress> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Address_Delete, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                Address.Item = task.Read<Address>().SingleOrDefault();
            }

            return Address;
        }
    }
}

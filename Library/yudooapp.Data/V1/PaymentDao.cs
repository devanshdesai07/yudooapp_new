﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class PaymentDao : AbstractPaymentDao
    {
        public override PagedList<AbstractCustomerPaymentStatus> CustomerPaymentStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCustomerPaymentStatus> Payment = new PagedList<AbstractCustomerPaymentStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerPaymentStatus_All, param, commandType: CommandType.StoredProcedure);
                Payment.Values.AddRange(task.Read<CustomerPaymentStatus>());
                Payment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Payment;
        }

        public override PagedList<AbstractPayment> Payment_All(PageParam pageParam, string search , AbstractPayment abstractPayment)
        {
            PagedList<AbstractPayment> Payment = new PagedList<AbstractPayment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_All, param, commandType: CommandType.StoredProcedure);
                Payment.Values.AddRange(task.Read<Payment>());
                Payment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Payment;
        }
        public override SuccessResult<AbstractPayment> Payment_ById(long Id)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_ById, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }

            return Payment;
        }
      
        public override SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPayment.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractPayment.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryPaymentFee", abstractPayment.DeliveryPaymentFee, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryCharges", abstractPayment.DeliveryCharges, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentCharges", abstractPayment.PaymentCharges, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TotalPayment", abstractPayment.TotalPayment, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentStatus", abstractPayment.PaymentStatus, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_Upsert, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }

            return Payment;
        }

     

      

    }
}

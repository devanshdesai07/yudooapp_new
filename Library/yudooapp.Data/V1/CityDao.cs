﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CityDao : AbstractCityDao
    {

        public override PagedList<AbstractCity> City_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCity> City = new PagedList<AbstractCity>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_All, param, commandType: CommandType.StoredProcedure);
                City.Values.AddRange(task.Read<City>());
                City.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return City;
        }
        public override SuccessResult<AbstractCity> City_Upsert(AbstractCity abstractCity)
        {
            SuccessResult<AbstractCity> Address = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCity.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractCity.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractCity.StateId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_Upsert, param, commandType: CommandType.StoredProcedure);
                Address = task.Read<SuccessResult<AbstractCity>>().SingleOrDefault();
                Address.Item = task.Read<City>().SingleOrDefault();
            }

            return Address;
        }

        public override SuccessResult<AbstractCity> City_ById(long Id)
        {
            SuccessResult<AbstractCity> City = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_ById, param, commandType: CommandType.StoredProcedure);
                City = task.Read<SuccessResult<AbstractCity>>().SingleOrDefault();
                City.Item = task.Read<City>().SingleOrDefault();
            }

            return City;
        }


        public override PagedList<AbstractCity> City_ByStateId(PageParam pageParam, long StateId)
        {
            PagedList<AbstractCity> City = new PagedList<AbstractCity>();
            var param = new DynamicParameters();

            param.Add("@StateId", StateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_ByStateId, param, commandType: CommandType.StoredProcedure);
                City.Values.AddRange(task.Read<City>());
                City.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return City;
        }

        public override SuccessResult<AbstractCity> City_Delete(long Id)
        {
            SuccessResult<AbstractCity> City = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_Delete, param, commandType: CommandType.StoredProcedure);
                City = task.Read<SuccessResult<AbstractCity>>().SingleOrDefault();
                City.Item = task.Read<City>().SingleOrDefault();
            }

            return City;
        }

    }
}

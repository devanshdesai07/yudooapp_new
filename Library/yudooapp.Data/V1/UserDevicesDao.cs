﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserDevicesDao : AbstractUserDevicesDao
    {

        public override PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractUserDevices> UserDevices = new PagedList<AbstractUserDevices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserDevices_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserDevices.Values.AddRange(task.Read<UserDevices>());
                UserDevices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserDevices;
        }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopImagesAndVideosDao : AbstractShopImagesAndVideosDao
    {


        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_Upsert(AbstractShopImagesAndVideos abstractShopImagesAndVideos)
        {
            SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopImagesAndVideos.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopImagesAndVideos.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MediaUrl", abstractShopImagesAndVideos.MediaUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsVideo", abstractShopImagesAndVideos.IsVideo, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsCoverImage", abstractShopImagesAndVideos.IsCoverImage, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ThumbnailImage", abstractShopImagesAndVideos.ThumbnailImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsUploadedFromMobile", abstractShopImagesAndVideos.IsUploadedFromMobile, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsApprovedByEmployee", abstractShopImagesAndVideos.IsApprovedByEmployee, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsApprovedByAdmin", abstractShopImagesAndVideos.IsApprovedByAdmin, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractShopImagesAndVideos.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopImagesAndVideos.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos = task.Read<SuccessResult<AbstractShopImagesAndVideos>>().SingleOrDefault();
                ShopImagesAndVideos.Item = task.Read<ShopImagesAndVideos>().SingleOrDefault();
            }

            return ShopImagesAndVideos;
        }


        public override PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos_All(PageParam pageParam, string search)
        {
            PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos = new PagedList<AbstractShopImagesAndVideos>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_All, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos.Values.AddRange(task.Read<ShopImagesAndVideos>());
                ShopImagesAndVideos.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopImagesAndVideos;
        }


        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_ById(long Id)
        {
            SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_ById, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos = task.Read<SuccessResult<AbstractShopImagesAndVideos>>().SingleOrDefault();
                ShopImagesAndVideos.Item = task.Read<ShopImagesAndVideos>().SingleOrDefault();
            }

            return ShopImagesAndVideos;
        }

        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_IsVideo(long Id)
        {
            SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_IsVideo, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos = task.Read<SuccessResult<AbstractShopImagesAndVideos>>().SingleOrDefault();
                ShopImagesAndVideos.Item = task.Read<ShopImagesAndVideos>().SingleOrDefault();
            }

            return ShopImagesAndVideos;
        }

        public override PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos = new PagedList<AbstractShopImagesAndVideos>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_ByShopId, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos.Values.AddRange(task.Read<ShopImagesAndVideos>());
                ShopImagesAndVideos.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopImagesAndVideos;
        }


        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_IsCoverImage(long Id, bool IsVideo)
        {
            SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsVideo", IsVideo, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_IsCoverImage, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos = task.Read<SuccessResult<AbstractShopImagesAndVideos>>().SingleOrDefault();
                ShopImagesAndVideos.Item = task.Read<ShopImagesAndVideos>().SingleOrDefault();
            }

            return ShopImagesAndVideos;
        }

        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_Delete(long Id)
        {
            SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopImagesAndVideos_Delete, param, commandType: CommandType.StoredProcedure);
                ShopImagesAndVideos = task.Read<SuccessResult<AbstractShopImagesAndVideos>>().SingleOrDefault();
                ShopImagesAndVideos.Item = task.Read<ShopImagesAndVideos>().SingleOrDefault();
            }

            return ShopImagesAndVideos;
        }
    }
}


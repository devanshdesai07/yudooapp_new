﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopShortingValuesDao : AbstractShopShortingValuesDao
    {


        public override SuccessResult<AbstractShopShortingValues> ShopShortingValues_Upsert(AbstractShopShortingValues abstractShopShortingValues)
        {
            SuccessResult<AbstractShopShortingValues> ShopShortingValues = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopShortingValues.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@No_Ratings", abstractShopShortingValues.No_Ratings ,dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Rating_Weigth", abstractShopShortingValues.Rating_Weigth, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Offer_Weight", abstractShopShortingValues.Offer_Weight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ShopOnline_Weight", abstractShopShortingValues.ShopOnline_Weight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
           
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopShortingValues_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopShortingValues = task.Read<SuccessResult<AbstractShopShortingValues>>().SingleOrDefault();
                ShopShortingValues.Item = task.Read<ShopShortingValues>().SingleOrDefault();
            }

            return ShopShortingValues;
        }


        public override PagedList<AbstractShopShortingValues> ShopShortingValues_All(PageParam pageParam)
        {
            PagedList<AbstractShopShortingValues> ShopShortingValues = new PagedList<AbstractShopShortingValues>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopShortingValues_All, param, commandType: CommandType.StoredProcedure);
                ShopShortingValues.Values.AddRange(task.Read<ShopShortingValues>());
                ShopShortingValues.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopShortingValues;
        }


    }
}

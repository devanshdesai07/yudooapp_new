﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class AdminShopDao : AbstractAdminShopDao
    {
        public override PagedList<AbstractAdminShop> AdminShop_All(PageParam pageParam, string search, long ShopOBStatus, long ShopId, string ShopName, string MobileNumber, long CityId, string PrimaryCategory, string AllCategory,string DateOfJoining)
        {
            PagedList<AbstractAdminShop> AdminShop = new PagedList<AbstractAdminShop>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOBStatus", ShopOBStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PrimaryCategory", PrimaryCategory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AllCategory", AllCategory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DateOfJoining", DateOfJoining, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminShop_All, param, commandType: CommandType.StoredProcedure);
                AdminShop.Values.AddRange(task.Read<AdminShop>());
                AdminShop.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminShop;
        }

        public override PagedList<AbstractAdminShop> ShopApproval_All(PageParam pageParam, string search, long ShopOBStatus, long ShopId, string ShopName, string MobileNumber, long CityId, string PrimaryCategory, string AllCategory, string DateOfJoining)
        {
            PagedList<AbstractAdminShop> AdminShop = new PagedList<AbstractAdminShop>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOBStatus", ShopOBStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PrimaryCategory", PrimaryCategory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AllCategory", AllCategory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DateOfJoining", DateOfJoining, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopApproval_All, param, commandType: CommandType.StoredProcedure);
                AdminShop.Values.AddRange(task.Read<AdminShop>());
                AdminShop.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminShop;
        }
        public override SuccessResult<AbstractAdminShop> AdminShop_ById(long Id)
        {
            SuccessResult<AbstractAdminShop> AdminShop = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            try
            {
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.AdminShop_ById, param, commandType: CommandType.StoredProcedure);
                    AdminShop = task.Read<SuccessResult<AbstractAdminShop>>().SingleOrDefault();
                    AdminShop.Item = task.Read<AdminShop>().SingleOrDefault();

                    if (AdminShop.Item != null)
                    {
                        AdminShop.Item.ShopTimgings = new List<ShopTimgings>();
                        AdminShop.Item.ShopTimgings.AddRange(task.Read<ShopTimgings>());

                        AdminShop.Item.ShopImagesandVideos = new List<ShopImagesAndVideos>();
                        AdminShop.Item.ShopImagesandVideos.AddRange(task.Read<ShopImagesAndVideos>());
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            

            return AdminShop;
        }

        public override SuccessResult<AbstractAdminShop> SendForApproval(long Shopid)
        {
            SuccessResult<AbstractAdminShop> AdminShop = null;
            var param = new DynamicParameters();

            param.Add("@Shopid", Shopid, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SendForApproval, param, commandType: CommandType.StoredProcedure);
                AdminShop = task.Read<SuccessResult<AbstractAdminShop>>().SingleOrDefault();
                AdminShop.Item = task.Read<AdminShop>().SingleOrDefault();

                if (AdminShop.Item != null)
                {
                    AdminShop.Item.ShopTimgings = new List<ShopTimgings>();
                    AdminShop.Item.ShopTimgings.AddRange(task.Read<ShopTimgings>());

                    AdminShop.Item.ShopImagesandVideos = new List<ShopImagesAndVideos>();
                    AdminShop.Item.ShopImagesandVideos.AddRange(task.Read<ShopImagesAndVideos>());
                }

            }

            return AdminShop;
        }

        public override PagedList<AbstractAdminShop> CurrentyOfflineShops_All(PageParam pageParam, string search)
        {
            try
            {
                PagedList<AbstractAdminShop> AdminShop = new PagedList<AbstractAdminShop>();

                var param = new DynamicParameters();
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.CurrentyOfflineShops_All, param, commandType: CommandType.StoredProcedure);
                    AdminShop.Values.AddRange(task.Read<AdminShop>());
                    AdminShop.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return AdminShop;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        public override PagedList<AbstractAdminShop> GetUnansweredcall(PageParam pageParam, string search)
        {
            PagedList<AbstractAdminShop> AdminShop = new PagedList<AbstractAdminShop>();

            var param = new DynamicParameters();
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetUnansweredcall, param, commandType: CommandType.StoredProcedure);
                AdminShop.Values.AddRange(task.Read<AdminShop>());
                AdminShop.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return AdminShop;
        }

        
    }
}

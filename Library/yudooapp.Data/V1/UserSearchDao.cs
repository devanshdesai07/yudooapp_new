﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserSearchDao : AbstractUserSearchDao
    {


        public override SuccessResult<AbstractUserSearch> UserSearch_Upsert(AbstractUserSearch abstractUserSearch)
        {
            SuccessResult<AbstractUserSearch> UserSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserSearch.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserSearch.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SearchText", abstractUserSearch.SearchText, dbType: DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_Upsert, param, commandType: CommandType.StoredProcedure);
                UserSearch = task.Read<SuccessResult<AbstractUserSearch>>().SingleOrDefault();
                UserSearch.Item = task.Read<UserSearch>().SingleOrDefault();
            }

            return UserSearch;
        }


        public override PagedList<AbstractUserSearch> UserSearch_ByUserId(PageParam pageParam , long UserId)
        {
            PagedList<AbstractUserSearch> UserSearch = new PagedList<AbstractUserSearch>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId",UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserSearch.Values.AddRange(task.Read<UserSearch>());
                UserSearch.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserSearch;
        }

        public override PagedList<AbstractUserSearch> UserSearch_All(PageParam pageParam, string Search ,long UserId)
        {
            PagedList<AbstractUserSearch> UserSearch = new PagedList<AbstractUserSearch>();

            var param = new DynamicParameters();
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_All, param, commandType: CommandType.StoredProcedure);
                UserSearch.Values.AddRange(task.Read<UserSearch>());
                UserSearch.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserSearch;
        }

        public override PagedList<AbstractUserSearch> UserSearch_Recentsearch(PageParam pageParam, string Search, long UserId)
        {
            PagedList<AbstractUserSearch> UserSearch = new PagedList<AbstractUserSearch>();

            var param = new DynamicParameters();
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_Recentsearch, param, commandType: CommandType.StoredProcedure);
                UserSearch.Values.AddRange(task.Read<UserSearch>());
                UserSearch.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserSearch;
        }

        public override PagedList<AbstractUserSearch> UserSearch_TrendingSearches(PageParam pageParam, string Search)
        {
            PagedList<AbstractUserSearch> UserSearch = new PagedList<AbstractUserSearch>();

            var param = new DynamicParameters();
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_TrendingSearches, param, commandType: CommandType.StoredProcedure);
                UserSearch.Values.AddRange(task.Read<UserSearch>());
                UserSearch.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserSearch;
        }


        public override SuccessResult<AbstractUserSearch> UserSearch_ById(long Id)
        {
            SuccessResult<AbstractUserSearch> UserSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_ById, param, commandType: CommandType.StoredProcedure);
                UserSearch = task.Read<SuccessResult<AbstractUserSearch>>().SingleOrDefault();
                UserSearch.Item = task.Read<UserSearch>().SingleOrDefault();
            }

            return UserSearch;
        }



        public override SuccessResult<AbstractUserSearch> UserSearch_ActInact(long Id)
        {
            SuccessResult<AbstractUserSearch> UserSearch = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserSearch_ActInact, param, commandType: CommandType.StoredProcedure);
                UserSearch = task.Read<SuccessResult<AbstractUserSearch>>().SingleOrDefault();
                UserSearch.Item = task.Read<UserSearch>().SingleOrDefault();
            }

            return UserSearch;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CategoryDao : AbstractCategoryDao
    {

        public override PagedList<AbstractCategory> Category_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCategory> Category = new PagedList<AbstractCategory>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Category_All, param, commandType: CommandType.StoredProcedure);
                Category.Values.AddRange(task.Read<Category>());
                Category.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Category;
        }

        public override SuccessResult<AbstractCategory> Category_ById(long Id)
        {
            SuccessResult<AbstractCategory> Category = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Category_ById, param, commandType: CommandType.StoredProcedure);
                Category = task.Read<SuccessResult<AbstractCategory>>().SingleOrDefault();
                Category.Item = task.Read<Category>().SingleOrDefault();
            }

            return Category;
        }

        public override SuccessResult<AbstractCategory> Category_Delete(long Id)
        {
            SuccessResult<AbstractCategory> Category = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Category_Delete, param, commandType: CommandType.StoredProcedure);
                Category = task.Read<SuccessResult<AbstractCategory>>().SingleOrDefault();
                Category.Item = task.Read<Category>().SingleOrDefault();
            }

            return Category;
        }

        public override SuccessResult<AbstractCategory> Category_Upsert(AbstractCategory abstractCategory)
        {
            SuccessResult<AbstractCategory> Category = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCategory.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NAME", abstractCategory.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageUrl", abstractCategory.ImageUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DefaultCharge", abstractCategory.DefaultCharge, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@NameGujarati", abstractCategory.NameGujarati, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NameHindi", abstractCategory.NameHindi, dbType: DbType.String, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Category_Upsert, param, commandType: CommandType.StoredProcedure);
                Category = task.Read<SuccessResult<AbstractCategory>>().SingleOrDefault();
                Category.Item = task.Read<Category>().SingleOrDefault();
            }

            return Category;
        }
        public override SuccessResult<AbstractCategory> Category_Update_DisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractCategory> Category = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsUp", IsUp, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Category_Update_DisplayOrder, param, commandType: CommandType.StoredProcedure);
                Category = task.Read<SuccessResult<AbstractCategory>>().SingleOrDefault();
                Category.Item = task.Read<Category>().SingleOrDefault();
            }

            return Category;
        }
    }
}

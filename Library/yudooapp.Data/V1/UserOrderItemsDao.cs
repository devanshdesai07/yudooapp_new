﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserOrderItemsDao : AbstractUserOrderItemsDao
    {


        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_Upsert(AbstractUserOrderItems abstractUserOrderItems)
        {
            SuccessResult<AbstractUserOrderItems> UserOrderItems = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserOrderItems.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractUserOrderItems.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Qty", abstractUserOrderItems.Qty, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Price", abstractUserOrderItems.Price, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Size", abstractUserOrderItems.Size, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ItemName", abstractUserOrderItems.ItemName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Productimage", abstractUserOrderItems.ProductImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductInfo", abstractUserOrderItems.ProductInfo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserOrderItems.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserOrderItems.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_Upsert, param, commandType: CommandType.StoredProcedure);
                UserOrderItems = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                UserOrderItems.Item = task.Read<UserOrderItems>().SingleOrDefault();
            }

            return UserOrderItems;
        }


        public override PagedList<AbstractUserOrderItems> UserOrderItems_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserOrderItems> UserOrderItems = new PagedList<AbstractUserOrderItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_All, param, commandType: CommandType.StoredProcedure);
                UserOrderItems.Values.AddRange(task.Read<UserOrderItems>());
                UserOrderItems.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrderItems;
        }

        public override PagedList<AbstractUserOrderItems> UserOrderItems_ByOrderId(PageParam pageParam , long Id)
        {
            PagedList<AbstractUserOrderItems> UserOrderItems = new PagedList<AbstractUserOrderItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_ByOrderId, param, commandType: CommandType.StoredProcedure);
                UserOrderItems.Values.AddRange(task.Read<UserOrderItems>());
                UserOrderItems.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrderItems;
        }

        public override PagedList<AbstractUserOrderItems> UserOrderItems_ByMobileNumber(PageParam pageParam, string MobileNumber)
        {
            PagedList<AbstractUserOrderItems> UserOrderItems = new PagedList<AbstractUserOrderItems>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_ByMobileNumber, param, commandType: CommandType.StoredProcedure);
                UserOrderItems.Values.AddRange(task.Read<UserOrderItems>());
                UserOrderItems.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrderItems;
        }
        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_ById(long Id)
        {
            SuccessResult<AbstractUserOrderItems> UserOrderItems = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_ById, param, commandType: CommandType.StoredProcedure);
                UserOrderItems = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                UserOrderItems.Item = task.Read<UserOrderItems>().SingleOrDefault();
            }

            return UserOrderItems;
        }

        public override SuccessResult<AbstractUserOrderItems> ManifestUrl_ShipmentId(string ManifestUrl ,string LabelUrl, long ShipmentId)
        {
            SuccessResult<AbstractUserOrderItems> UserOrderItems = null;
            var param = new DynamicParameters();

            param.Add("@ShipmentId", ShipmentId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ManifestUrl", ManifestUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LabelUrl", LabelUrl, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ManifestUrl_ShipmentId, param, commandType: CommandType.StoredProcedure);
                UserOrderItems = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                UserOrderItems.Item = task.Read<UserOrderItems>().SingleOrDefault();
            }

            return UserOrderItems;
        }

        public override SuccessResult<AbstractUserOrderItems> AddShipmentIdUserOrderItems_ById(string Id, int ShipmentId)
        {
            SuccessResult<AbstractUserOrderItems> UserOrderItems = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShipmentId", ShipmentId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddShipmentIdUserOrderItems_ById, param, commandType: CommandType.StoredProcedure);
                UserOrderItems = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                UserOrderItems.Item = task.Read<UserOrderItems>().SingleOrDefault();
            }

            return UserOrderItems;
        }


        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractUserOrderItems> UserOrderItems = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_Delete, param, commandType: CommandType.StoredProcedure);
                UserOrderItems = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                UserOrderItems.Item = task.Read<UserOrderItems>().SingleOrDefault();
            }

            return UserOrderItems;
        }

        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_Status(long Id, long StatusId)
        {
            SuccessResult<AbstractUserOrderItems> UserOrderItems = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderItems_Status, param, commandType: CommandType.StoredProcedure);
                UserOrderItems = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                UserOrderItems.Item = task.Read<UserOrderItems>().SingleOrDefault();
            }

            return UserOrderItems;
        }

        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_StatusChange(long Id, long ItemStatusId, long UpdatedBy)
        {
            SuccessResult<AbstractUserOrderItems> returnValues = new SuccessResult<AbstractUserOrderItems>();
            try
            {
                var param = new DynamicParameters();

                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ItemStatusId", ItemStatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserOrderItems_StatusChange, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractUserOrderItems>>().SingleOrDefault();
                    returnValues.Item = task.Read<UserOrderItems>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400; 
                returnValues.Message = ex.Message; 
            }
            return returnValues;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ParcelDao : AbstractParcelDao
    {


        public override SuccessResult<AbstractParcel> Parcel_Upsert(AbstractParcel abstractParcel)
        {
            SuccessResult<AbstractParcel> Parcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractParcel.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractParcel.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderItemIds", abstractParcel.OrderItemIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Weight", abstractParcel.Weight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Length", abstractParcel.Length, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Breadth", abstractParcel.Breadth, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Height", abstractParcel.Height, dbType: DbType.Decimal, direction: ParameterDirection.Input); 
            param.Add("@ParcelStatus", abstractParcel.ParcelStatus, dbType: DbType.Boolean, direction: ParameterDirection.Input); 
            param.Add("@CreatedBy", abstractParcel.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractParcel.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Parcel_Upsert, param, commandType: CommandType.StoredProcedure);
                Parcel = task.Read<SuccessResult<AbstractParcel>>().SingleOrDefault();
                Parcel.Item = task.Read<Parcel>().SingleOrDefault();
            }

            return Parcel;
        }


        public override SuccessResult<AbstractParcel> Parcel_StatusChange(long Id)
        {
            SuccessResult<AbstractParcel> Parcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Parcel_StatusChange, param, commandType: CommandType.StoredProcedure);
                Parcel = task.Read<SuccessResult<AbstractParcel>>().SingleOrDefault();
                Parcel.Item = task.Read<Parcel>().SingleOrDefault();
            }

            return Parcel;
        }

        public override PagedList<AbstractParcel> Parcel_ByOrderId(long OrderId, PageParam pageParam)
        {
            PagedList<AbstractParcel> Parcel = new PagedList<AbstractParcel>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Parcel_ByOrderId, param, commandType: CommandType.StoredProcedure);
                Parcel.Values.AddRange(task.Read<Parcel>());
                Parcel.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Parcel;
        }


        public override SuccessResult<AbstractParcel> Parcel_ById(long Id)
        {
            SuccessResult<AbstractParcel> Parcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Parcel_ById, param, commandType: CommandType.StoredProcedure);
                Parcel = task.Read<SuccessResult<AbstractParcel>>().SingleOrDefault();
                Parcel.Item = task.Read<Parcel>().SingleOrDefault();
            }

            return Parcel;
        }


        public override SuccessResult<AbstractParcel> Parcel_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractParcel> Parcel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Parcel_Delete, param, commandType: CommandType.StoredProcedure);
                Parcel = task.Read<SuccessResult<AbstractParcel>>().SingleOrDefault();
                Parcel.Item = task.Read<Parcel>().SingleOrDefault();
            }

            return Parcel;
        }
    }
}

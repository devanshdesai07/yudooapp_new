﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class UserCartDao : AbstractUserCartDao
    {
        public override PagedList<AbstractUserCart> UserCart_All(PageParam pageParam, string Search, long UserId, long ShopId)
        {
            PagedList<AbstractUserCart> UserCart = new PagedList<AbstractUserCart>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_All, param, commandType: CommandType.StoredProcedure);
                UserCart.Values.AddRange(task.Read<UserCart>());
                UserCart.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCart;
        }
        public override PagedList<AbstractUserCart> UserCart_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserCart> UserCart = new PagedList<AbstractUserCart>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserCart.Values.AddRange(task.Read<UserCart>());
                UserCart.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCart;
        }

        public override PagedList<AbstractUserCart> UserCart_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractUserCart> UserCart = new PagedList<AbstractUserCart>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_ByShopId, param, commandType: CommandType.StoredProcedure);
                UserCart.Values.AddRange(task.Read<UserCart>());
                UserCart.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCart;
        }
        public override PagedList<AbstractUserCart> UserCart_ByVideoCallId(PageParam pageParam, long VideoCallId)
        {
            PagedList<AbstractUserCart> UserCart = new PagedList<AbstractUserCart>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_ByVideoCallId, param, commandType: CommandType.StoredProcedure);
                UserCart.Values.AddRange(task.Read<UserCart>());
                UserCart.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCart;
        }
        public override SuccessResult<AbstractUserCart> UserCart_ById(long Id)
        {
            SuccessResult<AbstractUserCart> UserCart = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_ById, param, commandType: CommandType.StoredProcedure);
                UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
                UserCart.Item = task.Read<UserCart>().SingleOrDefault();
            }
            return UserCart;
        }

        public override SuccessResult<AbstractPersonalUserCart> AddUserCart(AbstractPersonalUserCart abstractPersonalUserCart)
        {
            SuccessResult<AbstractPersonalUserCart> PersonalUserCart = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPersonalUserCart.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractPersonalUserCart.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractPersonalUserCart.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserCartMasterId", abstractPersonalUserCart.UserCartMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductImage", abstractPersonalUserCart.ProductImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Qty", abstractPersonalUserCart.Qty, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Price", abstractPersonalUserCart.Price, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Weight", abstractPersonalUserCart.Weight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@WeightTypeId", abstractPersonalUserCart.WeightTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Size", abstractPersonalUserCart.Size, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductInfo", abstractPersonalUserCart.ProductInfo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ItemName", abstractPersonalUserCart.ItemName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPersonalUserCart.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPersonalUserCart.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", abstractPersonalUserCart.VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Pincode", abstractPersonalUserCart.Pincode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Country", abstractPersonalUserCart.Country, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@State", abstractPersonalUserCart.State, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractPersonalUserCart.City, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddUserCart, param, commandType: CommandType.StoredProcedure);
                PersonalUserCart = task.Read<SuccessResult<AbstractPersonalUserCart>>().SingleOrDefault();
                PersonalUserCart.Item = task.Read<PersonalUserCart>().SingleOrDefault();
            }
            return PersonalUserCart;
        }

        public override SuccessResult<AbstractUserCart> UserCart_Upsert(AbstractUserCart abstractUserCart)
        {
            SuccessResult<AbstractUserCart> UserCart = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserCart.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserCart.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractUserCart.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserCartMasterId", abstractUserCart.UserCartMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductImage", abstractUserCart.ProductImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Qty", abstractUserCart.Qty, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Price", abstractUserCart.Price, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Weight", abstractUserCart.Weight, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@WeightTypeId", abstractUserCart.WeightTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Size", abstractUserCart.Size, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductInfo", abstractUserCart.ProductInfo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ItemName", abstractUserCart.ItemName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserCart.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserCart.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", abstractUserCart.VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Pincode", abstractUserCart.Pincode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Country", abstractUserCart.Country, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@State", abstractUserCart.State, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractUserCart.City, dbType: DbType.String, direction: ParameterDirection.Input);
                
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_Upsert, param, commandType: CommandType.StoredProcedure);
                UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
                UserCart.Item = task.Read<UserCart>().SingleOrDefault();
            }

            return UserCart;
        }
        public override SuccessResult<AbstractUserCart> UserCart_Delete(long Id)
        {
            SuccessResult<AbstractUserCart> UserCart = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_Delete, param, commandType: CommandType.StoredProcedure);
                UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
                UserCart.Item = task.Read<UserCart>().SingleOrDefault();
            }

            return UserCart;
        }
        public override SuccessResult<AbstractUserCart> UserCartDeleteByVideoCallId(long VideoCallId)
        {
            SuccessResult<AbstractUserCart> UserCart = null;
            var param = new DynamicParameters();

            param.Add("@VideoCallId", VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartDeleteByVideoCallId, param, commandType: CommandType.StoredProcedure);
                UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
                UserCart.Item = task.Read<UserCart>().SingleOrDefault();
            }

            return UserCart;
        }

        public override PagedList<AbstractUserCart> UserCart_IsSendToCustomer(PageParam pageParam, string Search,long VideoCallId)
        {

            PagedList<AbstractUserCart> UserCart = new PagedList<AbstractUserCart>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_IsSendToCustomer, param, commandType: CommandType.StoredProcedure);
                UserCart.Values.AddRange(task.Read<UserCart>());
                UserCart.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCart;
        }
        public override SuccessResult<AbstractUserCart> UserCart_ConfirmOrder(long VideoCallId, decimal ShippingChargeFromAPI, decimal CODChargefromAPI, long IsOnline,string Address, string UserName)
        {
            try
            {
                SuccessResult<AbstractUserCart> UserCart = null;
                var param = new DynamicParameters();

                param.Add("@VideoCallId", VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ShippingChargeFromAPI", ShippingChargeFromAPI, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                param.Add("@CODChargefromAPI", CODChargefromAPI, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                param.Add("@IsOnline", IsOnline, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Address", Address, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@UserName", UserName, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserCart_ConfirmOrder, param, commandType: CommandType.StoredProcedure);
                    UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
                    UserCart.Item = task.Read<UserCart>().SingleOrDefault();
                }

                return UserCart;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public override PagedList<AbstractUserCart> UserCart_IsVisibleToCustomer(PageParam pageParam, string Search, long UserCartMasterId)
        {
            PagedList<AbstractUserCart> UserCart = new PagedList<AbstractUserCart>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserCartMasterId", UserCartMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_IsVisibleToCustomer, param, commandType: CommandType.StoredProcedure);
                UserCart.Values.AddRange(task.Read<UserCart>());
                UserCart.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCart;
        }

        public override SuccessResult<AbstractUserCart> UserCart_IsSendToOrder(long UserCartMasterId)
        {
            SuccessResult<AbstractUserCart> UserCart = null;
            var param = new DynamicParameters();

            param.Add("@UserCartMasterId", UserCartMasterId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCart_IsSendToOrder, param, commandType: CommandType.StoredProcedure);
                UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
                UserCart.Item = task.Read<UserCart>().SingleOrDefault();
            }

            return UserCart;
        }

        //public override SuccessResult<AbstractUserCart> UserCart_Details(long Id)
        //{
        //    SuccessResult<AbstractUserCart> UserCart = null;
        //    var param = new DynamicParameters();

        //    param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.UserCart_Details, param, commandType: CommandType.StoredProcedure);
        //        UserCart = task.Read<SuccessResult<AbstractUserCart>>().SingleOrDefault();
        //        UserCart.Item = task.Read<UserCart>().SingleOrDefault();
        //    }
        //    return UserCart;
        //}
    }
}

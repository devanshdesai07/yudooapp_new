﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopCommissionDao : AbstractShopCommissionDao
    {


        public override SuccessResult<AbstractShopCommission> ShopCommission_Upsert(AbstractShopCommission abstractShopCommission)
        {
            SuccessResult<AbstractShopCommission> ShopCommission = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopCommission.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopCommission.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CommisionPercentage", abstractShopCommission.CommisionPercentage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractShopCommission.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopCommission.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCommission_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopCommission = task.Read<SuccessResult<AbstractShopCommission>>().SingleOrDefault();
                ShopCommission.Item = task.Read<ShopCommission>().SingleOrDefault();
            }

            return ShopCommission;
        }


        public override PagedList<AbstractShopCommission> ShopCommission_All(PageParam pageParam, string search, long ShopId)
        {
            PagedList<AbstractShopCommission> ShopCommission = new PagedList<AbstractShopCommission>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCommission_All, param, commandType: CommandType.StoredProcedure);
                ShopCommission.Values.AddRange(task.Read<ShopCommission>());
                ShopCommission.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopCommission;
        }


        public override SuccessResult<AbstractShopCommission> ShopCommission_ById(long Id)
        {
            SuccessResult<AbstractShopCommission> ShopCommission = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCommission_ById, param, commandType: CommandType.StoredProcedure);
                ShopCommission = task.Read<SuccessResult<AbstractShopCommission>>().SingleOrDefault();
                ShopCommission.Item = task.Read<ShopCommission>().SingleOrDefault();
            }

            return ShopCommission;
        }


        public override PagedList<AbstractShopCommission> ShopCommission_ByShopOwnerId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractShopCommission> ShopCommission = new PagedList<AbstractShopCommission>();
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCommission_ByShopOwnerId, param, commandType: CommandType.StoredProcedure);
                ShopCommission.Values.AddRange(task.Read<ShopCommission>());
                ShopCommission.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return ShopCommission;
        }


        public override SuccessResult<AbstractShopCommission> ShopCommission_ActInAct(long Id)
        {
            SuccessResult<AbstractShopCommission> ShopCommission = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCommission_ActInAct, param, commandType: CommandType.StoredProcedure);
                ShopCommission = task.Read<SuccessResult<AbstractShopCommission>>().SingleOrDefault();
                ShopCommission.Item = task.Read<ShopCommission>().SingleOrDefault();
            }

            return ShopCommission;
        }

        public override SuccessResult<AbstractShopCommission> ShopCommission_Delete(long Id)
        {
            SuccessResult<AbstractShopCommission> ShopCommission = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCommission_Delete, param, commandType: CommandType.StoredProcedure);
                ShopCommission = task.Read<SuccessResult<AbstractShopCommission>>().SingleOrDefault();
                ShopCommission.Item = task.Read<ShopCommission>().SingleOrDefault();
            }

            return ShopCommission;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class ShopKeeperMasterDao : AbstractShopKeeperMasterDao
    {
        public override SuccessResult<AbstractShopKeeperMaster> ShopKeeperMaster_ById(long Id)
        {
            SuccessResult<AbstractShopKeeperMaster> ShopKeeperMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopKeeperMaster_ById, param, commandType: CommandType.StoredProcedure);
                ShopKeeperMaster = task.Read<SuccessResult<AbstractShopKeeperMaster>>().SingleOrDefault();
                ShopKeeperMaster.Item = task.Read<ShopKeeperMaster>().SingleOrDefault();
            }

            return ShopKeeperMaster;
        }

        public override SuccessResult<AbstractShopKeeperMaster> ShopKeeperMaster_Upsert(AbstractShopKeeperMaster abstractShopKeeperMaster)
        {
            SuccessResult<AbstractShopKeeperMaster> ShopKeeperMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopKeeperMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallBackButton", abstractShopKeeperMaster.CallBackButton, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ShopKeeperVCRingTime", abstractShopKeeperMaster.ShopKeeperVCRingTime, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryOption", abstractShopKeeperMaster.DeliveryOption, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@KeywordSearchRelevancyWeigtage", abstractShopKeeperMaster.KeywordSearchRelevancyWeigtage, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CustomerVCRingingTime", abstractShopKeeperMaster.CustomerVCRingingTime, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@VisitSimilarShopsButtonAppearTime", abstractShopKeeperMaster.VisitSimilarShopsButtonAppearTime, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@RequestCallBackButtonAppearTime", abstractShopKeeperMaster.RequestCallBackButtonAppearTime, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@HelpPageCallTimingStart", abstractShopKeeperMaster.HelpPageCallTimingStart, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@HelpPageCallTimingEnd", abstractShopKeeperMaster.HelpPageCallTimingEnd, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CustomerAppRating", abstractShopKeeperMaster.CustomerAppRating, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopKeeperMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopKeeperMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopKeeperMaster = task.Read<SuccessResult<AbstractShopKeeperMaster>>().SingleOrDefault();
                ShopKeeperMaster.Item = task.Read<ShopKeeperMaster>().SingleOrDefault();
            }

            return ShopKeeperMaster;
        }
    }
}

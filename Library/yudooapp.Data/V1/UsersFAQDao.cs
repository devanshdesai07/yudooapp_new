﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class UsersFAQDao : AbstractUsersFAQDao
    {
        public override PagedList<AbstractUsersFAQ> UsersFAQ_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractUsersFAQ> UsersFAQ = new PagedList<AbstractUsersFAQ>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFAQ_All, param, commandType: CommandType.StoredProcedure);
                UsersFAQ.Values.AddRange(task.Read<UsersFAQ>());
                UsersFAQ.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UsersFAQ;
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_ById(long Id)
        {
            SuccessResult<AbstractUsersFAQ> UsersFAQ = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFAQ_ById, param, commandType: CommandType.StoredProcedure);
                UsersFAQ = task.Read<SuccessResult<AbstractUsersFAQ>>().SingleOrDefault();
                UsersFAQ.Item = task.Read<UsersFAQ>().SingleOrDefault();
            }

            return UsersFAQ;
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_Delete(long Id)
        {
            SuccessResult<AbstractUsersFAQ> UsersFAQ = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFAQ_Delete, param, commandType: CommandType.StoredProcedure);
                UsersFAQ = task.Read<SuccessResult<AbstractUsersFAQ>>().SingleOrDefault();
                UsersFAQ.Item = task.Read<UsersFAQ>().SingleOrDefault();
            }
            return UsersFAQ;
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_ActInAct(long Id)
        {
            SuccessResult<AbstractUsersFAQ> UsersFAQ = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFAQ_ActInAct, param, commandType: CommandType.StoredProcedure);
                UsersFAQ = task.Read<SuccessResult<AbstractUsersFAQ>>().SingleOrDefault();
                UsersFAQ.Item = task.Read<UsersFAQ>().SingleOrDefault();
            }
            return UsersFAQ;
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_Upsert(AbstractUsersFAQ abstractUsersFAQ)
        {
            SuccessResult<AbstractUsersFAQ> UsersFAQ = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUsersFAQ.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Question", abstractUsersFAQ.Question, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Answer", abstractUsersFAQ.Answer, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MediaUrl", abstractUsersFAQ.MediaUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUsersFAQ.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUsersFAQ.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractUsersFAQ.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersFAQ_Upsert, param, commandType: CommandType.StoredProcedure);
                UsersFAQ = task.Read<SuccessResult<AbstractUsersFAQ>>().SingleOrDefault();
                UsersFAQ.Item = task.Read<UsersFAQ>().SingleOrDefault();
            }

            return UsersFAQ;
        }
    }
}

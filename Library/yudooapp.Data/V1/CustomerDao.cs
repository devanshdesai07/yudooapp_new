﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CustomerDao : AbstractCustomerDao
    {


        public override SuccessResult<AbstractCustomer> Customer_Upsert(AbstractCustomer abstractCustomer)
        {
            SuccessResult<AbstractCustomer> Customer = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCustomer.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerName", abstractCustomer.CustomerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerMobileNumber", abstractCustomer.CustomerMobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerEmail", abstractCustomer.CustomerEmail, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerAddress", abstractCustomer.CustomerAddress, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractCustomer.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Password", abstractCustomer.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCustomer.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCustomer.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_Upsert, param, commandType: CommandType.StoredProcedure);
                Customer = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                Customer.Item = task.Read<Customer>().SingleOrDefault();
            }

            return Customer;
        }


        public override PagedList<AbstractCustomer> Customer_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCustomer> Customer = new PagedList<AbstractCustomer>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_All, param, commandType: CommandType.StoredProcedure);
                Customer.Values.AddRange(task.Read<Customer>());
                Customer.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Customer;
        }


        public override SuccessResult<AbstractCustomer> Customer_ById(long Id)
        {
            SuccessResult<AbstractCustomer> Customer = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_ById, param, commandType: CommandType.StoredProcedure);
                Customer = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                Customer.Item = task.Read<Customer>().SingleOrDefault();
            }

            return Customer;
        }


        public override SuccessResult<AbstractCustomer> Customer_ActInact(long Id)
        {
            SuccessResult<AbstractCustomer> Customer = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_ActInact, param, commandType: CommandType.StoredProcedure);
                Customer = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                Customer.Item = task.Read<Customer>().SingleOrDefault();
            }

            return Customer;
        }

        public override SuccessResult<AbstractCustomer> Customer_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractCustomer> Customer = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_Delete, param, commandType: CommandType.StoredProcedure);
                Customer = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                Customer.Item = task.Read<Customer>().SingleOrDefault();
            }

            return Customer;
        }

        public override SuccessResult<AbstractCustomer> Customer_Overview(long MobileNumber,int OrderId)
        {
            SuccessResult<AbstractCustomer> Customer = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", MobileNumber, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_Overview, param, commandType: CommandType.StoredProcedure);
                Customer = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                Customer.Item = task.Read<Customer>().SingleOrDefault();
            }

            return Customer;
        }

        public override PagedList<AbstractCustomer> Customer_Orders(PageParam pageParam, long MobileNumber, int OrderId)
        {
            PagedList<AbstractCustomer> Customer = new PagedList<AbstractCustomer>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customer_Orders, param, commandType: CommandType.StoredProcedure);
                Customer.Values.AddRange(task.Read<Customer>());
                Customer.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Customer;
        }
        

    }
}

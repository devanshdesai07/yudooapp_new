﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class TagsDao : AbstractTagsDao
    {
        public override SuccessResult<AbstractTags> Tags_Upsert(AbstractTags abstractTags)
        {
            SuccessResult<AbstractTags> Tags = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTags.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Tags", abstractTags.Tags, dbType: DbType.String, direction: ParameterDirection.Input);            
            param.Add("@CreatedBy", abstractTags.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTags.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Tags_Upsert, param, commandType: CommandType.StoredProcedure);
                Tags = task.Read<SuccessResult<AbstractTags>>().SingleOrDefault();
                Tags.Item = task.Read<Tags>().SingleOrDefault();
            }

            return Tags;
        }


        public override PagedList<AbstractTags> Tags_All(PageParam pageParam, string search)
        {
            PagedList<AbstractTags> Tags = new PagedList<AbstractTags>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Tags_All, param, commandType: CommandType.StoredProcedure);
                Tags.Values.AddRange(task.Read<Tags>());
                Tags.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Tags;
        }

        public override SuccessResult<AbstractTags> Tags_ById(long Id)
        {
            SuccessResult<AbstractTags> Tags = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Tags_ById, param, commandType: CommandType.StoredProcedure);
                Tags = task.Read<SuccessResult<AbstractTags>>().SingleOrDefault();
                Tags.Item = task.Read<Tags>().SingleOrDefault();
            }

            return Tags;
        }


        public override SuccessResult<AbstractTags> Tags_Delete(long Id)
        {
            SuccessResult<AbstractTags> Tags = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Tags_Delete, param, commandType: CommandType.StoredProcedure);
                Tags = task.Read<SuccessResult<AbstractTags>>().SingleOrDefault();
                Tags.Item = task.Read<Tags>().SingleOrDefault();
            }

            return Tags;
        }

    }
}

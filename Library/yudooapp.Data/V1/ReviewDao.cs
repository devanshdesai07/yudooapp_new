﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ReviewDao : AbstractReviewDao
    {
        
        public override PagedList<AbstractReview> Review_All(PageParam pageParam, string search , AbstractReview abstractReview)
        {
            PagedList<AbstractReview> Review = new PagedList<AbstractReview>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Review_All, param, commandType: CommandType.StoredProcedure);
                Review.Values.AddRange(task.Read<Review>());
                Review.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Review;
        }
        public override PagedList<AbstractReview> Review_ByCustomerId(PageParam pageParam, string search, AbstractReview abstractReview)
        {
            PagedList<AbstractReview> Review = new PagedList<AbstractReview>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractReview.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Review_ByCustomerId, param, commandType: CommandType.StoredProcedure);
                Review.Values.AddRange(task.Read<Review>());
                Review.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Review;
        }
        public override SuccessResult<AbstractReview> Review_Upsert(AbstractReview abstractReview)
        {
            SuccessResult<AbstractReview> Review = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractReview.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ReviewComment", abstractReview.ReviewComment, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractReview.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Review_Upsert, param, commandType: CommandType.StoredProcedure);
                Review = task.Read<SuccessResult<AbstractReview>>().SingleOrDefault();
                Review.Item = task.Read<Review>().SingleOrDefault();
            }

            return Review;
        }
    }
}

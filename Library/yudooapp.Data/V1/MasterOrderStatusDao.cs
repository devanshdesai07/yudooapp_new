﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class MasterOrderStatusDao : AbstractMasterOrderStatusDao
    {


        public override SuccessResult<AbstractMasterOrderStatus> MasterOrderStatus_Upsert(AbstractMasterOrderStatus abstractMasterOrderStatus)
        {
            SuccessResult<AbstractMasterOrderStatus> MasterOrderStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterOrderStatus.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractMasterOrderStatus.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterOrderStatus_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterOrderStatus = task.Read<SuccessResult<AbstractMasterOrderStatus>>().SingleOrDefault();
                MasterOrderStatus.Item = task.Read<MasterOrderStatus>().SingleOrDefault();
            }

            return MasterOrderStatus;
        }


        public override PagedList<AbstractMasterOrderStatus> MasterOrderStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterOrderStatus> MasterOrderStatus = new PagedList<AbstractMasterOrderStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterOrderStatus_All, param, commandType: CommandType.StoredProcedure);
                MasterOrderStatus.Values.AddRange(task.Read<MasterOrderStatus>());
                MasterOrderStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterOrderStatus;
        }


        
    }
}

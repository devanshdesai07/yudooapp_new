﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class MasterDepartmentShopDao : AbstractMasterDepartmentShopDao
    {
        public override SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_Upsert(AbstractMasterDepartmentShop abstractMasterDepartmentShop)
        {
            SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterDepartmentShop.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractMasterDepartmentShop.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMasterDepartmentShop.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractMasterDepartmentShop.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterDepartmentShop_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterDepartmentShop = task.Read<SuccessResult<AbstractMasterDepartmentShop>>().SingleOrDefault();
                MasterDepartmentShop.Item = task.Read<MasterDepartmentShop>().SingleOrDefault();
            }

            return MasterDepartmentShop;
        }
        public override PagedList<AbstractMasterDepartmentShop> MasterDepartmentShop_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractMasterDepartmentShop> MasterDepartmentShop = new PagedList<AbstractMasterDepartmentShop>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterDepartmentShop_All, param, commandType: CommandType.StoredProcedure);
                MasterDepartmentShop.Values.AddRange(task.Read<MasterDepartmentShop>());
                MasterDepartmentShop.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterDepartmentShop;
        }
        public override SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_ById(long Id)
        {
            SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterDepartmentShop_ById, param, commandType: CommandType.StoredProcedure);
                MasterDepartmentShop = task.Read<SuccessResult<AbstractMasterDepartmentShop>>().SingleOrDefault();
                MasterDepartmentShop.Item = task.Read<MasterDepartmentShop>().SingleOrDefault();
            }

            return MasterDepartmentShop;
        }

        public override SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_Delete(long Id)
        {
            SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterDepartmentShop_Delete, param, commandType: CommandType.StoredProcedure);
                MasterDepartmentShop = task.Read<SuccessResult<AbstractMasterDepartmentShop>>().SingleOrDefault();
                MasterDepartmentShop.Item = task.Read<MasterDepartmentShop>().SingleOrDefault();
            }

            return MasterDepartmentShop;
        }

    }
}

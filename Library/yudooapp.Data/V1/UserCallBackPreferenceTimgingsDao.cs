﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;


namespace yudooapp.Data.V1
{
    public class UserCallBackPreferenceTimgingsDao : AbstractUserCallBackPreferenceTimgingsDao
    {
        public override PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_All(PageParam pageParam, string search,long UserId)
        {
            PagedList<AbstractUserCallBackPreferenceTimgings> returnValues = new PagedList<AbstractUserCallBackPreferenceTimgings>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserCallBackPreferenceTimgings_All, param, commandType: CommandType.StoredProcedure);
                    returnValues.Values.AddRange(task.Read<UserCallBackPreferenceTimgings>());
                    returnValues.TotalRecords = task.Read<long>().SingleOrDefault();
                }
            }
            catch (Exception)
            {
                returnValues.TotalRecords = 0;
            }
            return returnValues;
        }

        public override bool UserCallBackPreferenceTimgings_Available(long UserId)
        {
            bool returnValues = false;
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserCallBackPreferenceTimgings_Available, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<bool>().SingleOrDefault();
                }
            }
            catch (Exception)
            {
                returnValues = false;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_ById(long Id)
        {
            SuccessResult<AbstractUserCallBackPreferenceTimgings> returnValues = new SuccessResult<AbstractUserCallBackPreferenceTimgings>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserCallBackPreferenceTimgings_ById, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractUserCallBackPreferenceTimgings>>().SingleOrDefault();
                    returnValues.Item = task.Read<UserCallBackPreferenceTimgings>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings = new PagedList<AbstractUserCallBackPreferenceTimgings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCallBackPreferenceTimgings_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserCallBackPreferenceTimgings.Values.AddRange(task.Read<UserCallBackPreferenceTimgings>());
                UserCallBackPreferenceTimgings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCallBackPreferenceTimgings;
        }

        public override SuccessResult<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_Delete(long Id)
        {
            SuccessResult<AbstractUserCallBackPreferenceTimgings> returnValues = new SuccessResult<AbstractUserCallBackPreferenceTimgings>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserCallBackPreferenceTimgings_Delete, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractUserCallBackPreferenceTimgings>>().SingleOrDefault();

                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractUserCallBackPreferenceTimingJson> UserCallBackPreferenceTimgings_Upsert(AbstractUserCallBackPreferenceTimingJson abstractUserCallBackPreferenceTimingJson)
        {
            SuccessResult<AbstractUserCallBackPreferenceTimingJson> returnValues = null;
            var param = new DynamicParameters();

            for (int i = 0; i < abstractUserCallBackPreferenceTimingJson.data.Count; i++)
            {
                param.Add("@Id", abstractUserCallBackPreferenceTimingJson.data[i].Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UserId", abstractUserCallBackPreferenceTimingJson.data[i].UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Day", abstractUserCallBackPreferenceTimingJson.data[i].Day, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@FromTime", abstractUserCallBackPreferenceTimingJson.data[i].FromTime, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@ToTime", abstractUserCallBackPreferenceTimingJson.data[i].ToTime, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractUserCallBackPreferenceTimingJson.data[i].CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractUserCallBackPreferenceTimingJson.data[i].UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@IsActive", abstractUserCallBackPreferenceTimingJson.data[i].IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserCallBackPreferenceTimgings_Upsert, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractUserCallBackPreferenceTimingJson>>().SingleOrDefault();
                    returnValues.Item = task.Read<UserCallBackPreferenceTimingJson>().SingleOrDefault();
                }
            }

            return returnValues;
        }
    }
}

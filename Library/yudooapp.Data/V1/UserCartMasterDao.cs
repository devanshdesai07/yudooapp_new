﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserCartMasterDao : AbstractUserCartMasterDao
    {

        public override SuccessResult<AbstractUserCartMaster> UserCartMaster_Upsert(AbstractUserCartMaster abstractUserCartMaster)
        {
            SuccessResult<AbstractUserCartMaster> UserCartMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserCartMaster.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserCartMaster.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractUserCartMaster.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserName", abstractUserCartMaster.UserName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FlatNo", abstractUserCartMaster.FlatNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Pincode", abstractUserCartMaster.Pincode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Address", abstractUserCartMaster.Address, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentMode", abstractUserCartMaster.PaymentMode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ExtraCharge", abstractUserCartMaster.ExtraCharge, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Discount", abstractUserCartMaster.Discount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserCartMaster.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserCartMaster.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", abstractUserCartMaster.VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeliveryCharge", abstractUserCartMaster.DeliveryCharge, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@PaymentCharge", abstractUserCartMaster.PaymentCharge, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Country", abstractUserCartMaster.Country, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@State", abstractUserCartMaster.State, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractUserCartMaster.City, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                UserCartMaster = task.Read<SuccessResult<AbstractUserCartMaster>>().SingleOrDefault();
                UserCartMaster.Item = task.Read<UserCartMaster>().SingleOrDefault();
            }

            return UserCartMaster;
        }

        public override PagedList<AbstractUserCartMaster> UserCartMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserCartMaster> UserCartMaster = new PagedList<AbstractUserCartMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartMaster_All, param, commandType: CommandType.StoredProcedure);
                UserCartMaster.Values.AddRange(task.Read<UserCartMaster>());
                UserCartMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCartMaster;
        }

        public override PagedList<AbstractUserCartMaster> UserCartMaster_ByShopId(PageParam pageParam, string search , long ShopId)
        {
            PagedList<AbstractUserCartMaster> UserCartMaster = new PagedList<AbstractUserCartMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartMaster_ByShopId, param, commandType: CommandType.StoredProcedure);
                UserCartMaster.Values.AddRange(task.Read<UserCartMaster>());
                UserCartMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCartMaster;
        }

        public override PagedList<AbstractUserCartMaster> UserCartMaster_ByUserId(PageParam pageParam, string search,long UserId)
        {
            PagedList<AbstractUserCartMaster> UserCartMaster = new PagedList<AbstractUserCartMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartMaster_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserCartMaster.Values.AddRange(task.Read<UserCartMaster>());
                UserCartMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserCartMaster;
        }

        public override SuccessResult<AbstractUserCartMaster> UserCartMaster_ById(long Id)
        {
            SuccessResult<AbstractUserCartMaster> UserCartMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartMaster_ById, param, commandType: CommandType.StoredProcedure);
                UserCartMaster = task.Read<SuccessResult<AbstractUserCartMaster>>().SingleOrDefault();
                UserCartMaster.Item = task.Read<UserCartMaster>().SingleOrDefault();
            }

            return UserCartMaster;
        }
        public override SuccessResult<AbstractUserCartMaster> UserCartMaster_ByVideoCallId(long VideoCallId , long UserId,int Type)
        {
            SuccessResult<AbstractUserCartMaster> UserCartMaster = null;
            var param = new DynamicParameters();

            param.Add("@VideoCallId", VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserCartMaster_ByVideoCallId, param, commandType: CommandType.StoredProcedure);
                UserCartMaster = task.Read<SuccessResult<AbstractUserCartMaster>>().SingleOrDefault();
                UserCartMaster.Item = task.Read<UserCartMaster>().SingleOrDefault();
            }

            return UserCartMaster;
        }
    }
}

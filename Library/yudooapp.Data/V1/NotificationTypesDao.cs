﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class NotificationTypesDao : AbstractNotificationTypesDao
    {
        public override SuccessResult<AbstractNotificationTypes> NotificationTypes_Upsert(AbstractNotificationTypes abstractNotificationTypes)
        {
            SuccessResult<AbstractNotificationTypes> NotificationTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractNotificationTypes.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NotificationName", abstractNotificationTypes.NotificationName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractNotificationTypes.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractNotificationTypes.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.NotificationTypes_Upsert, param, commandType: CommandType.StoredProcedure);
                NotificationTypes = task.Read<SuccessResult<AbstractNotificationTypes>>().SingleOrDefault();
                NotificationTypes.Item = task.Read<NotificationTypes>().SingleOrDefault();
            }

            return NotificationTypes;
        }

        public override PagedList<AbstractNotificationTypes> NotificationTypes_All(PageParam pageParam, string search)
        {
            PagedList<AbstractNotificationTypes> NotificationTypes = new PagedList<AbstractNotificationTypes>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.NotificationTypes_All, param, commandType: CommandType.StoredProcedure);
                NotificationTypes.Values.AddRange(task.Read<NotificationTypes>());
                NotificationTypes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return NotificationTypes;
        }

        public override SuccessResult<AbstractNotificationTypes> NotificationTypes_ById(long Id)
        {
            SuccessResult<AbstractNotificationTypes> NotificationTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.NotificationTypes_ById, param, commandType: CommandType.StoredProcedure);
                NotificationTypes = task.Read<SuccessResult<AbstractNotificationTypes>>().SingleOrDefault();
                NotificationTypes.Item = task.Read<NotificationTypes>().SingleOrDefault();
            }

            return NotificationTypes;
        }

        public override SuccessResult<AbstractNotificationTypes> NotificationTypes_Delete(long Id)
        {
            SuccessResult<AbstractNotificationTypes> NotificationTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.NotificationTypes_Delete, param, commandType: CommandType.StoredProcedure);
                NotificationTypes = task.Read<SuccessResult<AbstractNotificationTypes>>().SingleOrDefault();
                NotificationTypes.Item = task.Read<NotificationTypes>().SingleOrDefault();
            }

            return NotificationTypes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserShopCallLogsDao : AbstractUserShopCallLogsDao
    {
        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_Upsert(AbstractUserShopCallLogs abstractUserShopCallLogs)
        {
            SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserShopCallLogs.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserShopCallLogs.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractUserShopCallLogs.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@CallDateTime", abstractUserShopCallLogs.CallDateTime, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CallStatus", abstractUserShopCallLogs.CallStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DescriptionByUser", abstractUserShopCallLogs.DescriptionByUser, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DescriptionByShop", abstractUserShopCallLogs.DescriptionByShop, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@CallStartTime", abstractUserShopCallLogs.CallStartTime, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            //param.Add("@CallEndTime", abstractUserShopCallLogs.CallEndTime, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@WhoInitiated", abstractUserShopCallLogs.WhoInitiated, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CallBackHrs", abstractUserShopCallLogs.CallBackHrs, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUserShopCallLogs.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserShopCallLogs.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminUserId", abstractUserShopCallLogs.AdminUserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallActionPersonId", abstractUserShopCallLogs.CallActionPersonId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VideoSessionId", abstractUserShopCallLogs.VideoSessionId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractUserShopCallLogs.Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@VideoCallLink", abstractUserShopCallLogs.VideoCallLink, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_Upsert, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs = task.Read<SuccessResult<AbstractUserShopCallLogs>>().SingleOrDefault();
                UserShopCallLogs.Item = task.Read<UserShopCallLogs>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }


        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_All(PageParam pageParam, string search, long UserId, long ShopId)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_All, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserShopCallLogs;
        }


        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_ById(long Id)
        {
            SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_ById, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs = task.Read<SuccessResult<AbstractUserShopCallLogs>>().SingleOrDefault();
                UserShopCallLogs.Item = task.Read<UserShopCallLogs>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }


        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();
            var param = new DynamicParameters();

            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_ByUserId, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByOrderId(PageParam pageParam, long OrderId)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();
            var param = new DynamicParameters();

            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_ByOrderId, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }


        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_ByShopId, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }

        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_Delete, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs = task.Read<SuccessResult<AbstractUserShopCallLogs>>().SingleOrDefault();
                UserShopCallLogs.Item = task.Read<UserShopCallLogs>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogsDetails_All(PageParam pageParam, string Search, long Id, long OrderId, string UserMobileNumber, long ShopId, string ShopName, string MobileNumber, string CallDateTime)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserMobileNumber", UserMobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CallDateTime", CallDateTime, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogsDetails_All, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserShopCallLogs;
        }

        public override PagedList<AbstractUserShopCallLogs> Request_CallBack_ShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Request_CallBack_ShopId, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ReceiveVideoCall(PageParam pageParam)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_ReceiveVideoCall, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserShopCallLogs;
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_PendingCall(PageParam pageParam, long UserId)
        {
            PagedList<AbstractUserShopCallLogs> UserShopCallLogs = new PagedList<AbstractUserShopCallLogs>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_PendingCall, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs.Values.AddRange(task.Read<UserShopCallLogs>());
                UserShopCallLogs.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserShopCallLogs;
        }

        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_CallStatus_Update(long Id, long CallStatus)
        {
            SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallStatus", CallStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserShopCallLogs_CallStatus_Update, param, commandType: CommandType.StoredProcedure);
                UserShopCallLogs = task.Read<SuccessResult<AbstractUserShopCallLogs>>().SingleOrDefault();
                UserShopCallLogs.Item = task.Read<UserShopCallLogs>().SingleOrDefault();
            }

            return UserShopCallLogs;
        }
        public override PagedList<AbstractRequest_CallBack_Admin> Request_CallBack_Admin(PageParam pageParam, string search)
        {
            PagedList<AbstractRequest_CallBack_Admin> Request_CallBack_Admin = new PagedList<AbstractRequest_CallBack_Admin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Request_CallBack_Admin, param, commandType: CommandType.StoredProcedure);
                Request_CallBack_Admin.Values.AddRange(task.Read<Request_CallBack_Admin>());
                Request_CallBack_Admin.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Request_CallBack_Admin;
        }

    }
    
}

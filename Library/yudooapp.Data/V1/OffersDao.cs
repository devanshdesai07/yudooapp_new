﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class OffersDao : AbstractOffersDao
    {


        public override SuccessResult<AbstractOffers> Offers_Upsert(AbstractOffers abstractOffers)
        {
            SuccessResult<AbstractOffers> Offers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOffers.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractOffers.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", abstractOffers.ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OfferText", abstractOffers.OfferText, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OfferStatus", abstractOffers.OfferStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StartDate", abstractOffers.StartDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@EndDate", abstractOffers.EndDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractOffers.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input); 
            param.Add("@CreatedBy", abstractOffers.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOffers.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_Upsert, param, commandType: CommandType.StoredProcedure);
                Offers = task.Read<SuccessResult<AbstractOffers>>().SingleOrDefault();
                Offers.Item = task.Read<Offers>().SingleOrDefault();
            }

            return Offers;
        }


        public override PagedList<AbstractOffers> Offers_All(PageParam pageParam, string search)
        {
            PagedList<AbstractOffers> Offers = new PagedList<AbstractOffers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_All, param, commandType: CommandType.StoredProcedure);
                Offers.Values.AddRange(task.Read<Offers>());
                Offers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Offers;
        }

        public override PagedList<AbstractOffers> Offers_All_Active(PageParam pageParam, string search)
        {
            PagedList<AbstractOffers> Offers = new PagedList<AbstractOffers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_All_Active, param, commandType: CommandType.StoredProcedure);
                Offers.Values.AddRange(task.Read<Offers>());
                Offers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Offers;
        }


        public override SuccessResult<AbstractOffers> Offers_ById(long Id)
        {
            SuccessResult<AbstractOffers> Offers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_ById, param, commandType: CommandType.StoredProcedure);
                Offers = task.Read<SuccessResult<AbstractOffers>>().SingleOrDefault();
                Offers.Item = task.Read<Offers>().SingleOrDefault();
            }

            return Offers;
        }

        public override PagedList<AbstractOffers> Offers_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractOffers> Offers = new PagedList<AbstractOffers>();
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_ByShopId, param, commandType: CommandType.StoredProcedure);
                Offers.Values.AddRange(task.Read<Offers>());
                Offers.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Offers;
        }

        public override SuccessResult<AbstractOffers> OfferStatus_ChangeStatus(long Id, long Status)
        {
            SuccessResult<AbstractOffers> Offers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Status", Status, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OfferStatus_ChangeStatus, param, commandType: CommandType.StoredProcedure);
                Offers = task.Read<SuccessResult<AbstractOffers>>().SingleOrDefault();
                Offers.Item = task.Read<Offers>().SingleOrDefault();
            }

            return Offers;
        }


        public override SuccessResult<AbstractOffers> Offers_Delete(long Id)
        {
            SuccessResult<AbstractOffers> Offers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_Delete, param, commandType: CommandType.StoredProcedure);
                Offers = task.Read<SuccessResult<AbstractOffers>>().SingleOrDefault();
                Offers.Item = task.Read<Offers>().SingleOrDefault();
            }

            return Offers;
        }

        public override SuccessResult<AbstractOffers> Offers_ActInAct(long Id, bool IsActive)
        {
            SuccessResult<AbstractOffers> Offers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Offers_ActInAct, param, commandType: CommandType.StoredProcedure);
                Offers = task.Read<SuccessResult<AbstractOffers>>().SingleOrDefault();
                Offers.Item = task.Read<Offers>().SingleOrDefault();
            }

            return Offers;
        }
    }
}

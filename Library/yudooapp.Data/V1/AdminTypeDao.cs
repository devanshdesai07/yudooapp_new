﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class AdminTypeDao : AbstractAdmintypeDao 
    {

        public override PagedList<AbstractAdminType> AdminType_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdminType> admintype = new PagedList<AbstractAdminType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminType_All_Active, param, commandType: CommandType.StoredProcedure);
                admintype.Values.AddRange(task.Read<AdminType>());
                admintype.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return admintype;
        }

        public override SuccessResult<AbstractAdminType> AdminType_ById(int Id)
        {
            SuccessResult<AbstractAdminType> AdminType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminType_ById, param, commandType: CommandType.StoredProcedure);
                AdminType = task.Read<SuccessResult<AbstractAdminType>>().SingleOrDefault();
                AdminType.Item = task.Read<AdminType>().SingleOrDefault();
            }
            return AdminType;
        }

        public override SuccessResult<AbstractAdminType> AdminType_Delete(int Id,int DeletedBy)
        {
            SuccessResult<AbstractAdminType> AdminType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminType_Delete, param, commandType: CommandType.StoredProcedure);
                AdminType = task.Read<SuccessResult<AbstractAdminType>>().SingleOrDefault();
                AdminType.Item = task.Read<AdminType>().SingleOrDefault();
            }
            return AdminType;
        }

        public override SuccessResult<AbstractAdminType> AdminType_ActInAct(int Id)
        {
            SuccessResult<AbstractAdminType> AdminType = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminType_ActInAct, param, commandType: CommandType.StoredProcedure);
                AdminType = task.Read<SuccessResult<AbstractAdminType>>().SingleOrDefault();
                AdminType.Item = task.Read<AdminType>().SingleOrDefault();
            }
            return AdminType;
        }

        public override SuccessResult<AbstractAdminType> AdminType_Upsert(AbstractAdminType abstractAdminType)
        {
            SuccessResult<AbstractAdminType> AdminType = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAdminType.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractAdminType.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractAdminType.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractAdminType.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminType_Upsert, param, commandType: CommandType.StoredProcedure);
                AdminType = task.Read<SuccessResult<AbstractAdminType>>().SingleOrDefault();
                AdminType.Item = task.Read<AdminType>().SingleOrDefault();
            }
            return AdminType;
        }

    }
}

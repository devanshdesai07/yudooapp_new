﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;

namespace yudooapp.Data.V1
{
    public class FAQsDao : AbstractFAQsDao
    {
        public override SuccessResult<AbstractFAQs> FAQs_ActInAct(long Id)
        {
            SuccessResult<AbstractFAQs> returnValues = new SuccessResult<AbstractFAQs>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FAQs_ActInAct, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFAQs>>().SingleOrDefault();
                    returnValues.Item = task.Read<FAQs>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override PagedList<AbstractFAQs> FAQs_All(PageParam pageParam, string search)
        {
            PagedList<AbstractFAQs> returnValues = new PagedList<AbstractFAQs>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FAQs_All, param, commandType: CommandType.StoredProcedure);
                    returnValues.Values.AddRange(task.Read<FAQs>());
                    returnValues.TotalRecords = task.Read<long>().SingleOrDefault();
                }
            }
            catch (Exception)
            {
                returnValues.TotalRecords = 0;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractFAQs> FAQs_ById(long Id)
        {
            SuccessResult<AbstractFAQs> returnValues = new SuccessResult<AbstractFAQs>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FAQs_ById, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFAQs>>().SingleOrDefault();
                    returnValues.Item = task.Read<FAQs>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractFAQs> FAQs_Delete(long Id)
        {
            SuccessResult<AbstractFAQs> returnValues = new SuccessResult<AbstractFAQs>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FAQs_Delete, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFAQs>>().SingleOrDefault();
                    
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractFAQs> FAQs_Upsert(AbstractFAQs abstractFAQs)
        {
            SuccessResult<AbstractFAQs> returnValues = new SuccessResult<AbstractFAQs>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", abstractFAQs.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@FaqHeading", abstractFAQs.FaqHeading, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@IsActive", abstractFAQs.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@Description", abstractFAQs.Description, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractFAQs.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractFAQs.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FAQs_Upsert, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFAQs>>().SingleOrDefault();
                    returnValues.Item = task.Read<FAQs>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }
    }
}

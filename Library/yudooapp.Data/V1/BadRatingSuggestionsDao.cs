﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class BadRatingSuggestionsDao : AbstractBadRatingSuggestionsDao
    {
        public override PagedList<AbstractBadRatingSuggestions> BadRatingSuggestions_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractBadRatingSuggestions> BadRatingSuggestions = new PagedList<AbstractBadRatingSuggestions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BadRatingSuggestions_All, param, commandType: CommandType.StoredProcedure);
                BadRatingSuggestions.Values.AddRange(task.Read<BadRatingSuggestions>());
                BadRatingSuggestions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return BadRatingSuggestions;
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_ById(long Id)
        {
            SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BadRatingSuggestions_ById, param, commandType: CommandType.StoredProcedure);
                BadRatingSuggestions = task.Read<SuccessResult<AbstractBadRatingSuggestions>>().SingleOrDefault();
                BadRatingSuggestions.Item = task.Read<BadRatingSuggestions>().SingleOrDefault();
            }

            return BadRatingSuggestions;
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Delete(long Id)
        {
            SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BadRatingSuggestions_Delete, param, commandType: CommandType.StoredProcedure);
                BadRatingSuggestions = task.Read<SuccessResult<AbstractBadRatingSuggestions>>().SingleOrDefault();
                BadRatingSuggestions.Item = task.Read<BadRatingSuggestions>().SingleOrDefault();
            }
            return BadRatingSuggestions;
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_ActInActive(long Id)
        {
            SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BadRatingSuggestions_ActInActive, param, commandType: CommandType.StoredProcedure);
                BadRatingSuggestions = task.Read<SuccessResult<AbstractBadRatingSuggestions>>().SingleOrDefault();
                BadRatingSuggestions.Item = task.Read<BadRatingSuggestions>().SingleOrDefault();
            }
            return BadRatingSuggestions;
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Upsert(AbstractBadRatingSuggestions abstractBadRatingSuggestions)
        {
            SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractBadRatingSuggestions.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Text", abstractBadRatingSuggestions.Text, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractBadRatingSuggestions.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractBadRatingSuggestions.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractBadRatingSuggestions.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractBadRatingSuggestions.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BadRatingSuggestions_Upsert, param, commandType: CommandType.StoredProcedure);
                BadRatingSuggestions = task.Read<SuccessResult<AbstractBadRatingSuggestions>>().SingleOrDefault();
                BadRatingSuggestions.Item = task.Read<BadRatingSuggestions>().SingleOrDefault();
            }

            return BadRatingSuggestions;
        }
        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Update_DisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsUp", IsUp, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BadRatingSuggestions_Update_DisplayOrder, param, commandType: CommandType.StoredProcedure);
                BadRatingSuggestions = task.Read<SuccessResult<AbstractBadRatingSuggestions>>().SingleOrDefault();
                BadRatingSuggestions.Item = task.Read<BadRatingSuggestions>().SingleOrDefault();
            }

            return BadRatingSuggestions;
        }
    }
}

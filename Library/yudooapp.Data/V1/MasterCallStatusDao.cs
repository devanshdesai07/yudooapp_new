﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class MasterCallStatusDao : AbstractMasterCallStatusDao
    {


        public override SuccessResult<AbstractMasterCallStatus> MasterCallStatus_Upsert(AbstractMasterCallStatus abstractMasterCallStatus)
        {
            SuccessResult<AbstractMasterCallStatus> MasterCallStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterCallStatus.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractMasterCallStatus.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCallStatus_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterCallStatus = task.Read<SuccessResult<AbstractMasterCallStatus>>().SingleOrDefault();
                MasterCallStatus.Item = task.Read<MasterCallStatus>().SingleOrDefault();
            }

            return MasterCallStatus;
        }


        public override PagedList<AbstractMasterCallStatus> MasterCallStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterCallStatus> MasterCallStatus = new PagedList<AbstractMasterCallStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCallStatus_All, param, commandType: CommandType.StoredProcedure);
                MasterCallStatus.Values.AddRange(task.Read<MasterCallStatus>());
                MasterCallStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterCallStatus;
        }



    }
}

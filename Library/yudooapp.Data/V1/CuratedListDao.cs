﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CuratedListDao : AbstractCuratedListDao
    {


        public override SuccessResult<AbstractCuratedList> CuratedList_Upsert(AbstractCuratedList abstractCuratedList)
        {
            SuccessResult<AbstractCuratedList> CuratedList = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCuratedList.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CuratedListName", abstractCuratedList.CuratedListName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractCuratedList.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Tags", abstractCuratedList.Tags, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopIdList", abstractCuratedList.ShopIdList, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCuratedList.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCuratedList.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CuratedList_Upsert, param, commandType: CommandType.StoredProcedure);
                CuratedList = task.Read<SuccessResult<AbstractCuratedList>>().SingleOrDefault();
                CuratedList.Item = task.Read<CuratedList>().SingleOrDefault();
            }

            return CuratedList;
        }


        public override PagedList<AbstractCuratedList> CuratedList_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCuratedList> CuratedList = new PagedList<AbstractCuratedList>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CuratedList_All, param, commandType: CommandType.StoredProcedure);
                CuratedList.Values.AddRange(task.Read<CuratedList>());
                CuratedList.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CuratedList;
        }


        public override SuccessResult<AbstractCuratedList> CuratedList_ById(long Id)
        {
            SuccessResult<AbstractCuratedList> CuratedList = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CuratedList_ById, param, commandType: CommandType.StoredProcedure);
                CuratedList = task.Read<SuccessResult<AbstractCuratedList>>().SingleOrDefault();
                CuratedList.Item = task.Read<CuratedList>().SingleOrDefault();
            }

            return CuratedList;
        }


        public override SuccessResult<AbstractCuratedList> CuratedList_ActInAct(long Id)
        {
            SuccessResult<AbstractCuratedList> CuratedList = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CuratedList_ActInAct, param, commandType: CommandType.StoredProcedure);
                CuratedList = task.Read<SuccessResult<AbstractCuratedList>>().SingleOrDefault();
                CuratedList.Item = task.Read<CuratedList>().SingleOrDefault();
            }

            return CuratedList;
        }

        public override SuccessResult<AbstractCuratedList> CuratedList_Delete(long Id,long DeletedBy)
        {
            SuccessResult<AbstractCuratedList> CuratedList = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CuratedList_Delete, param, commandType: CommandType.StoredProcedure);
                CuratedList = task.Read<SuccessResult<AbstractCuratedList>>().SingleOrDefault();
                CuratedList.Item = task.Read<CuratedList>().SingleOrDefault();
            }

            return CuratedList;
        }
    }
}

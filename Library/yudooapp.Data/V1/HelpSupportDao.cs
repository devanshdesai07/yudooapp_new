﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class HelpSupportDao : AbstractHelpSupportDao
    {
        public override PagedList<AbstractHelpSupport> HelpSupport_All(PageParam pageParam, string search)
        {
            PagedList<AbstractHelpSupport> HelpSupport = new PagedList<AbstractHelpSupport>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HelpSupport_All, param, commandType: CommandType.StoredProcedure);
                HelpSupport.Values.AddRange(task.Read<HelpSupport>());
                HelpSupport.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return HelpSupport;
        }
        public override SuccessResult<AbstractHelpSupport> HelpSupport_Action(long Id,long StatusId)
        {
            SuccessResult<AbstractHelpSupport> HelpSupport = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HelpSupport_Action, param, commandType: CommandType.StoredProcedure);
                HelpSupport = task.Read<SuccessResult<AbstractHelpSupport>>().SingleOrDefault();
                HelpSupport.Item = task.Read<HelpSupport>().SingleOrDefault();
            }

            return HelpSupport;
        }
       

        public override SuccessResult<AbstractHelpSupport> HelpSupport_Upsert(AbstractHelpSupport abstractHelpSupport)
        {
            SuccessResult<AbstractHelpSupport> HelpSupport = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractHelpSupport.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId ", abstractHelpSupport.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HelpSupport_Upsert, param, commandType: CommandType.StoredProcedure);
                HelpSupport = task.Read<SuccessResult<AbstractHelpSupport>>().SingleOrDefault();
                HelpSupport.Item = task.Read<HelpSupport>().SingleOrDefault();
            }

            return HelpSupport;
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopRatingsDao : AbstractShopRatingsDao
    {


        public override SuccessResult<AbstractShopRatings> ShopRatings_Upsert(AbstractShopRatings abstractShopRatings)
        {
            SuccessResult<AbstractShopRatings> ShopRatings = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopRatings.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopRatings.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractShopRatings.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Experience", abstractShopRatings.Experience, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Rating", abstractShopRatings.Rating, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Review", abstractShopRatings.Review, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VideoCallId", abstractShopRatings.VideoCallId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractShopRatings.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopRatings.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopRatings_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopRatings = task.Read<SuccessResult<AbstractShopRatings>>().SingleOrDefault();
                ShopRatings.Item = task.Read<ShopRatings>().SingleOrDefault();
            }

            return ShopRatings;
        }


        public override PagedList<AbstractShopRatings> ShopRatings_All(PageParam pageParam, string search , long ShopId , long UserId)
        {
            PagedList<AbstractShopRatings> ShopRatings = new PagedList<AbstractShopRatings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopRatings_All, param, commandType: CommandType.StoredProcedure);
                ShopRatings.Values.AddRange(task.Read<ShopRatings>());
                ShopRatings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopRatings;
        }

        public override PagedList<AbstractShopRatings> ShopRatings_ByUserId(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractShopRatings> ShopRatings = new PagedList<AbstractShopRatings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopRatings_ByUserId, param, commandType: CommandType.StoredProcedure);
                ShopRatings.Values.AddRange(task.Read<ShopRatings>());
                ShopRatings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopRatings;
        }

        public override PagedList<AbstractShopRatings> ShopRatings_ByShopId(PageParam pageParam, string search, long ShopId)
        {
            PagedList<AbstractShopRatings> ShopRatings = new PagedList<AbstractShopRatings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopRatings_ByShopId, param, commandType: CommandType.StoredProcedure);
                ShopRatings.Values.AddRange(task.Read<ShopRatings>());
                ShopRatings.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopRatings;
        }
        public override SuccessResult<AbstractShopRatings> ShopRatings_ById(long Id)
        {
            SuccessResult<AbstractShopRatings> ShopRatings = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopRatings_ById, param, commandType: CommandType.StoredProcedure);
                ShopRatings = task.Read<SuccessResult<AbstractShopRatings>>().SingleOrDefault();
                ShopRatings.Item = task.Read<ShopRatings>().SingleOrDefault();
            }

            return ShopRatings;
        }


        public override SuccessResult<AbstractShopRatings> ShopRatings_Delete(long Id)
        {
            SuccessResult<AbstractShopRatings> ShopRatings = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopRatings_Delete, param, commandType: CommandType.StoredProcedure);
                ShopRatings = task.Read<SuccessResult<AbstractShopRatings>>().SingleOrDefault();
                ShopRatings.Item = task.Read<ShopRatings>().SingleOrDefault();
            }

            return ShopRatings;
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class PageDao : AbstractPageDao
    {
        public override PagedList<AbstractPage> Pages_All(PageParam pageParam, string search)
        {
            PagedList<AbstractPage> Page = new PagedList<AbstractPage>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Pages_All, param, commandType: CommandType.StoredProcedure);
                Page.Values.AddRange(task.Read<Page>());
                Page.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Page;
        }
        public override SuccessResult<AbstractPage> Pages_ById(long Id)
        {
            SuccessResult<AbstractPage> Page = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Pages_ById, param, commandType: CommandType.StoredProcedure);
                Page = task.Read<SuccessResult<AbstractPage>>().SingleOrDefault();
                Page.Item = task.Read<Page>().SingleOrDefault();
            }

            return Page;
        }
        public override SuccessResult<AbstractPage> Pages_Delete(long Id)
        {
            SuccessResult<AbstractPage> Page = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Pages_Delete, param, commandType: CommandType.StoredProcedure);
                Page = task.Read<SuccessResult<AbstractPage>>().SingleOrDefault();
                Page.Item = task.Read<Page>().SingleOrDefault();
            }
            return Page;
        }
        public override SuccessResult<AbstractPage> Pages_Upsert(AbstractPage abstractPage)
        {
            SuccessResult<AbstractPage> Page = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPage.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractPage.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SubPageAction", abstractPage.SubPageAction, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OrderNo", abstractPage.OrderNo, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractPage.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPage.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPage.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Pages_Upsert, param, commandType: CommandType.StoredProcedure);
                Page = task.Read<SuccessResult<AbstractPage>>().SingleOrDefault();
                Page.Item = task.Read<Page>().SingleOrDefault();
            }

            return Page;
        }

        public override SuccessResult<AbstractPage> Pages_DisplayOrderNo(long Id, bool IsUp)
        {
            SuccessResult<AbstractPage> Pages = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsUp", IsUp, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Pages_DisplayOrderNo, param, commandType: CommandType.StoredProcedure);
                Pages = task.Read<SuccessResult<AbstractPage>>().SingleOrDefault();
                Pages.Item = task.Read<Page>().SingleOrDefault();
            }

            return Pages;
        }
    }
}

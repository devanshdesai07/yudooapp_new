﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class AdminDao : AbstractAdminDao
    {
        public override PagedList<AbstractAdmin> Admin_All(PageParam pageParam, string search)
        {
            PagedList<AbstractAdmin> Admin = new PagedList<AbstractAdmin>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_All, param, commandType: CommandType.StoredProcedure);
                Admin.Values.AddRange(task.Read<Admin>());
                Admin.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Admin;
        }
        public override SuccessResult<AbstractAdmin> Admin_ById(long Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ById, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }
        public override SuccessResult<AbstractAdmin> Admin_Delete(long Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Delete, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_ActInAct(long Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ActInAct, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
        }

        public override bool Admin_Logout(long Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Admin_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;

        }
        public override SuccessResult<AbstractAdmin> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OldPassword", OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_Upsert(AbstractAdmin abstractAdmin)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAdmin.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AdminTypesId", abstractAdmin.AdminTypesId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractAdmin.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractAdmin.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractAdmin.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractAdmin.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractAdmin.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", abstractAdmin.MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DOB", abstractAdmin.DOB, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractAdmin.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractAdmin.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserImages", abstractAdmin.UserImages, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Upsert, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_Login(string Email, string Password)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Login, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

        public override SuccessResult<AbstractAdmin> ForGotPassword(string Email, string OTP)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OTP", OTP, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminForgotPSWOTP, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_ByEmail(string Email)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ByEmail, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_IsCallAdmin(long Id, bool CallStatus)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallStatus", CallStatus, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_IsCallAdmin, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
        }

        public override SuccessResult<AbstractAdmin> ResetPassword(string email, string Password, string ConfirmPassword)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Email", email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ResetPassword, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class GoodRatingSuggestionsDao : AbstractGoodRatingSuggestionsDao
    {
        public override PagedList<AbstractGoodRatingSuggestions> GoodRatingSuggestions_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractGoodRatingSuggestions> GoodRatingSuggestions = new PagedList<AbstractGoodRatingSuggestions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GoodRatingSuggestions_All, param, commandType: CommandType.StoredProcedure);
                GoodRatingSuggestions.Values.AddRange(task.Read<GoodRatingSuggestions>());
                GoodRatingSuggestions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return GoodRatingSuggestions;
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_ById(long Id)
        {
            SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GoodRatingSuggestions_ById, param, commandType: CommandType.StoredProcedure);
                GoodRatingSuggestions = task.Read<SuccessResult<AbstractGoodRatingSuggestions>>().SingleOrDefault();
                GoodRatingSuggestions.Item = task.Read<GoodRatingSuggestions>().SingleOrDefault();
            }

            return GoodRatingSuggestions;
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Delete(long Id)
        {
            SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GoodRatingSuggestions_Delete, param, commandType: CommandType.StoredProcedure);
                GoodRatingSuggestions = task.Read<SuccessResult<AbstractGoodRatingSuggestions>>().SingleOrDefault();
                GoodRatingSuggestions.Item = task.Read<GoodRatingSuggestions>().SingleOrDefault();
            }
            return GoodRatingSuggestions;
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_ActInActive(long Id)
        {
            SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GoodRatingSuggestions_ActInActive, param, commandType: CommandType.StoredProcedure);
                GoodRatingSuggestions = task.Read<SuccessResult<AbstractGoodRatingSuggestions>>().SingleOrDefault();
                GoodRatingSuggestions.Item = task.Read<GoodRatingSuggestions>().SingleOrDefault();
            }
            return GoodRatingSuggestions;
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Upsert(AbstractGoodRatingSuggestions abstractGoodRatingSuggestions)
        {
            SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractGoodRatingSuggestions.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Text", abstractGoodRatingSuggestions.Text, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractGoodRatingSuggestions.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractGoodRatingSuggestions.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractGoodRatingSuggestions.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GoodRatingSuggestions_Upsert, param, commandType: CommandType.StoredProcedure);
                GoodRatingSuggestions = task.Read<SuccessResult<AbstractGoodRatingSuggestions>>().SingleOrDefault();
                GoodRatingSuggestions.Item = task.Read<GoodRatingSuggestions>().SingleOrDefault();
            }

            return GoodRatingSuggestions;
        }
        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Update_DisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsUp", IsUp, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GoodRatingSuggestions_Update_DisplayOrder, param, commandType: CommandType.StoredProcedure);
                GoodRatingSuggestions = task.Read<SuccessResult<AbstractGoodRatingSuggestions>>().SingleOrDefault();
                GoodRatingSuggestions.Item = task.Read<GoodRatingSuggestions>().SingleOrDefault();
            }

            return GoodRatingSuggestions;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class OrderDao : AbstractOrderDao
    {


        public override SuccessResult<AbstractOrder> Order_Upsert(AbstractOrder abstractOrder)
        {
            SuccessResult<AbstractOrder> Order = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrder.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", abstractOrder.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderRatings", abstractOrder.OrderRatings, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Amount", abstractOrder.Amount, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractOrder.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentType", abstractOrder.PaymentType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Ratings", abstractOrder.Ratings, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RatingsBy", abstractOrder.RatingsBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RatingsCreatedDate", abstractOrder.RatingsCreatedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@RatingsUpdatedDate", abstractOrder.RatingsUpdatedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrder.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrder.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterOrderStatusId", abstractOrder.MasterOrderStatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentId", abstractOrder.PaymentId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerId", abstractOrder.ShopOwnerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityId", abstractOrder.CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Order_Upsert, param, commandType: CommandType.StoredProcedure);
                Order = task.Read<SuccessResult<AbstractOrder>>().SingleOrDefault();
                Order.Item = task.Read<Order>().SingleOrDefault();
            }

            return Order;
        }


        public override PagedList<AbstractOrder> Order_All(PageParam pageParam, string search)
        {
            PagedList<AbstractOrder> Order = new PagedList<AbstractOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Order_All, param, commandType: CommandType.StoredProcedure);
                Order.Values.AddRange(task.Read<Order>());
                Order.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Order;
        }


        public override SuccessResult<AbstractOrder> Order_ById(long Id)
        {
            SuccessResult<AbstractOrder> Order = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Order_ById, param, commandType: CommandType.StoredProcedure);
                Order = task.Read<SuccessResult<AbstractOrder>>().SingleOrDefault();
                Order.Item = task.Read<Order>().SingleOrDefault();
            }

            return Order;
        }

        public override SuccessResult<AbstractOrderDetails> OrderDetail_ById(long OrderId)
        {
            SuccessResult<AbstractOrderDetails> Order = null;
            var param = new DynamicParameters();

            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderDetail_ById, param, commandType: CommandType.StoredProcedure);
                Order = task.Read<SuccessResult<AbstractOrderDetails>>().SingleOrDefault();
                Order.Item = task.Read<OrderDetails>().SingleOrDefault();
            }
            return Order;
        }

        public override PagedList<AbstractOrder> AdminOrderDetails_All(PageParam pageParam, string search, long Id ,long MasterOrderStatusId,string PaymentStatus,long ShopId,string ShopName,long CityId,string CustomerMobileNumber,string CustomerName,long Amount)
        {
            PagedList<AbstractOrder> Order = new PagedList<AbstractOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterOrderStatusId", MasterOrderStatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentStatus", PaymentStatus, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerMobileNumber", CustomerMobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerName", CustomerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Amount", Amount, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminOrderDetails_All, param, commandType: CommandType.StoredProcedure);
                Order.Values.AddRange(task.Read<Order>());
                Order.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Order;
        }
        public override SuccessResult<AbstractOrder> User_OrderDetails(long Id)
        {
            SuccessResult<AbstractOrder> Order = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.User_OrderDetails, param, commandType: CommandType.StoredProcedure);
                Order = task.Read<SuccessResult<AbstractOrder>>().SingleOrDefault();
                Order.Item = task.Read<Order>().SingleOrDefault();
            }
            return Order;
        }

        public override SuccessResult<AbstractOrder> OrderDetails_Status(long Id,long StatusId)
        {
            SuccessResult<AbstractOrder> Order = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OrderDetails_Status, param, commandType: CommandType.StoredProcedure);
                Order = task.Read<SuccessResult<AbstractOrder>>().SingleOrDefault();
                Order.Item = task.Read<Order>().SingleOrDefault();
            }
            return Order;
        }

    }
}

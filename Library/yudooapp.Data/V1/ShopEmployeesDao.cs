﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
  public  class ShopEmployeesDao : AbstractShopEmployeesDao
    {
        public override PagedList<AbstractShopEmployees> ShopEmployees_All(PageParam pageParam, string Search, long ShopId)
        {
            PagedList<AbstractShopEmployees> ShopEmployees = new PagedList<AbstractShopEmployees>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_All, param, commandType: CommandType.StoredProcedure);
                ShopEmployees.Values.AddRange(task.Read<ShopEmployees>());
                ShopEmployees.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopEmployees;
        }

        public override PagedList<AbstractShopEmployees> ShopEmployees_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractShopEmployees> ShopEmployees = new PagedList<AbstractShopEmployees>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_ByShopId, param, commandType: CommandType.StoredProcedure);
                ShopEmployees.Values.AddRange(task.Read<ShopEmployees>());
                ShopEmployees.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopEmployees;
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_ById(long Id)
        {
            SuccessResult<AbstractShopEmployees> ShopEmployees = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_ById, param, commandType: CommandType.StoredProcedure);
                ShopEmployees = task.Read<SuccessResult<AbstractShopEmployees>>().SingleOrDefault();
                ShopEmployees.Item = task.Read<ShopEmployees>().SingleOrDefault();
            }

            return ShopEmployees;
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_Upsert(AbstractShopEmployees abstractShopEmployees)
        {
            SuccessResult<AbstractShopEmployees> ShopEmployees = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopEmployees.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopEmployees.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmpFirstName", abstractShopEmployees.EmpFirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmpLastName", abstractShopEmployees.EmpLastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmpEmail", abstractShopEmployees.EmpEmail, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmpPhoneNumber", abstractShopEmployees.EmpPhoneNumber, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@EmpType", abstractShopEmployees.EmpType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsReceiveCalls_Access", abstractShopEmployees.IsReceiveCalls_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsConductCalls_Access", abstractShopEmployees.IsConductCalls_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsOrdersPage_Access", abstractShopEmployees.IsOrdersPage_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsPerformancePage_Access", abstractShopEmployees.IsPerformancePage_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsAdd_ChangeProductGallery_Access", abstractShopEmployees.IsAdd_ChangeProductGallery_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsSDTVU_Access", abstractShopEmployees.IsSDTVU_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsShippingPage_Access", abstractShopEmployees.IsShippingPage_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractShopEmployees.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopEmployees.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubUserId", abstractShopEmployees.SubUserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopEmployees = task.Read<SuccessResult<AbstractShopEmployees>>().SingleOrDefault();
                ShopEmployees.Item = task.Read<ShopEmployees>().SingleOrDefault();
            }

            return ShopEmployees;
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_ActInact(long Id)
        {
            SuccessResult<AbstractShopEmployees> ShopEmployees = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_ActInact, param, commandType: CommandType.StoredProcedure);
                ShopEmployees = task.Read<SuccessResult<AbstractShopEmployees>>().SingleOrDefault();
                ShopEmployees.Item = task.Read<ShopEmployees>().SingleOrDefault();
            }

            return ShopEmployees;
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_IsOnline(long Id)
        {
            SuccessResult<AbstractShopEmployees> ShopEmployees = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_ActInact, param, commandType: CommandType.StoredProcedure);
                ShopEmployees = task.Read<SuccessResult<AbstractShopEmployees>>().SingleOrDefault();
                ShopEmployees.Item = task.Read<ShopEmployees>().SingleOrDefault();
            }

            return ShopEmployees;
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_Delete(long Id)
        {
            SuccessResult<AbstractShopEmployees> ShopEmployees = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployees_Delete, param, commandType: CommandType.StoredProcedure);
                ShopEmployees = task.Read<SuccessResult<AbstractShopEmployees>>().SingleOrDefault();
                ShopEmployees.Item = task.Read<ShopEmployees>().SingleOrDefault();
            }

            return ShopEmployees;
        }
    }
}

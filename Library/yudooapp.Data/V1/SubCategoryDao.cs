﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class SubCategoryDao : AbstractSubCategoryDao
    {

        public override PagedList<AbstractSubCategory> SubCategory_All(PageParam pageParam, string search)
        {
            PagedList<AbstractSubCategory> SubCategory = new PagedList<AbstractSubCategory>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubCategory_All, param, commandType: CommandType.StoredProcedure);
                SubCategory.Values.AddRange(task.Read<SubCategory>());
                SubCategory.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return SubCategory;
        }

        public override PagedList<AbstractSubCategory> SubCategory_LookUpCategoryId(PageParam pageParam, string search,long CategoryId)
        {
            PagedList<AbstractSubCategory> SubCategory = new PagedList<AbstractSubCategory>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CategoryId", CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubCategory_LookUpCategoryId, param, commandType: CommandType.StoredProcedure);
                SubCategory.Values.AddRange(task.Read<SubCategory>());
                SubCategory.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return SubCategory;
        }


        public override SuccessResult<AbstractSubCategory> SubCategory_ById(long Id)
        {
            SuccessResult<AbstractSubCategory> SubCategory = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubCategory_ById, param, commandType: CommandType.StoredProcedure);
                SubCategory = task.Read<SuccessResult<AbstractSubCategory>>().SingleOrDefault();
                SubCategory.Item = task.Read<SubCategory>().SingleOrDefault();
            }

            return SubCategory;
        }


    }
}

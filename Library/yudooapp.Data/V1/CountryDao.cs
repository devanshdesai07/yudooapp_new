﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CountryDao : AbstractCountryDao
    {

        public override PagedList<AbstractCountry> Country_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCountry> Country = new PagedList<AbstractCountry>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Country_All, param, commandType: CommandType.StoredProcedure);
                Country.Values.AddRange(task.Read<Country>());
                Country.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Country;
        }

        public override SuccessResult<AbstractCountry> Country_ById(long Id)
        {
            SuccessResult<AbstractCountry> Country = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Country_ById, param, commandType: CommandType.StoredProcedure);
                Country = task.Read<SuccessResult<AbstractCountry>>().SingleOrDefault();
                Country.Item = task.Read<Country>().SingleOrDefault();
            }

            return Country;
        }
        public override SuccessResult<AbstractCountry> Country_Delete(long Id)
        {
            SuccessResult<AbstractCountry> Country = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Country_Delete, param, commandType: CommandType.StoredProcedure);
                Country = task.Read<SuccessResult<AbstractCountry>>().SingleOrDefault();
                Country.Item = task.Read<Country>().SingleOrDefault();
            }
            return Country;
        }

    }
}

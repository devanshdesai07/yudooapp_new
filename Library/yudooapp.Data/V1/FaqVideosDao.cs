﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;

namespace yudooapp.Data.V1
{
    public class FaqVideosDao : AbstractFaqVideosDao
    {
        public override SuccessResult<AbstractFaqVideos> FaqVideo_ActInAct(long Id)
        {
            SuccessResult<AbstractFaqVideos> returnValues = new SuccessResult<AbstractFaqVideos>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FaqVideos_ActInAct, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFaqVideos>>().SingleOrDefault();
                    returnValues.Item = task.Read<FaqVideos>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override PagedList<AbstractFaqVideos> FaqVideo_All(PageParam pageParam, string search)
        {
            PagedList<AbstractFaqVideos> returnValues = new PagedList<AbstractFaqVideos>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FaqVideos_All, param, commandType: CommandType.StoredProcedure);
                    returnValues.Values.AddRange(task.Read<FaqVideos>());
                    returnValues.TotalRecords = task.Read<long>().SingleOrDefault();
                }
            }
            catch (Exception)
            {
                returnValues.TotalRecords = 0;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractFaqVideos> FaqVideo_ById(long Id)
        {
            SuccessResult<AbstractFaqVideos> returnValues = new SuccessResult<AbstractFaqVideos>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FaqVideos_ById, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFaqVideos>>().SingleOrDefault();
                    returnValues.Item = task.Read<FaqVideos>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractFaqVideos> FaqVideo_Delete(long Id)
        {
            SuccessResult<AbstractFaqVideos> returnValues = new SuccessResult<AbstractFaqVideos>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FaqVideos_Delete, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFaqVideos>>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }

        public override SuccessResult<AbstractFaqVideos> FaqVideo_Upsert(AbstractFaqVideos abstractFaqVideos)
        {
            SuccessResult<AbstractFaqVideos> returnValues = new SuccessResult<AbstractFaqVideos>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@Id", abstractFaqVideos.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@VideoHeading", abstractFaqVideos.VideoHeading, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@IsActive", abstractFaqVideos.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@VideoUrl", abstractFaqVideos.VideoUrl, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@ThumbnailImage", abstractFaqVideos.ThumbnailImage, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Description", abstractFaqVideos.Description, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractFaqVideos.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractFaqVideos.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.FaqVideos_Upsert, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractFaqVideos>>().SingleOrDefault();
                    returnValues.Item = task.Read<FaqVideos>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }
    }
}

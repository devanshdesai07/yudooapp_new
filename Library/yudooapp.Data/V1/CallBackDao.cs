﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CallBackDao : AbstractCallBackDao
    {


        public override SuccessResult<AbstractCallBack> CallBack_Upsert(AbstractCallBack abstractCallBack)
        {
            SuccessResult<AbstractCallBack> CallBack = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCallBack.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractCallBack.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SellerId", abstractCallBack.SellerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractCallBack.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallId", abstractCallBack.CallId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Status", abstractCallBack.Status, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCallBack.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCallBack.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallBack_Upsert, param, commandType: CommandType.StoredProcedure);
                CallBack = task.Read<SuccessResult<AbstractCallBack>>().SingleOrDefault();
                CallBack.Item = task.Read<CallBack>().SingleOrDefault();
            }

            return CallBack;
        }
        public override SuccessResult<AbstractUserShopCallLogs> CallBack_CallManage(UserShopCallLogs abstractCallBack)
        {
            SuccessResult<AbstractUserShopCallLogs> CallBack = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCallBack.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractCallBack.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractCallBack.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallId", abstractCallBack.VideoCallLink, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractCallBack.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CallStatus", abstractCallBack.CallStatus, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@WhoInitiated", abstractCallBack.WhoInitiated, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AdminId", abstractCallBack.AdminId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCallBack.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCallBack.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallBack_CallManage, param, commandType: CommandType.StoredProcedure);
                CallBack = task.Read<SuccessResult<AbstractUserShopCallLogs>>().SingleOrDefault();
                CallBack.Item = task.Read<UserShopCallLogs>().SingleOrDefault();
            }

            return CallBack;
        }
        public override SuccessResult<AbstractUserShopCallLogs> RazorPay(UserShopCallLogs abstractCallBack)
        {
            SuccessResult<AbstractUserShopCallLogs> CallBack = null;
            var param = new DynamicParameters();

            //param.Add("@Id", abstractCallBack.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@ShopId", abstractCallBack.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@UserId", abstractCallBack.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@CallId", abstractCallBack.VideoCallLink, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@Type", abstractCallBack.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@CallStatus", abstractCallBack.CallStatus, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@WhoInitiated", abstractCallBack.WhoInitiated, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@CreatedBy", abstractCallBack.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@UpdatedBy", abstractCallBack.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallBack_RazorPay, param, commandType: CommandType.StoredProcedure);
                CallBack = task.Read<SuccessResult<AbstractUserShopCallLogs>>().SingleOrDefault();
                CallBack.Item = task.Read<UserShopCallLogs>().SingleOrDefault();
            }

            return CallBack;
        }


        public override PagedList<AbstractCallBack> CallBack_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCallBack> CallBack = new PagedList<AbstractCallBack>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallBack_All, param, commandType: CommandType.StoredProcedure);
                CallBack.Values.AddRange(task.Read<CallBack>());
                CallBack.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CallBack;
        }


        public override SuccessResult<AbstractCallBack> CallBack_ById(long Id)
        {
            SuccessResult<AbstractCallBack> CallBack = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CallBack_ById, param, commandType: CommandType.StoredProcedure);
                CallBack = task.Read<SuccessResult<AbstractCallBack>>().SingleOrDefault();
                CallBack.Item = task.Read<CallBack>().SingleOrDefault();
            }

            return CallBack;
        }

    }
}

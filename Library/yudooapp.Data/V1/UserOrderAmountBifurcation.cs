﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class UserOrderAmountBifurcationDao : AbstractUserOrderAmountBifurcationDao
    {
        public override SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_Upsert(AbstractUserOrderAmountBifurcation abstractUserOrderAmountBifurcation)
        {
            SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserOrderAmountBifurcation.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractUserOrderAmountBifurcation.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);            
            param.Add("@Amount", abstractUserOrderAmountBifurcation.Amount, dbType: DbType.Decimal, direction: ParameterDirection.Input);            
            param.Add("@CreatedBy", abstractUserOrderAmountBifurcation.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUserOrderAmountBifurcation.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderAmountBifurcation_Upsert, param, commandType: CommandType.StoredProcedure);
                UserOrderAmountBifurcation = task.Read<SuccessResult<AbstractUserOrderAmountBifurcation>>().SingleOrDefault();
                UserOrderAmountBifurcation.Item = task.Read<UserOrderAmountBifurcation>().SingleOrDefault();
            }

            return UserOrderAmountBifurcation;
        }
        public override SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_UpdateNote(AbstractUserOrderAmountBifurcation abstractUserOrderAmountBifurcation)
        {
            SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation = null;
            var param = new DynamicParameters();

            param.Add("@Ids", abstractUserOrderAmountBifurcation.Ids, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentDate", abstractUserOrderAmountBifurcation.PaymentDateStr, dbType: DbType.DateTime, direction: ParameterDirection.Input);            
            param.Add("@Note", abstractUserOrderAmountBifurcation.Note, dbType: DbType.String, direction: ParameterDirection.Input);            
            


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderAmountBifurcation_UpdateNote, param, commandType: CommandType.StoredProcedure);
                UserOrderAmountBifurcation = task.Read<SuccessResult<AbstractUserOrderAmountBifurcation>>().SingleOrDefault();
                UserOrderAmountBifurcation.Item = task.Read<UserOrderAmountBifurcation>().SingleOrDefault();
            }

            return UserOrderAmountBifurcation;
        }

        public override PagedList<AbstractUserOrder> UserOrderAmountBifurcation_All(PageParam pageParam, string search, long OrderId, string FromDate, string ToDate, string Type, int ispaid)
        {
            PagedList<AbstractUserOrder> UserOrderAmountBifurcation = new PagedList<AbstractUserOrder>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FromDate", FromDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ToDate", ToDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ispaid", ispaid, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderAmountBifurcation_All, param, commandType: CommandType.StoredProcedure);
                UserOrderAmountBifurcation.Values.AddRange(task.Read<UserOrder>());
                UserOrderAmountBifurcation.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrderAmountBifurcation;
        }
        public override PagedList<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_ByOrderId(PageParam pageParam, long OrderId)
        {
            PagedList<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation = new PagedList<AbstractUserOrderAmountBifurcation>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderAmountBifurcation_ByOrderId, param, commandType: CommandType.StoredProcedure);
                UserOrderAmountBifurcation.Values.AddRange(task.Read<UserOrderAmountBifurcation>());
                UserOrderAmountBifurcation.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserOrderAmountBifurcation;
        }

        public override SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_UpdateIsPaid(long Id, int IsPaid)
        {
            SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsPaid", IsPaid, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserOrderAmountBifurcation_UpdateIsPaid, param, commandType: CommandType.StoredProcedure);
                UserOrderAmountBifurcation = task.Read<SuccessResult<AbstractUserOrderAmountBifurcation>>().SingleOrDefault();
                UserOrderAmountBifurcation.Item = task.Read<UserOrderAmountBifurcation>().SingleOrDefault();
            }

            return UserOrderAmountBifurcation;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserPermissionsDao : AbstractUserPermissionsDao
    {


        public override SuccessResult<AbstractUserPermissions> UserPermissions_Upsert(List<AbstractUserPermissions> AbstractUserPermissions)
        {
            var param_del = new DynamicParameters();
            param_del.Add("@AdminTypeId", AbstractUserPermissions[0].AdminTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPermissions_Delete, param_del, commandType: CommandType.StoredProcedure);                
            }

            SuccessResult<AbstractUserPermissions> UserPermissions = null;
            var param = new DynamicParameters();
            int x = 1;
            foreach (var item in AbstractUserPermissions)
            {                
                param.Add("@Id", item.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@AdminTypeId", item.AdminTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@PageId", item.PageId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Status", item.Status, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", item.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", item.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CreatedDate", item.CreatedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@UpdatedDate", item.UpdatedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.UserPermissions_Upsert, param, commandType: CommandType.StoredProcedure);
                    //UserPermissions = task.Read<SuccessResult<AbstractUserPermissions>>().SingleOrDefault();
                    //UserPermissions.Item = task.Read<UserPermissions>().SingleOrDefault();
                }
                x++;
            }
            return UserPermissions;
        }
        public override PagedList<AbstractUserPermissions> UserPermissions_All(PageParam pageParam, string search)
        {
            PagedList<AbstractUserPermissions> UserPermissions = new PagedList<AbstractUserPermissions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPermissions_All, param, commandType: CommandType.StoredProcedure);
                UserPermissions.Values.AddRange(task.Read<UserPermissions>());
                UserPermissions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserPermissions;
        }
        public override PagedList<AbstractUserPermissions> UserPermissions_ByAdminId(PageParam pageParam, string search, int AdminTypeId)
        {
            PagedList<AbstractUserPermissions> UserPermissions = new PagedList<AbstractUserPermissions>();
            //PagedList<AbstractPage> pagedList = new PagedList<AbstractPage>();
            //using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            //{
            //    var task = con.QueryMultiple(SQLConfig.Pages_All, null, commandType: CommandType.StoredProcedure);
            //    pagedList.Values.AddRange(task.Read<AbstractPage>());
            //    pagedList.TotalRecords = task.Read<long>().SingleOrDefault();
            //}

            var param = new DynamicParameters();
            param.Add("@AdminTypeId", AdminTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPermissions_ByAdminId, param, commandType: CommandType.StoredProcedure);
                UserPermissions.Values.AddRange(task.Read<UserPermissions>());
                UserPermissions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            //if (UserPermissions == null)
            //{
            //    //foreach (var item in pagedList.Values)
            //    //{
            //    //    UserPermissions.Values.Add(new UserPermissions()
            //    //    {
            //    //        PageId = item.Id,
            //    //        Page
            //    //    });
            //    //}
            //}
            //else
            //{

            //}



            return UserPermissions;
        }
        public override PagedList<AbstractUserPermissions> UserPermissions_All_Active(PageParam pageParam, string search)
        {
            PagedList<AbstractUserPermissions> UserPermissions = new PagedList<AbstractUserPermissions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPermissions_All_Active, param, commandType: CommandType.StoredProcedure);
                UserPermissions.Values.AddRange(task.Read<UserPermissions>());
                UserPermissions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return UserPermissions;
        }
        public override SuccessResult<AbstractUserPermissions> UserPermissions_ById(long Id)
        {
            SuccessResult<AbstractUserPermissions> UserPermissions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPermissions_ById, param, commandType: CommandType.StoredProcedure);
                UserPermissions = task.Read<SuccessResult<AbstractUserPermissions>>().SingleOrDefault();
                UserPermissions.Item = task.Read<UserPermissions>().SingleOrDefault();
            }

            return UserPermissions;
        }
        public override SuccessResult<AbstractUserPermissions> UserPermissions_ActInAct(long Id, bool IsActive)
        {
            SuccessResult<AbstractUserPermissions> UserPermissions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserPermissions_ActInAct, param, commandType: CommandType.StoredProcedure);
                UserPermissions = task.Read<SuccessResult<AbstractUserPermissions>>().SingleOrDefault();
                UserPermissions.Item = task.Read<UserPermissions>().SingleOrDefault();
            }

            return UserPermissions;
        }


    }
}

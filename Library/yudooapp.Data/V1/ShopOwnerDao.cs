﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;
using Newtonsoft.Json;

namespace yudooapp.Data.V1
{
    public class ShopOwnerDao : AbstractShopOwnerDao
    {


        public override SuccessResult<AbstractShopOwner> ShopOwner_Upsert(AbstractShopOwner abstractShopOwner)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopOwner.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@MasterDepartmentShopId", abstractShopOwner.MasterDepartmentShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ASIN", abstractShopOwner.ASIN, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractShopOwner.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopName", abstractShopOwner.ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerName", abstractShopOwner.ShopOwnerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CategoryId", abstractShopOwner.CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractShopOwner.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractShopOwner.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@YudooCommission", abstractShopOwner.YudooCommission, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@LandMark", abstractShopOwner.LandMark, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractShopOwner.PinCode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopNameGujarati", abstractShopOwner.ShopNameGujarati, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DescriptionGujarati", abstractShopOwner.DescriptionGujarati, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopNameHindi", abstractShopOwner.ShopNameHindi, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DescriptionHindi", abstractShopOwner.DescriptionHindi, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GstNumber", abstractShopOwner.GstNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", abstractShopOwner.MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PersonalPanNumber", abstractShopOwner.PersonalPanNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyPanNumber", abstractShopOwner.CompanyPanNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyRegisterAddress", abstractShopOwner.CompanyRegisterAddress, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Packer", abstractShopOwner.Packer, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Rank", abstractShopOwner.Rank, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@About", abstractShopOwner.About, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Tags", abstractShopOwner.Tags, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SubcategoryArray", abstractShopOwner.SubcategoryArray, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GpsLatitude", abstractShopOwner.GpsLatitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GpsLongitude", abstractShopOwner.GpsLongitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsGSTRegisteredShop", abstractShopOwner.IsGSTRegisteredShop, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Description", abstractShopOwner.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsOwner", abstractShopOwner.IsOwner, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractShopOwner.ParentId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractShopOwner.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceType", abstractShopOwner.DeviceType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UDID", abstractShopOwner.UDID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IMEI", abstractShopOwner.IMEI, dbType: DbType.String, direction: ParameterDirection.Input);

            //param.Add("@Ratings", abstractShopOwner.Ratings, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@RatingsBy", abstractShopOwner.RatingsBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@OTP", abstractShopOwner.OTP, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_UpdateCommision(long Id,decimal YudooCommission)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@YudooCommission", YudooCommission, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_UpdateCommision, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_ShopEmployeeActInAct(bool IsActive, long ShopOwnerId, long UserId)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@ShopOwnerId", ShopOwnerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ShopEmployeeActInAct, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopAudit> ShopAudit_Upsert(AbstractShopAudit abstractShopAudit)
        {
            SuccessResult<AbstractShopAudit> ShopAudit = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopAudit.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopAudit.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PrimaryCategoryId", abstractShopAudit.PrimaryCategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubCategoryId", abstractShopAudit.SubCategoryId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductTag", abstractShopAudit.ProductTag, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@FullLengthVideo", abstractShopAudit.FullLengthVideo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GifVideo", abstractShopAudit.GifVideo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageUrl", abstractShopAudit.ImageUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractShopAudit.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Specialities", abstractShopAudit.Specialities, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CommissionPercentage", abstractShopAudit.CommissionPercentage, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", abstractShopAudit.ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopNameGujarati", abstractShopAudit.ShopNameGujarati, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopNameHindi", abstractShopAudit.ShopNameHindi, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DescriotionHindi", abstractShopAudit.DescriotionHindi, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DescriptionGujarati", abstractShopAudit.DescriptionGujarati, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerName", abstractShopAudit.ShopOwnerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@GSTNo", abstractShopAudit.GSTNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractShopAudit.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PersonalPAN", abstractShopAudit.PersonalPAN, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyPAN", abstractShopAudit.CompanyPAN, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopAudit.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsSave", abstractShopAudit.IsSave, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractShopAudit.StateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityId", abstractShopAudit.CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractShopAudit.PinCode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractShopAudit.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FlatNumber", abstractShopAudit.FlatNumber, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopAudit_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopAudit = task.Read<SuccessResult<AbstractShopAudit>>().SingleOrDefault();
                ShopAudit.Item = task.Read<ShopAudit>().SingleOrDefault();
            }

            return ShopAudit;
        }

        public override SuccessResult<AbstractShopAudit> ShopAudit_DeleteMedia(string Url, int Type, long ShopId)
        {
            SuccessResult<AbstractShopAudit> ShopAudit = null;
            var param = new DynamicParameters();

            param.Add("@Url", Url, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopAudit_DeleteMedia, param, commandType: CommandType.StoredProcedure);
                ShopAudit = task.Read<SuccessResult<AbstractShopAudit>>().SingleOrDefault();
                ShopAudit.Item = task.Read<ShopAudit>().SingleOrDefault();
            }

            return ShopAudit;
        }


        public override PagedList<AbstractShopOwner> ShopOwner_All(PageParam pageParam, string search, long CityId, long SpecialityId, long UserId, long CuratedListId, long ShopId)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();
            IList<ShopTimgings> objtime = new List<ShopTimgings>();
            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SpecialityId", SpecialityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CuratedListId", CuratedListId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_All, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();

            }
            return ShopOwner;
        }
        public override PagedList<AbstractShopOwner> ShopEmployee_ByParentId(PageParam pageParam, long ParentId)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();
            IList<ShopTimgings> objtime = new List<ShopTimgings>();
            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopEmployee_ByParentId, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();

            }
            return ShopOwner;
        }
        public override PagedList<AbstractShopOwner> AdminShopOwnerDetails_All(PageParam pageParam, string search ,long CityId, long CategoryId, string AllCategory, long ShopRatingsId, long ShopStatus, int? DraftStatus)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AllCategory", AllCategory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopRatingsId", ShopRatingsId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopStatus", ShopStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DraftStatus", DraftStatus, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminShopOwnerDetails_All, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> ShopOwner_ById(long Id, long UserId)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ById, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_IsRating(bool IsRating)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@IsRating", IsRating, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_IsRating, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopAudit> ShopAudit_ShopId(long ShopId)
        {
            SuccessResult<AbstractShopAudit> ShopAudit = null;
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopAudit_ShopId, param, commandType: CommandType.StoredProcedure);
                ShopAudit = task.Read<SuccessResult<AbstractShopAudit>>().SingleOrDefault();
                ShopAudit.Item = task.Read<ShopAudit>().SingleOrDefault();
            }

            return ShopAudit;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_TagsByShopId(long ShopId)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_TagsByShopId, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }
        
        public override SuccessResult<AbstractShopOwner> AddShopOwnerPickUpId(long ShopOwnerId, string PickUpId)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", ShopOwnerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PickUpId", PickUpId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddShopOwnerPickUpId, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> ShopOwner_ActInAct(long Id)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ActInAct, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_Delete(long Id, long DeletedBy)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_Delete, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> CheckSeller_Exists(string ShopOwnerName, string ShopName, string MobileNumber)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@ShopOwnerName", ShopOwnerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopName", ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CheckSeller_Exists, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> CheckSeller_Exists_ById(long Id)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CheckSeller_Exists_ById, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        
        public override SuccessResult<AbstractShopOwner> CheckSeller_Exists_ByMobileNumberAndById(long Id, string Mobilenumber, string DeviceToken)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Mobilenumber", Mobilenumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CheckSeller_Exists_ByMobileNumberAndById, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_TagsUpdate(long Id, string Tags)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Tags", Tags, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_TagsUpdate, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_VerifyOtp(string MobileNumber, long Otp)
        {
            SuccessResult<AbstractShopOwner> Users = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", Convert.ToInt64(MobileNumber), dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Otp", Otp, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_VerifyOtp, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                Users.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_SendOTP(string MobileNumber, long Otp)
        {
            SuccessResult<AbstractShopOwner> Users = null;
            var param = new DynamicParameters();

            param.Add("@MobileNumber", MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Otp", Otp, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_SendOTP, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                Users.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return Users;
        }

        



        public override PagedList<AbstractShopOwner> Shop_Orders(PageParam pageParam, string search, long ShopId, string ShopMobileNumber)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopMobileNumber", ShopMobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shop_Orders, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> Shop_Overview(int shopId = 0, string OwnerPhoneNo = "", string shopName = "", string FilterBy ="")
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@shopId", shopId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OwnerPhoneNo", OwnerPhoneNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@shopName", shopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FilterBy", FilterBy, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shop_Overview, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_UpdateStatus(long Id, long ShopStatus)
        {
            SuccessResult<AbstractShopOwner> returnValues = new SuccessResult<AbstractShopOwner>();
            try
            {
                var param = new DynamicParameters();

                param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ShopStatus", ShopStatus, dbType: DbType.Int64, direction: ParameterDirection.Input);
                

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.ShopOwner_UpdateStatus, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                    returnValues.Item = task.Read<ShopOwner>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }
        public override bool ShopOwner_Logout(long Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.ShopOwner_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;

        }

        public override bool MasterOnline_Status(bool Status)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Status", Status, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.MasterOnline_Status, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }
            return result;

        }

        public override PagedList<AbstractShopOwner> ShopOwner_Specialities_All(PageParam pageParam, long CityId, long SubCategoryId)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CityId", CityId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubCategoryId", SubCategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_Specialities_All, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopOwner;
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_OnlineOffline(long Id, bool IsOnline)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsOnline", IsOnline, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_OnlineOffline, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_IsApprovedByAdmin(long Id,long Status)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Status", Status, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ApprovedByAdmin, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwnerRights> ShopOwnerRights_Upsert(AbstractShopOwnerRights abstractShopOwnerRights)
        {
            SuccessResult<AbstractShopOwnerRights> ShopOwnerRights = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopOwnerRights.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerId", abstractShopOwnerRights.ShopOwnerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractShopOwnerRights.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopOwnerRights.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsAdd_ChangeProductGallery_Access", abstractShopOwnerRights.IsAdd_ChangeProductGallery_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsConductCalls_Access", abstractShopOwnerRights.IsConductCalls_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsOrdersPage_Access", abstractShopOwnerRights.IsOrdersPage_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsPerformancePage_Access", abstractShopOwnerRights.IsPerformancePage_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsReceiveCalls_Access", abstractShopOwnerRights.IsReceiveCalls_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsSDTVU_Access", abstractShopOwnerRights.IsSDTVU_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsShippingPage_Access", abstractShopOwnerRights.IsShippingPage_Access, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractShopOwnerRights.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractShopOwnerRights.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShopOwnerRights.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwnerRights_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopOwnerRights = task.Read<SuccessResult<AbstractShopOwnerRights>>().SingleOrDefault();
                ShopOwnerRights.Item = task.Read<ShopOwnerRights>().SingleOrDefault();
            }

            return ShopOwnerRights;
        }

        public override SuccessResult<AbstractShopOwnerRights> ShopOwnerRights_BySOU(long ShopId, long UserId)
        {
            SuccessResult<AbstractShopOwnerRights> ShopOwnerRights = null;
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwnerRights_BySOU, param, commandType: CommandType.StoredProcedure);
                ShopOwnerRights = task.Read<SuccessResult<AbstractShopOwnerRights>>().SingleOrDefault();
                ShopOwnerRights.Item = task.Read<ShopOwnerRights>().SingleOrDefault();
            }

            return ShopOwnerRights;
        }

        public override SuccessResult<AbstractShopOwner> AddressKey_Update(long ShopId, string AddressKey,long AddressCount,long Type)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressKey", AddressKey, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressCount", AddressCount, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AddressKey_Update, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override PagedList<AbstractShopOwner> ShopOwner_ByCallLog(PageParam pageParam, string search, long UserId)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();
            IList<ShopTimgings> objtime = new List<ShopTimgings>();
            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ByCallLog, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();

            }
            return ShopOwner;
        }
    }
}

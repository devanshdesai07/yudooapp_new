﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopCategoriesDao : AbstractShopCategoriesDao
    {


        public override SuccessResult<AbstractShopCategories> ShopCategories_Upsert(AbstractShopCategories abstractShopCategories)
        {
            SuccessResult<AbstractShopCategories> ShopCategories = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopCategories.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractShopCategories.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", abstractShopCategories.CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SubCategoryId", abstractShopCategories.SubCategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopCategories = task.Read<SuccessResult<AbstractShopCategories>>().SingleOrDefault();
                ShopCategories.Item = task.Read<ShopCategories>().SingleOrDefault();
            }

            return ShopCategories;
        }


        public override PagedList<AbstractShopCategories> ShopCategories_All(PageParam pageParam, string search)
        {
            PagedList<AbstractShopCategories> ShopCategories = new PagedList<AbstractShopCategories>();

            var param = new DynamicParameters();
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_All, param, commandType: CommandType.StoredProcedure);
                ShopCategories.Values.AddRange(task.Read<ShopCategories>());
                ShopCategories.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopCategories;
        }


        public override SuccessResult<AbstractShopCategories> ShopCategories_ById(long Id)
        {
            SuccessResult<AbstractShopCategories> ShopCategories = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_ById, param, commandType: CommandType.StoredProcedure);
                ShopCategories = task.Read<SuccessResult<AbstractShopCategories>>().SingleOrDefault();
                ShopCategories.Item = task.Read<ShopCategories>().SingleOrDefault();
            }

            return ShopCategories;
        }


        public override PagedList<AbstractShopCategories> ShopCategories_ByShopId(PageParam pageParam, long ShopId)
        {
            PagedList<AbstractShopCategories> ShopCategories = new PagedList<AbstractShopCategories>();
            var param = new DynamicParameters();

            param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_ByShopId, param, commandType: CommandType.StoredProcedure);
                ShopCategories.Values.AddRange(task.Read<ShopCategories>());
                ShopCategories.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return ShopCategories;
        }


        public override PagedList<AbstractShopCategories> ShopCategories_ByCategoryId(PageParam pageParam, long CategoryId)
        {
            PagedList<AbstractShopCategories> ShopCategories = new PagedList<AbstractShopCategories>();
            var param = new DynamicParameters();

            param.Add("@CategoryId", CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_ByCategoryId, param, commandType: CommandType.StoredProcedure);
                ShopCategories.Values.AddRange(task.Read<ShopCategories>());
                ShopCategories.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return ShopCategories;
        }

        public override PagedList<AbstractShopCategories> ShopCategories_BySubCategoryId(PageParam pageParam, long SubCategoryId)
        {
            PagedList<AbstractShopCategories> ShopCategories = new PagedList<AbstractShopCategories>();
            var param = new DynamicParameters();

            param.Add("@SubCategoryId", SubCategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_BySubCategoryId, param, commandType: CommandType.StoredProcedure);
                ShopCategories.Values.AddRange(task.Read<ShopCategories>());
                ShopCategories.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return ShopCategories;
        }

        public override SuccessResult<AbstractShopCategories> ShopCategories_Delete(long Id)
        {
            SuccessResult<AbstractShopCategories> ShopCategories = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopCategories_Delete, param, commandType: CommandType.StoredProcedure);
                ShopCategories = task.Read<SuccessResult<AbstractShopCategories>>().SingleOrDefault();
                ShopCategories.Item = task.Read<ShopCategories>().SingleOrDefault();
            }

            return ShopCategories;
        }
    }
}

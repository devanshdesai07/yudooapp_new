﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class CustomerCallsDao : AbstractCustomerCallsDao
    {


        public override SuccessResult<AbstractCustomerCalls> CustomerCalls_Upsert(AbstractCustomerCalls abstractCustomerCalls)
        {
            SuccessResult<AbstractCustomerCalls> CustomerCalls = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCustomerCalls.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractCustomerCalls.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UploadedId", abstractCustomerCalls.UploadedId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CallStartedDatime", abstractCustomerCalls.CallStartedDatime, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CallEndDatetime", abstractCustomerCalls.CallEndDatetime, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@ShopId", abstractCustomerCalls.ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopMobileNo", abstractCustomerCalls.ShopMobileNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCustomerCalls.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCustomerCalls.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerCalls_Upsert, param, commandType: CommandType.StoredProcedure);
                CustomerCalls = task.Read<SuccessResult<AbstractCustomerCalls>>().SingleOrDefault();
                CustomerCalls.Item = task.Read<CustomerCalls>().SingleOrDefault();
            }

            return CustomerCalls;
        }


        public override PagedList<AbstractCustomerCalls> CustomerCalls_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCustomerCalls> CustomerCalls = new PagedList<AbstractCustomerCalls>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerCalls_All, param, commandType: CommandType.StoredProcedure);
                CustomerCalls.Values.AddRange(task.Read<CustomerCalls>());
                CustomerCalls.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CustomerCalls;
        }


        public override SuccessResult<AbstractCustomerCalls> CustomerCalls_ById(long Id)
        {
            SuccessResult<AbstractCustomerCalls> CustomerCalls = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerCalls_ById, param, commandType: CommandType.StoredProcedure);
                CustomerCalls = task.Read<SuccessResult<AbstractCustomerCalls>>().SingleOrDefault();
                CustomerCalls.Item = task.Read<CustomerCalls>().SingleOrDefault();
            }

            return CustomerCalls;
        }

    }
}

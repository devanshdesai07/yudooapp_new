﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class UserFeedBackDao : AbstractUserFeedBackDao
    {


        public override SuccessResult<AbstractUserFeedBack> UserFeedBack_Upsert(AbstractUserFeedBack abstractUserFeedBack)
        {
            SuccessResult<AbstractUserFeedBack> UserFeedBack = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUserFeedBack.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FeedBackSuggestions", abstractUserFeedBack.FeedBackSuggestions, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@VideoCallID", abstractUserFeedBack.VideoCallID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUserFeedBack.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ExperienceId", abstractUserFeedBack.ExperienceId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UserFeedBack_Upsert, param, commandType: CommandType.StoredProcedure);
                UserFeedBack = task.Read<SuccessResult<AbstractUserFeedBack>>().SingleOrDefault();
                UserFeedBack.Item = task.Read<UserFeedBack>().SingleOrDefault();
            }

            return UserFeedBack;
        }


      
        

    }
}

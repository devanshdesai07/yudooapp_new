﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
   public class OfferStatusDao : AbstractOfferStatusDao
    {
        public override SuccessResult<AbstractOfferStatus> OfferStatus_Upsert(AbstractOfferStatus abstractOfferStatus)
        {
            SuccessResult<AbstractOfferStatus> OfferStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOfferStatus.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OfferStatusName", abstractOfferStatus.OfferStatusName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOfferStatus.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOfferStatus.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OfferStatus_Upsert, param, commandType: CommandType.StoredProcedure);
                OfferStatus = task.Read<SuccessResult<AbstractOfferStatus>>().SingleOrDefault();
                OfferStatus.Item = task.Read<OfferStatus>().SingleOrDefault();
            }

            return OfferStatus;
        }

        public override PagedList<AbstractOfferStatus> OfferStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractOfferStatus> OfferStatus = new PagedList<AbstractOfferStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OfferStatus_All, param, commandType: CommandType.StoredProcedure);
                OfferStatus.Values.AddRange(task.Read<OfferStatus>());
                OfferStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return OfferStatus;
        }


        public override SuccessResult<AbstractOfferStatus> OfferStatus_ById(long Id)
        {
            SuccessResult<AbstractOfferStatus> OfferStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OfferStatus_ById, param, commandType: CommandType.StoredProcedure);
                OfferStatus = task.Read<SuccessResult<AbstractOfferStatus>>().SingleOrDefault();
                OfferStatus.Item = task.Read<OfferStatus>().SingleOrDefault();
            }

            return OfferStatus;
        }

        public override SuccessResult<AbstractOfferStatus> OfferStatus_Delete(long Id)
        {
            SuccessResult<AbstractOfferStatus> OfferStatus = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.OfferStatus_Delete, param, commandType: CommandType.StoredProcedure);
                OfferStatus = task.Read<SuccessResult<AbstractOfferStatus>>().SingleOrDefault();
                OfferStatus.Item = task.Read<OfferStatus>().SingleOrDefault();
            }

            return OfferStatus;
        }
    }
}

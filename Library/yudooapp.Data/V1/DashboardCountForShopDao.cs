﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;

namespace yudooapp.Data.V1
{
    public class DashboardCountForShopDao : AbstractDashboardCountForShopDao
    {
        public override SuccessResult<AbstractDashboardCountForShop> DashboardCount_ByShopId(long ShopId, string Period)
        {
            SuccessResult<AbstractDashboardCountForShop> returnValues = new SuccessResult<AbstractDashboardCountForShop>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@ShopId", ShopId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Period", Period, dbType: DbType.String, direction: ParameterDirection.Input);
                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple(SQLConfig.DashboardCount_ByShopId, param, commandType: CommandType.StoredProcedure);
                    returnValues = task.Read<SuccessResult<AbstractDashboardCountForShop>>().SingleOrDefault();
                    returnValues.Item = task.Read<DashboardCountForShop>().SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            return returnValues;
        }
    }
}

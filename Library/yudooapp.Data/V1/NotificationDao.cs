﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class NotificationDao : AbstractNotificationDao
    {
        public override SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractnotification)
        {
            SuccessResult<AbstractNotification> Notification = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractnotification.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", abstractnotification.Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NotificationTypeId", abstractnotification.NotificationTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractnotification.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Text", abstractnotification.Text, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractnotification.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ModifiedBy", abstractnotification.ModifiedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_Upsert, param, commandType: CommandType.StoredProcedure);
                Notification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                Notification.Item = task.Read<Notification>().SingleOrDefault();
            }

            return Notification;
        }
        public override SuccessResult<AbstractNotification> SendGeneralNotifications(AbstractNotification abstractnotification)
        {
            SuccessResult<AbstractNotification> Notification = null;
            var param = new DynamicParameters();
            param.Add("@Description", abstractnotification.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractnotification.Type, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SendGeneralNotifications, param, commandType: CommandType.StoredProcedure);
                Notification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                Notification.Item = task.Read<Notification>().SingleOrDefault();
            }
            
            return Notification;
        }
        public override PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search)
        {
            PagedList<AbstractNotification> Notification = new PagedList<AbstractNotification>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_All, param, commandType: CommandType.StoredProcedure);
                Notification.Values.AddRange(task.Read<Notification>());
                Notification.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Notification;
        }

        public override SuccessResult<AbstractNotification> Notification_ById(long Id)
        {
            SuccessResult<AbstractNotification> Notification = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_ById, param, commandType: CommandType.StoredProcedure);
                Notification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                Notification.Item = task.Read<Notification>().SingleOrDefault();
            }

            return Notification;
        }

        public override PagedList<AbstractNotification> Notification_ByUserId(PageParam pageParam, long UserId)
        {
            PagedList<AbstractNotification> Notification = new PagedList<AbstractNotification>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_ByUserId, param, commandType: CommandType.StoredProcedure);
                Notification.Values.AddRange(task.Read<Notification>());
                Notification.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Notification;
        }

        public override SuccessResult<AbstractNotification> Notification_Delete(long Id)
        {
            SuccessResult<AbstractNotification> Notification = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_Delete, param, commandType: CommandType.StoredProcedure);
                Notification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                Notification.Item = task.Read<Notification>().SingleOrDefault();
            }

            return Notification;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ProductsDao : AbstractProductsDao
    {
        public override SuccessResult<AbstractProducts> Products_ActInAct(long Id)
        {
            SuccessResult<AbstractProducts> Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Products_ActInAct, param, commandType: CommandType.StoredProcedure);
                Products = task.Read<SuccessResult<AbstractProducts>>().SingleOrDefault();
                Products.Item = task.Read<Products>().SingleOrDefault();
            }

            return Products;
        }
        public override PagedList<AbstractProducts> Products_All(PageParam pageParam, string search , AbstractProducts abstractProducts)
        {
            PagedList<AbstractProducts> Products = new PagedList<AbstractProducts>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Products_All, param, commandType: CommandType.StoredProcedure);
                Products.Values.AddRange(task.Read<Products>());
                Products.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Products;
        }
        public override SuccessResult<AbstractProducts> Products_ById(long Id)
        {
            SuccessResult<AbstractProducts> Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Products_ById, param, commandType: CommandType.StoredProcedure);
                Products = task.Read<SuccessResult<AbstractProducts>>().SingleOrDefault();
                Products.Item = task.Read<Products>().SingleOrDefault();
            }

            return Products;
        }
        public override SuccessResult<AbstractProducts> Products_Delete(long Id)
        {
            SuccessResult<AbstractProducts> Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Products_Delete, param, commandType: CommandType.StoredProcedure);
                Products = task.Read<SuccessResult<AbstractProducts>>().SingleOrDefault();
                Products.Item = task.Read<Products>().SingleOrDefault();
            }
            return Products;
        }

        
        public override SuccessResult<AbstractProducts> Products_Upsert(AbstractProducts abstractProducts)
        {
            SuccessResult<AbstractProducts> Products = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProducts.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductName", abstractProducts.ProductName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductTypeId", abstractProducts.ProductTypeId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Ratings", abstractProducts.Ratings, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RatingsBy", abstractProducts.RatingsBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductExperience", abstractProducts.ProductExperience, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductPrice", abstractProducts.ProductPrice, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductDescription", abstractProducts.ProductDescription, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceExperience", abstractProducts.ServiceExperience, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductWeight", abstractProducts.ProductWeight, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProducts.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Products_Upsert, param, commandType: CommandType.StoredProcedure);
                Products = task.Read<SuccessResult<AbstractProducts>>().SingleOrDefault();
                Products.Item = task.Read<Products>().SingleOrDefault();
            }

            return Products;
        }
    }
}

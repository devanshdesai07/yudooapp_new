﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
  public class HomePageShopOrderFactorsDao : AbstractHomePageShopOrderFactorsDao
    {
        public override PagedList<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors = new PagedList<AbstractHomePageShopOrderFactors>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HomePageShopOrderFactors_All, param, commandType: CommandType.StoredProcedure);
                HomePageShopOrderFactors.Values.AddRange(task.Read<HomePageShopOrderFactors>());
                HomePageShopOrderFactors.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return HomePageShopOrderFactors;
        }

        public override SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_ById(int Id)
        {
            SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HomePageShopOrderFactors_ById, param, commandType: CommandType.StoredProcedure);
                HomePageShopOrderFactors = task.Read<SuccessResult<AbstractHomePageShopOrderFactors>>().SingleOrDefault();
                HomePageShopOrderFactors.Item = task.Read<HomePageShopOrderFactors>().SingleOrDefault();
            }

            return HomePageShopOrderFactors;
        }

        public override SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_Upsert(AbstractHomePageShopOrderFactors abstractHomePageShopOrderFactors)
        {
            SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractHomePageShopOrderFactors.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Offer_Discount_Announcement", abstractHomePageShopOrderFactors.Offer_Discount_Announcement, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ShopBusy_ShopAvailable_ShopOffline", abstractHomePageShopOrderFactors.ShopBusy_ShopAvailable_ShopOffline, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ShopCallNo_ReceiveRatio", abstractHomePageShopOrderFactors.ShopCallNo_ReceiveRatio, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ShopVideoCalltoOrderRatiointhecategory", abstractHomePageShopOrderFactors.ShopVideoCalltoOrderRatiointhecategory, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ShopViewtoVideoCallRatiointhecategory", abstractHomePageShopOrderFactors.ShopViewtoVideoCallRatiointhecategory, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ShopRating_OrderRating", abstractHomePageShopOrderFactors.ShopRating_OrderRating, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@ReturnRatio", abstractHomePageShopOrderFactors.ReturnRatio, dbType: DbType.Decimal, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.HomePageShopOrderFactors_Upsert, param, commandType: CommandType.StoredProcedure);
                HomePageShopOrderFactors = task.Read<SuccessResult<AbstractHomePageShopOrderFactors>>().SingleOrDefault();
                HomePageShopOrderFactors.Item = task.Read<HomePageShopOrderFactors>().SingleOrDefault();
            }

            return HomePageShopOrderFactors;
        }
    }
}

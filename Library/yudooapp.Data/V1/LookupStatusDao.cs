﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class LookupStatusDao : AbstractLookupStatusDao
    {
        public override PagedList<AbstractLookupStatus> LookupStatus_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupStatus> LookupStatus = new PagedList<AbstractLookupStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupStatus_All, param, commandType: CommandType.StoredProcedure);
                LookupStatus.Values.AddRange(task.Read<LookupStatus>());
                LookupStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupStatus;
        }
       
    }
}


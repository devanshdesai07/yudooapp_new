﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShipmentDao : AbstractShipmentDao
    {


        public override SuccessResult<AbstractShipment> Shipment_Upsert(AbstractShipment abstractShipment)
        {
            SuccessResult<AbstractShipment> Shipment = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShipment.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractShipment.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ParcelId", abstractShipment.ParcelId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShipmentStatus", abstractShipment.ShipmentStatus, dbType: DbType.Boolean, direction: ParameterDirection.Input); 
            param.Add("@CreatedBy", abstractShipment.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShipment.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shipment_Upsert, param, commandType: CommandType.StoredProcedure);
                Shipment = task.Read<SuccessResult<AbstractShipment>>().SingleOrDefault();
                Shipment.Item = task.Read<Shipment>().SingleOrDefault();
            }

            return Shipment;
        }


        public override SuccessResult<AbstractShipment> Shipment_StatusChange(long Id)
        {
            SuccessResult<AbstractShipment> Shipment = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shipment_StatusChange, param, commandType: CommandType.StoredProcedure);
                Shipment = task.Read<SuccessResult<AbstractShipment>>().SingleOrDefault();
                Shipment.Item = task.Read<Shipment>().SingleOrDefault();
            }

            return Shipment;
        }

        public override PagedList<AbstractShipment> Shipment_ByOrderId(long OrderId, PageParam pageParam)
        {
            PagedList<AbstractShipment> Shipment = new PagedList<AbstractShipment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shipment_ByOrderId, param, commandType: CommandType.StoredProcedure);
                Shipment.Values.AddRange(task.Read<Shipment>());
                Shipment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Shipment;
        }


        public override SuccessResult<AbstractShipment> Shipment_ById(long Id)
        {
            SuccessResult<AbstractShipment> Shipment = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shipment_ById, param, commandType: CommandType.StoredProcedure);
                Shipment = task.Read<SuccessResult<AbstractShipment>>().SingleOrDefault();
                Shipment.Item = task.Read<Shipment>().SingleOrDefault();
            }

            return Shipment;
        }


        public override PagedList<AbstractShipment> Shipment_ByParcelId(long ParcelId, PageParam pageParam)
        {
            PagedList<AbstractShipment> Shipment = new PagedList<AbstractShipment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ParcelId", ParcelId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shipment_ByParcelId, param, commandType: CommandType.StoredProcedure);
                Shipment.Values.AddRange(task.Read<Shipment>());
                Shipment.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Shipment;
        }
    }
}

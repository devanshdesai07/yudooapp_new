﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using Dapper;

namespace yudooapp.Data.V1
{
    public class ShopOwnerDevicesDao : AbstractShopOwnerDevicesDao
    {

        public override PagedList<AbstractShopOwnerDevices> ShopOwnerDevices_ByShopId(PageParam pageParam, string search, long ShopOwnerId)
        {
            PagedList<AbstractShopOwnerDevices> ShopOwnerDevices = new PagedList<AbstractShopOwnerDevices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerId", ShopOwnerId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwnerDevices_ByShopId, param, commandType: CommandType.StoredProcedure);
                ShopOwnerDevices.Values.AddRange(task.Read<ShopOwnerDevices>());
                ShopOwnerDevices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopOwnerDevices;
        }


    }
}


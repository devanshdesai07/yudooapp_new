﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace yudooapp.Data
{
    using Autofac;
    using yudooapp.Data.Contract;

    //using yudooapp.Data.Contract;


    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {   
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            builder.RegisterType<V1.HelpSupportDao>().As<AbstractHelpSupportDao>().InstancePerDependency();
            builder.RegisterType<V1.UserOrderAmountBifurcationDao>().As<AbstractUserOrderAmountBifurcationDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopOwnerDao>().As<AbstractShopOwnerDao>().InstancePerDependency();
            builder.RegisterType<V1.AddressDao>().As<AbstractAddressDao>().InstancePerDependency();
            builder.RegisterType<V1.BankMasterDao>().As<AbstractBankMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.OrderDao>().As<AbstractOrderDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomerDao>().As<AbstractCustomerDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomerCallsDao>().As<AbstractCustomerCallsDao>().InstancePerDependency();
            builder.RegisterType<V1.CategoryDao>().As<AbstractCategoryDao>().InstancePerDependency();
            builder.RegisterType<V1.SubCategoryDao>().As<AbstractSubCategoryDao>().InstancePerDependency();
            builder.RegisterType<V1.CityDao>().As<AbstractCityDao>().InstancePerDependency();
            builder.RegisterType<V1.CountryDao>().As<AbstractCountryDao>().InstancePerDependency();
            builder.RegisterType<V1.StateDao>().As<AbstractStateDao>().InstancePerDependency();
            builder.RegisterType<V1.UsersDao>().As<AbstractUsersDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductsDao>().As<AbstractProductsDao>().InstancePerDependency();
            builder.RegisterType<V1.PaymentDao>().As<AbstractPaymentDao>().InstancePerDependency();
            builder.RegisterType<V1.ProductTypeDao>().As<AbstractProductTypeDao>().InstancePerDependency();
            builder.RegisterType<V1.PermissionDao>().As<AbstractPermissionDao>().InstancePerDependency();
            builder.RegisterType<V1.ReviewDao>().As<AbstractReviewDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterCallStatusDao>().As<AbstractMasterCallStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterOrderStatusDao>().As<AbstractMasterOrderStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.UserSearchDao>().As<AbstractUserSearchDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterDepartmentShopDao>().As<AbstractMasterDepartmentShopDao>().InstancePerDependency();
            builder.RegisterType<V1.UserWishlistDao>().As<AbstractUserWishlistDao>().InstancePerDependency();
            builder.RegisterType<V1.UserCartDao>().As<AbstractUserCartDao>().InstancePerDependency();
            builder.RegisterType<V1.BannersDao>().As<AbstractBannersDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopTimgingsDao>().As<AbstractShopTimgingsDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopRatingsDao>().As<AbstractShopRatingsDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopImagesAndVideosDao>().As<AbstractShopImagesAndVideosDao>().InstancePerDependency();
            builder.RegisterType<V1.UserOrderDao>().As<AbstractUserOrderDao>().InstancePerDependency();
            builder.RegisterType<V1.UserOrderItemsDao>().As<AbstractUserOrderItemsDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopCategoriesDao>().As<AbstractShopCategoriesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserShopCallLogsDao>().As<AbstractUserShopCallLogsDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopCommissionDao>().As<AbstractShopCommissionDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopEmployeesDao>().As<AbstractShopEmployeesDao>().InstancePerDependency();
            builder.RegisterType<V1.FAQsDao>().As<AbstractFAQsDao>().InstancePerDependency();
            builder.RegisterType<V1.FaqVideosDao>().As<AbstractFaqVideosDao>().InstancePerDependency();
            builder.RegisterType<V1.DashboardCountForShopDao>().As<AbstractDashboardCountForShopDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminShopDao>().As<AbstractAdminShopDao>().InstancePerDependency();
            builder.RegisterType<V1.UserCartMasterDao>().As<AbstractUserCartMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.UserCallBackPreferenceTimgingsDao>().As<AbstractUserCallBackPreferenceTimgingsDao>().InstancePerDependency();
            builder.RegisterType<V1.OffersDao>().As<AbstractOffersDao>().InstancePerDependency();
            builder.RegisterType<V1.ParcelDao>().As<AbstractParcelDao>().InstancePerDependency();
            builder.RegisterType<V1.ShipmentDao>().As<AbstractShipmentDao>().InstancePerDependency();
            builder.RegisterType<V1.PageDao>().As<AbstractPageDao>().InstancePerDependency();
            builder.RegisterType<V1.CuratedListDao>().As<AbstractCuratedListDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminTypeDao>().As<AbstractAdmintypeDao>().InstancePerDependency();
            builder.RegisterType<V1.UserPermissionsDao>().As<AbstractUserPermissionsDao>().InstancePerDependency();
            builder.RegisterType<V1.SpecialityDao>().As<AbstractSpecialityDao>().InstancePerDependency();
            builder.RegisterType<V1.HomePageShopOrderFactorsDao>().As<AbstractHomePageShopOrderFactorsDao>().InstancePerDependency();
            builder.RegisterType<V1.TrendingSearchDao>().As<AbstractTrendingSearchDao>().InstancePerDependency();
            builder.RegisterType<V1.GoodRatingSuggestionsDao>().As<AbstractGoodRatingSuggestionsDao>().InstancePerDependency();
            builder.RegisterType<V1.BadRatingSuggestionsDao>().As<AbstractBadRatingSuggestionsDao>().InstancePerDependency();
            builder.RegisterType<V1.UsersFAQDao>().As<AbstractUsersFAQDao>().InstancePerDependency();
            builder.RegisterType<V1.DownloadDao>().As<AbstractDownloadDao>().InstancePerDependency();
            builder.RegisterType<V1.OfferStatusDao>().As<AbstractOfferStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.TagsDao>().As<AbstractTagsDao>().InstancePerDependency();
            builder.RegisterType<V1.NotificationTypesDao>().As<AbstractNotificationTypesDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopKeeperMasterDao>().As<AbstractShopKeeperMasterDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupStatusDao>().As<AbstractLookupStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopOwnerDevicesDao>().As<AbstractShopOwnerDevicesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserDevicesDao>().As<AbstractUserDevicesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsDao>().As<AbstractUserNotificationsDao>().InstancePerDependency();
            builder.RegisterType<V1.NotificationDao>().As<AbstractNotificationDao>().InstancePerDependency();
            builder.RegisterType<V1.CallBackDao>().As<AbstractCallBackDao>().InstancePerDependency();
            builder.RegisterType<V1.OrderParcelDao>().As<AbstractOrderParcelDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopCallingNotificationDao>().As<AbstractShopCallingNotificationDao>().InstancePerDependency();
            builder.RegisterType<V1.ShopShortingValuesDao>().As<AbstractShopShortingValuesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserFeedBackDao>().As<AbstractUserFeedBackDao>().InstancePerDependency();

            base.Load(builder);
        }
    }
}

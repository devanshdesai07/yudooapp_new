﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace yudooapp.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        public const string DashboardCount_ByShopId = "DashboardCount_ByShopId";
        public const string Caller_Info = "Caller_Info";
        public const string UserCallBackPreferenceTimgings_Upsert = "UserCallBackPreferenceTimgings_Upsert";
        public const string UserCallBackPreferenceTimgings_ById = "UserCallBackPreferenceTimgings_ById";
        public const string UserCallBackPreferenceTimgings_Delete = "UserCallBackPreferenceTimgings_Delete";
        public const string UserCallBackPreferenceTimgings_All = "UserCallBackPreferenceTimgings_All";
        public const string UserCallBackPreferenceTimgings_Available = "UserCallBackPreferenceTimgings_Available";
        public const string UserCallBackPreferenceTimgings_ByUserId = "UserCallBackPreferenceTimgings_ByUserId";

        public const string ShopAudit_DeleteMedia = "ShopAudit_DeleteMedia";
        public const string ShopShortingValues_Upsert = "ShopShortingValues_Upsert";
        public const string ShopShortingValues_All = "ShopShortingValues_All";

        #region Notification
        public const string Notification_Upsert = "Notification_Upsert";
        public const string Notification_All = "Notification_All";
        public const string Notification_ById = "Notification_ById";
        public const string Notification_ByUserId = "Notification_ByUserId";
        public const string Notification_Delete = "Notification_Delete";
        public const string SendGeneralNotifications = "SendGeneralNotifications";
        #endregion

        #region UserOrderAmountBifurcation
        public const string UserOrderAmountBifurcation_Upsert = "UserOrderAmountBifurcation_Upsert";
        public const string UserOrderAmountBifurcation_UpdateNote = "UserOrderAmountBifurcation_UpdateNote";
        public const string UserOrderAmountBifurcation_All = "UserOrderAmountBifurcation_All";
        public const string UserOrderAmountBifurcation_UpdateIsPaid = "UserOrderAmountBifurcation_UpdateIsPaid";
        public const string UserOrderAmountBifurcation_ByOrderId = "UserOrderAmountBifurcation_ByOrderId";
        #endregion

        #region ShopOwner
        public const string ShopOwner_Upsert = "ShopOwner_Upsert";
        public const string ShopOwner_All = "ShopOwner_All";
        public const string ShopOwner_ById = "ShopOwner_ById";
        public const string ShopOwner_ActInAct = "ShopOwner_ActInAct";
        public const string ShopOwner_Delete = "ShopOwner_Delete";
        public const string CheckSeller_Exists = "CheckSeller_Exists";
        public const string CheckSeller_Exists_ById = "CheckSeller_Exists_ById";
        public const string CheckSeller_Exists_ByMobileNumberAndById = "CheckSeller_Exists_ByMobileNumberAndById";
        public const string ShopOwner_TagsByShopId = "ShopOwner_TagsByShopId";
        public const string AddShopOwnerPickUpId = "AddShopOwnerPickUpId";
        public const string ShopOwner_TagsUpdate = "ShopOwner_TagsUpdate";
        public const string ShopOwner_SendOTP = "ShopOwner_SendOTP";
        public const string ShopOwner_VerifyOtp = "ShopOwner_VerifyOtp";
        public const string AdminShopOwnerDetails_All = "ShopOwnerDetails_All";
        public const string ShopAudit_Upsert = "ShopAudit_Upsert";
        public const string ShopAudit_ShopId = "ShopAudit_ShopId";
        public const string Shop_Orders = "Shop_Orders";
        public const string Shop_Overview = "Shop_Overview";
        public const string ShopOwner_UpdateStatus = "ShopOwner_UpdateStatus";
        public const string ShopOwner_Logout = "ShopOwner_Logout";
        public const string ShopOwner_Specialities_All = "Specialities_All";
        public const string ShopOwner_OnlineOffline = "ShopOwner_OnlineOffline";
        public const string ShopOwner_ApprovedByAdmin = "ShopOwner_ApprovedByAdmin";
        public const string ShopOwner_UpdateCommision = "ShopOwner_UpdateCommision";
        public const string ShopOwnerRights_Upsert = "ShopOwnerRights_Upsert";
        public const string ShopOwnerRights_BySOU = "ShopOwnerRights_BySOU";
        public const string ShopOwner_ShopEmployeeActInAct = "ShopOwner_ShopEmployeeActInAct";
        public const string ShopEmployee_ByParentId = "ShopEmployee_ByParentId";
        public const string ShopOwner_ByCallLog = "ShopOwner_ByCallLog";
        public const string ShopOwner_IsRating = "ShopOwner_IsRating";
        public const string AddressKey_Update = "AddressKey_Update";
        #endregion

        #region Order
        public const string Order_Upsert = "Order_Upsert";
        public const string Order_All = "Order_All";
        public const string Order_ById = "Order_ById";
        public const string AdminOrderDetails_All = "OrderDetails_All";
        public const string OrderDetail_ById = "OrderDetail_ById";
        public const string OfferStatus_ChangeStatus = "OfferStatus_ChangeStatus";
        #endregion

        #region Address
        public const string Address_Upsert = "Address_Upsert";
        public const string Address_All = "Address_All";
        public const string Address_ById = "Address_ById";
        public const string Address_ByUserId = "Address_ByUserId";
        public const string Address_ActInAct = "Address_ActInAct";
        public const string Address_Delete = "Address_Delete";

        #endregion

        #region ShopCommission
        public const string ShopCommission_Upsert = "ShopCommission_Upsert";
        public const string ShopCommission_All = "ShopCommission_All";
        public const string ShopCommission_ById = "ShopCommission_ById";
        public const string ShopCommission_ByShopOwnerId = "ShopCommission_ByShopOwnerId";
        public const string ShopCommission_ActInAct = "ShopCommission_ActInAct";
        public const string ShopCommission_Delete = "ShopCommission_Delete";

        #endregion

        #region ShopCategories
        public const string ShopCategories_Upsert = "ShopCategories_Upsert";
        public const string ShopCategories_All = "ShopCategories_All";
        public const string ShopCategories_ById = "ShopCategories_ById";
        public const string ShopCategories_ByShopId = "ShopCategories_ByShopId";
        public const string ShopCategories_ByCategoryId = "ShopCategories_ByCategoryId";
        public const string ShopCategories_BySubCategoryId = "ShopCategories_BySubCategoryId";
        public const string ShopCategories_Delete = "ShopCategories_Delete";
        #endregion

        #region BankMaster
        public const string BankMaster_Upsert = "BankDetails_Upsert";
        public const string BankMaster_ById = "BankDetails_ById";
        public const string BankMaster_ByShopId = "BankDetails_ByShopId";
        public const string BankMaster_ActInAct = "BankDetails_ActInact";

        #endregion

        #region Customer
        public const string Customer_Upsert = "Customer_Upsert";
        public const string Customer_All = "Customer_All";
        public const string Customer_ById = "Customer_ById";
        public const string Customer_ActInact = "Customer_ActInact";
        public const string Customer_Delete = "Customer_Delete";
        public const string Customer_Overview = "Customer_Overview";
        public const string Customer_Orders = "Customer_Orders_All";
        #endregion

        #region HelpSupport
        public const string HelpSupport_Upsert = "HelpSupport_Upsert";
        public const string HelpSupport_All = "HelpSupport_All";
        public const string HelpSupport_Action = "HelpSupport_Action";
        #endregion

        #region CustomerCalls
        public const string CustomerCalls_Upsert = "CustomerCalls_Upsert";
        public const string CustomerCalls_All = "CustomerCalls_All";
        public const string CustomerCalls_ById = "CustomerCalls_ById";
        #endregion

        #region Category
        public const string Category_All = "Category_All";
        public const string Category_ById = "Category_ById";
        public const string Category_Delete = "Category_Delete";
        public const string Category_Upsert = "Category_Upsert";
        public const string Category_Update_DisplayOrder = "Category_Update_DisplayOrder";
        #endregion

        #region UsersFAQ
        public const string UsersFAQ_All = "UsersFAQ_All";
        public const string UsersFAQ_ById = "UsersFAQ_ById";
        public const string UsersFAQ_Delete = "UsersFAQ_Delete";
        public const string UsersFAQ_Upsert = "UsersFAQ_Upsert";
        public const string UsersFAQ_ActInAct = "UsersFAQ_ActInAct";
        #endregion

        #region Tables
        public const string Tables_All = "Tables_All";
        #endregion

        #region TrendingSearch
        public const string TrendingSearch_All = "TrendingSearch_All";
        public const string TrendingSearch_ById = "TrendingSearch_ById";
        public const string TrendingSearch_Delete = "TrendingSearch_Delete";
        public const string TrendingSearch_Upsert = "TrendingSearch_Upsert";
        public const string TrendingSearch_ActInActive = "TrendingSearch_ActInActive";
        public const string TrendingSearch_Update_DisplayOrder = "TrendingSearch_Update_DisplayOrder";
        #endregion

        #region GoodRatingSuggestions
        public const string GoodRatingSuggestions_All = "GoodRatingSuggestions_All";
        public const string GoodRatingSuggestions_ById = "GoodRatingSuggestions_ById";
        public const string GoodRatingSuggestions_Delete = "GoodRatingSuggestions_Delete";
        public const string GoodRatingSuggestions_Upsert = "GoodRatingSuggestions_Upsert";
        public const string GoodRatingSuggestions_ActInActive = "GoodRatingSuggestions_ActInActive";
        public const string GoodRatingSuggestions_Update_DisplayOrder = "GoodRatingSuggestions_Update_DisplayOrder";
        #endregion

        #region BadRatingSuggestions
        public const string BadRatingSuggestions_All = "BadRatingSuggestions_All";
        public const string BadRatingSuggestions_ById = "BadRatingSuggestions_ById";
        public const string BadRatingSuggestions_Delete = "BadRatingSuggestions_Delete";
        public const string BadRatingSuggestions_Upsert = "BadRatingSuggestions_Upsert";
        public const string BadRatingSuggestions_ActInActive = "BadRatingSuggestions_ActInActive";
        public const string BadRatingSuggestions_Update_DisplayOrder = "BadRatingSuggestions_Update_DisplayOrder";
        #endregion

        #region HomePageShopOrderFactors
        public const string HomePageShopOrderFactors_All = "HomePageShopOrderFactors_All";
        public const string HomePageShopOrderFactors_ById = "HomePageShopOrderFactors_ById";
        public const string HomePageShopOrderFactors_Upsert = "HomePageShopOrderFactors_Upsert";
        #endregion

        #region Speciality
        public const string Speciality_All = "Speciality_All";
        public const string Speciality_ById = "Speciality_ById";
        public const string Speciality_Delete = "Speciality_Delete";
        public const string Speciality_Upsert = "Speciality_Upsert";
        public const string Speciality_ActInActive = "Speciality_ActInActive";
        #endregion

        #region SubCategory
        public const string SubCategory_All = "SubCategory_All";
        public const string SubCategory_ById = "SubCategory_ById";
        public const string SubCategory_LookUpCategoryId = "SubCategory_LookUpCategoryId";
        #endregion

        #region Users
        public const string Users_All = "Users_All";
        public const string Users_ById = "Users_ById";
        public const string Users_ActInAct = "Users_ActInAct";
        public const string Users_ChangePassword = "Users_ChangePassword ";
        public const string Users_Upsert = "Users_Upsert";
        public const string Users_Logout = "Users_Logout";
        public const string Users_Delete = "Users_Delete";
        public const string Users_VerifyOtp = "Users_VerifyOtp";
        public const string Users_SendOTP = "Users_SendOTP";
        public const string Users_OnlineOffline = "Users_OnlineOffline";
        public const string CheckUser_Exists = "CheckUser_Exists";
        public const string Users_IsCallBackPreference = "Users_IsCallBackPreference";
        public const string Users_IsCallStatus = "Users_IsCallStatus";
        #endregion

        #region Tags
        public const string Tags_All = "Tags_All";
        public const string Tags_ById = "Tags_ById";
        public const string Tags_Delete = "Tags_Delete";
        public const string Tags_Upsert = "Tags_Upsert";

        #endregion

        #region Admin
        public const string Admin_All = "Admin_All";
        public const string Admin_ById = "Admin_ById";
        public const string Admin_Upsert = "Admin_Upsert";
        public const string Admin_ChangePassword = "Admin_ChangePassword ";
        public const string Admin_Logout = "Admin_Logout";
        public const string Admin_Delete = "Admin_Delete";
        public const string Admin_Login = "Admin_Login";
        public const string Admin_ActInAct = "Admin_ActInAct";
        public const string AdminForgotPSWOTP = "AddForgotPSWOTP";
        public const string Admin_ByEmail = "Admin_ByEmail";
        public const string Admin_ResetPassword = "Admin_ResetPassword";
        public const string Admin_IsCallAdmin = "Admin_IsCallAdmin";
        #endregion

        #region Products
        public const string Products_All = "Products_All";
        public const string Products_ById = "Products_ById";
        public const string Products_ActInAct = "Products_ActInAct";
        public const string Products_Upsert = "Products_Upsert";
        public const string Products_Delete = "Products_Delete";
        #endregion

        #region Payment
        public const string Payment_All = "Payment_All";
        public const string Payment_ById = "Payment_ById";
        public const string Payment_Upsert = "Payment_Upsert";
        public const string CustomerPaymentStatus_All = "CustomerPaymentStatus_All";
        #endregion

        #region City
        public const string City_All = "City_All";
        public const string City_ById = "City_ById";
        public const string City_ByStateId = "City_ByStateId";
        public const string City_Delete = "City_Delete";
        public const string City_Upsert = "City_Upsert";
        #endregion

        #region Country
        public const string Country_All = "Country_All";
        public const string Country_ById = "Country_ById";
        public const string Country_Delete = "Country_Delete";
        #endregion

        #region State
        public const string State_All = "State_All";
        public const string State_ById = "State_ById";
        public const string State_ByCountryId = "State_ByCountryId";
        public const string State_Delete = "State_Delete";

        #endregion

        #region Permission
        public const string Permission_ByCustomerId = "Permission_ByCustomerId";
        public const string Permission_Upsert = "Permission_Upsert";
        #endregion
        #region ShopKeeperMaster
        public const string ShopKeeperMaster_ById = "ShopKeeperMaster_ById";
        public const string ShopKeeperMaster_Upsert = "ShopKeeperMaster_Upsert";
        #endregion
        #region Review
        public const string Review_All = "Review_All";
        public const string Review_ByCustomerId = "Review_ByCustomerId";
        public const string Review_Upsert = "Review_Upsert";
        #endregion

        #region NotificationTypes
        public const string NotificationTypes_All = "NotificationTypes_All";
        public const string NotificationTypes_ById = "NotificationTypes_ById";
        public const string NotificationTypes_Upsert = "NotificationTypes_Upsert";
        public const string NotificationTypes_Delete = "NotificationTypes_Delete";

        #endregion

        #region ProductType
        public const string ProductType_All = "ProductType_All";
        public const string ProductType_ById = "ProductType_ById";
        public const string ProductType_Upsert = "ProductType_Upsert";
        #endregion

        #region MasterCallStatus
        public const string MasterCallStatus_All = "MasterCallStatus_All";
        public const string MasterCallStatus_Upsert = "MasterCallStatus_Upsert";
        #endregion

        #region MasterOrderStatus
        public const string MasterOrderStatus_Upsert = "MasterOrderStatus_Upsert";
        public const string MasterOrderStatus_All = "MasterOrderStatus_All";
        #endregion

        #region UserSearch
        public const string UserSearch_Upsert = "UserSearch_Upsert";
        public const string UserSearch_ById = "UserSearch_ById";
        public const string UserSearch_All = "UserSearch_All";
        public const string UserSearch_ByUserId = "UserSearch_ByUserId";
        public const string UserSearch_ActInact = "UserSearch_ActInact";
        public const string UserSearch_Recentsearch = "UserSearch_Recentsearch";
        public const string UserSearch_TrendingSearches = "UserSearch_TrendingSearches";
        #endregion

        #region MasterDepartmentShop
        public const string MasterDepartmentShop_Upsert = "MasterDepartmentShop_Upsert";
        public const string MasterDepartmentShop_All = "MasterDepartmentShop_All";
        public const string MasterDepartmentShop_ById = "MasterDepartmentShop_ById";
        public const string MasterDepartmentShop_Delete = "MasterDepartmentShop_Delete";
        #endregion

        #region UserWishlist
        public const string UserWishlist_Upsert = "UserWishlist_Upsert";
        public const string UserWishlist_All = "UserWishlist_All";
        public const string UserWishlist_ById = "UserWishlist_ById";
        public const string UserWishlist_Delete = "UserWishlist_Delete";
        public const string UserWishlist_ByUserId = "UserWishlist_ByUserId";
        public const string UserWishlist_ByShopId = "UserWishlist_ByShopId";
        #endregion

        #region UserCart
        public const string UserCart_Upsert = "UserCart_Upsert";
        public const string UserCart_All = "UserCart_All";
        public const string UserCart_ById = "UserCart_ById";
        public const string AddUserCart = "AddUserCart_Upsert";
        public const string UserCart_ByUserId = "UserCart_ByUserId";
        public const string UserCart_ByShopId = "UserCart_ByShopId";
        public const string UserCart_ByVideoCallId = "UserCart_ByVideoCallId";
        public const string UserCart_Delete = "UserCart_Delete";
        public const string UserCartDeleteByVideoCallId = "UserCartDeleteByVideoCallId";
        public const string UserCart_IsVisibleToCustomer = "UserCart_IsVisibleToCustomer";
        public const string UserCart_IsSendToOrder = "IsSendToOrder";
        
        public const string UserCart_IsSendToCustomer = "UserCart_IsSendToCustomer";
        public const string UserCart_ConfirmOrder = "UserCart_ConfirmOrder";
        //public const string UserCart_Details = "UserCart_Details";
        #endregion

        #region Banners
        public const string Banners_Upsert = "Banners_Upsert";
        public const string Banners_ById = "Banners_ById";
        public const string Banners_All = "Banners_All";
        public const string Banners_ActInAct = "Banners_ActInAct";
        public const string Banners_Delete = "Banners_Delete";
        public const string Banners_Update_DisplayOrder = "Banners_Update_DisplayOrder";
        #endregion
        #region FAQs
        public const string FAQs_ActInAct = "FAQs_ActInAct";
        public const string FAQs_All = "FAQs_All";
        public const string FAQs_ById = "FAQs_ById";
        public const string FAQs_Delete = "FAQs_Delete";
        public const string FAQs_Upsert = "FAQs_Upsert";
        #endregion
        #region FaqVideos
        public const string FaqVideos_ActInAct = "FaqVideos_ActInAct";
        public const string FaqVideos_All = "FaqVideos_All";
        public const string FaqVideos_ById = "FaqVideos_ById";
        public const string FaqVideos_Delete = "FaqVideos_Delete";
        public const string FaqVideos_Upsert = "FaqVideos_Upsert";

        #endregion

        #region ShopTimgings
        public const string ShopTimgings_All = "ShopTimgings_All";
        public const string ShopTimgings_ById = "ShopTimgings_ById";
        public const string ShopTimgings_IsClosedIsopen = "ShopTimgings_IsClosedIsopen";
        public const string ShopTimgings_ShopId = "ShopTimgings_ShopId";
        public const string ShopTimgings_Upsert = "ShopTimgings_Upsert";
        public const string ShopTimgings_Delete = "ShopTimgings_Delete";
        #endregion

        #region ShopRatings
        public const string ShopRatings_All = "ShopRatings_All";
        public const string ShopRatings_ById = "ShopRatings_ById";
        public const string ShopRatings_ByShopId = "ShopRatings_ByShopId";
        public const string ShopRatings_ByUserId = "ShopRatings_ByUserId";
        public const string ShopRatings_Upsert = "ShopRatings_Upsert";
        public const string ShopRatings_Delete = "ShopRatings_Delete";
        #endregion

        #region ShopImagesAndVideos
        public const string ShopImagesAndVideos_Upsert = "TestingShopImagesAndVideos_Upsert";
        public const string ShopImagesAndVideos_ById = "ShopImagesAndVideos_ById";
        public const string ShopImagesAndVideos_All = "ShopImagesAndVideos_All";
        public const string ShopImagesAndVideos_ByShopId = "ShopImagesAndVideos_ByShopId ";
        public const string ShopImagesAndVideos_IsCoverImage = "ShopImagesAndVideos_IsCoverImage";
        public const string ShopImagesAndVideos_IsVideo = "ShopImagesAndVideos_IsVideo";
        public const string ShopImagesAndVideos_Delete = "ShopImagesAndVideos_Delete";
        #endregion

        #region UserOrder
        public const string UserOrder_All = "UserOrder_All";
        public const string UserOrder_ById = "UserOrder_ById";
        public const string UserOrder_ByShopId = "UserOrder_ByShopId";
        public const string UserOrder_ByUserId = "UserOrder_ByUserId ";
        public const string UserOrder_Upsert = "UserOrder_Upsert";
        public const string UserOrder_Delete = "UserOrder_Delete";
        public const string OrderDetails_All = "OrderDetails_All";
        public const string UserOrder_StatusChange = "UserOrder_StatusChange";
        public const string UserOrder_StatusChangeByShipmentId = "UserOrder_StatusChangeByShipmentId";
        public const string UserOrder_DeliveryRatingUpdate = "UserOrder_DeliveryRatingUpdate";
        public const string UserOrder_Upsert_paymentLink = "UserOrder_Upsert_paymentLink";
        public const string User_OrderDetails = "User_OrderDetails";
        public const string OrderDetails_Status = "OrderDetails_Status";

        #endregion

        #region UserOrderItems
        public const string UserOrderItems_Upsert = "UserOrderItems_Upsert";
        public const string UserOrderItems_All = "UserOrderItems_All";
        public const string UserOrderItems_ById = "UserOrderItems_ById";
        public const string AddShipmentIdUserOrderItems_ById = "AddShipmentIdUserOrderItems_ById";
        public const string UserOrderItems_Delete = "UserOrderItems_Delete ";
        public const string UserOrderItems_ByOrderId = "UserOrderItems_ByOrderId";
        public const string UserOrderItems_StatusChange = "UserOrderItems_StatusChange";
        public const string UserOrderItems_ByMobileNumber = "UserOrderItems_ByMobileNumber";
        public const string ManifestUrl_ShipmentId = "ManifestUrl_ShipmentId";
        #endregion

        #region UserShopCallLogs
        public const string UserShopCallLogs_Upsert = "UserShopCallLogs_Upsert";
        public const string UserShopCallLogs_All = "UserShopCallLogs_All";
        public const string UserShopCallLogs_ById = "UserShopCallLogs_ById";
        public const string UserShopCallLogs_ByUserId = "UserShopCallLogs_ByUserId";
        public const string UserShopCallLogs_ByShopId = "UserShopCallLogs_ByShopId";
        public const string UserShopCallLogs_Delete = "UserShopCallLogs_Delete";
        public const string UserShopCallLogsDetails_All = "UserShopCallLogsDetails_All";
        public const string UserShopCallLogs_ByOrderId = "UserShopCallLogs_ByOrderId";
        public const string GetDataUserShopCallLogs = "GetDataUserShopCallLogs";
        public const string Request_CallBack_ShopId = "Request_CallBack_ShopId";
        public const string UserShopCallLogs_ReceiveVideoCall = "UserShopCallLogs_ReceiveVideoCall";
        public const string UserShopCallLogs_CallStatus_Update = "UserShopCallLogs_CallStatus_Update";
        public const string UserShopCallLogs_PendingCall = "UserShopCallLogs_PendingCall";
        public const string Request_CallBack_Admin = "Request_CallBack_Admin";
        #endregion

        #region ShopEmployees
        public const string ShopEmployees_Upsert = "ShopEmployees_Upsert";
        public const string ShopEmployees_ById = "ShopEmployees_ById";
        public const string ShopEmployees_All = "ShopEmployees_All";
        public const string ShopEmployees_ByShopId = "ShopEmployees_ByShopId";
        public const string ShopEmployees_ActInact = "ShopEmployees_ActInact";
        public const string ShopEmployees_Delete = "ShopEmployees_Delete";
        public const string ShopEmployees_IsOnline = "ShopEmployees_IsOnline";
        #endregion

        #region AdminShop
        public const string AdminShop_All = "AdminShop_All";
        public const string ShopApproval_All = "ShopApproval_All";
        public const string AdminShop_ById = "AdminShop_ById";
        public const string SendForApproval = "SendForApproval";
        public const string CurrentyOfflineShops_All = "CurrentyOfflineShops_All";
        public const string GetUnansweredcall = "GetUnansweredcall";
        #endregion


        #region UserCartMaster
        public const string UserCartMaster_Upsert = "UserCartMaster_Upsert";
        public const string UserCartMaster_All = "UserCartMaster_All";
        public const string UserCartMaster_ByShopId = "UserCartMaster_ByShopId";
        public const string UserCartMaster_ByUserId = "UserCartMaster_ByUserId";
        public const string UserCartMaster_ById = "UserCartMaster_ById";
        public const string UserCartMaster_ByVideoCallId = "UserCartMaster_ByVideoCallId";
        #endregion

        #region Offers
        public const string Offers_ByShopId = "Offers_ByShopId";
        public const string Offers_Upsert = "Offers_Upsert";
        public const string Offers_All = "Offers_All";
        public const string Offers_Delete = "Offers_Delete";
        public const string Offers_All_Active = "Offers_All_Active";
        public const string Offers_ById = "Offers_ById";
        public const string Offers_ActInAct = "Offers_ActInAct";
        #endregion
        #region UserPermissions
        public const string UserPermissions_Upsert = "UserPermissions_Upsert";
        public const string UserPermissions_Delete = "UserPermissions_Delete";
        public const string UserPermissions_All = "UserPermissions_All";
        public const string UserPermissions_ByAdminId = "UserPermissions_ByAdminId";
        public const string UserPermissions_All_Active = "UserPermissions_All_Active";
        public const string UserPermissions_ById = "UserPermissions_ByAdminId";
        public const string UserPermissions_ActInAct = "UserPermissions_ActInAct";
        #endregion

        #region AdminType
        public const string AdminType_All_Active = "AdminType_All_Active";
        public const string AdminType_ById = "AdminType_ById";
        public const string AdminType_Delete = "AdminType_Delete";
        public const string AdminType_ActInAct = "AdminType_ActInAct";
        public const string AdminType_Upsert = "AdminType_Upsert";
        #endregion

        #region Parcel
        public const string Parcel_Upsert = "Parcel_Upsert";
        public const string Parcel_StatusChange = "Parcel_StatusChange";
        public const string Parcel_Delete = "Parcel_Delete";
        public const string Parcel_ByOrderId = "Parcel_ByOrderId";
        public const string Parcel_ById = "Parcel_ById";
        #endregion
        #region Shipment
        public const string Shipment_Upsert = "Shipment_Upsert";
        public const string Shipment_StatusChange = "Shipment_StatusChange";
        public const string Shipment_ByParcelId = "Shipment_ByParcelId";
        public const string Shipment_ByOrderId = "Shipment_ByOrderId";
        public const string Shipment_ById = "Shipment_ById";
        #endregion
        #region Pages
        public const string Pages_Upsert = "Pages_Upsert";
        public const string Pages_All = "Pages_All";
        public const string Pages_ById = "Pages_ById";
        public const string Pages_Delete = "Pages_Delete";
        public const string Pages_DisplayOrderNo = "Pages_DisplayOrderNo";
        #endregion
        #region CuratedList
        public const string CuratedList_Upsert = "CuratedList_Upsert";
        public const string CuratedList_All = "CuratedList_All";
        public const string CuratedList_ById = "CuratedList_ById";
        public const string CuratedList_ActInAct = "CuratedList_ActInact";
        public const string CuratedList_Delete = "CuratedList_Delete";
        #endregion

        #region OfferStatus
        public const string OfferStatus_All = "OfferStatus_All";
        public const string OfferStatus_ById = "OfferStatus_ById";
        public const string OfferStatus_Delete = "OfferStatus_Delete";
        public const string OfferStatus_Upsert = "OfferStatus_Upsert";

        #endregion
        #region LookupStatus
        public const string LookupStatus_All = "LookupStatus_All";
        #endregion
        #region ShopOwnerDevices
        public const string ShopOwnerDevices_ByShopId = "ShopOwnerDevices_ByShopId";
        #endregion 
        #region UserDevices
        public const string UserDevices_ByUserId = "UserDevices_ByUserId";
        #endregion 
        #region UserNotifications
        public const string UserNotifications_Insert = "UserNotifications_Insert";
        public const string UserNotifications_ByUserId = "UserNotifications_ByUserId";
        public const string UserNotifications_ByNotificationsTypeId = "UserNotifications_ByNotificationsTypeId";
        #endregion

        #region CallBack
        public const string CallBack_Upsert = "CallBack_Upsert";
        public const string CallBack_CallManage = "CallBack_CallManage";
        public const string CallBack_RazorPay = "CallBack_RazorPay";
        public const string CallBack_All = "CallBack_All";
        public const string CallBack_ById = "CallBack_ById";
        #endregion

        #region OrderParcel
        public const string OrderParcel_Upsert = "OrderParcel_Upsert";
        public const string OrderParcelProduct_All = "OrderParcelProduct_All";
        public const string OrderParcel_ByOrderId = "OrderParcel_ByOrderId";
        public const string OrderParcel_Delete = "OrderParcel_Delete";
        public const string OrderParcel_ById = "OrderParcel_ById";
        public const string OrderParcelShipment_ByOrderId = "OrderParcelShipment_ByOrderId";
        public const string Check_CreateShipment = "Check_CreateShipment";
        #endregion

        #region ShopCallingNotification
        public const string ShopCallingNotification_Upsert = "ShopCallingNotification_Upsert";
        #endregion
        #region UserFeedBack_Upsert
        public const string UserFeedBack_Upsert = "UserFeedBack_Upsert";
        #endregion

        public const string UserOrderItems_Status = "UserOrderItems_Status";
        public const string MasterOnline_Status = "MasterOnline_Status";

    }
}

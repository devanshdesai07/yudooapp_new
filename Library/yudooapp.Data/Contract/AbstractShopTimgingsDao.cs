﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractShopTimgingsDao  : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractShopTimingJson> ShopTimgings_Upsert(AbstractShopTimingJson abstractShopTimingJson);
        public abstract PagedList<AbstractShopTimgings> ShopTimgings_All(PageParam pageParam, string search , long ShopId);
        public abstract PagedList<AbstractShopTimgings> ShopTimgings_ShopId(PageParam pageParam, long ShopId);
        public abstract SuccessResult<AbstractShopTimgings> ShopTimgings_ById(long Id);
        public abstract SuccessResult<AbstractShopTimgings> ShopTimgings_IsClosedIsopen(long Id);
        public abstract SuccessResult<AbstractShopTimgings> ShopTimgings_Delete(long Id);


    }
}

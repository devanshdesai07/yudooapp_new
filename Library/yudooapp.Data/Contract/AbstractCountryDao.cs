﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractCountryDao : AbstractBaseDao
    {
        
        public abstract PagedList<AbstractCountry> Country_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCountry> Country_ById(long Id);
        public abstract SuccessResult<AbstractCountry> Country_Delete(long Id);
    }
}

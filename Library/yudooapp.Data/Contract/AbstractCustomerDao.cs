﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractCustomerDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractCustomer> Customer_Upsert(AbstractCustomer abstractCustomer);
        public abstract PagedList<AbstractCustomer> Customer_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCustomer> Customer_ById(long Id);
        public abstract SuccessResult<AbstractCustomer> Customer_ActInact(long Id);
        public abstract SuccessResult<AbstractCustomer> Customer_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractCustomer> Customer_Overview(long MobileNumber, int OrderId);
        public abstract PagedList<AbstractCustomer> Customer_Orders(PageParam pageParam,long MobileNumber, int OrderId);
        
    }
}

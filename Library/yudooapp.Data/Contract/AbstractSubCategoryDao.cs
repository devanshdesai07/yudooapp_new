﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractSubCategoryDao : AbstractBaseDao
    {
        
        public abstract PagedList<AbstractSubCategory> SubCategory_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractSubCategory> SubCategory_LookUpCategoryId(PageParam pageParam, string search, long CategoryId);
        public abstract SuccessResult<AbstractSubCategory> SubCategory_ById(long Id);
        
        
        
        

    }
}

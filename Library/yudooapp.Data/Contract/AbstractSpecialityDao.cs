﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
   public abstract class AbstractSpecialityDao
    {
        public abstract SuccessResult<AbstractSpeciality> Speciality_Upsert(AbstractSpeciality abstractSpeciality);
        public abstract PagedList<AbstractSpeciality> Speciality_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractSpeciality> Speciality_ById(long Id);
        public abstract SuccessResult<AbstractSpeciality> Speciality_ActInActive(long Id);
        public abstract SuccessResult<AbstractSpeciality> Speciality_Delete(long Id, long DeletedBy);
    }
}

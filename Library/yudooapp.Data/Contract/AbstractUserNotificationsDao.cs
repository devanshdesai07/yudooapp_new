﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractUserNotificationsDao : AbstractBaseDao

    {
        public abstract SuccessResult<AbstractUserNotifications> UserNotifications_Insert(AbstractUserNotifications abstractUserNotifications);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent);
        public abstract PagedList<AbstractUserNotifications> UserNotifications_ByNotificationsTypeId(PageParam pageParam, long NotificationsTypeId, long UserId);

    }
}
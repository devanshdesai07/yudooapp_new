﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractOrderParcelDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractOrderParcel> OrderParcel_Upsert(AbstractOrderParcel abstractOrderParcel);
        public abstract PagedList<AbstractOrderParcel> OrderParcel_ByOrderId(long OrderId);
        public abstract SuccessResult<AbstractOrderParcel> OrderParcel_ById(long Id);
        public abstract SuccessResult<AbstractOrderParcel> OrderParcels_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractOrderParcelProduct> OrderParcelProduct_All(PageParam pageParam, string search,long OrderId);
        public abstract SuccessResult<AbstractCheckCreateShipment> Check_CreateShipment(long OrderId);


        //Shipment Method
        public abstract PagedList<AbstarctCreateShipment> OrderParcelShipment_ByOrderId(PageParam pageParam, string search, long OrderId);


    }
}

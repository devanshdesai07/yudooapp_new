﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
   public abstract class AbstractTagsDao
    {
        public abstract SuccessResult<AbstractTags> Tags_Upsert(AbstractTags abstractTags);
        public abstract PagedList<AbstractTags> Tags_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractTags> Tags_ById(long Id);
        public abstract SuccessResult<AbstractTags> Tags_Delete(long Id);
    }
}

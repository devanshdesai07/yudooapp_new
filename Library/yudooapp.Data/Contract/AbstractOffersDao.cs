﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractOffersDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractOffers> Offers_Upsert(AbstractOffers abstractOffers);
        public abstract PagedList<AbstractOffers> Offers_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractOffers> Offers_All_Active(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractOffers> Offers_ById(long Id);
        public abstract SuccessResult<AbstractOffers> Offers_Delete(long Id);
        public abstract PagedList<AbstractOffers> Offers_ByShopId(PageParam pageParam, long ShopId);
        public abstract SuccessResult<AbstractOffers> Offers_ActInAct(long Id ,bool IsActive);
        public abstract SuccessResult<AbstractOffers> OfferStatus_ChangeStatus(long Id, long Status);
    }
}

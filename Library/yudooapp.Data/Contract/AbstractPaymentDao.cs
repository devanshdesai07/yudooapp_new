﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractPaymentDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractPayment> Payment_All(PageParam pageParam, string search, AbstractPayment abstractPayment);
        public abstract SuccessResult<AbstractPayment> Payment_ById(long Id);
        public abstract SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment);
        public abstract PagedList<AbstractCustomerPaymentStatus> CustomerPaymentStatus_All(PageParam pageParam, string search);
    }
}

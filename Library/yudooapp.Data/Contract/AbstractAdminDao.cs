﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractAdminDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractAdmin> Admin_Upsert(AbstractAdmin abstractAdmin);
        public abstract PagedList<AbstractAdmin> Admin_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractAdmin> Admin_ById(long Id);

        public abstract SuccessResult<AbstractAdmin> Admin_ActInAct(long Id);

        public abstract SuccessResult<AbstractAdmin> Admin_Delete(long Id);
        public abstract SuccessResult<AbstractAdmin> Admin_Login(string Email, string Password);
        public abstract bool Admin_Logout(long Id);
        public abstract SuccessResult<AbstractAdmin> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword);
        public abstract SuccessResult<AbstractAdmin> ForGotPassword(string Email, string OTP);
        public abstract SuccessResult<AbstractAdmin> Admin_ByEmail(string Email);
        public abstract SuccessResult<AbstractAdmin> Admin_IsCallAdmin(long Id,bool CallStatus);
        public abstract SuccessResult<AbstractAdmin> ResetPassword(string email, string Password, string ConfirmPassword);
    }
}

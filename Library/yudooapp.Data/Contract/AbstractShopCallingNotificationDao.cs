﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractShopCallingNotificationDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractShopCallingNotification> ShopCallingNotification_Upsert(AbstractShopCallingNotification abstractShopCallingNotification);
       
    }
}

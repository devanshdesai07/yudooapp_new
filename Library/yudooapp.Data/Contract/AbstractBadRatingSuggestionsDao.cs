﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
   public abstract class AbstractBadRatingSuggestionsDao
    {
        public abstract SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Upsert(AbstractBadRatingSuggestions abstractBadRatingSuggestions);
        public abstract PagedList<AbstractBadRatingSuggestions> BadRatingSuggestions_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_ById(long Id);
        public abstract SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_ActInActive(long Id);
        public abstract SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Delete(long Id);
        public abstract SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Update_DisplayOrder(long Id, bool IsUp);
    }
}

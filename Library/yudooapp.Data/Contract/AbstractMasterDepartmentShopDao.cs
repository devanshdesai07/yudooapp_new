﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
   public abstract class AbstractMasterDepartmentShopDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_Upsert(AbstractMasterDepartmentShop abstractMasterDepartmentShop);
        public abstract PagedList<AbstractMasterDepartmentShop> MasterDepartmentShop_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_ById(long Id);
        public abstract SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_Delete(long Id);
    }
}

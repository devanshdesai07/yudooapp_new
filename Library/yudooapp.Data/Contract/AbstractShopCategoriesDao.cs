﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractShopCategoriesDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractShopCategories> ShopCategories_Upsert(AbstractShopCategories abstractShopCategories);
        public abstract PagedList<AbstractShopCategories> ShopCategories_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractShopCategories> ShopCategories_ById(long Id);
        public abstract PagedList<AbstractShopCategories> ShopCategories_ByShopId(PageParam pageParam, long ShopId);
        public abstract PagedList<AbstractShopCategories> ShopCategories_ByCategoryId(PageParam pageParam, long CategoryId);
        public abstract PagedList<AbstractShopCategories> ShopCategories_BySubCategoryId(PageParam pageParam, long SubCategoryId);
        public abstract SuccessResult<AbstractShopCategories> ShopCategories_Delete(long Id);
    }
}

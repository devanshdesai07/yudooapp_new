﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractHelpSupportDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractHelpSupport> HelpSupport_Upsert(AbstractHelpSupport abstractHelpSupport);
        public abstract PagedList<AbstractHelpSupport> HelpSupport_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractHelpSupport> HelpSupport_Action(long Id,long StatusId);

    }
}

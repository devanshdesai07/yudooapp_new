﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractPermissionDao : AbstractBaseDao
    {

        public abstract PagedList<AbstractPermission> Permission_ByCustomerId(PageParam pageParam, string search, AbstractPermission abstractPermission);

        public abstract SuccessResult<AbstractPermission> Permission_Upsert(AbstractPermission abstractPermission);
    }
}

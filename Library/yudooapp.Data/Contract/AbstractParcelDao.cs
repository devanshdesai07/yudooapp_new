﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractParcelDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractParcel> Parcel_Upsert(AbstractParcel abstractParcel);
        public abstract SuccessResult<AbstractParcel> Parcel_StatusChange(long Id);
        public abstract SuccessResult<AbstractParcel> Parcel_Delete(long Id,long DeletedBy);
        public abstract PagedList<AbstractParcel> Parcel_ByOrderId(long OrderId, PageParam pageParam);
        public abstract SuccessResult<AbstractParcel> Parcel_ById(long Id );
    }
}

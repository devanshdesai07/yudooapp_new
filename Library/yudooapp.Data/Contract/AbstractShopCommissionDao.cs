﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractShopCommissionDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractShopCommission> ShopCommission_Upsert(AbstractShopCommission abstractShopCommission);
        public abstract PagedList<AbstractShopCommission> ShopCommission_All(PageParam pageParam, string search, long ShopId);
        public abstract SuccessResult<AbstractShopCommission> ShopCommission_ById(long Id);
        public abstract PagedList<AbstractShopCommission> ShopCommission_ByShopOwnerId(PageParam pageParam, long ByShopId);
        public abstract SuccessResult<AbstractShopCommission> ShopCommission_ActInAct(long Id);
        public abstract SuccessResult<AbstractShopCommission> ShopCommission_Delete(long Id);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractUserSearchDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUserSearch> UserSearch_Upsert(AbstractUserSearch abstractUserSearch);
        public abstract PagedList<AbstractUserSearch> UserSearch_ByUserId(PageParam pageParam , long USerId);
        public abstract PagedList<AbstractUserSearch> UserSearch_All(PageParam pageParam, string Serach, long UserId);
        public abstract PagedList<AbstractUserSearch> UserSearch_Recentsearch(PageParam pageParam, string Serach, long UserId);
        public abstract PagedList<AbstractUserSearch> UserSearch_TrendingSearches(PageParam pageParam, string Serach);
        public abstract SuccessResult<AbstractUserSearch> UserSearch_ById(long Id);
        
        public abstract SuccessResult<AbstractUserSearch> UserSearch_ActInact(long Id);
        

    }
}

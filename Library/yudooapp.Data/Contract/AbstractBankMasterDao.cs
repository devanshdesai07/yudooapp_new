﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractBankMasterDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractBankMaster> BankMaster_Upsert(AbstractBankMaster abstractBankMaster);
        public abstract SuccessResult<AbstractBankMaster> BankMaster_ById(long Id);
        public abstract PagedList<AbstractBankMaster> BankMaster_ByShopId(PageParam pageParam, long ShopId);
        public abstract SuccessResult<AbstractBankMaster> BankMaster_ActInAct(long Id);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractUserCartMasterDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUserCartMaster> UserCartMaster_Upsert(AbstractUserCartMaster abstractUserCartMaster);
        public abstract PagedList<AbstractUserCartMaster> UserCartMaster_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractUserCartMaster> UserCartMaster_ByShopId(PageParam pageParam, string search, long ShopId);
        public abstract PagedList<AbstractUserCartMaster> UserCartMaster_ByUserId(PageParam pageParam, string search, long UserId);
        public abstract SuccessResult<AbstractUserCartMaster> UserCartMaster_ById(long Id);
        public abstract SuccessResult<AbstractUserCartMaster> UserCartMaster_ByVideoCallId(long VideoCallId,long UserId,int Type);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
   public abstract class AbstractTrendingSearchDao
    {
        public abstract SuccessResult<AbstractTrendingSearch> TrendingSearch_Upsert(AbstractTrendingSearch abstractTrendingSearch);
        public abstract PagedList<AbstractTrendingSearch> TrendingSearch_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractTrendingSearch> TrendingSearch_ById(long Id);
        public abstract SuccessResult<AbstractTrendingSearch> TrendingSearch_ActInActive(long Id);
        public abstract SuccessResult<AbstractTrendingSearch> TrendingSearch_Delete(long Id);
        public abstract SuccessResult<AbstractTrendingSearch> TrendingSearch_Update_DisplayOrder(long Id, bool IsUp);
    }
}

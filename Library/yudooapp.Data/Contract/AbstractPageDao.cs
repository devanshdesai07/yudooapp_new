﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractPageDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractPage> Pages_Upsert(AbstractPage abstractPage);
        public abstract PagedList<AbstractPage> Pages_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractPage> Pages_ById(long Id);
        public abstract SuccessResult<AbstractPage> Pages_Delete(long Id);
        public abstract SuccessResult<AbstractPage> Pages_DisplayOrderNo(long Id, bool IsUp);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractUserDevicesDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId);

    }
}

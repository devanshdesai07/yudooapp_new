﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractCategoryDao : AbstractBaseDao
    {
        
        public abstract PagedList<AbstractCategory> Category_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCategory> Category_ById(long Id);
        public abstract SuccessResult<AbstractCategory> Category_Delete(long Id);
        public abstract SuccessResult<AbstractCategory> Category_Upsert(AbstractCategory abstractCategory);
        public abstract SuccessResult<AbstractCategory> Category_Update_DisplayOrder(long Id, bool IsUp);


    }
}

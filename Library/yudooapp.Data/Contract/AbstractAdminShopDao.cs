﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractAdminShopDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractAdminShop> AdminShop_All(PageParam pageParam, string search,long ShopOBStatus,long ShopId,string ShopName,string MobileNumber,long CityId,string PrimaryCategory, string AllCategory,string DateOfJoining);
        public abstract PagedList<AbstractAdminShop> ShopApproval_All(PageParam pageParam, string search,long ShopOBStatus,long ShopId,string ShopName,string MobileNumber,long CityId,string PrimaryCategory, string AllCategory,string DateOfJoining);
        public abstract SuccessResult<AbstractAdminShop> AdminShop_ById(long Id);
        public abstract SuccessResult<AbstractAdminShop> SendForApproval(long Shopid);
        public abstract PagedList<AbstractAdminShop> CurrentyOfflineShops_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractAdminShop> GetUnansweredcall(PageParam pageParam, string search);
        
    }
}

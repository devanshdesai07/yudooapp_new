﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractCustomerCallsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractCustomerCalls> CustomerCalls_Upsert(AbstractCustomerCalls abstractCustomerCalls);
        public abstract PagedList<AbstractCustomerCalls> CustomerCalls_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCustomerCalls> CustomerCalls_ById(long Id);

    }
}

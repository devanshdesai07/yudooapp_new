﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractMasterOrderStatusDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractMasterOrderStatus> MasterOrderStatus_Upsert(AbstractMasterOrderStatus abstractMasterOrderStatus);
        public abstract PagedList<AbstractMasterOrderStatus> MasterOrderStatus_All(PageParam pageParam, string search);
    }
}

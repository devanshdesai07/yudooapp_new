﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractLookupStatusDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractLookupStatus> LookupStatus_All(PageParam pageParam, string search);
    }
}

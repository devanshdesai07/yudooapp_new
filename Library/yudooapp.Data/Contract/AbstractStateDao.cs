﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractStateDao : AbstractBaseDao
    {
        
        public abstract PagedList<AbstractState> State_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractState> State_ById(long Id);
        public abstract PagedList<AbstractState> State_ByCountryId(PageParam pageParam, long CountryId);
        public abstract SuccessResult<AbstractState> State_Delete(long Id);

    }
}

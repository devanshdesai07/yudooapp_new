﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractDashboardCountForShopDao
    {
        public abstract SuccessResult<AbstractDashboardCountForShop> DashboardCount_ByShopId(long ShopId,string Period);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractBannersDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractBanners> Banners_Upsert(AbstractBanners abstractBanners);
        public abstract PagedList<AbstractBanners> Banners_All(PageParam pageParam, string search,int IsSpecialist);
        public abstract SuccessResult<AbstractBanners> Banners_ById(long Id);
        public abstract SuccessResult<AbstractBanners> Banners_Update_DisplayOrder(long Id, bool IsUp);
        public abstract SuccessResult<AbstractBanners> Banners_ActInAct(long Id);
        public abstract SuccessResult<AbstractBanners> Banners_Delete(long Id);
    }
}

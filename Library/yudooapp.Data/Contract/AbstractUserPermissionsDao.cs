﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractUserPermissionsDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractUserPermissions> UserPermissions_Upsert(List<AbstractUserPermissions> AbstractUserPermissions);
        public abstract PagedList<AbstractUserPermissions> UserPermissions_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractUserPermissions> UserPermissions_All_Active(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractUserPermissions> UserPermissions_ById(long Id);
        public abstract SuccessResult<AbstractUserPermissions> UserPermissions_ActInAct(long Id ,bool IsActive);
        public abstract PagedList<AbstractUserPermissions> UserPermissions_ByAdminId(PageParam pageParam, string search, int AdminTypeId);
    }
}

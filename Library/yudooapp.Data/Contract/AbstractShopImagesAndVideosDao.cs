﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractShopImagesAndVideosDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_Upsert(AbstractShopImagesAndVideos abstractShopImagesAndVideos);
        public abstract PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_ById(long Id);
        public abstract SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_IsVideo(long Id);

        public abstract PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos_ByShopId(PageParam pageParam, long ShopId);
        public abstract SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_IsCoverImage(long Id, bool IsVideo);
        public abstract SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_Delete(long Id); 

    }
}

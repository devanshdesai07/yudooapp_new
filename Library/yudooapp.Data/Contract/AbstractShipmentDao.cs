﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractShipmentDao : AbstractBaseDao
    {
        public abstract SuccessResult<AbstractShipment> Shipment_Upsert(AbstractShipment abstractShipment);
        public abstract SuccessResult<AbstractShipment> Shipment_StatusChange(long Id);
        public abstract PagedList<AbstractShipment> Shipment_ByParcelId(long ParcelId, PageParam pageParam);
        public abstract PagedList<AbstractShipment> Shipment_ByOrderId(long OrderId, PageParam pageParam);
        public abstract SuccessResult<AbstractShipment> Shipment_ById(long Id );
    }
}

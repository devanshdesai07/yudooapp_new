﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Data.Contract
{
    public abstract class AbstractFAQsDao
    {
        public abstract SuccessResult<AbstractFAQs> FAQs_Upsert(AbstractFAQs abstractFAQs);
        public abstract PagedList<AbstractFAQs> FAQs_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractFAQs> FAQs_ById(long Id);
        public abstract SuccessResult<AbstractFAQs> FAQs_ActInAct(long Id);
        public abstract SuccessResult<AbstractFAQs> FAQs_Delete(long Id);
    }
}

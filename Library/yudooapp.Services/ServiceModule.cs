﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace yudooapp.Services
{
    using Autofac;
    using Data;
    using yudooapp.Services.Contract;
    



    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();
            builder.RegisterType<V1.AdminServices>().As<AbstractAdminServices>().InstancePerDependency();
            builder.RegisterType<V1.HelpSupportServices>().As<AbstractHelpSupportServices>().InstancePerDependency();
            builder.RegisterType<V1.UserOrderAmountBifurcationServices>().As<AbstractUserOrderAmountBifurcationServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopOwnerServices>().As<AbstractShopOwnerServices>().InstancePerDependency();
            builder.RegisterType<V1.AddressServices>().As<AbstractAddressServices>().InstancePerDependency();
            builder.RegisterType<V1.BankMasterServices>().As<AbstractBankMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.OrderServices>().As<AbstractOrderServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerServices>().As<AbstractCustomerServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerCallsServices>().As<AbstractCustomerCallsServices>().InstancePerDependency();
            builder.RegisterType<V1.CategoryServices>().As<AbstractCategoryServices>().InstancePerDependency();
            builder.RegisterType<V1.SubCategoryServices>().As<AbstractSubCategoryServices>().InstancePerDependency();
            builder.RegisterType<V1.CityServices>().As<AbstractCityServices>().InstancePerDependency();
            builder.RegisterType<V1.CountryServices>().As<AbstractCountryServices>().InstancePerDependency();
            builder.RegisterType<V1.StateServices>().As<AbstractStateServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersServices>().As<AbstractUsersServices>().InstancePerDependency();
            builder.RegisterType<V1.PaymentServices>().As<AbstractPaymentServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductsServices>().As<AbstractProductsServices>().InstancePerDependency();
            builder.RegisterType<V1.ProductTypeServices>().As<AbstractProductTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.PermissionServices>().As<AbstractPermissionServices>().InstancePerDependency();
            builder.RegisterType<V1.ReviewServices>().As<AbstractReviewServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCallStatusServices>().As<AbstractMasterCallStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterOrderStatusServices>().As<AbstractMasterOrderStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.UserSearchServices >().As< AbstractUserSearchServices >().InstancePerDependency();
            builder.RegisterType<V1.MasterDepartmentShopServices>().As<AbstractMasterDepartmentShopServices>().InstancePerDependency();
            builder.RegisterType<V1.UserWishlistServices>().As<AbstractUserWishlistServices>().InstancePerDependency();
            builder.RegisterType<V1.UserCartServices>().As<AbstractUserCartServices>().InstancePerDependency();
            builder.RegisterType<V1.BannersServices>().As<AbstractBannersServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopTimgingsServices>().As<AbstractShopTimgingsServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopRatingsServices>().As<AbstractShopRatingsServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopImagesAndVideosServices>().As<AbstractShopImagesAndVideosServices>().InstancePerDependency();
            builder.RegisterType<V1.UserOrderServices>().As<AbstractUserOrderServices>().InstancePerDependency();
            builder.RegisterType<V1.UserOrderItemsServices>().As<AbstractUserOrderItemsServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopCategoriesServices>().As<AbstractShopCategoriesServices>().InstancePerDependency();
            builder.RegisterType<V1.UserShopCallLogsServices>().As<AbstractUserShopCallLogsServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopCommissionServices>().As<AbstractShopCommissionServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopEmployeesServices>().As<AbstractShopEmployeesServices>().InstancePerDependency();
            builder.RegisterType<V1.FAQsServices>().As<AbstractFAQsServices>().InstancePerDependency();
            builder.RegisterType<V1.FaqVideosServices>().As<AbstractFaqVideosServices>().InstancePerDependency();
            builder.RegisterType<V1.DashboardCountForShopServices>().As<AbstractDashboardCountForShopServices>().InstancePerDependency();
            builder.RegisterType<V1.AdminShopServices>().As<AbstractAdminShopServices>().InstancePerDependency();
            builder.RegisterType<V1.UserCartMasterServices>().As<AbstractUserCartMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.UserCallBackPreferenceTimgingsServices>().As<AbstractUserCallBackPreferenceTimgingsServices>().InstancePerDependency();
            builder.RegisterType<V1.OffersServices>().As<AbstractOffersServices>().InstancePerDependency();
            builder.RegisterType<V1.ParcelServices>().As<AbstractParcelServices>().InstancePerDependency();
            builder.RegisterType<V1.ShipmentServices>().As<AbstractShipmentServices>().InstancePerDependency();
            builder.RegisterType<V1.PageServices>().As<AbstractPageServices>().InstancePerDependency();
            builder.RegisterType<V1.CuratedListServices>().As<AbstractCuratedListServices>().InstancePerDependency();
            builder.RegisterType<V1.AdminTypeServices>().As<AbstractAdminTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.UserPermissionsServices>().As<AbstractUserPermissionsServices>().InstancePerDependency();
            builder.RegisterType<V1.SpecialityServices>().As<AbstractSpecialityServices>().InstancePerDependency();
            builder.RegisterType<V1.HomePageShopOrderFactorsServices>().As<AbstractHomePageShopOrderFactorsServices>().InstancePerDependency();
            builder.RegisterType<V1.TrendingSearchServices>().As<AbstractTrendingSearchServices>().InstancePerDependency();
            builder.RegisterType<V1.GoodRatingSuggestionsServices>().As<AbstractGoodRatingSuggestionsServices>().InstancePerDependency();
            builder.RegisterType<V1.BadRatingSuggestionsServices>().As<AbstractBadRatingSuggestionsServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersFAQServices>().As<AbstractUsersFAQServices>().InstancePerDependency();
            builder.RegisterType<V1.DownloadServices>().As<AbstractDownloadServices>().InstancePerDependency();
            builder.RegisterType<V1.OfferStatusServices>().As<AbstractOfferStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.TagsServices>().As<AbstractTagsServices>().InstancePerDependency();
            builder.RegisterType<V1.NotificationTypesServices>().As<AbstractNotificationTypesServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopKeeperMasterServices>().As<AbstractShopKeeperMasterServices>().InstancePerDependency();
            builder.RegisterType<V1.NotificationServices>().As<AbstractNotificationServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupStatusServices>().As<AbstractLookupStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopOwnerDevicesServices>().As<AbstractShopOwnerDevicesServices>().InstancePerDependency();
            builder.RegisterType<V1.UserDevicesServices>().As<AbstractUserDevicesServices>().InstancePerDependency();
            builder.RegisterType<V1.UserNotificationsServices>().As<AbstractUserNotificationsServices>().InstancePerDependency();
            builder.RegisterType<V1.NotificationServices>().As<AbstractNotificationServices>().InstancePerDependency();
            builder.RegisterType<V1.CallBackServices>().As<AbstractCallBackServices>().InstancePerDependency();
            builder.RegisterType<V1.OrderParcelServices>().As<AbstractOrderParcelServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopCallingNotificationServices>().As<AbstractShopCallingNotificationServices>().InstancePerDependency();
            builder.RegisterType<V1.UserFeedBackServices>().As<AbstractUserFeedBackServices>().InstancePerDependency();
            builder.RegisterType<V1.ShopShortingValuesServices>().As<AbstractShopShortingValuesServices>().InstancePerDependency();
            base.Load(builder);
        }
    }
}

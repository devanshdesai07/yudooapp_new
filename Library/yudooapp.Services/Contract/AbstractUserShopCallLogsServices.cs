﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractUserShopCallLogsServices
    {
        public abstract SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_Upsert(AbstractUserShopCallLogs abstractUserShopCallLogs);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogs_All(PageParam pageParam, string search, long UserId, long ShopId);
        public abstract SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_ById(long Id);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByUserId(PageParam pageParam, long UserId);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByShopId(PageParam pageParam, long ShopId);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByOrderId(PageParam pageParam, long OrderId);
        public abstract SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_Delete(long Id, long DeletedBy);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogsDetails_All(PageParam pageParam, string Search, long Id, long OrderId, string UserMobileNumber, long ShopId, string ShopName, string MobileNumber, string CallDateTime);
        public abstract PagedList<AbstractUserShopCallLogs> Request_CallBack_ShopId(PageParam pageParam, long ShopId);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ReceiveVideoCall(PageParam pageParam);
        public abstract PagedList<AbstractUserShopCallLogs> UserShopCallLogs_PendingCall(PageParam pageParam, long UserId);
        public abstract SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_CallStatus_Update(long Id, long CallStatus);
        public abstract PagedList<AbstractRequest_CallBack_Admin> Request_CallBack_Admin(PageParam pageParam, string Search);

    }
   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractShopEmployeesServices
    {
        public abstract SuccessResult<AbstractShopEmployees> ShopEmployees_Upsert(AbstractShopEmployees abstractShopEmployees);
        public abstract SuccessResult<AbstractShopEmployees> ShopEmployees_ById(long Id);
        public abstract PagedList<AbstractShopEmployees> ShopEmployees_All(PageParam pageParam, string Search, long ShopId);
        public abstract PagedList<AbstractShopEmployees> ShopEmployees_ByShopId(PageParam pageParam, long ShopId);
        public abstract SuccessResult<AbstractShopEmployees> ShopEmployees_ActInact(long Id);
        public abstract SuccessResult<AbstractShopEmployees> ShopEmployees_IsOnline(long Id);
        public abstract SuccessResult<AbstractShopEmployees> ShopEmployees_Delete(long Id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractReviewServices
    {
        public abstract PagedList<AbstractReview> Review_ByCustomerId(PageParam pageParam, string search, AbstractReview abstractReview);
        public abstract PagedList<AbstractReview> Review_All(PageParam pageParam, string search, AbstractReview abstractReview);
        public abstract SuccessResult<AbstractReview> Review_Upsert(AbstractReview abstractReview);
    }
}

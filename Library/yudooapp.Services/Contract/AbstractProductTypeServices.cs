﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractProductTypeServices
    {
        public abstract PagedList<AbstractProductType> ProductType_All(PageParam pageParam, string search, AbstractProductType abstractProductType);
        public abstract SuccessResult<AbstractProductType> ProductType_ById(long Id);
        public abstract SuccessResult<AbstractProductType> ProductType_Upsert(AbstractProductType abstractProductType);
    }
}

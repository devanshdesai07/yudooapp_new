﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractUserOrderItemsServices:AbstractBaseService
    {
        public abstract SuccessResult<AbstractUserOrderItems> UserOrderItems_Upsert(AbstractUserOrderItems abstractUserOrderItems);
        public abstract PagedList<AbstractUserOrderItems> UserOrderItems_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractUserOrderItems> UserOrderItems_ById(long Id);
        public abstract SuccessResult<AbstractUserOrderItems> ManifestUrl_ShipmentId(string ManifestUrl,string LabelUrl, long ShipmentId);
        public abstract SuccessResult<AbstractUserOrderItems> AddShipmentIdUserOrderItems_ById(string Id,int ShipmentId);
        public abstract SuccessResult<AbstractUserOrderItems> UserOrderItems_StatusChange(long Id,long ItemStatusId,long UpdatedBy);
        public abstract SuccessResult<AbstractUserOrderItems> UserOrderItems_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractUserOrderItems> UserOrderItems_Status(long Id, long StatusId);
        public abstract PagedList<AbstractUserOrderItems> UserOrderItems_ByOrderId(PageParam pageParam, long OrderId);
        public abstract PagedList<AbstractUserOrderItems> UserOrderItems_ByMobileNumber(PageParam pageParam, string MobileNumber);


    }
}

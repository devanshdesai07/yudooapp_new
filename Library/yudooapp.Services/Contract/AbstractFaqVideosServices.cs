﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractFaqVideosServices:AbstractBaseService
    {
        public abstract SuccessResult<AbstractFaqVideos> FaqVideo_Upsert(AbstractFaqVideos abstractFaqVideos);
        public abstract PagedList<AbstractFaqVideos> FaqVideo_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractFaqVideos> FaqVideo_ById(long Id);
        public abstract SuccessResult<AbstractFaqVideos> FaqVideo_ActInAct(long Id);
        public abstract SuccessResult<AbstractFaqVideos> FaqVideo_Delete(long Id);
    }
}

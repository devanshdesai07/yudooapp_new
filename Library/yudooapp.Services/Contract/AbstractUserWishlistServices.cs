﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractUserWishlistServices
    {
        public abstract SuccessResult<AbstractUserWishlist> UserWishlist_Upsert(AbstractUserWishlist abstractUserWishlist);
        public abstract SuccessResult<AbstractUserWishlist> UserWishlist_ById(long Id);
        public abstract SuccessResult<AbstractUserWishlist> UserWishlist_Delete(int UserId, int ShopId);
        public abstract PagedList<AbstractUserWishlist> UserWishlist_All(PageParam pageParam, string Search, long UserId, long ShopId);
        public abstract PagedList<AbstractShopOwner> UserWishlist_ByUserId(PageParam pageParam, long UserId);
        public abstract PagedList<AbstractUserWishlist> UserWishlist_ByShopId(PageParam pageParam, long ShopId);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractUserDevicesServices
    {

        public abstract PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId);


    }
}

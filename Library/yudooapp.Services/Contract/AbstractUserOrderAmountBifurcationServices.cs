﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractUserOrderAmountBifurcationServices
    {
        public abstract SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_Upsert(AbstractUserOrderAmountBifurcation abstractUserOrderAmountBifurcation);
        public abstract PagedList<AbstractUserOrder> UserOrderAmountBifurcation_All(PageParam pageParam, string search, long OrderId, string FromDate, string ToDate, string type, int ispaid);
        public abstract SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_UpdateNote(AbstractUserOrderAmountBifurcation abstractUserOrderAmountBifurcation);
        public abstract PagedList<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_ByOrderId(PageParam pageParam,  long OrderId);
        public abstract SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_UpdateIsPaid(long Id, int IsPaid);
    }
}

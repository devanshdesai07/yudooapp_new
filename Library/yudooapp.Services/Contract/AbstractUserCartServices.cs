﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractUserCartServices : AbstractBaseService
    {
        public abstract SuccessResult<AbstractUserCart> UserCart_Upsert(AbstractUserCart abstractUserCart);
        public abstract SuccessResult<AbstractUserCart> UserCart_ById(long Id);
        public abstract SuccessResult<AbstractPersonalUserCart> AddUserCart(AbstractPersonalUserCart abstractPersonalUserCart);
        public abstract PagedList<AbstractUserCart> UserCart_All(PageParam pageParam, string Search, long UserId, long ShopId);
        public abstract PagedList<AbstractUserCart> UserCart_ByUserId(PageParam pageParam, long UserId);
        public abstract PagedList<AbstractUserCart> UserCart_ByShopId(PageParam pageParam, long ShopId);
        public abstract PagedList<AbstractUserCart> UserCart_ByVideoCallId(PageParam pageParam, long VideoCallId);
        public abstract SuccessResult<AbstractUserCart> UserCart_Delete(long Id);
        public abstract SuccessResult<AbstractUserCart> UserCartDeleteByVideoCallId(long VideoCallId);
        public abstract PagedList<AbstractUserCart> UserCart_IsVisibleToCustomer(PageParam pageParam, string Search, long UserCartMasterId);
        public abstract SuccessResult<AbstractUserCart> UserCart_IsSendToOrder(long UserCartMasterId);
        //public abstract SuccessResult<AbstractUserCart> UserCart_Details(long Id);
        public abstract PagedList<AbstractUserCart> UserCart_IsSendToCustomer(PageParam pageParam, string Search, long VideoCallId);
        public abstract SuccessResult<AbstractUserCart> UserCart_ConfirmOrder(long VideoCallId, decimal ShippingChargeFromAPI, decimal CODChargefromAPI,long IsOnline,string Address, string UserName);
    }
}

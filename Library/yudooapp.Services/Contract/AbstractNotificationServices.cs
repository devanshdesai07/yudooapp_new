﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractNotificationServices
    {
        public abstract PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractNotification> Notification_ById(long Id);
        public abstract PagedList<AbstractNotification> Notification_ByUserId(PageParam pageParam, long UserId);
        public abstract SuccessResult<AbstractNotification> Notification_Delete(long Id);
        public abstract SuccessResult<AbstractNotification> SendGeneralNotifications(AbstractNotification abstractnotification);
        public abstract SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractnotification);
    }
}

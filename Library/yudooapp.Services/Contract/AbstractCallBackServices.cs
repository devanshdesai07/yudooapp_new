﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractCallBackServices
    {
        public abstract SuccessResult<AbstractCallBack> CallBack_Upsert(AbstractCallBack abstractCallBack);
        public abstract SuccessResult<AbstractUserShopCallLogs> CallBack_CallManage(UserShopCallLogs userShopCallLogs);
        public abstract SuccessResult<AbstractUserShopCallLogs> RazorPay(UserShopCallLogs userShopCallLogs);
        public abstract PagedList<AbstractCallBack> CallBack_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCallBack> CallBack_ById(long Id);
    }
}

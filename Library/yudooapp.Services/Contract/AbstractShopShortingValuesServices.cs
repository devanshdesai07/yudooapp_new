﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractShopShortingValuesServices
    {
        public abstract SuccessResult<AbstractShopShortingValues> ShopShortingValues_Upsert(AbstractShopShortingValues AbstractShopShortingValues);
        public abstract PagedList<AbstractShopShortingValues> ShopShortingValues_All(PageParam pageParam);
    }
}

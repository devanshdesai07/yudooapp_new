﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractUserOrderServices
    {
        public abstract SuccessResult<AbstractUserOrder> UserOrder_Upsert(AbstractUserOrder abstractUserOrder);
        public abstract SuccessResult<AbstractUserOrder> UserOrder_Upsert_paymentLink(long Id, string PaymentLink, string RazorPayOrderId = "");
        public abstract PagedList<AbstractUserOrder> UserOrder_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractUserOrder> UserOrder_ById(long Id);
        
        public abstract PagedList<AbstractUserOrder> UserOrder_ByShopId(PageParam pageParam, string search, long Id,string OrderStatus);
        public abstract PagedList<AbstractUserOrder> UserOrder_ByUserId(PageParam pageParam, long UserId, long IsPendingPayment, long IsPlaced);
        public abstract SuccessResult<AbstractUserOrder> UserOrder_Delete(long Id);
        public abstract SuccessResult<AbstractUserOrder> UserOrder_StatusChange(long Id, long OrderStatus);
        public abstract SuccessResult<AbstractUserOrder> UserOrder_StatusChangeByShipmentId(long Id, long OrderStatus);
        public abstract SuccessResult<AbstractUserOrder> UserOrder_DeliveryRatingUpdate(long Id, long DeliveryRating);
        public abstract PagedList<AbstractUserOrder> OrderDetails_All(PageParam pageParam, string Search, long OrderStatus, string PaymentStatus,long CityId,decimal Amount);

    }
}

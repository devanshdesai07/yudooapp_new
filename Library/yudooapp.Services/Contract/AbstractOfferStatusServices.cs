﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractOfferStatusServices
    {
        public abstract SuccessResult<AbstractOfferStatus> OfferStatus_Upsert(AbstractOfferStatus abstractOfferStatus);
        public abstract PagedList<AbstractOfferStatus> OfferStatus_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractOfferStatus> OfferStatus_ById(long Id);
        public abstract SuccessResult<AbstractOfferStatus> OfferStatus_Delete(long Id);
    }
}

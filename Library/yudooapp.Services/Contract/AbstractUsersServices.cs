﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractUsersServices: AbstractBaseService
    {
        public abstract SuccessResult<AbstractUsers> Users_ActInAct(long Id);
        public abstract PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Users_ById(long Id);
        public abstract SuccessResult<AbstractUsers> Users_IsCallStatus(long Id);
        public abstract SuccessResult<AbstractCallerInfo> Caller_Info(long ShpopId,long UserId);
        public abstract SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy);
        public abstract bool Users_Logout(long Id);
        public abstract SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword);
        public abstract SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Users_VerifyOtp(long MobileNumber, long Otp, string DeviceType, string UDID, string IMEI, string DeviceToken);
        public abstract SuccessResult<AbstractUsers> Users_SendOTP(long MobileNumber, long Otp);
        public abstract SuccessResult<AbstractUsers> Users_OnlineOffline(long Id,bool IsOnline);
        public abstract SuccessResult<AbstractUsers> CheckUser_Exists(string MobileNumber);
        public abstract SuccessResult<AbstractUsers> Users_IsCallBackPreference(long Id, bool IsCallBackPreference);
    }
}

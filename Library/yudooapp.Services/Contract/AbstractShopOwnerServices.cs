﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractShopOwnerServices :AbstractBaseService
    {
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_Upsert(AbstractShopOwner abstractShopOwner);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_UpdateCommision(long Id,decimal YudooCommission);
        public abstract SuccessResult<AbstractShopAudit> ShopAudit_Upsert(AbstractShopAudit abstractShopAudit);
        public abstract SuccessResult<AbstractShopAudit> ShopAudit_DeleteMedia(string Url, int Type, long ShopId);
        public abstract PagedList<AbstractShopOwner> ShopOwner_All(PageParam pageParam, string search, long CityId, long SpecialityId, long UserId, long CuratedListId=0, long ShopId=0);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_ById(long Id, long UserId);
        public abstract SuccessResult<AbstractShopAudit> ShopAudit_ShopId(long ShopId);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_TagsByShopId(long ShopId);
        public abstract PagedList<AbstractShopOwner> ShopEmployee_ByParentId(PageParam pageParam, long ParentId);

        public abstract SuccessResult<AbstractShopOwner> ShopOwner_TagsUpdate(long Id, string Tags);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_UpdateStatus(long Id, long ShopStatus);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_ActInAct(long Id);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_Delete(long Id, long DeletedBy);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_IsRating(bool IsRating);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_ShopEmployeeActInAct(bool IsActive, long ShopOwnerId, long UserId);
        public abstract SuccessResult<AbstractShopOwner> AddShopOwnerPickUpId(long ShopOwnerId, string PickUpId);
        public abstract SuccessResult<AbstractShopOwner> CheckSeller_Exists(string ShopOwnerName, string ShopName, string MobileNumber);
        public abstract SuccessResult<AbstractShopOwner> CheckSeller_Exists_ById(long Id);
        public abstract SuccessResult<AbstractShopOwner> CheckSeller_Exists_ByMobileNumberAndById(long Id, string Mobilenumber, string DeviceToken);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_SendOTP(string Mobilenumber,long otp);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_VerifyOtp(string Mobilenumber, long otp);
        public abstract PagedList<AbstractShopOwner> AdminShopOwnerDetails_All(PageParam pageParam, string search, long CityId, long CategoryId, string AllCategory, long ShopRatingsId, long ShopStatus, int? DraftStatus);
        public abstract PagedList<AbstractShopOwner> Shop_Orders(PageParam pageParam, string search, long ShopId, string ShopMobileNumber);
        public abstract SuccessResult<AbstractShopOwner> Shop_Overview(int shopId, string OwnerPhoneNo, string shopName, string FilterBy);
        public abstract bool ShopOwner_Logout(long Id);
        public abstract bool MasterOnline_Status(bool Status);
        public abstract PagedList<AbstractShopOwner> ShopOwner_Specialities_All(PageParam pageParam, long CityId, long SubCategoryId);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_OnlineOffline(long Id,bool IsOnline);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_IsApprovedByAdmin(long Id, long Status);

        public abstract SuccessResult<AbstractShopOwnerRights> ShopOwnerRights_Upsert(AbstractShopOwnerRights abstractShopOwnerRights);
        public abstract SuccessResult<AbstractShopOwnerRights> ShopOwnerRights_BySOU(long ShopId, long UserId);
        public abstract SuccessResult<AbstractShopOwner> AddressKey_Update(long ShopId, string AddressKey,long AddressCount,long Type);
        public abstract PagedList<AbstractShopOwner> ShopOwner_ByCallLog(PageParam pageParam, string search, long UserId);
    }
}

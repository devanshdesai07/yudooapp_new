﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractProductsServices
    {
        public abstract SuccessResult<AbstractProducts> Products_ActInAct(long Id);
        public abstract PagedList<AbstractProducts> Products_All(PageParam pageParam, string search, AbstractProducts abstractProducts);
        public abstract SuccessResult<AbstractProducts> Products_ById(long Id);
        public abstract SuccessResult<AbstractProducts> Products_Delete(long Id);
        public abstract SuccessResult<AbstractProducts> Products_Upsert(AbstractProducts abstractProducts);
    }
}

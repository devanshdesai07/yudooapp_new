﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractCuratedListServices
    {
        public abstract SuccessResult<AbstractCuratedList> CuratedList_Upsert(AbstractCuratedList abstractCuratedList);
        public abstract PagedList<AbstractCuratedList> CuratedList_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCuratedList> CuratedList_ById(long Id);
        public abstract SuccessResult<AbstractCuratedList> CuratedList_ActInAct(long Id);
        public abstract SuccessResult<AbstractCuratedList> CuratedList_Delete(long Id, long DeletedBy);
    }
}

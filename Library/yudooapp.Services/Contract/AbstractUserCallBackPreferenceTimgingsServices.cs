﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;


namespace yudooapp.Services.Contract
{
    public abstract class AbstractUserCallBackPreferenceTimgingsServices
    {
        public abstract SuccessResult<AbstractUserCallBackPreferenceTimingJson> UserCallBackPreferenceTimgings_Upsert(AbstractUserCallBackPreferenceTimingJson abstractUserCallBackPreferenceTimingJson);
        public abstract PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_All(PageParam pageParam, string search, long UserId = 0);
        public abstract SuccessResult<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_ById(long Id);
        public abstract PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_ByUserId(PageParam pageParam, long UserId);
        public abstract bool UserCallBackPreferenceTimgings_Available(long UserId);
        public abstract SuccessResult<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_Delete(long Id);
    }
}

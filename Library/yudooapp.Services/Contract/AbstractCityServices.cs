﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractCityServices
    {
        
        public abstract PagedList<AbstractCity> City_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractCity> City_ById(long Id);
        public abstract PagedList<AbstractCity> City_ByStateId(PageParam pageParam, long StateId);
        public abstract SuccessResult<AbstractCity> City_Delete(long Id);
        public abstract SuccessResult<AbstractCity> City_Upsert(AbstractCity abstractCity);

    }
}

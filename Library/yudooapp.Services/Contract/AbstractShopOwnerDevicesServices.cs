﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractShopOwnerDevicesServices
    {
        public abstract PagedList<AbstractShopOwnerDevices> ShopOwnerDevices_ByShopId(PageParam pageParam, string search, long ShopOwnerId);

    }
}

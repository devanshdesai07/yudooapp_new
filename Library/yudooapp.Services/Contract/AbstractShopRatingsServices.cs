﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractShopRatingsServices
    {
        public abstract SuccessResult<AbstractShopRatings> ShopRatings_Upsert(AbstractShopRatings abstractShopRatings);
        public abstract PagedList<AbstractShopRatings> ShopRatings_All(PageParam pageParam, string search , long ShopId , long UserId);
        public abstract PagedList<AbstractShopRatings> ShopRatings_ByShopId(PageParam pageParam, string search, long ShopId);
        public abstract PagedList<AbstractShopRatings> ShopRatings_ByUserId(PageParam pageParam, string search , long UserId);
        public abstract SuccessResult<AbstractShopRatings> ShopRatings_ById(long Id);
        public abstract SuccessResult<AbstractShopRatings> ShopRatings_Delete(long Id);
    }
}

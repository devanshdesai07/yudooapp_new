﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractOrderServices
    {
        public abstract SuccessResult<AbstractOrder> Order_Upsert(AbstractOrder abstractOrder);
        public abstract PagedList<AbstractOrder> Order_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractOrder> Order_ById(long Id);
        public abstract SuccessResult<AbstractOrderDetails> OrderDetail_ById(long OrderId);
        public abstract PagedList<AbstractOrder> AdminOrderDetails_All(PageParam pageParam, string search, long Id, long MasterOrderStatusId, string PaymentStatus, long ShopId, string ShopName, long CityId, string CustomerMobileNumber, string CustomerName, long Amount);
        public abstract SuccessResult<AbstractOrder> User_OrderDetails(long Id);
        public abstract SuccessResult<AbstractOrder> OrderDetails_Status(long Id, long StatusId);
    }
}

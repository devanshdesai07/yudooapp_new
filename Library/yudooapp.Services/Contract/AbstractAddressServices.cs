﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractAddressServices
    {
        public abstract SuccessResult<AbstractAddress> Address_Upsert(AbstractAddress abstractAddress);
        public abstract PagedList<AbstractAddress> Address_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractAddress> Address_ById(long Id);
        public abstract PagedList<AbstractAddress> Address_ByUserId(PageParam pageParam, long UserId);
        public abstract SuccessResult<AbstractAddress> Address_ActInAct(long Id);
        public abstract SuccessResult<AbstractAddress> Address_Delete(long Id);
    }
}

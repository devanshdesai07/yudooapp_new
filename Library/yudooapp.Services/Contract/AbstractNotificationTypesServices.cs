﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractNotificationTypesServices
    {
        public abstract SuccessResult<AbstractNotificationTypes> NotificationTypes_Upsert(AbstractNotificationTypes abstractNotificationTypes);
        public abstract PagedList<AbstractNotificationTypes> NotificationTypes_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractNotificationTypes> NotificationTypes_ById(long Id);
        public abstract SuccessResult<AbstractNotificationTypes> NotificationTypes_Delete(long Id);
    }
}

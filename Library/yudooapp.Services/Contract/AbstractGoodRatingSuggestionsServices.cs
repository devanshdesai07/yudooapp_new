﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractGoodRatingSuggestionsServices
    {
        public abstract SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Upsert(AbstractGoodRatingSuggestions abstractGoodRatingSuggestions);
        public abstract PagedList<AbstractGoodRatingSuggestions> GoodRatingSuggestions_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_ById(long Id);
        public abstract SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_ActInActive(long Id);
        public abstract SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Delete(long Id);
        public abstract SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Update_DisplayOrder(long Id, bool IsUp);
    }
}

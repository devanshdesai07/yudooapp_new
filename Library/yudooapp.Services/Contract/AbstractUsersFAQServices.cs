﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractUsersFAQServices
    {
        public abstract SuccessResult<AbstractUsersFAQ> UsersFAQ_Upsert(AbstractUsersFAQ abstractUsersFAQ);
        public abstract PagedList<AbstractUsersFAQ> UsersFAQ_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractUsersFAQ> UsersFAQ_ById(long Id);
        public abstract SuccessResult<AbstractUsersFAQ> UsersFAQ_ActInAct(long Id);
        public abstract SuccessResult<AbstractUsersFAQ> UsersFAQ_Delete(long Id);
    }
}

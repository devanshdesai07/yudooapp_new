﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
   public abstract class AbstractHomePageShopOrderFactorsServices
    {
        public abstract SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_Upsert(AbstractHomePageShopOrderFactors abstractHomePageShopOrderFactors);
        public abstract PagedList<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_ById(int Id);
    }
}

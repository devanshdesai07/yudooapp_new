﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;

namespace yudooapp.Services.Contract
{
    public abstract class AbstractAdminTypeServices
    {
        public abstract PagedList<AbstractAdminType> AdminType_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractAdminType> AdminType_ById(int Id);
        public abstract SuccessResult<AbstractAdminType> AdminType_ActInAct(int Id);
        public abstract SuccessResult<AbstractAdminType> AdminType_Delete(int Id, int DeletedBy);
        public abstract SuccessResult<AbstractAdminType> AdminType_Upsert(AbstractAdminType abstractAdminType);

    }
}

﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class OrderServices : AbstractOrderServices
    {
        private AbstractOrderDao abstractOrderDao;

        public OrderServices(AbstractOrderDao abstractOrderDao)
        {
            this.abstractOrderDao = abstractOrderDao;
        }

        public override SuccessResult<AbstractOrder> Order_Upsert(AbstractOrder abstractOrder)
        {
            return this.abstractOrderDao.Order_Upsert(abstractOrder); ;
        }

        public override PagedList<AbstractOrder> Order_All(PageParam pageParam, string search)
        {
            return this.abstractOrderDao.Order_All(pageParam, search);
        }

        public override SuccessResult<AbstractOrder> Order_ById(long Id)
        {
            return this.abstractOrderDao.Order_ById(Id);
        }

        public override SuccessResult<AbstractOrderDetails> OrderDetail_ById(long OrderId)
        {
            return this.abstractOrderDao.OrderDetail_ById(OrderId);
        }

        public override PagedList<AbstractOrder> AdminOrderDetails_All(PageParam pageParam, string search, long Id, long MasterOrderStatusId, string PaymentStatus, long ShopId, string ShopName, long CityId, string CustomerMobileNumber, string CustomerName, long Amount)
        {
            return this.abstractOrderDao.AdminOrderDetails_All(pageParam, search, Id, MasterOrderStatusId, PaymentStatus, ShopId, ShopName, CityId, CustomerMobileNumber, CustomerName,Amount);
        }

        public override SuccessResult<AbstractOrder> User_OrderDetails(long Id)
        {
            return this.abstractOrderDao.User_OrderDetails(Id);
        }
        public override SuccessResult<AbstractOrder> OrderDetails_Status(long Id, long StatusId)
        {
            return this.abstractOrderDao.OrderDetails_Status(Id, StatusId);
        }
    }
}
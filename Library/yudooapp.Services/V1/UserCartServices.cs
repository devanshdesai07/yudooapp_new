﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class UserCartServices : AbstractUserCartServices
    {
            private AbstractUserCartDao abstractUserCartDao;

        public UserCartServices(AbstractUserCartDao abstractUserCartDao)
        {
            this.abstractUserCartDao = abstractUserCartDao;
        }

        public override PagedList<AbstractUserCart> UserCart_All(PageParam pageParam, string Search,long UserId,long ShopId)
        {
            return this.abstractUserCartDao.UserCart_All(pageParam, Search,UserId,ShopId);
        }

        public override PagedList<AbstractUserCart> UserCart_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserCartDao.UserCart_ByUserId(pageParam, UserId);
        }

        public override PagedList<AbstractUserCart> UserCart_ByShopId(PageParam pageParam,  long ShopId)
        {
            return this.abstractUserCartDao.UserCart_ByShopId(pageParam, ShopId);
        }
        public override PagedList<AbstractUserCart> UserCart_ByVideoCallId(PageParam pageParam,  long VideoCallId)
        {
            return this.abstractUserCartDao.UserCart_ByVideoCallId(pageParam, VideoCallId);
        }

        public override SuccessResult<AbstractUserCart> UserCart_ById(long Id)
        {
            return this.abstractUserCartDao.UserCart_ById(Id);
        }
        public override SuccessResult<AbstractPersonalUserCart> AddUserCart(AbstractPersonalUserCart abstractPersonalUserCart)
        {
            return this.abstractUserCartDao.AddUserCart(abstractPersonalUserCart);
        }
        public override SuccessResult<AbstractUserCart> UserCart_Upsert(AbstractUserCart abstractUserCart)
        {
            return this.abstractUserCartDao.UserCart_Upsert(abstractUserCart);
        }
        public override SuccessResult<AbstractUserCart> UserCart_Delete(long Id)
        {
            return this.abstractUserCartDao.UserCart_Delete(Id);
        } 
        public override SuccessResult<AbstractUserCart> UserCartDeleteByVideoCallId(long VideoCallId)
        {
            return this.abstractUserCartDao.UserCartDeleteByVideoCallId(VideoCallId);
        } 
        public override PagedList<AbstractUserCart> UserCart_IsSendToCustomer(PageParam pageParam, string Search, long VideoCallId)
        {
            return this.abstractUserCartDao.UserCart_IsSendToCustomer(pageParam,Search, VideoCallId);
        }
        public override SuccessResult<AbstractUserCart> UserCart_ConfirmOrder(long VideoCallId, decimal ShippingChargeFromAPI, decimal CODChargefromAPI,long IsOnline,string Address, string UserName)
        {
            return this.abstractUserCartDao.UserCart_ConfirmOrder(VideoCallId, ShippingChargeFromAPI, CODChargefromAPI, IsOnline, Address, UserName);
        }

        public override PagedList<AbstractUserCart> UserCart_IsVisibleToCustomer(PageParam pageParam, string Search, long UserCartMasterId)
        {
            return this.abstractUserCartDao.UserCart_IsVisibleToCustomer(pageParam,Search,UserCartMasterId);
        }

        public override SuccessResult<AbstractUserCart> UserCart_IsSendToOrder(long UserCartMasterId)
        {
            return this.abstractUserCartDao.UserCart_IsSendToOrder(UserCartMasterId);
        }
        //public override SuccessResult<AbstractUserCart> UserCart_Details(long Id)
        //{
        //    return this.abstractUserCartDao.UserCart_Details(Id);
        //}
    }
}

﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class GoodRatingSuggestionsServices : AbstractGoodRatingSuggestionsServices
    {
        private AbstractGoodRatingSuggestionsDao abstractGoodRatingSuggestionsDao;

        public GoodRatingSuggestionsServices(AbstractGoodRatingSuggestionsDao abstractGoodRatingSuggestionsDao)
        {
            this.abstractGoodRatingSuggestionsDao = abstractGoodRatingSuggestionsDao;
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Upsert(AbstractGoodRatingSuggestions abstractGoodRatingSuggestions)
        {
            return this.abstractGoodRatingSuggestionsDao.GoodRatingSuggestions_Upsert(abstractGoodRatingSuggestions);
        }

        public override PagedList<AbstractGoodRatingSuggestions> GoodRatingSuggestions_All(PageParam pageParam, string Search)
        {
            return this.abstractGoodRatingSuggestionsDao.GoodRatingSuggestions_All(pageParam, Search);
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_ById(long Id)
        {
            return this.abstractGoodRatingSuggestionsDao.GoodRatingSuggestions_ById(Id);
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_ActInActive(long Id)
        {
            return this.abstractGoodRatingSuggestionsDao.GoodRatingSuggestions_ActInActive(Id);
        }

        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Delete(long Id)
        {
            return this.abstractGoodRatingSuggestionsDao.GoodRatingSuggestions_Delete(Id);
        }
        public override SuccessResult<AbstractGoodRatingSuggestions> GoodRatingSuggestions_Update_DisplayOrder(long Id, bool IsUp)
        {
            return this.abstractGoodRatingSuggestionsDao.GoodRatingSuggestions_Update_DisplayOrder(Id, IsUp);
        }
    }
}

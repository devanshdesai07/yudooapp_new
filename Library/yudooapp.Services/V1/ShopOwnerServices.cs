﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopOwnerServices : AbstractShopOwnerServices
    {
        private AbstractShopOwnerDao abstractShopOwnerDao;

        public ShopOwnerServices(AbstractShopOwnerDao abstractShopOwnerDao)
        {
            this.abstractShopOwnerDao = abstractShopOwnerDao;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_Upsert(AbstractShopOwner abstractShopOwner)
        {
            return this.abstractShopOwnerDao.ShopOwner_Upsert(abstractShopOwner); ;
        }

        public override SuccessResult<AbstractShopAudit> ShopAudit_Upsert(AbstractShopAudit abstractShopAudit)
        {
            return this.abstractShopOwnerDao.ShopAudit_Upsert(abstractShopAudit); ;
        }

        public override SuccessResult<AbstractShopAudit> ShopAudit_DeleteMedia(string Url, int Type, long ShopId)
        {
            return this.abstractShopOwnerDao.ShopAudit_DeleteMedia(Url, Type, ShopId);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_UpdateCommision(long Id,decimal YudooCommission)
        {
            return this.abstractShopOwnerDao.ShopOwner_UpdateCommision(Id, YudooCommission); 
        }

        public override PagedList<AbstractShopOwner> ShopOwner_All(PageParam pageParam, string search, long CityId, long SpecialityId, long UserId, long CuratedListId=0, long ShopId=0)
        {
            return this.abstractShopOwnerDao.ShopOwner_All(pageParam, search, CityId, SpecialityId, UserId,CuratedListId,ShopId);
        }
        public override PagedList<AbstractShopOwner> ShopEmployee_ByParentId(PageParam pageParam, long ParentId = 0)
        {
            return this.abstractShopOwnerDao.ShopEmployee_ByParentId(pageParam, ParentId);
        }

        public override PagedList<AbstractShopOwner> AdminShopOwnerDetails_All(PageParam pageParam, string search, long CityId, long CategoryId, string AllCategory, long ShopRatingsId, long ShopStatus, int? DraftStatus)
        {
            return this.abstractShopOwnerDao.AdminShopOwnerDetails_All(pageParam, search, CityId, CategoryId, AllCategory, ShopRatingsId, ShopStatus, DraftStatus);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_ById(long Id, long UserId)
        {
            return this.abstractShopOwnerDao.ShopOwner_ById(Id, UserId);
        }

        public override SuccessResult<AbstractShopAudit> ShopAudit_ShopId(long ShopId)
        {
            return this.abstractShopOwnerDao.ShopAudit_ShopId(ShopId);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_TagsByShopId(long ShopId)
        {
            return this.abstractShopOwnerDao.ShopOwner_TagsByShopId(ShopId);
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_ActInAct(long Id)
        {
            return this.abstractShopOwnerDao.ShopOwner_ActInAct(Id);
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_IsRating(bool IsRating)
        {
            return this.abstractShopOwnerDao.ShopOwner_IsRating(IsRating);
        }
        public override SuccessResult<AbstractShopOwner> AddShopOwnerPickUpId(long ShopOwnerId, string PickUpId)
        {
            return this.abstractShopOwnerDao.AddShopOwnerPickUpId(ShopOwnerId, PickUpId);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_Delete(long Id, long DeletedBy)
        {
            return this.abstractShopOwnerDao.ShopOwner_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_ShopEmployeeActInAct(bool IsActive, long ShopOwnerId= 0, long UserId=0)
        {
            return this.abstractShopOwnerDao.ShopOwner_ShopEmployeeActInAct(IsActive,ShopOwnerId, UserId);
        }

        public override SuccessResult<AbstractShopOwner> CheckSeller_Exists(string ShopOwnerName, string ShopName, string MobileNumber)
        {
            return this.abstractShopOwnerDao.CheckSeller_Exists(ShopOwnerName, ShopName, MobileNumber);
        }

        public override SuccessResult<AbstractShopOwner> CheckSeller_Exists_ById(long Id)
        {
            return this.abstractShopOwnerDao.CheckSeller_Exists_ById(Id);
        }


        public override SuccessResult<AbstractShopOwner> CheckSeller_Exists_ByMobileNumberAndById(long Id, string Mobilenumber, string DeviceToken)
        {
            return this.abstractShopOwnerDao.CheckSeller_Exists_ByMobileNumberAndById(Id, Mobilenumber, DeviceToken);
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_TagsUpdate(long Id, string Tags)
        {
            return this.abstractShopOwnerDao.ShopOwner_TagsUpdate(Id, Tags);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_SendOTP(string Mobilenumber,long otp)
        {
            return this.abstractShopOwnerDao.ShopOwner_SendOTP(Mobilenumber, otp);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_VerifyOtp(string Mobilenumber, long otp)
        {
            return this.abstractShopOwnerDao.ShopOwner_VerifyOtp(Mobilenumber, otp);
        }



        public override PagedList<AbstractShopOwner> Shop_Orders(PageParam pageParam, string search, long ShopId, string ShopMobileNumber)
        {
            return this.abstractShopOwnerDao.Shop_Orders(pageParam, search, ShopId, ShopMobileNumber);
        }
        public override SuccessResult<AbstractShopOwner> Shop_Overview(int shopId, string OwnerPhoneNo, string shopName,string FilterBy)
        {
            return this.abstractShopOwnerDao.Shop_Overview(shopId, OwnerPhoneNo, shopName, FilterBy);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_UpdateStatus(long Id, long ShopStatus)
        {
            return this.abstractShopOwnerDao.ShopOwner_UpdateStatus(Id,ShopStatus);
        }
        public override bool ShopOwner_Logout(long Id)
        {
            return this.abstractShopOwnerDao.ShopOwner_Logout(Id);
        }

        public override bool MasterOnline_Status(bool Status)
        {
            return this.abstractShopOwnerDao.MasterOnline_Status(Status);
        }

        public override PagedList<AbstractShopOwner> ShopOwner_Specialities_All(PageParam pageParam, long CityId, long SubCategoryId)
        {
            return this.abstractShopOwnerDao.ShopOwner_Specialities_All(pageParam, CityId, SubCategoryId);
        }
        public override SuccessResult<AbstractShopOwner> ShopOwner_OnlineOffline(long Id, bool IsOnline)
        {
            return this.abstractShopOwnerDao.ShopOwner_OnlineOffline(Id, IsOnline);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_IsApprovedByAdmin(long Id,long Status)
        {
            return this.abstractShopOwnerDao.ShopOwner_IsApprovedByAdmin(Id,Status);
        }

        public override SuccessResult<AbstractShopOwnerRights> ShopOwnerRights_Upsert(AbstractShopOwnerRights abstractShopOwnerRights)
        {
            return this.abstractShopOwnerDao.ShopOwnerRights_Upsert(abstractShopOwnerRights); ;
        }
        public override SuccessResult<AbstractShopOwnerRights> ShopOwnerRights_BySOU(long ShopId, long UserId)
        {
            return this.abstractShopOwnerDao.ShopOwnerRights_BySOU(ShopId, UserId);
        }
        public override PagedList<AbstractShopOwner> ShopOwner_ByCallLog(PageParam pageParam, string search, long UserId)
        {
            return this.abstractShopOwnerDao.ShopOwner_ByCallLog(pageParam, search, UserId);
        }
        public override SuccessResult<AbstractShopOwner> AddressKey_Update(long ShopId, string AddressKey,long AddressCount,long Type)
        {
            return this.abstractShopOwnerDao.AddressKey_Update(ShopId, AddressKey, AddressCount, Type);
        }
    }
}
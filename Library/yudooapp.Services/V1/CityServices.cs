﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class CityServices : AbstractCityServices
    {
        private AbstractCityDao abstractCityDao;

        public CityServices(AbstractCityDao abstractCityDao)
        {
            this.abstractCityDao = abstractCityDao;
        }

        public override SuccessResult<AbstractCity> City_Upsert(AbstractCity abstractCity)
        {
            return this.abstractCityDao.City_Upsert(abstractCity);
        }

        public override PagedList<AbstractCity> City_All(PageParam pageParam, string search)
        {
            return this.abstractCityDao.City_All(pageParam, search);
        }

        public override SuccessResult<AbstractCity> City_ById(long Id)
        {
            return this.abstractCityDao.City_ById(Id);
        }

        public override PagedList<AbstractCity> City_ByStateId(PageParam pageParam, long StateId)
        {
            return this.abstractCityDao.City_ByStateId(pageParam, StateId);
        }

        public override SuccessResult<AbstractCity> City_Delete(long Id)
        {
            return this.abstractCityDao.City_Delete(Id);
        }

    }

}
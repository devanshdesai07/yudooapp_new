﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class CategoryServices : AbstractCategoryServices
    {
        private AbstractCategoryDao abstractCategoryDao;

        public CategoryServices(AbstractCategoryDao abstractCategoryDao)
        {
            this.abstractCategoryDao = abstractCategoryDao;
        }

        

        public override PagedList<AbstractCategory> Category_All(PageParam pageParam, string search)
        {
            return this.abstractCategoryDao.Category_All(pageParam, search);
        }

        public override SuccessResult<AbstractCategory> Category_ById(long Id)
        {
            return this.abstractCategoryDao.Category_ById(Id);
        }

        public override SuccessResult<AbstractCategory> Category_Delete(long Id)
        {
            return this.abstractCategoryDao.Category_Delete(Id);
        }

        public override SuccessResult<AbstractCategory> Category_Upsert(AbstractCategory abstractCategory)
        {
            return this.abstractCategoryDao.Category_Upsert(abstractCategory);
        }
        public override SuccessResult<AbstractCategory> Category_Update_DisplayOrder(long Id, bool IsUp)
        {
            return this.abstractCategoryDao.Category_Update_DisplayOrder(Id, IsUp);
        }
    }

}
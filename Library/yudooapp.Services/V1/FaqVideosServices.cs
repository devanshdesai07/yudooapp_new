﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Data.Contract;

namespace yudooapp.Services.V1
{
    public class FaqVideosServices : AbstractFaqVideosServices
    {
        private AbstractFaqVideosDao _abstractFaqVideosDao = null;
        public FaqVideosServices(AbstractFaqVideosDao _abstractFaqVideosDao)
        {
            this._abstractFaqVideosDao = _abstractFaqVideosDao;
        }
        public override SuccessResult<AbstractFaqVideos> FaqVideo_ActInAct(long Id)
        {
            return _abstractFaqVideosDao.FaqVideo_ActInAct(Id);
        }

        public override PagedList<AbstractFaqVideos> FaqVideo_All(PageParam pageParam, string search)
        {
            return _abstractFaqVideosDao.FaqVideo_All(pageParam,search);
        }

        public override SuccessResult<AbstractFaqVideos> FaqVideo_ById(long Id)
        {
            return _abstractFaqVideosDao.FaqVideo_ById(Id);
        }

        public override SuccessResult<AbstractFaqVideos> FaqVideo_Delete(long Id)
        {
            return _abstractFaqVideosDao.FaqVideo_Delete(Id);
        }

        public override SuccessResult<AbstractFaqVideos> FaqVideo_Upsert(AbstractFaqVideos abstractFaqVideos)
        {
            return _abstractFaqVideosDao.FaqVideo_Upsert(abstractFaqVideos);
        }
    }
}

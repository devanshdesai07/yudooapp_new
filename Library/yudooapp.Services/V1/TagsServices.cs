﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class TagsServices : AbstractTagsServices
    {
        private AbstractTagsDao abstractTagsDao;

        public TagsServices(AbstractTagsDao abstractTagsDao)
        {
            this.abstractTagsDao = abstractTagsDao;
        }

        public override SuccessResult<AbstractTags> Tags_Upsert(AbstractTags abstractTags)
        {
            return this.abstractTagsDao.Tags_Upsert(abstractTags); ;
        }

        public override PagedList<AbstractTags> Tags_All(PageParam pageParam, string search)
        {
            return this.abstractTagsDao.Tags_All(pageParam, search);
        }

        public override SuccessResult<AbstractTags> Tags_ById(long Id)
        {
            return this.abstractTagsDao.Tags_ById(Id);
        }

        
        public override SuccessResult<AbstractTags> Tags_Delete(long Id)
        {
            return this.abstractTagsDao.Tags_Delete(Id);
        }

    }
}

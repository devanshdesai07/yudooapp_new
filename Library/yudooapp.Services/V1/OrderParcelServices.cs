﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class OrderParcelServices : AbstractOrderParcelServices
    {
        private AbstractOrderParcelDao abstractOrderParcelDao;

        public OrderParcelServices(AbstractOrderParcelDao abstractOrderParcelDao)
        {
            this.abstractOrderParcelDao = abstractOrderParcelDao;
        }

        public override SuccessResult<AbstractOrderParcel> OrderParcel_Upsert(AbstractOrderParcel abstractOrderParcel)
        {
            return this.abstractOrderParcelDao.OrderParcel_Upsert(abstractOrderParcel);
        }

        public override PagedList<AbstractOrderParcel> OrderParcel_ByOrderId(long OrderId)
        {
            return this.abstractOrderParcelDao.OrderParcel_ByOrderId(OrderId);
        }

        public override PagedList<AbstractOrderParcelProduct> OrderParcelProduct_All(PageParam pageParam, string search , long OrderId)
        {
            return this.abstractOrderParcelDao.OrderParcelProduct_All(pageParam, search, OrderId);
        }
        public override SuccessResult<AbstractOrderParcel> OrderParcels_Delete(long Id, long DeletedBy)
        {
            return this.abstractOrderParcelDao.OrderParcels_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractOrderParcel> OrderParcel_ById(long Id)
        {
            return this.abstractOrderParcelDao.OrderParcel_ById(Id);
        }

        public override SuccessResult<AbstractCheckCreateShipment> Check_CreateShipment(long OrderId)
        {
            return this.abstractOrderParcelDao.Check_CreateShipment(OrderId);
        }

        //Shipment method
        public override PagedList<AbstarctCreateShipment> OrderParcelShipment_ByOrderId(PageParam pageParam, string search, long OrderId)
        {
            return this.abstractOrderParcelDao.OrderParcelShipment_ByOrderId(pageParam, search, OrderId);
        }

    }

}
﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class CuratedListServices : AbstractCuratedListServices
    {
        private AbstractCuratedListDao abstractCuratedListDao;

        public CuratedListServices(AbstractCuratedListDao abstractCuratedListDao)
        {
            this.abstractCuratedListDao = abstractCuratedListDao;
        }

        public override SuccessResult<AbstractCuratedList> CuratedList_Upsert(AbstractCuratedList abstractCuratedList)
        {
            return this.abstractCuratedListDao.CuratedList_Upsert(abstractCuratedList);
        }

        public override PagedList<AbstractCuratedList> CuratedList_All(PageParam pageParam, string search)
        {
            return this.abstractCuratedListDao.CuratedList_All(pageParam, search);
        }

        public override SuccessResult<AbstractCuratedList> CuratedList_ById(long Id)
        {
            return this.abstractCuratedListDao.CuratedList_ById(Id);
        }

        

        public override SuccessResult<AbstractCuratedList> CuratedList_ActInAct(long Id)
        {
            return this.abstractCuratedListDao.CuratedList_ActInAct(Id);
        }

        public override SuccessResult<AbstractCuratedList> CuratedList_Delete(long Id, long DeletedBy)
        {
            return this.abstractCuratedListDao.CuratedList_Delete(Id, DeletedBy);
        }

    }

}
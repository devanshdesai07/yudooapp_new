﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserSearchServices : AbstractUserSearchServices
    {
        private AbstractUserSearchDao abstractUserSearchDao;

        public UserSearchServices(AbstractUserSearchDao abstractUserSearchDao)
        {
            this.abstractUserSearchDao = abstractUserSearchDao;
        }

        public override SuccessResult<AbstractUserSearch> UserSearch_Upsert(AbstractUserSearch abstractUserSearch)
        {
            return this.abstractUserSearchDao.UserSearch_Upsert(abstractUserSearch);
        }

        public override PagedList<AbstractUserSearch> UserSearch_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserSearchDao.UserSearch_ByUserId(pageParam , UserId);
        }

        public override PagedList<AbstractUserSearch> UserSearch_All(PageParam pageParam, string Search ,long UserId)
        {
            return this.abstractUserSearchDao.UserSearch_All(pageParam,Search, UserId);
        }

        public override PagedList<AbstractUserSearch> UserSearch_Recentsearch(PageParam pageParam, string Search, long UserId)
        {
            return this.abstractUserSearchDao.UserSearch_Recentsearch(pageParam, Search, UserId);
        }

        public override PagedList<AbstractUserSearch> UserSearch_TrendingSearches(PageParam pageParam, string Search)
        {
            return this.abstractUserSearchDao.UserSearch_TrendingSearches(pageParam, Search);
        }

        public override SuccessResult<AbstractUserSearch> UserSearch_ById(long Id)
        {
            return this.abstractUserSearchDao.UserSearch_ById(Id);
        }


        public override SuccessResult<AbstractUserSearch> UserSearch_ActInact(long Id)
        {
            return this.abstractUserSearchDao.UserSearch_ActInact(Id);
        }


    }

}
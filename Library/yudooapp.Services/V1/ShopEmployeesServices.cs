﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class ShopEmployeesServices : AbstractShopEmployeesServices
    {
        private AbstractShopEmployeesDao abstractShopEmployeesDao;

        public ShopEmployeesServices(AbstractShopEmployeesDao abstractShopEmployeesDao)
        {
            this.abstractShopEmployeesDao = abstractShopEmployeesDao;
        }

        public override PagedList<AbstractShopEmployees> ShopEmployees_All(PageParam pageParam, string Search, long ShopId)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_All(pageParam, Search, ShopId);
        }

        public override PagedList<AbstractShopEmployees> ShopEmployees_ByShopId(PageParam pageParam,  long ShopId)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_ByShopId(pageParam, ShopId);
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_ById(long Id)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_ById(Id);
        }
        public override SuccessResult<AbstractShopEmployees> ShopEmployees_Upsert(AbstractShopEmployees abstractShopEmployees)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_Upsert(abstractShopEmployees);
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_ActInact(long Id)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_ActInact(Id);
        }
        public override SuccessResult<AbstractShopEmployees> ShopEmployees_IsOnline(long Id)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_IsOnline(Id);
        }

        public override SuccessResult<AbstractShopEmployees> ShopEmployees_Delete(long Id)
        {
            return this.abstractShopEmployeesDao.ShopEmployees_Delete(Id);
        }
    }
}

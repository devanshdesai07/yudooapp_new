﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopCategoriesServices : AbstractShopCategoriesServices
    {
        private AbstractShopCategoriesDao abstractShopCategoriesDao;

        public ShopCategoriesServices(AbstractShopCategoriesDao abstractShopCategoriesDao)
        {
            this.abstractShopCategoriesDao = abstractShopCategoriesDao;
        }

        public override SuccessResult<AbstractShopCategories> ShopCategories_Upsert(AbstractShopCategories abstractShopCategories)
        {
            return this.abstractShopCategoriesDao.ShopCategories_Upsert(abstractShopCategories);
        }

        public override PagedList<AbstractShopCategories> ShopCategories_All(PageParam pageParam, string search)
        {
            return this.abstractShopCategoriesDao.ShopCategories_All(pageParam, search);
        }

        public override SuccessResult<AbstractShopCategories> ShopCategories_ById(long Id)
        {
            return this.abstractShopCategoriesDao.ShopCategories_ById(Id);
        }

        public override PagedList<AbstractShopCategories> ShopCategories_ByShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractShopCategoriesDao.ShopCategories_ByShopId(pageParam, ShopId);
        }

        public override PagedList<AbstractShopCategories> ShopCategories_ByCategoryId(PageParam pageParam, long CategoryId)
        {
            return this.abstractShopCategoriesDao.ShopCategories_ByCategoryId(pageParam, CategoryId);
        }

        public override PagedList<AbstractShopCategories> ShopCategories_BySubCategoryId(PageParam pageParam, long SubCategoryId)
        {
            return this.abstractShopCategoriesDao.ShopCategories_BySubCategoryId(pageParam, SubCategoryId);
        }

        public override SuccessResult<AbstractShopCategories> ShopCategories_Delete(long Id)
        {
            return this.abstractShopCategoriesDao.ShopCategories_Delete(Id);
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ReviewServices : AbstractReviewServices
    {
        private AbstractReviewDao abstractReviewDao;

        public ReviewServices(AbstractReviewDao abstractReviewDao)
        {
            this.abstractReviewDao = abstractReviewDao;
        }

        public override PagedList<AbstractReview> Review_All(PageParam pageParam, string search , AbstractReview abstractReview)
        {
            return this.abstractReviewDao.Review_All(pageParam, search, abstractReview);
        }


        public override PagedList<AbstractReview> Review_ByCustomerId(PageParam pageParam, string search, AbstractReview abstractReview)
        {
            return this.abstractReviewDao.Review_ByCustomerId(pageParam, search, abstractReview);
        }


        public override SuccessResult<AbstractReview> Review_Upsert(AbstractReview abstractReview)
        {
            return this.abstractReviewDao.Review_Upsert(abstractReview);
        }

    }

}
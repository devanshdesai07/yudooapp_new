﻿using System.Collections.Generic;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserPermissionsServices : AbstractUserPermissionsServices
    {
        private AbstractUserPermissionsDao AbstractUserPermissionsDao;

        public UserPermissionsServices(AbstractUserPermissionsDao AbstractUserPermissionsDao)
        {
            this.AbstractUserPermissionsDao = AbstractUserPermissionsDao;
        }

        public override SuccessResult<AbstractUserPermissions> UserPermissions_Upsert(List<AbstractUserPermissions> AbstractUserPermissions)
        {
            return this.AbstractUserPermissionsDao.UserPermissions_Upsert(AbstractUserPermissions);
        }

        public override PagedList<AbstractUserPermissions> UserPermissions_All(PageParam pageParam, string search)
        {
            return this.AbstractUserPermissionsDao.UserPermissions_All(pageParam, search);
        }
        public override PagedList<AbstractUserPermissions> UserPermissions_ByAdminId(PageParam pageParam, string search, int AdminTypeId)
        {
            return this.AbstractUserPermissionsDao.UserPermissions_ByAdminId(pageParam, search, AdminTypeId);
        }
        public override PagedList<AbstractUserPermissions> UserPermissions_All_Active(PageParam pageParam, string search)
        {
            return this.AbstractUserPermissionsDao.UserPermissions_All_Active(pageParam, search);
        }

        public override SuccessResult<AbstractUserPermissions> UserPermissions_ById(long Id)
        {
            return this.AbstractUserPermissionsDao.UserPermissions_ById(Id);
        }

        public override SuccessResult<AbstractUserPermissions> UserPermissions_ActInAct(long Id, bool IsActive)
        {
            return this.AbstractUserPermissionsDao.UserPermissions_ActInAct(Id, IsActive);
        }
    }

}
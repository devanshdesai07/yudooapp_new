﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class SubCategoryServices : AbstractSubCategoryServices
    {
        private AbstractSubCategoryDao abstractSubCategoryDao;

        public SubCategoryServices(AbstractSubCategoryDao abstractSubCategoryDao)
        {
            this.abstractSubCategoryDao = abstractSubCategoryDao;
        }



        public override PagedList<AbstractSubCategory> SubCategory_All(PageParam pageParam, string search)
        {
            return this.abstractSubCategoryDao.SubCategory_All(pageParam, search);
        }

        public override PagedList<AbstractSubCategory> SubCategory_LookUpCategoryId(PageParam pageParam, string search,long CategoryId)
        {
            return this.abstractSubCategoryDao.SubCategory_LookUpCategoryId(pageParam, search , CategoryId);
        }

        public override SuccessResult<AbstractSubCategory> SubCategory_ById(long Id)
        {
            return this.abstractSubCategoryDao.SubCategory_ById(Id);
        }




    }

}
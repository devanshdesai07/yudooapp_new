﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopTimgingsServices : AbstractShopTimgingsServices
    {
        private AbstractShopTimgingsDao abstractShopTimgingsDao;

        public ShopTimgingsServices(AbstractShopTimgingsDao abstractShopTimgingsDao)
        {
            this.abstractShopTimgingsDao = abstractShopTimgingsDao;
        }

        public override SuccessResult<AbstractShopTimingJson> ShopTimgings_Upsert(AbstractShopTimingJson abstractShopTimingJson)
        {
            return this.abstractShopTimgingsDao.ShopTimgings_Upsert(abstractShopTimingJson); ;
        }

        public override PagedList<AbstractShopTimgings> ShopTimgings_All(PageParam pageParam, string search, long ShopId)
        {
            return this.abstractShopTimgingsDao.ShopTimgings_All(pageParam, search , ShopId);
        }

        public override PagedList<AbstractShopTimgings> ShopTimgings_ShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractShopTimgingsDao.ShopTimgings_ShopId(pageParam, ShopId);
        }
        public override SuccessResult<AbstractShopTimgings> ShopTimgings_ById(long Id)
        {
            return this.abstractShopTimgingsDao.ShopTimgings_ById(Id);
        }

        public override SuccessResult<AbstractShopTimgings> ShopTimgings_IsClosedIsopen(long Id)
        {
            return this.abstractShopTimgingsDao.ShopTimgings_IsClosedIsopen(Id);
        }

        public override SuccessResult<AbstractShopTimgings> ShopTimgings_Delete(long Id)
        {
            return this.abstractShopTimgingsDao.ShopTimgings_Delete(Id);
        }

    }

}
﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class OfferStatusServices : AbstractOfferStatusServices
    {
        private AbstractOfferStatusDao abstractOfferStatusDao;

        public OfferStatusServices(AbstractOfferStatusDao abstractOfferStatusDao)
        {
            this.abstractOfferStatusDao = abstractOfferStatusDao;
        }

        public override SuccessResult<AbstractOfferStatus> OfferStatus_Upsert(AbstractOfferStatus abstractOfferStatus)
        {
            return this.abstractOfferStatusDao.OfferStatus_Upsert(abstractOfferStatus); ;
        }

        public override PagedList<AbstractOfferStatus> OfferStatus_All(PageParam pageParam, string search)
        {
            return this.abstractOfferStatusDao.OfferStatus_All(pageParam, search);
        }

        public override SuccessResult<AbstractOfferStatus> OfferStatus_ById(long Id)
        {
            return this.abstractOfferStatusDao.OfferStatus_ById(Id);
        }

        public override SuccessResult<AbstractOfferStatus> OfferStatus_Delete(long Id)
        {
            return this.abstractOfferStatusDao.OfferStatus_Delete(Id);
        }
    }
}

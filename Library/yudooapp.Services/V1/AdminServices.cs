﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class AdminServices : AbstractAdminServices
    {
        private AbstractAdminDao abstractAdminDao;

        public AdminServices(AbstractAdminDao abstractAdminDao)
        {
            this.abstractAdminDao = abstractAdminDao;
        }

        public override PagedList<AbstractAdmin> Admin_All(PageParam pageParam, string search)
        {
            return this.abstractAdminDao.Admin_All(pageParam, search);
        }

        public override SuccessResult<AbstractAdmin> Admin_ById(long Id)
        {
            return this.abstractAdminDao.Admin_ById(Id);
        }

       
        public override bool Admin_Logout(long Id)
        {
            return this.abstractAdminDao.Admin_Logout(Id);
        }

        public override SuccessResult<AbstractAdmin> Admin_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            return this.abstractAdminDao.Admin_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
        }

        public override SuccessResult<AbstractAdmin> Admin_Upsert(AbstractAdmin abstractAdmin)
        {
            return this.abstractAdminDao.Admin_Upsert(abstractAdmin);
        }

        public override SuccessResult<AbstractAdmin> Admin_Login(string Email, string Password)
        {
            return this.abstractAdminDao.Admin_Login(Email, Password);
        }

        public override SuccessResult<AbstractAdmin> Admin_Delete(long Id)
        {
            return this.abstractAdminDao.Admin_Delete(Id);
        }

        public override SuccessResult<AbstractAdmin> Admin_ActInAct(long Id)
        {
            return this.abstractAdminDao.Admin_ActInAct(Id);
        }

        public override SuccessResult<AbstractAdmin> ForGotPassword(string Email, string OTP)
        {
            return this.abstractAdminDao.ForGotPassword(Email, OTP);
        }

        public override SuccessResult<AbstractAdmin> Admin_ByEmail(string Email)
        {
            return this.abstractAdminDao.Admin_ByEmail(Email);
        }
        public override SuccessResult<AbstractAdmin> Admin_IsCallAdmin(long Id, bool CallStatus)
        {
            return this.abstractAdminDao.Admin_IsCallAdmin(Id, CallStatus);
        }

        public override SuccessResult<AbstractAdmin> ResetPassword(string email, string Password, string ConfirmPassword)
        {
            return this.abstractAdminDao.ResetPassword(email,Password,ConfirmPassword);
        }
    }

}
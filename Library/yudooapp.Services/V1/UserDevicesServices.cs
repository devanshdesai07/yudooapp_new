﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserDevicesServices : AbstractUserDevicesServices
    {
        private AbstractUserDevicesDao abstractUserDevicesDao;

        public UserDevicesServices(AbstractUserDevicesDao abstractUserDevicesDao)
        {
            this.abstractUserDevicesDao = abstractUserDevicesDao;
        }

        public override PagedList<AbstractUserDevices> UserDevices_ByUserId(PageParam pageParam, string search, long UserId)
        {
            return this.abstractUserDevicesDao.UserDevices_ByUserId(pageParam, search, UserId);
        }

    }

}

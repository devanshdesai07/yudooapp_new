﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class CustomerCallsServices : AbstractCustomerCallsServices
    {
        private AbstractCustomerCallsDao abstractCustomerCallsDao;

        public CustomerCallsServices(AbstractCustomerCallsDao abstractCustomerCallsDao)
        {
            this.abstractCustomerCallsDao = abstractCustomerCallsDao;
        }

        public override SuccessResult<AbstractCustomerCalls> CustomerCalls_Upsert(AbstractCustomerCalls abstractCustomerCalls)
        {
            return this.abstractCustomerCallsDao.CustomerCalls_Upsert(abstractCustomerCalls); ;
        }

        public override PagedList<AbstractCustomerCalls> CustomerCalls_All(PageParam pageParam, string search)
        {
            return this.abstractCustomerCallsDao.CustomerCalls_All(pageParam, search);
        }

        public override SuccessResult<AbstractCustomerCalls> CustomerCalls_ById(long Id)
        {
            return this.abstractCustomerCallsDao.CustomerCalls_ById(Id);
        }


    }

}
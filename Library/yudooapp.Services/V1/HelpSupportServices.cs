﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class HelpSupportServices : AbstractHelpSupportServices
    {
        private AbstractHelpSupportDao abstractHelpSupportDao;

        public HelpSupportServices(AbstractHelpSupportDao abstractHelpSupportDao)
        {
            this.abstractHelpSupportDao = abstractHelpSupportDao;
        }
        public override PagedList<AbstractHelpSupport> HelpSupport_All(PageParam pageParam, string search)
        {
            return this.abstractHelpSupportDao.HelpSupport_All(pageParam, search);
        }
        public override SuccessResult<AbstractHelpSupport> HelpSupport_Action(long Id,long StatusId)
        {
            return this.abstractHelpSupportDao.HelpSupport_Action(Id, StatusId);
        }
        public override SuccessResult<AbstractHelpSupport> HelpSupport_Upsert(AbstractHelpSupport abstractHelpSupport)
        {
            return this.abstractHelpSupportDao.HelpSupport_Upsert(abstractHelpSupport);
        }
    }

}
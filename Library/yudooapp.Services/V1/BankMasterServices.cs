﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class BankMasterServices : AbstractBankMasterServices
    {
        private AbstractBankMasterDao abstractBankMasterDao;

        public BankMasterServices(AbstractBankMasterDao abstractBankMasterDao)
        {
            this.abstractBankMasterDao = abstractBankMasterDao;
        }

        public override SuccessResult<AbstractBankMaster> BankMaster_Upsert(AbstractBankMaster abstractBankMaster)
        {
            return this.abstractBankMasterDao.BankMaster_Upsert(abstractBankMaster); ;
        }

        public override SuccessResult<AbstractBankMaster> BankMaster_ById(long Id)
        {
            return this.abstractBankMasterDao.BankMaster_ById(Id);
        }

        public override PagedList<AbstractBankMaster> BankMaster_ByShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractBankMasterDao.BankMaster_ByShopId(pageParam, ShopId);
        }

        public override SuccessResult<AbstractBankMaster> BankMaster_ActInAct(long Id)
        {
            return this.abstractBankMasterDao.BankMaster_ActInAct(Id);
        }

    }

}
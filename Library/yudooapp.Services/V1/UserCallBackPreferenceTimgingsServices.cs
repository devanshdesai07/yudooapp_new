﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Data.Contract;


namespace yudooapp.Services.V1
{
    public class UserCallBackPreferenceTimgingsServices : AbstractUserCallBackPreferenceTimgingsServices
    {
        private readonly AbstractUserCallBackPreferenceTimgingsDao _abstractUserCallBackPreferenceTimgingsDao = null;

        public UserCallBackPreferenceTimgingsServices(AbstractUserCallBackPreferenceTimgingsDao _abstractUserCallBackPreferenceTimgingsDao)
        {
            this._abstractUserCallBackPreferenceTimgingsDao = _abstractUserCallBackPreferenceTimgingsDao;
        }

        public override SuccessResult<AbstractUserCallBackPreferenceTimingJson> UserCallBackPreferenceTimgings_Upsert(AbstractUserCallBackPreferenceTimingJson abstractUserCallBackPreferenceTimingJson)
        {
            return _abstractUserCallBackPreferenceTimgingsDao.UserCallBackPreferenceTimgings_Upsert(abstractUserCallBackPreferenceTimingJson);
        }

        public override PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_All(PageParam pageParam, string search, long UserId = 0) 
        {
            return _abstractUserCallBackPreferenceTimgingsDao.UserCallBackPreferenceTimgings_All(pageParam,search,UserId);
        }
        public override SuccessResult<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_ById(long Id)
        {
            return _abstractUserCallBackPreferenceTimgingsDao.UserCallBackPreferenceTimgings_ById(Id);
        }
        public override PagedList<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_ByUserId(PageParam pageParam, long UserId)
        {
            return this._abstractUserCallBackPreferenceTimgingsDao.UserCallBackPreferenceTimgings_ByUserId(pageParam, UserId);
        }
        public override SuccessResult<AbstractUserCallBackPreferenceTimgings> UserCallBackPreferenceTimgings_Delete(long Id) 
        {
            return _abstractUserCallBackPreferenceTimgingsDao.UserCallBackPreferenceTimgings_Delete(Id);
        }

        public override bool UserCallBackPreferenceTimgings_Available(long UserId)
        {
            return _abstractUserCallBackPreferenceTimgingsDao.UserCallBackPreferenceTimgings_Available(UserId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopCallingNotificationServices : AbstractShopCallingNotificationServices
    {
        private AbstractShopCallingNotificationDao abstractShopCallingNotificationDao;

        public ShopCallingNotificationServices(AbstractShopCallingNotificationDao abstractShopCallingNotificationDao)
        {
            this.abstractShopCallingNotificationDao = abstractShopCallingNotificationDao;
        }

        public override SuccessResult<AbstractShopCallingNotification> ShopCallingNotification_Upsert(AbstractShopCallingNotification abstractShopCallingNotification)
        {
            return this.abstractShopCallingNotificationDao.ShopCallingNotification_Upsert(abstractShopCallingNotification);
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;


namespace yudooapp.Services.V1
{
   public class MasterDepartmentShopServices : AbstractMasterDepartmentShopServices
    {
        private AbstractMasterDepartmentShopDao abstractMasterDepartmentShopDao;

        public MasterDepartmentShopServices(AbstractMasterDepartmentShopDao abstractMasterDepartmentShopDao)
        {
            this.abstractMasterDepartmentShopDao = abstractMasterDepartmentShopDao;
        }

        public override PagedList<AbstractMasterDepartmentShop> MasterDepartmentShop_All(PageParam pageParam, string search)
        {
            return this.abstractMasterDepartmentShopDao.MasterDepartmentShop_All(pageParam, search);
        }
        public override SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_ById(long Id)
        {
            return this.abstractMasterDepartmentShopDao.MasterDepartmentShop_ById(Id);
        }
        public override SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_Upsert(AbstractMasterDepartmentShop abstractMasterDepartmentShop)
        {
            return this.abstractMasterDepartmentShopDao.MasterDepartmentShop_Upsert(abstractMasterDepartmentShop);
        }
        public override SuccessResult<AbstractMasterDepartmentShop> MasterDepartmentShop_Delete(long Id)
        {
            return this.abstractMasterDepartmentShopDao.MasterDepartmentShop_Delete(Id);
        }
    }
}

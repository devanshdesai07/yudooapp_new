﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;

namespace yudooapp.Services.V1
{
    public class CallBackServices : AbstractCallBackServices
    {
        private AbstractCallBackDao abstractCallBackDao;

        public CallBackServices(AbstractCallBackDao abstractCallBackDao)
        {
            this.abstractCallBackDao = abstractCallBackDao;
        }

        public override SuccessResult<AbstractCallBack> CallBack_Upsert(AbstractCallBack abstractCallBack)
        {
            return this.abstractCallBackDao.CallBack_Upsert(abstractCallBack); ;
        }
        public override SuccessResult<AbstractUserShopCallLogs> CallBack_CallManage(UserShopCallLogs userShopCallLogs)
        {
            return this.abstractCallBackDao.CallBack_CallManage(userShopCallLogs); ;
        }
        public override SuccessResult<AbstractUserShopCallLogs> RazorPay(UserShopCallLogs userShopCallLogs)
        {
            return this.abstractCallBackDao.RazorPay(userShopCallLogs); ;
        }

        public override PagedList<AbstractCallBack> CallBack_All(PageParam pageParam, string search)
        {
            return this.abstractCallBackDao.CallBack_All(pageParam, search);
        }

        public override SuccessResult<AbstractCallBack> CallBack_ById(long Id)
        {
            return this.abstractCallBackDao.CallBack_ById(Id);
        }


    }

}
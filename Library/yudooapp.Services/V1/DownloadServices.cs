﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class DownloadServices : AbstractDownloadServices
    {
        private AbstractDownloadDao abstractDownloadDao;

        public DownloadServices(AbstractDownloadDao abstractDownloadDao)
        {
            this.abstractDownloadDao = abstractDownloadDao;
        }


        public override PagedList<AbstractDownload> Tables_All(PageParam pageParam, string search)
        {
            return this.abstractDownloadDao.Tables_All(pageParam, search);
        }
    }
}

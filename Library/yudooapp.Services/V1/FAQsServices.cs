﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Data.Contract;

namespace yudooapp.Services.V1
{
    public class FAQsServices : AbstractFAQsServices
    {
        private AbstractFAQsDao _abstractFAQsDao = null;
        public FAQsServices(AbstractFAQsDao _abstractFAQsDao)
        {
            this._abstractFAQsDao = _abstractFAQsDao;
        }
        public override SuccessResult<AbstractFAQs> FAQs_ActInAct(long Id)
        {
            return _abstractFAQsDao.FAQs_ActInAct(Id);
        }

        public override PagedList<AbstractFAQs> FAQs_All(PageParam pageParam, string search)
        {
            return _abstractFAQsDao.FAQs_All(pageParam,search);
        }

        public override SuccessResult<AbstractFAQs> FAQs_ById(long Id)
        {
            return _abstractFAQsDao.FAQs_ById(Id);
        }

        public override SuccessResult<AbstractFAQs> FAQs_Delete(long Id)
        {
            return _abstractFAQsDao.FAQs_Delete(Id);
        }

        public override SuccessResult<AbstractFAQs> FAQs_Upsert(AbstractFAQs abstractFAQs)
        {
            return _abstractFAQsDao.FAQs_Upsert(abstractFAQs);
        }
    }
}

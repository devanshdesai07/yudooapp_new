﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class UsersFAQServices : AbstractUsersFAQServices
    {
        private AbstractUsersFAQDao abstractUsersFAQDao;

        public UsersFAQServices(AbstractUsersFAQDao abstractUsersFAQDao)
        {
            this.abstractUsersFAQDao = abstractUsersFAQDao;
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_Upsert(AbstractUsersFAQ abstractUsersFAQ)
        {
            return this.abstractUsersFAQDao.UsersFAQ_Upsert(abstractUsersFAQ);
        }

        public override PagedList<AbstractUsersFAQ> UsersFAQ_All(PageParam pageParam, string Search)
        {
            return this.abstractUsersFAQDao.UsersFAQ_All(pageParam, Search);
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_ById(long Id)
        {
            return this.abstractUsersFAQDao.UsersFAQ_ById(Id);
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_ActInAct(long Id)
        {
            return this.abstractUsersFAQDao.UsersFAQ_ActInAct(Id);
        }

        public override SuccessResult<AbstractUsersFAQ> UsersFAQ_Delete(long Id)
        {
            return this.abstractUsersFAQDao.UsersFAQ_Delete(Id);
        }
    }
}

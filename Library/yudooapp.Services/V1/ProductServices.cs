﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ProductsServices : AbstractProductsServices
    {
        private AbstractProductsDao abstractProductsDao;

        public ProductsServices(AbstractProductsDao abstractProductsDao)
        {
            this.abstractProductsDao = abstractProductsDao;
        }

        public override PagedList<AbstractProducts> Products_All(PageParam pageParam, string search , AbstractProducts abstractProducts)
        {
            return this.abstractProductsDao.Products_All(pageParam, search, abstractProducts);
        }

       
        public override SuccessResult<AbstractProducts> Products_ById(long Id)
        {
            return this.abstractProductsDao.Products_ById(Id);
        }

        public override SuccessResult<AbstractProducts> Products_ActInAct(long Id)
        {
            return this.abstractProductsDao.Products_ActInAct(Id);
        }

        public override SuccessResult<AbstractProducts> Products_Upsert(AbstractProducts abstractProducts)
        {
            return this.abstractProductsDao.Products_Upsert(abstractProducts);
        }

       

        public override SuccessResult<AbstractProducts> Products_Delete(long Id)
        {
            return this.abstractProductsDao.Products_Delete(Id);
        }
    }

}
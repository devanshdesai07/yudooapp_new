﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class MasterCallStatusServices : AbstractMasterCallStatusServices
    {
        private AbstractMasterCallStatusDao abstractMasterCallStatusDao;

        public MasterCallStatusServices(AbstractMasterCallStatusDao abstractMasterCallStatusDao)
        {
            this.abstractMasterCallStatusDao = abstractMasterCallStatusDao;
        }

        public override SuccessResult<AbstractMasterCallStatus> MasterCallStatus_Upsert(AbstractMasterCallStatus abstractMasterCallStatus)
        {
            return this.abstractMasterCallStatusDao.MasterCallStatus_Upsert(abstractMasterCallStatus);
        }

        public override PagedList<AbstractMasterCallStatus> MasterCallStatus_All(PageParam pageParam, string search)
        {
            return this.abstractMasterCallStatusDao.MasterCallStatus_All(pageParam, search);
        }

        

    }

}
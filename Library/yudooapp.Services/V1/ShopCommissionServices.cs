﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopCommissionServices : AbstractShopCommissionServices
    {
        private AbstractShopCommissionDao abstractShopCommissionDao;

        public ShopCommissionServices(AbstractShopCommissionDao abstractShopCommissionDao)
        {
            this.abstractShopCommissionDao = abstractShopCommissionDao;
        }

        public override SuccessResult<AbstractShopCommission> ShopCommission_Upsert(AbstractShopCommission abstractShopCommission)
        {
            return this.abstractShopCommissionDao.ShopCommission_Upsert(abstractShopCommission);
        }

        public override PagedList<AbstractShopCommission> ShopCommission_All(PageParam pageParam, string search, long ShopId)
        {
            return this.abstractShopCommissionDao.ShopCommission_All(pageParam, search, ShopId);
        }

        public override SuccessResult<AbstractShopCommission> ShopCommission_ById(long Id)
        {
            return this.abstractShopCommissionDao.ShopCommission_ById(Id);
        }

        public override PagedList<AbstractShopCommission> ShopCommission_ByShopOwnerId(PageParam pageParam, long ShopId)
        {
            return this.abstractShopCommissionDao.ShopCommission_ByShopOwnerId(pageParam , ShopId);
        }

        public override SuccessResult<AbstractShopCommission> ShopCommission_ActInAct(long Id)
        {
            return this.abstractShopCommissionDao.ShopCommission_ActInAct(Id);
        }

        public override SuccessResult<AbstractShopCommission> ShopCommission_Delete(long Id)
        {
            return this.abstractShopCommissionDao.ShopCommission_Delete(Id);
        }

    }

}
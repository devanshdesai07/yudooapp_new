﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShipmentServices : AbstractShipmentServices
    {
        private AbstractShipmentDao abstractShipmentDao;

        public ShipmentServices(AbstractShipmentDao abstractShipmentDao)
        {
            this.abstractShipmentDao = abstractShipmentDao;
        }

        public override SuccessResult<AbstractShipment> Shipment_Upsert(AbstractShipment abstractShipment)
        {
            return this.abstractShipmentDao.Shipment_Upsert(abstractShipment); ;
        }


        public override PagedList<AbstractShipment> Shipment_ByOrderId(long OrderId, PageParam pageParam)
        {
            return this.abstractShipmentDao.Shipment_ByOrderId(OrderId, pageParam);
        }
        public override PagedList<AbstractShipment> Shipment_ByParcelId(long ParcelId, PageParam pageParam)
        {
            return this.abstractShipmentDao.Shipment_ByParcelId(ParcelId, pageParam);
        }


        public override SuccessResult<AbstractShipment> Shipment_ById(long Id)
        {
            return this.abstractShipmentDao.Shipment_ById(Id);
        }


        public override SuccessResult<AbstractShipment> Shipment_StatusChange(long Id)
        {
            return this.abstractShipmentDao.Shipment_StatusChange(Id);
        }

        
    }

}
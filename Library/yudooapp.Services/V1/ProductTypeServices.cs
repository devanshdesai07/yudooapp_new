﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ProductTypeServices : AbstractProductTypeServices
    {
        private AbstractProductTypeDao abstractProductTypeDao;

        public ProductTypeServices(AbstractProductTypeDao abstractProductTypeDao)
        {
            this.abstractProductTypeDao = abstractProductTypeDao;
        }

        public override PagedList<AbstractProductType> ProductType_All(PageParam pageParam, string search , AbstractProductType abstractProductType)
        {
            return this.abstractProductTypeDao.ProductType_All(pageParam, search, abstractProductType);
        }
        public override SuccessResult<AbstractProductType> ProductType_ById(long Id)
        {
            return this.abstractProductTypeDao.ProductType_ById(Id);
        }
        public override SuccessResult<AbstractProductType> ProductType_Upsert(AbstractProductType abstractProductType)
        {
            return this.abstractProductTypeDao.ProductType_Upsert(abstractProductType);
        }
    }

}
﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class BannersServices : AbstractBannersServices
    {
        private AbstractBannersDao abstractBannersDao;

        public BannersServices(AbstractBannersDao abstractBannersDao)
        {
            this.abstractBannersDao = abstractBannersDao;
        }

        public override SuccessResult<AbstractBanners> Banners_Upsert(AbstractBanners abstractBanners)
        {
            return this.abstractBannersDao.Banners_Upsert(abstractBanners); ;
        }

        public override PagedList<AbstractBanners> Banners_All(PageParam pageParam, string search,int IsSpecialist)
        {
            return this.abstractBannersDao.Banners_All(pageParam, search,IsSpecialist);
        }

        public override SuccessResult<AbstractBanners> Banners_ById(long Id)
        {
            return this.abstractBannersDao.Banners_ById(Id);
        }

        public override SuccessResult<AbstractBanners> Banners_ActInAct(long Id)
        {
            return this.abstractBannersDao.Banners_ActInAct(Id);
        }

        public override SuccessResult<AbstractBanners> Banners_Delete(long Id)
        {
            return this.abstractBannersDao.Banners_Delete(Id);
        }

        public override SuccessResult<AbstractBanners> Banners_Update_DisplayOrder(long Id, bool IsUp)
        {
            return this.abstractBannersDao.Banners_Update_DisplayOrder(Id,IsUp);
        }
    }

}
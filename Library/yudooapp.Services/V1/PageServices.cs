﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class PageServices : AbstractPageServices
    {
        private AbstractPageDao abstractPageDao;

        public PageServices(AbstractPageDao abstractPageDao)
        {
            this.abstractPageDao = abstractPageDao;
        }

        public override PagedList<AbstractPage> Pages_All(PageParam pageParam, string search)
        {
            return this.abstractPageDao.Pages_All(pageParam, search);
        }

        public override SuccessResult<AbstractPage> Pages_ById(long Id)
        {
            return this.abstractPageDao.Pages_ById(Id);
        }

        public override SuccessResult<AbstractPage> Pages_Upsert(AbstractPage abstractPage)
        {
            return this.abstractPageDao.Pages_Upsert(abstractPage);
        }

        public override SuccessResult<AbstractPage> Pages_Delete(long Id)
        {
            return this.abstractPageDao.Pages_Delete(Id);
        }

        public override SuccessResult<AbstractPage> Pages_DisplayOrderNo(long Id, bool IsUp)
        {
            return this.abstractPageDao.Pages_DisplayOrderNo(Id, IsUp);
        }
    }

}
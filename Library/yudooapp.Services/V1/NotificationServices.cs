﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;


namespace yudooapp.Services.V1
{
    public class NotificationServices : AbstractNotificationServices
    {
        private AbstractNotificationDao abstractNotificationDao;

        public NotificationServices(AbstractNotificationDao abstractNotificationDao)
        {
            this.abstractNotificationDao = abstractNotificationDao;
        }

        public override SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractNotification)
        {
            return this.abstractNotificationDao.Notification_Upsert(abstractNotification); 
        } 
        public override SuccessResult<AbstractNotification> SendGeneralNotifications(AbstractNotification abstractNotification)
        {
            return this.abstractNotificationDao.SendGeneralNotifications(abstractNotification); 
        }

        public override PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search)
        {
            return this.abstractNotificationDao.Notification_All(pageParam, search);
        }

        public override SuccessResult<AbstractNotification> Notification_ById(long Id)
        {
            return this.abstractNotificationDao.Notification_ById(Id);
        }

        public override PagedList<AbstractNotification> Notification_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractNotificationDao.Notification_ByUserId(pageParam, UserId);
        }

        public override SuccessResult<AbstractNotification> Notification_Delete(long Id)
        {
            return this.abstractNotificationDao.Notification_Delete(Id);
        }
    }
}

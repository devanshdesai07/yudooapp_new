﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class UserOrderAmountBifurcationServices : AbstractUserOrderAmountBifurcationServices
    {
        private AbstractUserOrderAmountBifurcationDao abstractUserOrderAmountBifurcationDao;

        public UserOrderAmountBifurcationServices(AbstractUserOrderAmountBifurcationDao abstractUserOrderAmountBifurcationDao)
        {
            this.abstractUserOrderAmountBifurcationDao = abstractUserOrderAmountBifurcationDao;
        }

        public override SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_Upsert(AbstractUserOrderAmountBifurcation abstractUserOrderAmountBifurcation)
        {
            return this.abstractUserOrderAmountBifurcationDao.UserOrderAmountBifurcation_Upsert(abstractUserOrderAmountBifurcation);
        }
        public override SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_UpdateNote(AbstractUserOrderAmountBifurcation abstractUserOrderAmountBifurcation)
        {
            return this.abstractUserOrderAmountBifurcationDao.UserOrderAmountBifurcation_UpdateNote(abstractUserOrderAmountBifurcation);
        }

        public override PagedList<AbstractUserOrder> UserOrderAmountBifurcation_All(PageParam pageParam, string search, long OrderId, string FromDate, string ToDate, string type, int ispaid)
        {
            return this.abstractUserOrderAmountBifurcationDao.UserOrderAmountBifurcation_All(pageParam, search , OrderId ,FromDate ,ToDate ,type, ispaid);
        }

        public override PagedList<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_ByOrderId(PageParam pageParam,  long OrderId)
        {
            return this.abstractUserOrderAmountBifurcationDao.UserOrderAmountBifurcation_ByOrderId(pageParam,  OrderId);
        }

        public override SuccessResult<AbstractUserOrderAmountBifurcation> UserOrderAmountBifurcation_UpdateIsPaid(long Id, int IsPaid)
        {
            return this.abstractUserOrderAmountBifurcationDao.UserOrderAmountBifurcation_UpdateIsPaid(Id, IsPaid);
        }
    }
}

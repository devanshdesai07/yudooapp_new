﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopImagesAndVideosServices : AbstractShopImagesAndVideosServices
    {
        private AbstractShopImagesAndVideosDao abstractShopImagesAndVideosDao;

        public ShopImagesAndVideosServices(AbstractShopImagesAndVideosDao abstractShopImagesAndVideosDao)
        {
            this.abstractShopImagesAndVideosDao = abstractShopImagesAndVideosDao;
        }

        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_Upsert(AbstractShopImagesAndVideos abstractShopImagesAndVideos)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_Upsert(abstractShopImagesAndVideos); ;
        }

        public override PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos_All(PageParam pageParam, string search)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_All(pageParam, search);
        }

        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_ById(long Id)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_ById(Id);
        }
    
        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_IsVideo(long Id)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_IsVideo(Id);
        }


        public override PagedList<AbstractShopImagesAndVideos> ShopImagesAndVideos_ByShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_ByShopId(pageParam, ShopId);
        }

        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_IsCoverImage(long Id, bool IsVideo)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_IsCoverImage(Id, IsVideo);
        }

        public override SuccessResult<AbstractShopImagesAndVideos> ShopImagesAndVideos_Delete(long Id)
        {
            return this.abstractShopImagesAndVideosDao.ShopImagesAndVideos_Delete(Id);
        }
    }

}
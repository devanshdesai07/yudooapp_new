﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserShopCallLogsServices : AbstractUserShopCallLogsServices
    {
        private AbstractUserShopCallLogsDao abstractUserShopCallLogsDao;

        public UserShopCallLogsServices(AbstractUserShopCallLogsDao abstractUserShopCallLogsDao)
        {
            this.abstractUserShopCallLogsDao = abstractUserShopCallLogsDao;
        }

        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_Upsert(AbstractUserShopCallLogs abstractUserShopCallLogs)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_Upsert(abstractUserShopCallLogs);
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_All(PageParam pageParam, string search, long UserId, long ShopId)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_All(pageParam, search, UserId, ShopId);
        }

        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_ById(long Id)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_ById(Id);
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_ByUserId(pageParam, UserId);
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_ByShopId(pageParam, ShopId);
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ByOrderId(PageParam pageParam, long OrderId)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_ByOrderId(pageParam, OrderId);
        }

        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_Delete(long Id, long DeletedBy)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_Delete(Id, DeletedBy);
        }


        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogsDetails_All(PageParam pageParam, string Search, long Id, long OrderId, string UserMobileNumber, long ShopId, string ShopName, string MobileNumber, string CallDateTime)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogsDetails_All(pageParam, Search, Id, OrderId, UserMobileNumber, ShopId, ShopName, MobileNumber, CallDateTime);
        }

        public override PagedList<AbstractUserShopCallLogs> Request_CallBack_ShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractUserShopCallLogsDao.Request_CallBack_ShopId(pageParam, ShopId);
        }

        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_ReceiveVideoCall(PageParam pageParam)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_ReceiveVideoCall(pageParam);
        }
        public override PagedList<AbstractUserShopCallLogs> UserShopCallLogs_PendingCall(PageParam pageParam, long UserId)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_PendingCall(pageParam, UserId);
        }


        public override SuccessResult<AbstractUserShopCallLogs> UserShopCallLogs_CallStatus_Update(long Id, long CallStatus)
        {
            return this.abstractUserShopCallLogsDao.UserShopCallLogs_CallStatus_Update(Id, CallStatus);
        }
        public override PagedList<AbstractRequest_CallBack_Admin> Request_CallBack_Admin(PageParam pageParam, string Search)
        {
            return this.abstractUserShopCallLogsDao.Request_CallBack_Admin(pageParam, Search);
        }
    }
    
}
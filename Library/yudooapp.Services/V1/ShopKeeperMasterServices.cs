﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class ShopKeeperMasterServices : AbstractShopKeeperMasterServices
    {
        private AbstractShopKeeperMasterDao abstractShopKeeperMasterDao;

        public ShopKeeperMasterServices(AbstractShopKeeperMasterDao abstractShopKeeperMasterDao)
        {
            this.abstractShopKeeperMasterDao = abstractShopKeeperMasterDao;
        }

       
        public override SuccessResult<AbstractShopKeeperMaster> ShopKeeperMaster_ById(long Id)
        {
            return this.abstractShopKeeperMasterDao.ShopKeeperMaster_ById(Id);
        }

        public override SuccessResult<AbstractShopKeeperMaster> ShopKeeperMaster_Upsert(AbstractShopKeeperMaster abstractShopKeeperMaster)
        {
            return this.abstractShopKeeperMasterDao.ShopKeeperMaster_Upsert(abstractShopKeeperMaster);
        }
    }
}

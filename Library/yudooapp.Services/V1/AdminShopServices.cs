﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class AdminShopServices : AbstractAdminShopServices
    {
        private AbstractAdminShopDao abstractAdminShopDao;

        public AdminShopServices(AbstractAdminShopDao abstractAdminShopDao)
        {
            this.abstractAdminShopDao = abstractAdminShopDao;
        }

        public override PagedList<AbstractAdminShop> AdminShop_All(PageParam pageParam, string search, long ShopOBStatus, long ShopId, string ShopName, string MobileNumber, long CityId, string PrimaryCategory, string AllCategory,string DateOfJoining)
        {
            return this.abstractAdminShopDao.AdminShop_All(pageParam, search, ShopOBStatus, ShopId, ShopName, MobileNumber, CityId, PrimaryCategory, AllCategory, DateOfJoining);
        }

        public override PagedList<AbstractAdminShop> ShopApproval_All(PageParam pageParam, string search, long ShopOBStatus, long ShopId, string ShopName, string MobileNumber, long CityId, string PrimaryCategory, string AllCategory, string DateOfJoining)
        {
            return this.abstractAdminShopDao.ShopApproval_All(pageParam, search, ShopOBStatus, ShopId, ShopName, MobileNumber, CityId, PrimaryCategory, AllCategory, DateOfJoining);
        }

        public override SuccessResult<AbstractAdminShop> AdminShop_ById(long Id)
        {
            return this.abstractAdminShopDao.AdminShop_ById(Id);
        }
        public override SuccessResult<AbstractAdminShop> SendForApproval(long Shopid)
        {
            return this.abstractAdminShopDao.SendForApproval(Shopid);
        }
        public override PagedList<AbstractAdminShop> CurrentyOfflineShops_All(PageParam pageParam, string search)
        {
            return this.abstractAdminShopDao.CurrentyOfflineShops_All(pageParam, search);
        }
        public override PagedList<AbstractAdminShop> GetUnansweredcall(PageParam pageParam, string search)
        {
            return this.abstractAdminShopDao.GetUnansweredcall(pageParam, search);
        }
        


    }
}
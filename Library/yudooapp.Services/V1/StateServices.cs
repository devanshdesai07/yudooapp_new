﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class StateServices : AbstractStateServices
    {
        private AbstractStateDao abstractStateDao;

        public StateServices(AbstractStateDao abstractStateDao)
        {
            this.abstractStateDao = abstractStateDao;
        }

        

        public override PagedList<AbstractState> State_All(PageParam pageParam, string search)
        {
            return this.abstractStateDao.State_All(pageParam, search);
        }

        public override SuccessResult<AbstractState> State_ById(long Id)
        {
            return this.abstractStateDao.State_ById(Id);
        }

        public override PagedList<AbstractState> State_ByCountryId(PageParam pageParam, long CountryId)
        {
            return this.abstractStateDao.State_ByCountryId(pageParam, CountryId);
        }

        public override SuccessResult<AbstractState> State_Delete(long Id)
        {
            return this.abstractStateDao.State_Delete(Id);
        }
    }

}
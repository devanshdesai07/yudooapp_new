﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ParcelServices : AbstractParcelServices
    {
        private AbstractParcelDao abstractParcelDao;

        public ParcelServices(AbstractParcelDao abstractParcelDao)
        {
            this.abstractParcelDao = abstractParcelDao;
        }

        public override SuccessResult<AbstractParcel> Parcel_Upsert(AbstractParcel abstractParcel)
        {
            return this.abstractParcelDao.Parcel_Upsert(abstractParcel); ;
        }


        public override PagedList<AbstractParcel> Parcel_ByOrderId(long OrderId, PageParam pageParam)
        {
            return this.abstractParcelDao.Parcel_ByOrderId(OrderId, pageParam);
        }


        public override SuccessResult<AbstractParcel> Parcel_ById(long Id)
        {
            return this.abstractParcelDao.Parcel_ById(Id);
        }


        public override SuccessResult<AbstractParcel> Parcel_StatusChange(long Id)
        {
            return this.abstractParcelDao.Parcel_StatusChange(Id);
        }

        public override SuccessResult<AbstractParcel> Parcel_Delete(long Id, long DeletedBy)
        {
            return this.abstractParcelDao.Parcel_Delete(Id, DeletedBy);
        }

        
        
    }

}
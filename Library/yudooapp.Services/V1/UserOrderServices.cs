﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserOrderServices : AbstractUserOrderServices
    {
        private AbstractUserOrderDao abstractUserOrderDao;

        public UserOrderServices(AbstractUserOrderDao abstractUserOrderDao)
        {
            this.abstractUserOrderDao = abstractUserOrderDao;
        }

        public override SuccessResult<AbstractUserOrder> UserOrder_Upsert(AbstractUserOrder abstractUserOrder)
        {
            return this.abstractUserOrderDao.UserOrder_Upsert(abstractUserOrder); ;
        }
        public override SuccessResult<AbstractUserOrder> UserOrder_Upsert_paymentLink(long Id, string PaymentLink, string RazorPayOrderId = "")
        {
            return this.abstractUserOrderDao.UserOrder_Upsert_paymentLink(Id,PaymentLink,RazorPayOrderId); ;
        }

        public override PagedList<AbstractUserOrder> UserOrder_All(PageParam pageParam, string search)
        {
            return this.abstractUserOrderDao.UserOrder_All(pageParam, search);
        }
        public override PagedList<AbstractUserOrder> UserOrder_ByShopId(PageParam pageParam, string search, long Id,string OrderStatus)
        {
            return this.abstractUserOrderDao.UserOrder_ByShopId(pageParam, search,Id, OrderStatus);
        }

        public override SuccessResult<AbstractUserOrder> UserOrder_ById(long Id)
        {
            return this.abstractUserOrderDao.UserOrder_ById(Id);
        }

        public override PagedList<AbstractUserOrder> UserOrder_ByUserId(PageParam pageParam, long UserId, long IsPendingPayment, long IsPlaced)
        {
            return this.abstractUserOrderDao.UserOrder_ByUserId(pageParam, UserId, IsPendingPayment, IsPlaced);
        }

        public override SuccessResult<AbstractUserOrder> UserOrder_Delete(long Id)
        {
            return this.abstractUserOrderDao.UserOrder_Delete(Id);
        }

        public override PagedList<AbstractUserOrder> OrderDetails_All(PageParam pageParam, string Search,long OrderStatus, string PaymentStatus,long CityId, decimal Amount)
        {
            return this.abstractUserOrderDao.OrderDetails_All(pageParam, Search, OrderStatus, PaymentStatus, CityId,Amount);
        }
        public override SuccessResult<AbstractUserOrder> UserOrder_StatusChange(long Id, long OrderStatus)
        {
            return this.abstractUserOrderDao.UserOrder_StatusChange(Id,OrderStatus);
        }
        public override SuccessResult<AbstractUserOrder> UserOrder_StatusChangeByShipmentId(long Id, long OrderStatus)
        {
            return this.abstractUserOrderDao.UserOrder_StatusChangeByShipmentId(Id,OrderStatus);
        }
        public override SuccessResult<AbstractUserOrder> UserOrder_DeliveryRatingUpdate(long Id, long DeliveryRating)
        {
            return this.abstractUserOrderDao.UserOrder_DeliveryRatingUpdate(Id, DeliveryRating);
        }
    }
}
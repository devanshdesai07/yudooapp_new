﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class TrendingSearchServices : AbstractTrendingSearchServices
    {
        private AbstractTrendingSearchDao abstractTrendingSearchDao;

        public TrendingSearchServices(AbstractTrendingSearchDao abstractTrendingSearchDao)
        {
            this.abstractTrendingSearchDao = abstractTrendingSearchDao;
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_Upsert(AbstractTrendingSearch abstractTrendingSearch)
        {
            return this.abstractTrendingSearchDao.TrendingSearch_Upsert(abstractTrendingSearch);
        }

        public override PagedList<AbstractTrendingSearch> TrendingSearch_All(PageParam pageParam, string Search)
        {
            return this.abstractTrendingSearchDao.TrendingSearch_All(pageParam, Search);
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_ById(long Id)
        {
            return this.abstractTrendingSearchDao.TrendingSearch_ById(Id);
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_ActInActive(long Id)
        {
            return this.abstractTrendingSearchDao.TrendingSearch_ActInActive(Id);
        }

        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_Delete(long Id)
        {
            return this.abstractTrendingSearchDao.TrendingSearch_Delete(Id);
        }
        public override SuccessResult<AbstractTrendingSearch> TrendingSearch_Update_DisplayOrder(long Id, bool IsUp)
        {
            return this.abstractTrendingSearchDao.TrendingSearch_Update_DisplayOrder(Id, IsUp);
        }
    }
}

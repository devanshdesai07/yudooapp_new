﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopOwnerDevicesServices : AbstractShopOwnerDevicesServices
    {
        private AbstractShopOwnerDevicesDao abstractShopOwnerDevicesDao;

        public ShopOwnerDevicesServices(AbstractShopOwnerDevicesDao abstractShopOwnerDevicesDao)
        {
            this.abstractShopOwnerDevicesDao = abstractShopOwnerDevicesDao;
        }

        public override PagedList<AbstractShopOwnerDevices> ShopOwnerDevices_ByShopId(PageParam pageParam, string search, long ShopOwnerId)
        {
            return this.abstractShopOwnerDevicesDao.ShopOwnerDevices_ByShopId(pageParam, search, ShopOwnerId);
        }

    }

}
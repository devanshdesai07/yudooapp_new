﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class MasterOrderStatusServices : AbstractMasterOrderStatusServices
    {
        private AbstractMasterOrderStatusDao abstractMasterOrderStatusDao;

        public MasterOrderStatusServices(AbstractMasterOrderStatusDao abstractMasterOrderStatusDao)
        {
            this.abstractMasterOrderStatusDao = abstractMasterOrderStatusDao;
        }

        public override SuccessResult<AbstractMasterOrderStatus> MasterOrderStatus_Upsert(AbstractMasterOrderStatus abstractMasterOrderStatus)
        {
            return this.abstractMasterOrderStatusDao.MasterOrderStatus_Upsert(abstractMasterOrderStatus);
        }

        public override PagedList<AbstractMasterOrderStatus> MasterOrderStatus_All(PageParam pageParam, string search)
        {
            return this.abstractMasterOrderStatusDao.MasterOrderStatus_All(pageParam, search);
        }

    }

}
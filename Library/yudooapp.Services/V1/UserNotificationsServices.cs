﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;



namespace yudooapp.Services.V1
{
    public class UserNotificationsServices : AbstractUserNotificationsServices
    {
        private AbstractUserNotificationsDao abstractUserNotificationsDao;

        public UserNotificationsServices(AbstractUserNotificationsDao abstractUserNotificationsDao)
        {
            this.abstractUserNotificationsDao = abstractUserNotificationsDao;
        }
        public override SuccessResult<AbstractUserNotifications> UserNotifications_Insert(AbstractUserNotifications abstractUserNotifications)
        {
            return this.abstractUserNotificationsDao.UserNotifications_Insert(abstractUserNotifications);
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByUserId(PageParam pageParam, long UserId, long RequestId, string ActionFormDate, string ActionToDate, int IsSent)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ByUserId(pageParam, UserId, RequestId, ActionFormDate, ActionToDate, IsSent);
        }

        public override PagedList<AbstractUserNotifications> UserNotifications_ByNotificationsTypeId(PageParam pageParam, long NotificationsTypeId, long UserId)
        {
            return this.abstractUserNotificationsDao.UserNotifications_ByNotificationsTypeId(pageParam, NotificationsTypeId, UserId);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class PaymentServices : AbstractPaymentServices
    {
        private AbstractPaymentDao abstractPaymentDao;

        public PaymentServices(AbstractPaymentDao abstractPaymentDao)
        {
            this.abstractPaymentDao = abstractPaymentDao;
        }

        public override PagedList<AbstractPayment> Payment_All(PageParam pageParam, string search , AbstractPayment abstractPayment)
        {
            return this.abstractPaymentDao.Payment_All(pageParam, search, abstractPayment);
        }
        public override SuccessResult<AbstractPayment> Payment_ById(long Id)
        {
            return this.abstractPaymentDao.Payment_ById(Id);
        }
        public override SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment)
        {
            return this.abstractPaymentDao.Payment_Upsert(abstractPayment);
        }

        public override PagedList<AbstractCustomerPaymentStatus> CustomerPaymentStatus_All(PageParam pageParam, string search)
        {
            return this.abstractPaymentDao.CustomerPaymentStatus_All(pageParam, search);
        }
    }

}
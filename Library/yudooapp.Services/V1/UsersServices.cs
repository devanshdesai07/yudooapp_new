﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UsersServices : AbstractUsersServices
    {
        private AbstractUsersDao abstractUsersDao;

        public UsersServices(AbstractUsersDao abstractUsersDao)
        {
            this.abstractUsersDao = abstractUsersDao;
        }

        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search , AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_All(pageParam, search, abstractUsers);
        }

        public override SuccessResult<AbstractUsers> Users_ActInAct(long Id)
        {
            return this.abstractUsersDao.Users_ActInAct(Id);
        }
        public override SuccessResult<AbstractUsers> Users_ById(long Id)
        {
            return this.abstractUsersDao.Users_ById(Id);
        }

        public override SuccessResult<AbstractUsers> Users_IsCallStatus(long Id)
        {
            return this.abstractUsersDao.Users_IsCallStatus(Id);
        }

        public override SuccessResult<AbstractCallerInfo> Caller_Info(long ShopId,long UserId)
        {
            return this.abstractUsersDao.Caller_Info(ShopId,UserId);
        }

        public override bool Users_Logout(long Id)
        {
            return this.abstractUsersDao.Users_Logout(Id);
        }

        public override SuccessResult<AbstractUsers> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            return this.abstractUsersDao.Users_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
        }
        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_Upsert(abstractUsers);
        }

        public override SuccessResult<AbstractUsers> Users_SendOTP(long MobileNumber, long OTP)
        {
            return this.abstractUsersDao.Users_SendOTP(MobileNumber, OTP);
        }
        public override SuccessResult<AbstractUsers> Users_VerifyOtp(long MobileNumber, long OTP,string DeviceType, string UDID, string IMEI, string DeviceToken)
        {
            return this.abstractUsersDao.Users_VerifyOtp(MobileNumber, OTP, DeviceType, UDID, IMEI, DeviceToken);
        }

        public override SuccessResult<AbstractUsers> Users_Delete(long Id, long DeletedBy)
        {
            return this.abstractUsersDao.Users_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractUsers> Users_OnlineOffline(long Id, bool IsOnline)
        {
            return this.abstractUsersDao.Users_OnlineOffline(Id, IsOnline);
        }

        public override SuccessResult<AbstractUsers> CheckUser_Exists(string MobileNumber)
        {
            return this.abstractUsersDao.CheckUser_Exists(MobileNumber);
        }

        public override SuccessResult<AbstractUsers> Users_IsCallBackPreference(long Id, bool IsCallBackPreference)
        {
            return this.abstractUsersDao.Users_IsCallBackPreference(Id, IsCallBackPreference);
        }
    }

}
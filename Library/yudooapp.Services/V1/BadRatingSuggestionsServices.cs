﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class BadRatingSuggestionsServices : AbstractBadRatingSuggestionsServices
    {
        private AbstractBadRatingSuggestionsDao abstractBadRatingSuggestionsDao;

        public BadRatingSuggestionsServices(AbstractBadRatingSuggestionsDao abstractBadRatingSuggestionsDao)
        {
            this.abstractBadRatingSuggestionsDao = abstractBadRatingSuggestionsDao;
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Upsert(AbstractBadRatingSuggestions abstractBadRatingSuggestions)
        {
            return this.abstractBadRatingSuggestionsDao.BadRatingSuggestions_Upsert(abstractBadRatingSuggestions);
        }

        public override PagedList<AbstractBadRatingSuggestions> BadRatingSuggestions_All(PageParam pageParam, string Search)
        {
            return this.abstractBadRatingSuggestionsDao.BadRatingSuggestions_All(pageParam, Search);
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_ById(long Id)
        {
            return this.abstractBadRatingSuggestionsDao.BadRatingSuggestions_ById(Id);
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_ActInActive(long Id)
        {
            return this.abstractBadRatingSuggestionsDao.BadRatingSuggestions_ActInActive(Id);
        }

        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Delete(long Id)
        {
            return this.abstractBadRatingSuggestionsDao.BadRatingSuggestions_Delete(Id);
        }
        public override SuccessResult<AbstractBadRatingSuggestions> BadRatingSuggestions_Update_DisplayOrder(long Id, bool IsUp)
        {
            return this.abstractBadRatingSuggestionsDao.BadRatingSuggestions_Update_DisplayOrder(Id, IsUp);
        }
    }
}

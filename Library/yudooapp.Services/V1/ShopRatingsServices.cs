﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopRatingsServices : AbstractShopRatingsServices
    {
        private AbstractShopRatingsDao abstractShopRatingsDao;

        public ShopRatingsServices(AbstractShopRatingsDao abstractShopRatingsDao)
        {
            this.abstractShopRatingsDao = abstractShopRatingsDao;
        }

        public override SuccessResult<AbstractShopRatings> ShopRatings_Upsert(AbstractShopRatings abstractShopRatings)
        {
            return this.abstractShopRatingsDao.ShopRatings_Upsert(abstractShopRatings); ;
        }

        public override PagedList<AbstractShopRatings> ShopRatings_All(PageParam pageParam, string search , long ShopId , long UserId)
        {
            return this.abstractShopRatingsDao.ShopRatings_All(pageParam, search , ShopId , UserId);
        }

        public override PagedList<AbstractShopRatings> ShopRatings_ByShopId(PageParam pageParam, string search , long ShopId)
        {
            return this.abstractShopRatingsDao.ShopRatings_ByShopId(pageParam, search , ShopId);
        }

        public override PagedList<AbstractShopRatings> ShopRatings_ByUserId(PageParam pageParam, string search , long UserId)
        {
            return this.abstractShopRatingsDao.ShopRatings_ByUserId(pageParam, search , UserId);
        }
        public override SuccessResult<AbstractShopRatings> ShopRatings_ById(long Id)
        {
            return this.abstractShopRatingsDao.ShopRatings_ById(Id);
        }

        public override SuccessResult<AbstractShopRatings> ShopRatings_Delete(long Id)
        {
            return this.abstractShopRatingsDao.ShopRatings_Delete(Id);
        }

    }

}
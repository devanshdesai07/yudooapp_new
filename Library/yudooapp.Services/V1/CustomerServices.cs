﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class CustomerServices : AbstractCustomerServices
    {
        private AbstractCustomerDao abstractCustomerDao;

        public CustomerServices(AbstractCustomerDao abstractCustomerDao)
        {
            this.abstractCustomerDao = abstractCustomerDao;
        }

        public override SuccessResult<AbstractCustomer> Customer_Upsert(AbstractCustomer abstractCustomer)
        {
            return this.abstractCustomerDao.Customer_Upsert(abstractCustomer); ;
        }

        public override PagedList<AbstractCustomer> Customer_All(PageParam pageParam, string search)
        {
            return this.abstractCustomerDao.Customer_All(pageParam, search);
        }

        public override SuccessResult<AbstractCustomer> Customer_ById(long Id)
        {
            return this.abstractCustomerDao.Customer_ById(Id);
        }

        public override SuccessResult<AbstractCustomer> Customer_ActInact(long Id)
        {
            return this.abstractCustomerDao.Customer_ActInact(Id);
        }

        public override SuccessResult<AbstractCustomer> Customer_Delete(long Id, long DeletedBy)
        {
            return this.abstractCustomerDao.Customer_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractCustomer> Customer_Overview(long MobileNumber, int OrderId)
        {
            return this.abstractCustomerDao.Customer_Overview(MobileNumber, OrderId);
        }

        public override PagedList<AbstractCustomer> Customer_Orders(PageParam pageParam, long MobileNumber, int OrderId)
        {
            return this.abstractCustomerDao.Customer_Orders(pageParam, MobileNumber, OrderId);
        }

    }

}
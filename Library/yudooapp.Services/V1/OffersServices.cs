﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class OffersServices : AbstractOffersServices
    {
        private AbstractOffersDao abstractOffersDao;

        public OffersServices(AbstractOffersDao abstractOffersDao)
        {
            this.abstractOffersDao = abstractOffersDao;
        }

        public override SuccessResult<AbstractOffers> Offers_Upsert(AbstractOffers abstractOffers)
        {
            return this.abstractOffersDao.Offers_Upsert(abstractOffers);
        }

        public override PagedList<AbstractOffers> Offers_All(PageParam pageParam, string search)
        {
            return this.abstractOffersDao.Offers_All(pageParam, search);
        }
        public override PagedList<AbstractOffers> Offers_All_Active(PageParam pageParam, string search)
        {
            return this.abstractOffersDao.Offers_All_Active(pageParam, search);
        }

        public override SuccessResult<AbstractOffers> Offers_ById(long Id)
        {
            return this.abstractOffersDao.Offers_ById(Id);
        }

        public override PagedList<AbstractOffers> Offers_ByShopId(PageParam pageParam, long ShopId)
        {
            return this.abstractOffersDao.Offers_ByShopId(pageParam, ShopId);
        }
        public override SuccessResult<AbstractOffers> Offers_ActInAct(long Id, bool IsActive)
        {
            return this.abstractOffersDao.Offers_ActInAct(Id, IsActive);
        }

        public override SuccessResult<AbstractOffers> OfferStatus_ChangeStatus(long Id, long Status)
        {
            return this.abstractOffersDao.OfferStatus_ChangeStatus(Id, Status);
        }

        public override SuccessResult<AbstractOffers> Offers_Delete(long Id)
        {
            return this.abstractOffersDao.Offers_Delete(Id);
        }
    }

}
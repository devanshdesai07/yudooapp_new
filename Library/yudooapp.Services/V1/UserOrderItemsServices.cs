﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserOrderItemsServices : AbstractUserOrderItemsServices
    {
        private AbstractUserOrderItemsDao abstractUserOrderItemsDao;

        public UserOrderItemsServices(AbstractUserOrderItemsDao abstractUserOrderItemsDao)
        {
            this.abstractUserOrderItemsDao = abstractUserOrderItemsDao;
        }

        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_Upsert(AbstractUserOrderItems abstractUserOrderItems)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_Upsert(abstractUserOrderItems); ;
        }

        public override PagedList<AbstractUserOrderItems> UserOrderItems_All(PageParam pageParam, string search)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_All(pageParam, search);
        }
        public override PagedList<AbstractUserOrderItems> UserOrderItems_ByOrderId(PageParam pageParam, long OrderId)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_ByOrderId(pageParam, OrderId);
        }
        public override SuccessResult<AbstractUserOrderItems> AddShipmentIdUserOrderItems_ById(string Id, int ShipmentId)
        {
            return this.abstractUserOrderItemsDao.AddShipmentIdUserOrderItems_ById(Id, ShipmentId);
        }
        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_ById(long Id)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_ById(Id);
        }
        public override SuccessResult<AbstractUserOrderItems> ManifestUrl_ShipmentId(string ManifestUrl,string LabelUrl, long ShipmentId)
        {
            return this.abstractUserOrderItemsDao.ManifestUrl_ShipmentId(ManifestUrl, LabelUrl, ShipmentId);
        }
        public override PagedList<AbstractUserOrderItems> UserOrderItems_ByMobileNumber(PageParam pageParam, string MobileNumber)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_ByMobileNumber(pageParam, MobileNumber);
        }
        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_Delete(long Id, long DeletedBy)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_Status(long Id, long StatusId)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_Status(Id, StatusId);
        }

        public override SuccessResult<AbstractUserOrderItems> UserOrderItems_StatusChange(long Id, long ItemStatusId, long UpdatedBy)
        {
            return this.abstractUserOrderItemsDao.UserOrderItems_StatusChange(Id, ItemStatusId,UpdatedBy);
        }
    }

}
﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class AddressServices : AbstractAddressServices
    {
        private AbstractAddressDao abstractAddressDao;

        public AddressServices(AbstractAddressDao abstractAddressDao)
        {
            this.abstractAddressDao = abstractAddressDao;
        }

        public override SuccessResult<AbstractAddress> Address_Upsert(AbstractAddress abstractAddress)
        {
            return this.abstractAddressDao.Address_Upsert(abstractAddress);
        }

        public override PagedList<AbstractAddress> Address_All(PageParam pageParam, string search)
        {
            return this.abstractAddressDao.Address_All(pageParam, search);
        }

        public override SuccessResult<AbstractAddress> Address_ById(long Id)
        {
            return this.abstractAddressDao.Address_ById(Id);
        }

        public override PagedList<AbstractAddress> Address_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractAddressDao.Address_ByUserId(pageParam ,UserId);
        }

        public override SuccessResult<AbstractAddress> Address_ActInAct(long Id)
        {
            return this.abstractAddressDao.Address_ActInAct(Id);
        }

        public override SuccessResult<AbstractAddress> Address_Delete(long Id)
        {
            return this.abstractAddressDao.Address_Delete(Id);
        }

    }

}
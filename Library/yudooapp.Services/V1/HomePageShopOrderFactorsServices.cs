﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
namespace yudooapp.Services.V1
{
   public class HomePageShopOrderFactorsServices : AbstractHomePageShopOrderFactorsServices
    {
        private AbstractHomePageShopOrderFactorsDao abstractHomePageShopOrderFactorsDao;

        public HomePageShopOrderFactorsServices(AbstractHomePageShopOrderFactorsDao abstractHomePageShopOrderFactorsDao)
        {
            this.abstractHomePageShopOrderFactorsDao = abstractHomePageShopOrderFactorsDao;
        }


        public override PagedList<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_All(PageParam pageParam, string Search)
        {
            return this.abstractHomePageShopOrderFactorsDao.HomePageShopOrderFactors_All(pageParam, Search);
        }

        public override SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_ById(int Id)
        {
            return this.abstractHomePageShopOrderFactorsDao.HomePageShopOrderFactors_ById(Id);
        }

        public override SuccessResult<AbstractHomePageShopOrderFactors> HomePageShopOrderFactors_Upsert(AbstractHomePageShopOrderFactors abstractHomePageShopOrderFactors)
        {
            return this.abstractHomePageShopOrderFactorsDao.HomePageShopOrderFactors_Upsert(abstractHomePageShopOrderFactors);
        }
    }
}

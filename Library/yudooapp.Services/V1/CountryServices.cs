﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class CountryServices : AbstractCountryServices
    {
        private AbstractCountryDao abstractCountryDao;

        public CountryServices(AbstractCountryDao abstractCountryDao)
        {
            this.abstractCountryDao = abstractCountryDao;
        }

        

        public override PagedList<AbstractCountry> Country_All(PageParam pageParam, string search)
        {
            return this.abstractCountryDao.Country_All(pageParam, search);
        }

        public override SuccessResult<AbstractCountry> Country_ById(long Id)
        {
            return this.abstractCountryDao.Country_ById(Id);
        }


        public override SuccessResult<AbstractCountry> Country_Delete(long Id)
        {
            return this.abstractCountryDao.Country_Delete(Id);
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Data.Contract;

namespace yudooapp.Services.V1
{
    public class DashboardCountForShopServices : AbstractDashboardCountForShopServices
    {
        private readonly AbstractDashboardCountForShopDao _abstractDashboardCountForShopDao = null;

        public DashboardCountForShopServices(AbstractDashboardCountForShopDao _abstractDashboardCountForShopDao)
        {
            this._abstractDashboardCountForShopDao = _abstractDashboardCountForShopDao;
        }
        public override SuccessResult<AbstractDashboardCountForShop> DashboardCount_ByShopId(long ShopId, string Period)
        {
            return _abstractDashboardCountForShopDao.DashboardCount_ByShopId(ShopId,Period);
        }
    }
}

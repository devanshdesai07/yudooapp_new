﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class PermissionServices : AbstractPermissionServices
    {
        private AbstractPermissionDao abstractPermissionDao;

        public PermissionServices(AbstractPermissionDao abstractPermissionDao)
        {
            this.abstractPermissionDao = abstractPermissionDao;
        }
        public override PagedList<AbstractPermission> Permission_ByCustomerId(PageParam pageParam, string search, AbstractPermission abstractPermission)
        {
            return this.abstractPermissionDao.Permission_ByCustomerId(pageParam, search, abstractPermission);
        }

        public override SuccessResult<AbstractPermission> Permission_Upsert(AbstractPermission abstractPermission)
        {
            return this.abstractPermissionDao.Permission_Upsert(abstractPermission);
        }
    }

}
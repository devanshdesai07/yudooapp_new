﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class UserWishlistServices : AbstractUserWishlistServices
    {
        private AbstractUserWishlistDao abstractUserWishlistDao;

        public UserWishlistServices(AbstractUserWishlistDao abstractUserWishlistDao)
        {
            this.abstractUserWishlistDao = abstractUserWishlistDao;
        }

        public override PagedList<AbstractUserWishlist> UserWishlist_All(PageParam pageParam, string Search,long UserId,long ShopId)
        {
            return this.abstractUserWishlistDao.UserWishlist_All(pageParam, Search,UserId,ShopId);
        }

        public override PagedList<AbstractShopOwner> UserWishlist_ByUserId(PageParam pageParam, long UserId)
        {
            return this.abstractUserWishlistDao.UserWishlist_ByUserId(pageParam, UserId);
        }

        public override PagedList<AbstractUserWishlist> UserWishlist_ByShopId(PageParam pageParam,  long ShopId)
        {
            return this.abstractUserWishlistDao.UserWishlist_ByShopId(pageParam, ShopId);
        }

        public override SuccessResult<AbstractUserWishlist> UserWishlist_ById(long Id)
        {
            return this.abstractUserWishlistDao.UserWishlist_ById(Id);
        }
        public override SuccessResult<AbstractUserWishlist> UserWishlist_Upsert(AbstractUserWishlist abstractUserWishlist)
        {
            return this.abstractUserWishlistDao.UserWishlist_Upsert(abstractUserWishlist);
        }

        public override SuccessResult<AbstractUserWishlist> UserWishlist_Delete(int UserId, int ShopId)
        {
            return this.abstractUserWishlistDao.UserWishlist_Delete(UserId, ShopId);
        }
    }
}

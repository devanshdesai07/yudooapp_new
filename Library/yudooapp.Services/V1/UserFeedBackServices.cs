﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserFeedBackServices : AbstractUserFeedBackServices
    {
        private AbstractUserFeedBackDao abstractUserFeedBackDao;

        public UserFeedBackServices(AbstractUserFeedBackDao abstractUserFeedBackDao)
        {
            this.abstractUserFeedBackDao = abstractUserFeedBackDao;
        }

        public override SuccessResult<AbstractUserFeedBack> UserFeedBack_Upsert(AbstractUserFeedBack abstractUserFeedBack)
        {
            return this.abstractUserFeedBackDao.UserFeedBack_Upsert(abstractUserFeedBack); 
        }

       
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class UserCartMasterServices : AbstractUserCartMasterServices
    {
        private AbstractUserCartMasterDao abstractUserCartMasterDao;

        public UserCartMasterServices(AbstractUserCartMasterDao abstractUserCartMasterDao)
        {
            this.abstractUserCartMasterDao = abstractUserCartMasterDao;
        }

        public override PagedList<AbstractUserCartMaster> UserCartMaster_All(PageParam pageParam, string search)
        {
            return this.abstractUserCartMasterDao.UserCartMaster_All(pageParam, search);
        }

        public override PagedList<AbstractUserCartMaster> UserCartMaster_ByUserId(PageParam pageParam, string search,long UserId)
        {
            return this.abstractUserCartMasterDao.UserCartMaster_ByUserId(pageParam, search, UserId);
        }

        public override PagedList<AbstractUserCartMaster> UserCartMaster_ByShopId(PageParam pageParam, string search,long ShopId)
        {
            return this.abstractUserCartMasterDao.UserCartMaster_ByShopId(pageParam, search, ShopId);
        }

        public override SuccessResult<AbstractUserCartMaster> UserCartMaster_ById(long Id)
        {
            return this.abstractUserCartMasterDao.UserCartMaster_ById(Id);
        }
        public override SuccessResult<AbstractUserCartMaster> UserCartMaster_ByVideoCallId(long VideoCallId, long UserId,int Type =0)
        {
            return this.abstractUserCartMasterDao.UserCartMaster_ByVideoCallId(VideoCallId, UserId, Type);
        }

        public override SuccessResult<AbstractUserCartMaster> UserCartMaster_Upsert(AbstractUserCartMaster abstractUserCartMaster)
        {
            return this.abstractUserCartMasterDao.UserCartMaster_Upsert(abstractUserCartMaster);
        }
    }

}
﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class AdminTypeServices : AbstractAdminTypeServices
    {
        private AbstractAdmintypeDao abstractAdminTypeDao;

        public AdminTypeServices(AbstractAdmintypeDao abstractAdminTypeDao)
        {
            this.abstractAdminTypeDao = abstractAdminTypeDao;
        }

        public override PagedList<AbstractAdminType> AdminType_All(PageParam pageParam, string search)
        {
            return this.abstractAdminTypeDao.AdminType_All(pageParam, search);
        }
        public override SuccessResult<AbstractAdminType> AdminType_ById(int Id)
        {
            return this.abstractAdminTypeDao.AdminType_ById(Id);
        }
        public override SuccessResult<AbstractAdminType> AdminType_ActInAct(int Id)
        {
            return this.abstractAdminTypeDao.AdminType_ActInAct(Id);
        }
        public override SuccessResult<AbstractAdminType> AdminType_Delete(int Id,int DeletedBy)
        {
            return this.abstractAdminTypeDao.AdminType_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractAdminType> AdminType_Upsert(AbstractAdminType abstractAdminType)
        {
            return this.abstractAdminTypeDao.AdminType_Upsert(abstractAdminType);
        }

    }

}
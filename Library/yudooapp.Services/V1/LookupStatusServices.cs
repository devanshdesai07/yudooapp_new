﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class LookupStatusServices : AbstractLookupStatusServices
    {
        private AbstractLookupStatusDao abstractLookupStatusDao;

        public LookupStatusServices(AbstractLookupStatusDao abstractLookupStatusDao)
        {
            this.abstractLookupStatusDao = abstractLookupStatusDao;
        }

        public override PagedList<AbstractLookupStatus> LookupStatus_All(PageParam pageParam, string search)
        {
            return this.abstractLookupStatusDao.LookupStatus_All(pageParam, search);
        }

        
    }

}
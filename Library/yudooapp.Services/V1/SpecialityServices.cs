﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public  class SpecialityServices : AbstractSpecialityServices
    {
        private AbstractSpecialityDao abstractSpecialityDao;

        public SpecialityServices(AbstractSpecialityDao abstractSpecialityDao)
        {
            this.abstractSpecialityDao = abstractSpecialityDao;
        }
        public override PagedList<AbstractSpeciality> Speciality_All(PageParam pageParam, string Search)
        {
            return this.abstractSpecialityDao.Speciality_All(pageParam, Search);
        }

        public override SuccessResult<AbstractSpeciality> Speciality_ById(long Id)
        {
            return this.abstractSpecialityDao.Speciality_ById(Id);
        }

        public override SuccessResult<AbstractSpeciality> Speciality_Delete(long Id ,long DeletedBy)
        {
            return this.abstractSpecialityDao.Speciality_Delete(Id, DeletedBy);
        }

        public override SuccessResult<AbstractSpeciality> Speciality_Upsert(AbstractSpeciality abstractSpeciality)
        {
            return this.abstractSpecialityDao.Speciality_Upsert(abstractSpeciality);
        }
        public override SuccessResult<AbstractSpeciality> Speciality_ActInActive(long Id)
        {
            return this.abstractSpecialityDao.Speciality_ActInActive(Id);
        }
    }
}

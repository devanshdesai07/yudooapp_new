﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
   public class NotificationTypesServices : AbstractNotificationTypesServices
    {
        private AbstractNotificationTypesDao abstractNotificationTypesDao;

        public NotificationTypesServices(AbstractNotificationTypesDao abstractNotificationTypesDao)
        {
            this.abstractNotificationTypesDao = abstractNotificationTypesDao;
        }

        public override SuccessResult<AbstractNotificationTypes> NotificationTypes_Upsert(AbstractNotificationTypes abstractNotificationTypes)
        {
            return this.abstractNotificationTypesDao.NotificationTypes_Upsert(abstractNotificationTypes); ;
        }

        public override PagedList<AbstractNotificationTypes> NotificationTypes_All(PageParam pageParam, string search)
        {
            return this.abstractNotificationTypesDao.NotificationTypes_All(pageParam, search);
        }

        public override SuccessResult<AbstractNotificationTypes> NotificationTypes_ById(long Id)
        {
            return this.abstractNotificationTypesDao.NotificationTypes_ById(Id);
        }

        public override SuccessResult<AbstractNotificationTypes> NotificationTypes_Delete(long Id)
        {
            return this.abstractNotificationTypesDao.NotificationTypes_Delete(Id);
        }
    }
}

﻿using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Data.V1;
using yudooapp.Data.Contract;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;

namespace yudooapp.Services.V1
{
    public class ShopShortingValuesServices : AbstractShopShortingValuesServices
    {
        private AbstractShopShortingValuesDao abstractShopShortingValuesDao;

        public ShopShortingValuesServices(AbstractShopShortingValuesDao abstractShopShortingValuesDao)
        {
            this.abstractShopShortingValuesDao = abstractShopShortingValuesDao;
        }

        public override SuccessResult<AbstractShopShortingValues> ShopShortingValues_Upsert(AbstractShopShortingValues abstractShopShortingValues)
        {
            return this.abstractShopShortingValuesDao.ShopShortingValues_Upsert(abstractShopShortingValues); ;
        }

        public override PagedList<AbstractShopShortingValues> ShopShortingValues_All(PageParam pageParam)
        {
            return this.abstractShopShortingValuesDao.ShopShortingValues_All(pageParam);
        }

    }

}
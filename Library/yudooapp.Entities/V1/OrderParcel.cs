﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Entities.Contract;

namespace yudooapp.Entities.V1
{
    public class OrderParcel : AbstractOrderParcel { }
    public class OrderParcelProduct : AbstractOrderParcelProduct { }
    public class CheckCreateShipment : AbstractCheckCreateShipment { }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Entities.Contract;

namespace yudooapp.Entities.V1
{
    public class ShopTimingJson : AbstractShopTimingJson
    {

    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class ShopTime
    {

        public string Id { get; set; }
        public string ShopId { get; set; }
        public string Day { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string BreaktimeStart { get; set; }
        public string BreakTimeEnd { get; set; }
        public bool IsClosed { get; set; }
        //public string CreatedBy { get; set; }
        //public string UpdatedBy { get; set; }

        public string BreakStartTime { get; set; }
        public string BreakEndTime { get; set; }
        //public string IsClosed { get; set; }

    }
}

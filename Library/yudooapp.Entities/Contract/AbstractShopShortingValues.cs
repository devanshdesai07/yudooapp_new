﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShopShortingValues
    {
        public long Id { get; set; }
        public int No_Ratings { get; set; }
        public float Rating_Weigth { get; set; }
        public float Offer_Weight { get; set; }
        public float ShopOnline_Weight { get; set; }
    }
}


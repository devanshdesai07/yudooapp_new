﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
   public abstract class AbstractShopKeeperMaster
    {
        public long Id { get; set; }
        public bool CallBackButton { get; set; }
        public long ShopKeeperVCRingTime { get; set; }
        public bool DeliveryOption { get; set; }
        public decimal KeywordSearchRelevancyWeigtage { get; set; }
        public decimal CustomerVCRingingTime { get; set; }
        public decimal VisitSimilarShopsButtonAppearTime { get; set; }
        public decimal RequestCallBackButtonAppearTime { get; set; }
        public DateTime? HelpPageCallTimingStart { get; set; }
        public DateTime? HelpPageCallTimingEnd { get; set; }
        public bool CustomerAppRating { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsRating { get; set; }
        public bool IsMasterOnline { get; set; }

        [NotMapped]
        public string HelpPageCallTimingStartStr => HelpPageCallTimingStart != null ? HelpPageCallTimingStart?.ToString("HH:mm:ss") : "-";

        [NotMapped]
        public string HelpPageCallTimingEndStr => HelpPageCallTimingEnd != null ? HelpPageCallTimingEnd?.ToString("HH:mm:ss") : "-";
    }
}

﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public  abstract class AbstractUserCart
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long WeightTypeId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string RazorPayOrderId { get; set; }
        public string UserEmail { get; set; }
        public long ShopId { get; set; }
        public long UserCartMasterId { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public string ProductImage { get; set; }
        public long Qty { get; set; }
        public decimal Price { get; set; }
        public decimal DeliveryCharge { get; set; }
        public decimal PaymentCharge { get; set; }
        public decimal Weight { get; set; }
        public string Size { get; set; }
        public string ProductInfo { get; set; }
        public string ItemName { get; set; }
        public bool IsVisibleToCustomer { get; set; }
        public bool IsSendToOrder { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public long VideoCallId { get; set; }
        public string Pincode { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string FirstName { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public decimal AmountPaid { get; set; }

        [NotMapped]
        public string ProductImageStr => string.IsNullOrEmpty(ProductImage) ? "" : Configurations.S3BaseUrl + ProductImage;

        public object ProductDetails { get; set; }
    }
}

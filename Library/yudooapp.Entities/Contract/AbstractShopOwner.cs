﻿
using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using Newtonsoft.Json;
using static yudooapp.Entities.Contract.AbstractShopTimgings;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShopOwner
    {
        public long Id { get; set; }
        public long StateId { get; set; }
        public long ShopOwnerId { get; set; }
        public long UserId { get; set; }
        public long MasterDepartmentShopId { get; set; }
        public long ParentId { get; set; }
        public string PickUpId { get; set; }
        public long UserType { get; set; }
        public string ASIN { get; set; }
        public string CompanyName { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long SubCategoryId { get; set; }
        public long CuratedListId { get; set; }
        public string ShopIdList { get; set; }
        public string SubCategoryName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string LandMark { get; set; }
        public long PinCode { get; set; }
        public decimal YudooCommission { get; set; }
        public DateTime VideoCall { get; set; }
        public DateTime RegisteredDate { get; set; }
        public bool ShopStatus { get; set; }
        public bool IsRating { get; set; }
        public long ShopRatingsId { get; set; }
        public string ShopNameGujarati { get; set; }
        public string DescriptionGujarati { get; set; }
        public string ShopNameHindi { get; set; }
        public string DescriptionHindi { get; set; }
        public decimal ShopRatings { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public bool IsOnline { get; set; }
        public bool IsGSTRegisteredShop { get; set; }
        public string GstNumber { get; set; }
        public string MobileNumber { get; set; }
        public string PersonalPanNumber { get; set; }
        public string CompanyPanNumber { get; set; }
        public string CompanyRegisterAddress { get; set; }
        public bool IsRegister { get; set; }
        public long OTP { get; set; }
        public DateTime LastLogin { get; set; }
        public string Packer { get; set; }
        public long Rank { get; set; }
        public string About { get; set; }
        public string Tags { get; set; }
        public long NoOfUsers { get; set; }
        public long ShopOBStatus { get; set; }
        public bool DraftStatus { get; set; }
        public string TermsAndConditions { get; set; }
        public string GpsLatitude { get; set; }
        public string GpsLongitude { get; set; }
        public string VideoUrls { get; set; }
        public string ImageUrls { get; set; }
        public string GifUrls { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public object Timing { get; set; }
        //public List<Timging> TimingList { get; set; } //= JsonConvert.DeserializeObject()
        public bool IsOffer { get; set; }

        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string UDID { get; set; }
        public string IMEI { get; set; }
        
       

        public int IsApprovedByAdmin { get; set; }

        //For Shop_Overview SP (Adding Columns)
        public int TotalSuccessfulOrders { get; set; }
        public decimal TotalValueOfSuccessfulOrders { get; set; }
        public int SuccessfulOrdersLastMonth { get; set; }
        public decimal ValueOfSuccessfulOrdersLastMonth { get; set; }
        public decimal CallNotReceivedPerLastMonth { get; set; }
        public int CallBacksNonAttemptedPerLastMonth { get; set; }
        public decimal ShopRatingLastTwenty { get; set; }
        public decimal ShopRatingLastOverall { get; set; }
        public decimal SearchToViewRatio { get; set; }
        public decimal ViewToVCRatio { get; set; }
        public decimal VCToPurchaseRatio { get; set; }
        public decimal PurchaseToSuccessRatio { get; set; }
        public int Positives { get; set; }
        public int Negatives { get; set; }
        public int GoodQuality { get; set; }
        public int Variety { get; set; }
        public int HighPrice { get; set; }
        public int RudeBehaviour { get; set; }
        public string Description { get; set; }

        // For Shop_Orders SP (Adding Columns)
        public long OrderId { get; set; }
        public string ItemName { get; set; }
        public DateTime CreatedDate { get; set; }
        public long OrderStatus { get; set; }
        public string OrderName { get; set; }
        public string PaymentStatus { get; set; }
        public long CustomerMobileNumber { get; set; }
        public string CustomerName { get; set; }
        public string SubcategoryArray { get; set; }

        [NotMapped]
        public bool IsNew { get; set; }
        [NotMapped]
        public bool IsApprovalPending { get; set; }
        public bool FavoriteShop { get; set; }
        public string ShopReview { get; set; }
        public decimal AverageShopRating { get; set; }
        public string AllCategory { get; set; }
        public bool IsOwner { get; set; }
        public bool IsShopOnline { get; set; }
        public DateTime LastLogOut { get; set; }
        public string FlatNumber { get; set; }
        public int AddressCount { get; set; }
        public string AddressKey { get; set; }
        public bool IsMasterOnline { get; set; }
        public string ShopTimeMSG { get; set; }
        public string ShopClosedMSG { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string VideoUrlsStr => string.IsNullOrEmpty(VideoUrls) ? "" : Configurations.S3BaseUrl + VideoUrls;
        //public object VideoUrlsStr { get; set; }
        [NotMapped]
        public string ImageUrlsStr => string.IsNullOrEmpty(ImageUrls) ? "" : Configurations.S3BaseUrl + ImageUrls;
        //public object ImageUrlsStr { get; set; }
        [NotMapped]
        public string GifUrlsStr => string.IsNullOrEmpty(GifUrls) ? "" : Configurations.S3BaseUrl + GifUrls;
        //public object GifUrlsStr { get; set; }
        [NotMapped]
        public string ThumbnailImageUrlStr => string.IsNullOrEmpty(ThumbnailImageUrl) ? "" : Configurations.S3BaseUrl + ThumbnailImageUrl;
        //public object ThumbnailImageUrlStr { get; set; }
    }

    public abstract class AbstractShopAudit
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public long PrimaryCategoryId { get; set; }
        public string Category { get; set; }
        public string SubCategoryId { get; set; }
        public string SubCategory { get; set; }
        public string ProductTag { get; set; }
        public string FullLengthVideo { get; set; }
        public string GifVideo { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string Specialities { get; set; }
        public long CommissionPercentage { get; set; }
        public bool IsApproved { get; set; }
        public bool IsNew { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }
        public long PinCode { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public string SpecialitiesName { get; set; }
        public string ShopName { get; set; }
        public string ShopNameGujarati { get; set; }
        public string ShopNameHindi { get; set; }
        public string DescriotionHindi { get; set; }
        public string DescriptionGujarati { get; set; }
        public string ShopOwnerName { get; set; }
        public string GSTNo { get; set; }
        public string CompanyName { get; set; }
        public string PersonalPAN { get; set; }
        public string CompanyPAN { get; set; }
        public bool IsSave { get; set; }
        public string ThumbnailImage { get; set; }
        public string AddressLine1 { get; set; }
        public string FlatNumber { get; set; }
    }

    public class Root //NewShopOwner
    {
        public long Id { get; set; }
        public long MasterDepartmentShopId { get; set; }
        public string ASIN { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public string LandMark { get; set; }
        public long PinCode { get; set; }
        public DateTime VideoCall { get; set; }
        public DateTime RegisteredDate { get; set; }
        public bool ShopStatus { get; set; }
        public long ShopRatingsId { get; set; }
        public string ShopNameGujarati { get; set; }
        public string DescriptionGujarati { get; set; }
        public string ShopNameHindi { get; set; }
        public string DescriptionHindi { get; set; }
        public decimal ShopRatings { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public bool IsOnline { get; set; }
        public bool IsGSTRegisteredShop { get; set; }
        public string GstNumber { get; set; }
        public string MobileNumber { get; set; }
        public string PersonalPanNumber { get; set; }
        public string CompanyPanNumber { get; set; }
        public string CompanyRegisterAddress { get; set; }
        public bool IsRegister { get; set; }
        public long OTP { get; set; }
        public string Packer { get; set; }
        public long Rank { get; set; }
        public string About { get; set; }
        public string Tags { get; set; }
        public long NoOfUsers { get; set; }
        public long ShopOBStatus { get; set; }
        public bool DraftStatus { get; set; }
        public string TermsAndConditions { get; set; }
        public string GpsLatitude { get; set; }
        public string GpsLongitude { get; set; }
        public string VideoUrls { get; set; }
        public string ImageUrls { get; set; }
        public string GifUrls { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public List<Timging> Timing { get; set; }
        public bool IsOffer { get; set; }

        //For Shop_Overview SP (Adding Columns)
        public int TotalSuccessfulOrders { get; set; }
        public decimal TotalValueOfSuccessfulOrders { get; set; }
        public int SuccessfulOrdersLastMonth { get; set; }
        public decimal ValueOfSuccessfulOrdersLastMonth { get; set; }
        public decimal CallNotReceivedPerLastMonth { get; set; }
        public int CallBacksNonAttemptedPerLastMonth { get; set; }
        public decimal ShopRatingLastTwenty { get; set; }
        public decimal ShopRatingLastOverall { get; set; }
        public decimal SearchToViewRatio { get; set; }
        public decimal ViewToVCRatio { get; set; }
        public decimal VCToPurchaseRatio { get; set; }
        public decimal PurchaseToSuccessRatio { get; set; }
        public int Positives { get; set; }
        public int Negatives { get; set; }
        public int GoodQuality { get; set; }
        public int Variety { get; set; }
        public int HighPrice { get; set; }
        public int RudeBehaviour { get; set; }
        public string Description { get; set; }

        // For Shop_Orders SP (Adding Columns)
        public long OrderId { get; set; }
        public string ItemName { get; set; }
        public DateTime CreatedDate { get; set; }
        public long OrderStatus { get; set; }
        public string OrderName { get; set; }
        public string PaymentStatus { get; set; }
        public long CustomerMobileNumber { get; set; }
        public string CustomerName { get; set; }
        public string SubcategoryArray { get; set; }

        [NotMapped]
        public bool IsNew { get; set; }
        [NotMapped]
        public bool IsApprovalPending { get; set; }
        public bool FavoriteShop { get; set; }
        public string ShopReview { get; set; }
        public decimal AverageShopRating { get; set; }
      
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string VideoUrlsStr => string.IsNullOrEmpty(VideoUrls) ? "" : Configurations.S3BaseUrl + VideoUrls;
        [NotMapped]
        public string ImageUrlsStr => string.IsNullOrEmpty(ImageUrls) ? "" : Configurations.S3BaseUrl + ImageUrls;
        [NotMapped]
        public string GifUrlsStr => string.IsNullOrEmpty(GifUrls) ? "" : Configurations.S3BaseUrl + GifUrls;
        [NotMapped]
        public string ThumbnailImageUrlStr => string.IsNullOrEmpty(ThumbnailImageUrl) ? "" : Configurations.S3BaseUrl + ThumbnailImageUrl;
    }

    public class Timging
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public string Day { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string BreaktimeStart { get; set; }
        public string BreakTimeEnd { get; set; }
        public bool IsClosed { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int DeletedBy { get; set; }
       
    }


    public abstract class AbstractShopOwnerRights
    {
        public long Id { get; set; }
        public long ShopOwnerId { get; set; }
        public long UserId { get; set; }
        public long ShopId { get; set; }
        //public string ShopOwnerRights { get; set; }
        //public Object ShopOwnerRightsObj { get; set; }
        public bool IsAdd_ChangeProductGallery_Access { get; set; }
        public bool IsConductCalls_Access { get; set; }
        public bool IsOrdersPage_Access { get; set; }
        public bool IsPerformancePage_Access { get; set; }
        public bool IsReceiveCalls_Access { get; set; }
        public bool IsSDTVU_Access { get; set; }
        public bool IsShippingPage_Access { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
    }

}


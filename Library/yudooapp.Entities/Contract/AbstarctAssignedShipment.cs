﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class AAssignedDateTime
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class AShippedBy
    {
        public string shipper_company_name { get; set; }
        public string shipper_address_1 { get; set; }
        public string shipper_address_2 { get; set; }
        public string shipper_city { get; set; }
        public string shipper_state { get; set; }
        public string shipper_country { get; set; }
        public string shipper_postcode { get; set; }
        public int shipper_first_mile_activated { get; set; }
        public string shipper_phone { get; set; }
        public object lat { get; set; }
        public object @long { get; set; }
        public string shipper_email { get; set; }
        public string rto_company_name { get; set; }
        public string rto_address_1 { get; set; }
        public string rto_address_2 { get; set; }
        public string rto_city { get; set; }
        public string rto_state { get; set; }
        public string rto_country { get; set; }
        public string rto_postcode { get; set; }
        public string rto_phone { get; set; }
        public string rto_email { get; set; }
    }

    public class AData
    {
        public int courier_company_id { get; set; }
        public string awb_code { get; set; }
        public int cod { get; set; }
        public int order_id { get; set; }
        public int shipment_id { get; set; }
        public int awb_code_status { get; set; }
        public AAssignedDateTime assigned_date_time { get; set; }
        public double applied_weight { get; set; }
        public int company_id { get; set; }
        public string courier_name { get; set; }
        public object child_courier_name { get; set; }
        public string routing_code { get; set; }
        public string rto_routing_code { get; set; }
        public string invoice_no { get; set; }
        public string transporter_id { get; set; }
        public string transporter_name { get; set; }
        public AShippedBy shipped_by { get; set; }
    }

    public class AResponse
    {
        public Data data { get; set; }
    }

    public class ARoot
    {
        public int awb_assign_status { get; set; }
        public AResponse response { get; set; }
    }


}

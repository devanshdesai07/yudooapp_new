﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Entities.V1;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserCartMaster
    {

        public long Id { get; set; }
       public long UserId { get; set; }
        public long ShopId { get; set; }
        public string UserName { get; set; }
        public string FlatNo { get; set; }
        public string Pincode { get; set; }
         public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public long PaymentMode { get; set; }
        public decimal ExtraCharge { get; set; }
        public decimal Discount { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public long VideoCallId { get; set; }
        public decimal DeliveryCharge { get; set; }
        public decimal PaymentCharge { get; set; }
        public bool IsNameChange { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";


        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";


    }
}

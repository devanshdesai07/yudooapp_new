﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    public class AbstractPersonalUserCart
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long ShopId { get; set; }
        public long Qty { get; set; }
        public decimal Weight { get; set; }
        public long WeightTypeId { get; set; }
        public string ProductImage { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public string ProductInfo { get; set; }
        public string ItemName { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public long VideoCallId { get; set; }
        public long UserCartMasterId { get; set; }
        public string Pincode { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserFeedBack
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string FeedBackSuggestions { get; set; }
        public string VideoCallID { get; set; }
        public long ExperienceId { get; set; }
    }
}

﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractPickup
    {
        public bool success { get; set; }
        public PickupAddress address { get; set; }
        public int pickup_id { get; set; }
        public string company_name { get; set; }
        public string full_name { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class PickupAddress
    {
        public int company_id { get; set; }
        public string pickup_code { get; set; }
        public string address { get; set; }
        public string address_2 { get; set; }
        public object address_type { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public object gstin { get; set; }
        public string pin_code { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public object alternate_phone { get; set; }
        public object lat { get; set; }
        public object @long { get; set; }
        public int status { get; set; }
        public int phone_verified { get; set; }
        public object rto_address_id { get; set; }
        public string extra_info { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
        public int id { get; set; }
    }



    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class ShippingAddress
    {
        public int id { get; set; }
        public string pickup_location { get; set; }
        public object address_type { get; set; }
        public string address { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string pin_code { get; set; }
        public string email { get; set; }
        public int is_first_mile_pickup { get; set; }
        public string phone { get; set; }
        public string name { get; set; }
        public int company_id { get; set; }
        public object gstin { get; set; }
        public object vendor_name { get; set; }
        public int status { get; set; }
        public int phone_verified { get; set; }
        public object lat { get; set; }
        public object @long { get; set; }
        public object warehouse_code { get; set; }
        public object alternate_phone { get; set; }
        public object rto_address_id { get; set; }
        public int lat_long_status { get; set; }
        public int @new { get; set; }
        public object associated_rto_address { get; set; }
    }

    public class GetPickUpData
    {
        public List<ShippingAddress> shipping_address { get; set; }
        public string allow_more { get; set; }
        public bool is_blackbox_seller { get; set; }
        public string company_name { get; set; }
        public List<object> recent_addresses { get; set; }
    }

    public class GetPickUp
    {
        public GetPickUpData data { get; set; }
    }




}


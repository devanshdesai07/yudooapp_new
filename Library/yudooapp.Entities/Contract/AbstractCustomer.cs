﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractCustomer
    {
        public long Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNumber { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerAddress { get; set; }
        public long ShopId { get; set; }
        public bool IsActive { get; set; }
        public string LastLogin { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsDeleted { get; set; }


        
        [NotMapped]
        public int TotalSuccessfulOrders { get; set; }
        [NotMapped]
        public decimal TotalValueOfSuccessfulOrders { get; set; }
        [NotMapped]
        public decimal ShopRatingGivenByCustomer { get; set; }
        [NotMapped]
        public decimal RatingGivenToCustomer { get; set; }
        [NotMapped]
        public decimal ReturnedOrdersInPer { get; set; }
        [NotMapped]
        public int ReturnedOrdersCount { get; set; }
        [NotMapped]
        public int CanceledOrdersCount { get; set; }
        [NotMapped]
        public int AbandenedOrdersCount { get; set; }
        [NotMapped]
        public decimal NoOfVisitRatio { get; set; }
        [NotMapped]
        public decimal TotalVideoMinuteRatio { get; set; }
        [NotMapped]
        public int NoOfShopsTried { get; set; }

        [NotMapped]
        public int OrderId { get; set; }
        [NotMapped]
        public string ItemName { get; set; }
        [NotMapped]
        public string DateOfOrder { get; set; }
        [NotMapped]
        public string OrderStatus { get; set; }
        [NotMapped]
        public string PaymentStatus { get; set; }
        [NotMapped]
        public string ShopName { get; set; }


    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShopTimingJson
    {
        public List<Datum> data { get; set; }
    }

    public class Datum
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public string Day { get; set; }
        public bool IsClosed { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string BreaktimeStart { get; set; }
        public string BreakTimeEnd { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        [NotMapped]
        public string FromTimeStr => FromTime != null ? Convert.ToDateTime(FromTime).ToString("YYYY-MM-DD HH:mm:ss") : ""; //HH:mm:ss

        [NotMapped]
        public string ToTimeStr => ToTime != null ? Convert.ToDateTime(ToTime).ToString("YYYY-MM-DD HH:mm:ss") : "";

        [NotMapped]
        public string BreaktimeStartStr => BreaktimeStart != null ? Convert.ToDateTime(BreaktimeStart).ToString("YYYY-MM-DD HH:mm:ss") : "";

        [NotMapped]
        public string BreakTimeEndStr => BreakTimeEnd != null ? Convert.ToDateTime(BreakTimeEnd).ToString("YYYY-MM-DD HH:mm:ss") : "";

    }
}

﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShiprocket
    {
        public long awb { get; set; }
        public string current_status { get; set; }
        public string order_id { get; set; }
        public string current_timestamp { get; set; }
        public string etd { get; set; }
        public int current_status_id { get; set; }
        public string shipment_status { get; set; }
        public int shipment_status_id { get; set; }
        public string channel_order_id { get; set; }
        public string channel { get; set; }
        public string courier_name { get; set; }
        public List<Scan> scans { get; set; }

    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Scan
    {
        public string date { get; set; }
        public string activity { get; set; }
        public string location { get; set; }
    }



}


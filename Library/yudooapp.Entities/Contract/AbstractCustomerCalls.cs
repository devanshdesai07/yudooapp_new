﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractCustomerCalls
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long UploadedId { get; set; }
        public DateTime CallStartedDatetime { get; set; }
        public DateTime CallEndDatetime { get; set; }
        public DateTime CallStartedDatime { get; set; }
        public long ShopId { get; set; }
        public string ShopMobileNo { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }


    }
}


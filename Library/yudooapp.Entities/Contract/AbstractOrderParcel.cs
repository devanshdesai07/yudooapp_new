﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractOrderParcel
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public object OrderProductObj { get; set; }
        public string ProductIds { get; set; }
        public decimal Weight { get; set; }
        public string WeightType { get; set; }
        public decimal Length { get; set; }
        public string LengthType { get; set; }
        public decimal Breadth { get; set; }
        public string BreadthType { get; set; }
        public decimal Height { get; set; }
        public string HeightType { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsShiped { get; set; }
        public long ShipmentId { get; set; }
        public string ManifestUrl { get; set; }
        public string LabelUrl { get; set; }

    }

    public abstract class AbstractOrderParcelProduct
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public string ProductImage { get; set; }
        public long Qty { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public long ItemStatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string ProductInfo { get; set; }
        public string ItemName { get; set; }
        public long IsShiped { get; set; }
    }

    public abstract class AbstractCheckCreateShipment
    {
        public int number { get; set; }
    }


}


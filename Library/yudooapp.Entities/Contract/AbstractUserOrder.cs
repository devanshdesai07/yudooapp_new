﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using System.Web;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserOrder
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public long UserMobileNumber { get; set; }
        public string RazorPayOrderId { get; set; }
        public string UserEmail { get; set; }
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public string ShopMobileNumber { get; set; }
        public string CompanyPanNumber { get; set; }
        public string PersonalPanNumber { get; set; }
        public string GstNumber { get; set; }
        public string AmountBifurcationString { get; set; }
        public object ProductDetailsObj { get; set; }
        public string ProductDetails { get; set; }
        public string ProductName { get; set; }
        public long PaymentId { get; set; }
        public string PaymentStatus { get; set; }

        public string PaymentMode { get; set; }
        public decimal ExtraCharge { get; set; }
        public decimal Discount { get; set; }
        public decimal DeliveryCharge { get; set; }
        public decimal PaymentCharge { get; set; }

        public long AddressId { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal DeliveryAmount { get; set; }
        public decimal OtherAmount { get; set; }
        //public decimal Discount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AmountPaid { get; set; }
        public long OrderStatus { get; set; }
        public string OrderStatusName { get; set; }
        public string PaymentLink { get; set; }
        public string VideoCallLink { get; set; }
        public long VideoCallId { get; set; }
        public long OrderNumber { get; set; }
        public long DeliveryRating { get; set; }
        public int UserCartMasterId { get; set; }
        public long DeletedBy { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public bool IsShipment { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }

        [NotMapped]
        public string FirstOrderImage { get; set; }
        [NotMapped]
        public string FirstOrderItemName { get; set; }
        [NotMapped]
        public int TotalItemsCount { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string FirstOrderImageStr => string.IsNullOrEmpty(FirstOrderImage) ? "" : Configurations.S3BaseUrl + FirstOrderImage;
        [NotMapped]
        public string AmountBifurcationStringStr => AmountBifurcationString != null && AmountBifurcationString != "" ? HttpUtility.HtmlDecode(AmountBifurcationString) : null; 
    }


    public abstract class AbstractUserOrderDetails
    {

        public int Id { get; set; }
        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public int OrderStatus { get; set; }
        public string OrderStatusName { get; set; }
        public object ProductDetails { get; set; }
        public object ProductItem { get; set; }
        public string UserName { get; set; }
        public string UserAddress { get; set; }
        public string UserPincode { get; set; }
        public int PaymentMode { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal DeveliveryAndPaymentFeed { get; set; }
        public decimal DeveliveryCharges { get; set; }
        public decimal PaymentCharges { get; set; }
    }
}


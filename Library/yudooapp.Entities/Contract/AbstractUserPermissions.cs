﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserPermissions
    {
        public long Id { get; set; }
        public long AdminTypeId { get; set; }
        public int PageId { get; set; }
        public bool Status { get; set; }                             
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        [NotMapped]
        public string PageName { get; set; }
        [NotMapped]
        public string SubPageAction { get; set; }
    }
}


﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserOrderItems
    {
        public long Id { get; set; }
        public long UserOrderItemsId { get; set; }
        public long OrderId { get; set; }
        public long ShipmentId { get; set; }
        public string ProductImage { get; set; }
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public string PaymentStatus { get; set; }
        public long Qty { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public long ItemStatusId { get; set; }
        public string ProductInfo { get; set; }
        public string StatusName { get; set; }
        public string ItemName { get; set; }
        public long DeletedBy { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool IsShiped { get; set; }
        public int WeightTypeId { get; set; }
        public decimal Weight { get; set; }
        public string ManifestUrl { get; set; }
        public string LabelUrl { get; set; }
        public DateTime OrderDateAndTime { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string OrderDateAndTimeStr => OrderDateAndTime != null ? OrderDateAndTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string ProductImageStr => string.IsNullOrEmpty(ProductImage) ? "" : Configurations.S3BaseUrl + ProductImage;
    }
}


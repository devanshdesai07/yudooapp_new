﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractCategory
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string NameGujarati { get; set; }
        public string NameHindi { get; set; }
        public bool IsUp { get; set; }
        public decimal DefaultCharge { get; set; }
        public bool IsDeleted { get; set; }
        [NotMapped]
        public string CategoryUrlStr => string.IsNullOrEmpty(ImageUrl) ? "" : Configurations.S3BaseUrl + ImageUrl;

    }
}


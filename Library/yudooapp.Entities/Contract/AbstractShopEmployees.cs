﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShopEmployees
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public long SubUserId { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpLastName { get; set; }
        public string EmpEmail { get; set; }
        public long EmpPhoneNumber { get; set; }
        public string EmpType { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsReceiveCalls_Access { get; set; }
        public bool IsConductCalls_Access { get; set; }
        public bool IsOrdersPage_Access { get; set; }
        public bool IsPerformancePage_Access { get; set; }
        public bool IsAdd_ChangeProductGallery_Access { get; set; }
        public bool IsSDTVU_Access { get; set; }
        public bool IsShippingPage_Access { get; set; }
    }
}

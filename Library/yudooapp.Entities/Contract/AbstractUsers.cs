﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUsers
    {
        public long Id { get; set; }
        public string ProfileImage { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public long MobileNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DOB { get; set; }
        public long OTP { get; set; }
        public bool IsVerified { get; set; }
        public bool IsActive { get; set; }
        public bool IsCallBackPreference { get; set; }
        public long TotalOrder { get; set; }
        public long TotalAmount { get; set; }
        public DateTime LastPurchase { get; set; }
        public DateTime LastCall { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public string DeviceType { get; set; }
        public string UDID { get; set; }
        public string IMEI { get; set; }
        public string DeviceToken { get; set; }
        public long ShopId { get; set; }
        public long UserId { get; set; }
        public bool IsRating { get; set; }
        public bool IsSupportCall { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string ProfileImageStr => string.IsNullOrEmpty(ProfileImage) ? "" : Configurations.S3BaseUrl + ProfileImage;
        
    }

    public abstract class AbstractCallerInfo
    {
        public string UserName { get; set; }
        public long TotalOrder { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime LastPurchaseDate { get; set; }
        public DateTime LastCallDate { get; set; }
        public string LastPurchaseDay { get; set; }
        public string LastCallDay { get; set; }
        [NotMapped]
        public string LastPurchaseDateStr => LastPurchaseDate != null ? LastPurchaseDate.ToString("dd-mm-yyyy") : "-";
        [NotMapped]
        public string LastCallDateStr => LastCallDate != null ? LastCallDate.ToString("dd-mm-yyyy") : "-";
    }
}


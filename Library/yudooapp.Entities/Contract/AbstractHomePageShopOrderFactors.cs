﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
   public abstract class AbstractHomePageShopOrderFactors
    {
        public int Id { get; set; }
        public decimal Offer_Discount_Announcement { get; set; }
        public decimal ShopBusy_ShopAvailable_ShopOffline { get; set; }
        public decimal ShopCallNo_ReceiveRatio { get; set; }
        public decimal ShopVideoCalltoOrderRatiointhecategory { get; set; }
        public decimal ShopViewtoVideoCallRatiointhecategory { get; set; }
        public decimal ShopRating_OrderRating { get; set; }
        public decimal ReturnRatio { get; set; }




    }
}

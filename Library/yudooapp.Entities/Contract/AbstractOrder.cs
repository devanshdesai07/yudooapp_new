﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractOrder
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long ProductId { get; set; }
        public long OrderRatings { get; set; }
        public long Amount { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string UserAddress { get; set; }
        public string UserPincode { get; set; }
        public int PaymentMode { get; set; }
        public long ShopId { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNumber { get; set; }
        public string PaymentType { get; set; }
        public long Ratings { get; set; }
        public long RatingsBy { get; set; }
        public long MasterOrderStatusId { get; set; }
        public string MasterOrderStatusName { get; set; }
        public long PaymentId { get; set; }
        public string PaymentStatus { get; set; }
        public long ShopOwnerId { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public DateTime RatingsCreatedDate { get; set; }
        public DateTime RatingsUpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public decimal ExtraCharge { get; set; }
        public decimal Discount { get; set; }
        public decimal DeliveryCharge { get; set; }
        public decimal PaymentCharge { get; set; }
        public object ProductDetails { get; set; }
        public string PaymentLink { get; set; }
    }

    public abstract class AbstractOrderDetails
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long OrderStatus { get; set; }
        public long ShopId { get; set; }
        public string PaymentStatus { get; set; }
        public string CustomePhoneNumber { get; set; }
        public string ShopName { get; set; }
        public string ShopkeeperPaymentStatus { get; set; }
        public string CustomerName { get; set; }
        public decimal PaymentCharge { get; set; }
        public decimal DeliveryCharge { get; set; }
        public decimal TotalPaymentMadeByCustomer { get; set; }
        public DateTime CreatedDate { get; set; }
        public String CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("yyyy-MM-dd hh:mm") : "-";

    }

}


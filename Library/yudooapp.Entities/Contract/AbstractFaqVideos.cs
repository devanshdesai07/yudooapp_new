﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractFaqVideos
    {
        public long Id { get; set; }
        public string VideoHeading { get; set; }
        public string VideoUrl { get; set; }
        public string ThumbnailImage { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }

        [NotMapped]
        public string VideoUrlStr => string.IsNullOrEmpty(VideoUrl) ? "" : Configurations.S3BaseUrl + VideoUrl;
        [NotMapped]
        public string ThumbnailImageUrlStr => string.IsNullOrEmpty(ThumbnailImage) ? "" : Configurations.S3BaseUrl + ThumbnailImage;
    }
}

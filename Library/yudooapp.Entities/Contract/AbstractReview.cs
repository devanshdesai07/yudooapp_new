﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractReview
    {
        public long Id { get; set; }
        public string ReviewComment { get; set; }
        public long CustomerId { get; set; }
        public long ReviewBy { get; set; }
        public DateTime ReviewDate { get; set; }
        [NotMapped]
        public string ReviewDateStr => ReviewDate != null ? ReviewDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}


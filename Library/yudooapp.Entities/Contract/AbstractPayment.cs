﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractPayment
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long DeliveryPaymentFee { get; set; }
        public long DeliveryCharges { get; set; }
        public long PaymentCharges { get; set; }
        public long TotalPayment { get; set; }
        public string PaymentStatus { get; set; }
    }

    public abstract class AbstractCustomerPaymentStatus
    {
        public long Id { get; set; }
        public string Status { get; set; }
    }
}


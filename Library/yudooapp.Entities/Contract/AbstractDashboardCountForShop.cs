﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractDashboardCountForShop
    {
        public long PendingOnlinePaymentByCustomer { get; set; }
        public long NumberOfPendingDispatch { get; set; }
        public long InTransist { get; set; }
        public long Delivered { get; set; }
        public long PendingCallbacks { get; set; }
        public long CustomerVisit { get; set; }
        public long Last24HourPaymentReceived { get; set; }
        public long PaymentReveivedOnPeriod { get; set; }
        public long OrderCompletedOnPeriod { get; set; }
        public long OrderInProgress { get; set; }
        public bool ShopStatus { get; set; }


    }
}

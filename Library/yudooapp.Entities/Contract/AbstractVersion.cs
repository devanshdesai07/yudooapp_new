﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yudooapp.Entities.Contract
{
    public class AbstractVersion
    {
        public int VersionId { get; set; }
        public string VersionName { get; set; }
        public string Message { get; set; }
    }
}

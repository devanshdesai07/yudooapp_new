﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserOrderAmountBifurcation
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long OrderId { get; set; }
        public decimal OrderTotalAmount { get; set; }
        public DateTime OrderCreatedDate { get; set; }
        public string Type { get; set; }
        public string Ids { get; set; }
        public string Name { get; set; }
        public string UserMobileNumber { get; set; }
        public string ShopMobileNumber { get; set; }
        public string Note { get; set; }
        public string Email { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnername { get; set; }
        public string GstNumber { get; set; }
        public string PersonalPanNumber { get; set; }
        public string CompanyPanNumber { get; set; }
        public decimal Amount { get; set; }
        public int IsPaid{ get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
       
        

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";
        [NotMapped]
        public string PaymentDateStr => PaymentDate != null ? PaymentDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string OrderCreatedDateStr => OrderCreatedDate != null ? OrderCreatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";
    }
}


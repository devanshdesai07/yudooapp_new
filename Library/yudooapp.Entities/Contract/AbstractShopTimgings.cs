﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShopTimgings
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public string Day { get; set; }
        public DateTime? FromTime { get; set; }
        public DateTime? ToTime { get; set; }       
        public DateTime? BreaktimeStart { get; set; }
        public DateTime? BreakTimeEnd { get; set; }
        public bool IsClosed { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }

        [NotMapped]
        public string FromTimeStr => FromTime != null ? Convert.ToDateTime(FromTime).ToString("hh:mm tt") : "";
        [NotMapped]
        public string ToTimeStr => ToTime != null ? Convert.ToDateTime(ToTime).ToString("hh:mm tt") : "";
        [NotMapped]
        public string BreaktimeStartStr => BreaktimeStart != null ? Convert.ToDateTime(BreaktimeStart).ToString("hh:mm tt") : "";
        [NotMapped]
        public string BreakTimeEndStr => BreakTimeEnd != null ? Convert.ToDateTime(BreakTimeEnd).ToString("hh:mm tt") : "";


        [NotMapped]
        public string FromTime24 => FromTime != null ? Convert.ToDateTime(FromTime).ToString("HH:mm") : "";
        [NotMapped]
        public string ToTime24 => ToTime != null ? Convert.ToDateTime(ToTime).ToString("HH:mm") : "";
        [NotMapped]
        public string BreaktimeStart24 => BreaktimeStart != null ? Convert.ToDateTime(BreaktimeStart).ToString("HH:mm") : "";
        [NotMapped]
        public string BreakTimeEnd24 => BreakTimeEnd != null ? Convert.ToDateTime(BreakTimeEnd).ToString("HH:mm") : "";
    }
}


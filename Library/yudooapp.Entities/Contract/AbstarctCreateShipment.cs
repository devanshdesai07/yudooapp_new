﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstarctCreateShipment
    {
        public string order_id { get; set; }
        public string order_date { get; set; }
        public string pickup_location { get; set; }
        public string channel_id { get; set; }
        public string comment { get; set; }
        public string billing_customer_name { get; set; }
        public string billing_last_name { get; set; }
        public string billing_address { get; set; }
        public string billing_address_2 { get; set; }
        public string billing_city { get; set; }
        public string billing_pincode { get; set; }
        public string billing_state { get; set; }
        public string billing_country { get; set; }
        public string billing_email { get; set; }
        public string billing_phone { get; set; }
        public bool shipping_is_billing { get; set; }
        public string shipping_customer_name { get; set; }
        public string shipping_last_name { get; set; }
        public string shipping_address { get; set; }
        public string shipping_address_2 { get; set; }
        public string shipping_city { get; set; }
        public string shipping_pincode { get; set; }
        public string shipping_country { get; set; }
        public string shipping_state { get; set; }
        public string shipping_email { get; set; }
        public string shipping_phone { get; set; }
        public List<OrderItem> order_items { get; set; }
        public string payment_method { get; set; }
        public int shipping_charges { get; set; }
        public int giftwrap_charges { get; set; }
        public int transaction_charges { get; set; }
        public int total_discount { get; set; }
        public decimal sub_total { get; set; }
        public decimal length { get; set; }
        public decimal breadth { get; set; }
        public decimal height { get; set; }
        public decimal weight { get; set; }

    }
    public class OrderItem
    {
        public string name { get; set; }
        public string sku { get; set; }
        public int units { get; set; }
        public string selling_price { get; set; }
        public string discount { get; set; }
        public string tax { get; set; }
        public int hsn { get; set; }
    }


    public class CreateShipmentResData
    {
        public int phone_verified { get; set; }
        public int id { get; set; }
        public string pickup_location { get; set; }
        public string address { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string seller_name { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public int status { get; set; }
        public string pin_code { get; set; }
        public object lat { get; set; }
        public object @long { get; set; }
        public object warehouse_code { get; set; }
        public List<CreateShipmentResData> data { get; set; }
    }

    //public class CreateShipmentRes
    //{
    //    public string message { get; set; }
    //    public CreateShipmentResData data { get; set; }
    //}
    public class CreateShipmentRes
    {
        public int order_id { get; set; }
        public int shipment_id { get; set; }
        public string status { get; set; }
        public int status_code { get; set; }
        public int onboarding_completed_now { get; set; }
        public string awb_code { get; set; }
        public string courier_company_id { get; set; }
        public string courier_name { get; set; }
    }

    public class CreatePickupLocation
    {
        public string pickup_location { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string pin_code { get; set; }
    }
}


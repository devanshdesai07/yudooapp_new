﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractAdmin
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long AdminTypesId { get; set; }
        public string Name { get; set; }
        public string AdminTypesName { get; set; }
        public string AdminUsersName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
         public string NewPassword { get; set; }
         public string ConfirmPassword { get; set; }
        public string DOB { get; set; }
        public bool IsActive { get; set; }
        public bool IsOnline { get; set; }
        public DateTime LastLogin { get; set; }
        public string OTP { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string UserImages { get; set; }
        public bool IsAdminCall { get; set; }
        [NotMapped]
        public string UserUrlStr => string.IsNullOrEmpty(UserImages) ? "" : Configurations.S3BaseUrl + UserImages;

        //[NotMapped]
        //public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";
        //[NotMapped]
        //public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";
        //[NotMapped]
        //public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm:ss tt") : "-";
    }
}


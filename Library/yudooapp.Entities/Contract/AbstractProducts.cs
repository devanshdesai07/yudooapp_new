﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace yudooapp.Entities.Contract
{
    public abstract class AbstractProducts
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public long ProductTypeId { get; set; }
        public long Ratings { get; set; }
        public long RatingsBy { get; set; }
        public DateTime RatingsCreatedDate { get; set; }
        public DateTime RatingsUpdatedDate { get; set; }
        public long ProductExperience { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductDescription { get; set; }
        public long ServiceExperience { get; set; }
        public string ProductWeight { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}


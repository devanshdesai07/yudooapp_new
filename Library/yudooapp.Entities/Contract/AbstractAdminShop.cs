﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Entities.V1;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractAdminShop
    {

        public long Id { get; set; }
        public string ShopName { get; set; }
        public string GstNumber { get; set; }
        public string PersonalPanNumber { get; set; }
        public string CompanyPanNumber { get; set; }
        public string CompanyRegisterAddress { get; set; }
        public long ShopOBStatus { get; set; }
        public string ShopOwnerName { get; set; }
        public string MobileNumber { get; set; }
        public string StateName { get; set; }
        public long PinCode { get; set; }
        public string About { get; set; }
        public long CommisionPercentage { get; set; }
        public long CategoryId { set; get; }
        public string CategoryName { get; set; }
        public long SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string Tags { get; set; }
        public string TermsAndConditions { get; set; }
        public string Address { get; set; }
        public string GpsLatitude { get; set; }
        public string GpsLongitude { get; set; }
        public string BankAccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string BankName { get; set; }
        public string AccountHolderName { get; set; }
        public long ShopId { get; set; }
        public long CityId { get; set; }
        public long StateId { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string DateOfJoining { get; set; }
        public string CityName { get; set; }
        public string PrimaryCategory { get; set; }
        public string AllCategory { get; set; }
        public List<ShopTimgings> ShopTimgings { get;set; }
        public List<ShopImagesAndVideos> ShopImagesandVideos { get;set; }
        public bool IsNew { get; set; }
        public bool IsApprovalPending { get; set; }
        public bool DraftStatus { get; set; }
        public string FullLengthVideo { get; set; }
        public string GifVideo { get; set; }
        public string ImageUrl { get; set; }
        public string Specialities { get; set; }
        public string SubCategoryArray { get; set; }
        public string ShopNameGujarati { get; set; }
        public string ShopNameHindi { get; set; }
        public string DescriotionHindi { get; set; }
        public string DescriptionGujarati { get; set; }
        public string GSTNo { get; set; }
        public string CompanyName { get; set; }
        public string PersonalPAN { get; set; }
        public string CompanyPAN { get; set; }
        public int CommissionPercentage { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string SpecialitiesName { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public DateTime LastLogOut { get; set; }
        public DateTime CallStartTime { get; set; }
        public DateTime LocalCallStartTime { get; set; }
        public string ShopAction { get; set; }
        public string CustomerAction { get; set; }
        public string AddressLine1 { get; set; }
        public string FlatNumber { get; set; }
        public string ShopAuditAddress { get; set; }
        public string ShopAuditFlatNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNumber { get; set; }

        [NotMapped]
        public string RegisteredDateStr => RegisteredDate != null ? RegisteredDate.ToString("dd-MMM-yyyy") : "-";

        [NotMapped]
        public string LastLogOutStr => LastLogOut != null ? LastLogOut.ToString("dd-MMM-yyyy") : "-";

        [NotMapped]
        public string CallStartTimeStr => CallStartTime != null ? CallStartTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public string LocalCallStartTimeStr => LocalCallStartTime != null ? LocalCallStartTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractBanners
    {
        public long Id { get; set; }
        public long Priority { get; set; }
        public string BannerName { get; set; }
        public bool IsSpecialist { get; set; }
        public string Descriptions { get; set; }
        public string BannerUrl { get; set; }
        public string BannerType { get; set; }
        public long BannerShopId { get; set; }
        public long DisplayOrder { get; set; }
        public bool IsActive { get; set; }                             
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string NavigationURL { get; set; }
        public string CuratedShopList { get; set; }

        [NotMapped]
        public string BannerUrlStr => string.IsNullOrEmpty(BannerUrl) ? "" : Configurations.S3BaseUrl + BannerUrl;
    }
}


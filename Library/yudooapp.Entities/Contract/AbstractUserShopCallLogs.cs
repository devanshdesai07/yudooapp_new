﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractUserShopCallLogs
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long UserId { get; set; }
        public long CallActionPersonId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public string ProfileImage { get; set; }
        public long UserMobileNumber { get; set; } // For UserShopCallLogsDetails_All SP
        public string UserName { get; set; } // For UserShopCallLogsDetails_All SP
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public string ShopMobileNumber { get; set; } // For UserShopCallLogsDetails_All SP
        public DateTime CallDateTime { get; set; }
        //public string CallDateTime { get; set; }
        //public long CallStatus { get; set; } //because db type long in sql table
        public long CallStatus { get; set; }
        public string DescriptionByUser { get; set; }
        public string DescriptionByShop { get; set; }
        public DateTime CallStartTime { get; set; }
        public DateTime CallEndTime { get; set; }
        public int VideoCallDuration { get; set; } // For UserShopCallLogsDetails_All SP
        public string WhoInitiated { get; set; }
        public int CallBackHrs { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public string Durationtime { get; set; }
        public string VideoCallLink { get; set; }
        public string VideoSessionId { get; set; }
        public long AdminUserId { get; set; }
        public long AdminId { get; set; }
        public int Type { get; set; }
        public string CallStatusName { get; set; }
        [NotMapped]
        public decimal PastThreeOrdersWorth { get; set; }
        [NotMapped]
        public DateTime LastOrderDate { get; set; }
        [NotMapped]
        public DateTime LastCallDate { get; set; }
        public string VideoCallDurationMinute { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        [NotMapped]
        public string CallStartTimeStr => CallStartTime != null ? CallStartTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        [NotMapped]
        public string VideoCallLinkStr => string.IsNullOrEmpty(VideoCallLink) ? "" : Configurations.VideoCallUrl + VideoCallLink;

        [NotMapped]
        public string CallDateTimeeStr => CallDateTime != null ? CallDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }

    public abstract class AbstractRequest_CallBack_Admin
    {
        public long Id { get; set; }
        public long NoAnswerCount { get; set; }
        public long UserId { get; set; }
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public long CallActionPersonId { get; set; }
        public long AdminId { get; set; }
        public DateTime CallDateTime { get; set; }
        public long CallStatus { get; set; }
        public string DescriptionByUser { get; set; }
        public string DescriptionByShop { get; set; }
        public DateTime CallStartTime { get; set; }
        public DateTime CallEndTime { get; set; }
        public long WhoInitiated { get; set; }
        public DateTime CallBackHrs { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public long AdminUserId { get; set; }
        public string VideoCallLink { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        [NotMapped]
        public string CallStartTimeStr => CallStartTime != null ? CallStartTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        [NotMapped]
        public string VideoCallLinkStr => string.IsNullOrEmpty(VideoCallLink) ? "" : Configurations.VideoCallUrl + VideoCallLink;

        [NotMapped]
        public string CallDateTimeeStr => CallDateTime != null ? CallDateTime.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }

}


﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;


namespace yudooapp.Entities.Contract
{
    public abstract class AbstractNotification
    {
        public long Id { get; set; }
        public long ShopOwnerId { get; set; }
        public long StatusId { get; set; }
        public string Description { get; set; }
        //public string Type { get; set; }
        public long Type { get; set; }
        public bool IsSent { get; set; }
        public long NotificationTypeId { get; set; }
        public long UserId { get; set; }
        public string Text { get; set; }
        public DateTime ActionDateTime { get; set; }
        public DateTime ReadDateTime { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}

﻿using yudooapp;
using yudooapp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace yudooapp.Entities.Contract
{
    public abstract class AbstractShopImagesAndVideos
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public string MediaUrl { get; set; }
        public bool IsVideo { get; set; }
        public bool IsCoverImage { get; set; }
        public bool IsActive { get; set; }
        public string ThumbnailImage { get; set; }
        public bool IsUploadedFromMobile { get; set; }
        public bool IsApprovedByEmployee { get; set; }
        public bool IsApprovedByAdmin { get; set; }
        public long DeletedBy { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string MediaUrlStr => string.IsNullOrEmpty(MediaUrl) ? "" : Configurations.S3BaseUrl + MediaUrl;
        [NotMapped]
        public string ThumbnailImageUrlStr => string.IsNullOrEmpty(ThumbnailImage) ? "" : Configurations.S3BaseUrl + ThumbnailImage;
    }

   
}


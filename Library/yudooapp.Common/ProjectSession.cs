﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;

namespace yudooapp.Common
{
    public class ProjectSession
    {
        public static long AdminId
        {
            get
            {
                if (HttpContext.Current.Session["AdminId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["AdminId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminId"] = value;
            }
        } public static long UserId
        {
            get
            {
                if (HttpContext.Current.Session["UserId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["UserId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }

        public static string AdminName
        {
            get
            {
                if (HttpContext.Current.Session["AdminName"] == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AdminName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminName"] = value;
            }
        }
        public static string AdminProfile
        {
            get
            {
                if (HttpContext.Current.Session["AdminProfile"] == "")
                {
                    return "-";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AdminProfile"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminProfile"] = value;
            }
        }
        public static long AdminTypeId
        {
            get
            {
                if (HttpContext.Current.Session["AdminTypeId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["AdminTypeId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminTypeId"] = value;
            }
        }

        public static bool IsAdminCall
        {
            get
            {
                if (HttpContext.Current.Session["IsAdminCall"] == null)
                {
                    return false;
                }
                else
                {
                    return ConvertTo.Boolean(HttpContext.Current.Session["IsAdminCall"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsAdminCall"] = value;
            }
        }

    }
}
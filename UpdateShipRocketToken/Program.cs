﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace UpdateShipRocketToken
{
    class Program
    {
        //public static string DevConnStr = "Data Source=databaseyudoo.cdxog55h32lu.ap-south-1.rds.amazonaws.com;Initial Catalog=yudoo-dev;Persist Security Info=True;User ID=admin;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        //public static string ProdConnStr = "Data Source=databaseyudoo.cdxog55h32lu.ap-south-1.rds.amazonaws.com;Initial Catalog=yudoo-prod;Persist Security Info=True;User ID=admin;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";

        //public static string DevConnStr = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=YudooDev;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        //public static string ProdConnStr = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=YudooProd;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        static void Main(string[] args)
        {

            string DevConnStr = ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString;
            string ProdConnStr = ConfigurationManager.ConnectionStrings["prodConnection"].ConnectionString;

            SqlConnection devConn = new SqlConnection(DevConnStr);
            SqlConnection prodConn = new SqlConnection(ProdConnStr);

            try
            {
                Console.WriteLine("Getting Connection ...");

                HttpClient ClientDisableRedirect = new HttpClient();
                ClientDisableRedirect.BaseAddress = new Uri("https://apiv2.shiprocket.in/");

                ResponseOfGetAPI modelList = new ResponseOfGetAPI();

                var reqModel = new 
                {
                    email = "shiprocket.api@yudoo.co",
                    password = "*sS123123"
                };

                var serializedItemToCreate = JsonConvert.SerializeObject(reqModel);
                //ClientDisableRedirect.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                ClientDisableRedirect.DefaultRequestHeaders
                      .Accept
                      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                HttpResponseMessage redirectPaymentpage = ClientDisableRedirect.PostAsync("v1/external/auth/login", new StringContent(serializedItemToCreate, Encoding.UTF8,
                                    "application/json")).Result;

                if (redirectPaymentpage.IsSuccessStatusCode)
                {
                    string data = redirectPaymentpage.Content.ReadAsStringAsync().Result;
                    modelList = JsonConvert.DeserializeObject<ResponseOfGetAPI>(data);

                    string updateQuery = "UPDATE ShipRocketToken SET DeviceToken = '" + modelList.token + "' where Id = 1";
                    SqlCommand updateDevCmd = new SqlCommand(updateQuery, devConn);
                    devConn.Open();
                    updateDevCmd.ExecuteNonQuery();
                    devConn.Close();

                    SqlCommand updateProdCmd = new SqlCommand(updateQuery, prodConn);
                    prodConn.Open();
                    updateProdCmd.ExecuteNonQuery();
                    prodConn.Close();
                }
            }
            catch (Exception ex) 
            { 

            }
        }
    }

    public class ResponseOfGetAPI
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public int company_id { get; set; }
        public string created_at { get; set; }
        public string token { get; set; }
    }
}




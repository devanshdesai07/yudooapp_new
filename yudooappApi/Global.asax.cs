﻿using Autofac;
using NLog.LayoutRenderers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using yudooapp.Common;

namespace yudooappApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            NLog.Config.ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("mdlc", typeof(MdlcLayoutRenderer));
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ContainerBuilder builder = new ContainerBuilder();
            Bootstrapper.Resolve(builder);
        }

        protected void Application_Error()
        {
            //Code that runs when an unhandled error occurs
            Exception ErrorInfo = Server.GetLastError().GetBaseException();
            CommonHelper.LogError(Server.MapPath("~/ErrorLog/ErrorLog.txt"), ErrorInfo);
            Server.ClearError();
            //if (Request.RequestContext.RouteData.DataTokens["area"] != null && Request.RequestContext.RouteData.DataTokens["area"].ToString().ToLower() == Pages.Areas.Admin.ToLower())
            //    Response.Redirect(CommonHelper.UrlBase + Pages.Areas.Admin + "/" + Pages.Controllers.Account + "/" + Pages.Actions.Error);
            //else
            //    Response.Redirect(CommonHelper.UrlBase + Pages.Controllers.Account + "/" + Pages.Actions.Error);
        }


        //void Application_EndRequest(Object Sender, EventArgs e)
        //{
        //    //HttpApplication application = (HttpApplication)Sender;

        //    //HttpResponse responses = application.Context.Response;
        //    //ProcessResponse(responses.OutputStream);

        //    var request = (Sender as HttpApplication).Request;
        //    var response = (Sender as HttpApplication).Response;

        //    if (request.HttpMethod == "POST" || request.HttpMethod == "PUT" || request.HttpMethod == "GET")
        //    {


        //        byte[] bytes = request.BinaryRead(request.TotalBytes);
        //        string body = Encoding.UTF7.GetString(bytes);
        //        string APIUrl = request.CurrentExecutionFilePath;
        //        if (!String.IsNullOrEmpty(APIUrl))
        //        {
        //            //Exception ErrorInfo = Server.GetLastError().GetBaseException();
        //            //CommonHelper.LogError(System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog/ErrorLog1.txt"), APIUrl);

        //            // Do your logic here (Save in DB, Log in IIS etc.)
        //        }

                
        //    }
        //}

        private string ProcessResponse(Stream stream)
        {            
            StreamReader sr = new StreamReader(stream);
            string content = sr.ReadToEnd();
            return content;
        }

        private void Log(string line)
        {
            //Debugger.Log(0, null, String.Format("{0}\n", line));
        }
    }
}

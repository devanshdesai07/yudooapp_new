﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace yudooappApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult PaymentSuccess()
        {
            ViewBag.Title = "Payment Success";

            return View();
        }

        public ActionResult PaymentFaild()
        {
            ViewBag.Title = "Payment Faild";

            return View();
        }


    }
}

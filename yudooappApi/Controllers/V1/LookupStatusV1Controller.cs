﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LookupStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLookupStatusServices abstractLookupStatusServices;
        #endregion

        #region Cnstr
        public LookupStatusV1Controller(AbstractLookupStatusServices abstractLookupStatusServices)
        {
            this.abstractLookupStatusServices = abstractLookupStatusServices;
        }
        #endregion

        

        // LookupStatus_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("LookupStatus_All")]
        public async Task<IHttpActionResult> LookupStatus_All(PageParam pageParam, string search = "")
        {
            var quote = abstractLookupStatusServices.LookupStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        

    }
}
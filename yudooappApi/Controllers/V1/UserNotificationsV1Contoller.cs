﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
//using SendGrid;
//using SendGrid.Helpers.Mail;

namespace AAHApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserNotificationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserNotificationsServices abstractUserNotificationsServices;
        #endregion

        #region Cnstr
        public UserNotificationsV1Controller(AbstractUserNotificationsServices abstractUserNotificationsServices)
        {
            this.abstractUserNotificationsServices = abstractUserNotificationsServices;
        }
        #endregion


        // UserNotifications_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_Insert")]
        public async Task<IHttpActionResult> UserNotifications_Insert(UserNotifications userNotifications)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_Insert(userNotifications);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserNotifications_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_ByUserId")]
        public async Task<IHttpActionResult> UserNotifications_ByUserId(PageParam pageParam, long UserId = 0, long RequestId = 0, string ActionFormDate = "", string ActionToDate = "", int Sent = 0)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_ByUserId(pageParam, UserId, RequestId, ActionFormDate, ActionToDate, Sent);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserNotifications_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserNotifications_ByRequestId")]
        public async Task<IHttpActionResult> UserNotifications_ByNotificationsTypeId(PageParam pageParam, long NotificationsTypeId = 0, long UserId = 0)
        {
            var quote = abstractUserNotificationsServices.UserNotifications_ByNotificationsTypeId(pageParam, NotificationsTypeId, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HelpSupportV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractHelpSupportServices abstractHelpSupportServices;
        #endregion

        #region Cnstr
        public HelpSupportV1Controller(AbstractHelpSupportServices abstractHelpSupportServices)
        {
            this.abstractHelpSupportServices = abstractHelpSupportServices;
        }
        #endregion

        // HelpSupport_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("HelpSupport_Upsert")]
        public async Task<IHttpActionResult> HelpSupport_Upsert(HelpSupport HelpSupport)
        {
            var quote = abstractHelpSupportServices.HelpSupport_Upsert(HelpSupport);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // HelpSupport_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("HelpSupport_All")]
        public async Task<IHttpActionResult> HelpSupport_All(PageParam pageParam, string search = "")
        {
            var quote = abstractHelpSupportServices.HelpSupport_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //HelpSupport_Action API
        [System.Web.Http.HttpPost]
        [InheritedRoute("HelpSupport_Action")]
        public async Task<IHttpActionResult> HelpSupport_Action(long Id, long StatusId)
        {
            var quote = abstractHelpSupportServices.HelpSupport_Action(Id, StatusId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SpecialityV1Controller : AbstractBaseController
    {
        

        #region Fields
        private readonly AbstractSpecialityServices abstractSpecialityServices;
        #endregion

        #region Cnstr
        public SpecialityV1Controller(AbstractSpecialityServices abstractSpecialityServices)
        {
            this.abstractSpecialityServices = abstractSpecialityServices;
        }
        #endregion

        // Speciality_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Speciality_Upsert")]
        public async Task<IHttpActionResult> Speciality_Upsert(Speciality Speciality)
        {
            var quote = abstractSpecialityServices.Speciality_Upsert(Speciality);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Speciality_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Speciality_All")]
        public async Task<IHttpActionResult> Speciality_All(PageParam pageParam, string search = "")
        {
            var quote = abstractSpecialityServices.Speciality_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Speciality_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Speciality_ById")]
        public async Task<IHttpActionResult> Speciality_ById(long Id)
        {
            var quote = abstractSpecialityServices.Speciality_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Speciality_ActInActive Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Speciality_ActInActive")]
        public async Task<IHttpActionResult> Speciality_ActInActive(long Id)
        {
            var quote = abstractSpecialityServices.Speciality_ActInActive(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Speciality_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Speciality_Delete")]
        public async Task<IHttpActionResult> Speciality_Delete(long Id, long DeletedBy)
        {
            var quote = abstractSpecialityServices.Speciality_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
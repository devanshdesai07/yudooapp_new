﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserSearchV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserSearchServices abstractUserSearchServices;
        #endregion

        #region Cnstr
        public UserSearchV1Controller(AbstractUserSearchServices abstractUserSearchServices)
        {
            this.abstractUserSearchServices = abstractUserSearchServices;
        }
        #endregion

        // UserSearch_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_Upsert")]
        public async Task<IHttpActionResult> UserSearch_Upsert(UserSearch UserSearch)
        {
            var quote = abstractUserSearchServices.UserSearch_Upsert(UserSearch);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserSearch_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_ByUserId")]
        public async Task<IHttpActionResult> UserSearch_ByUserId(PageParam pageParam , long UserId = 0)
        {
            var quote = abstractUserSearchServices.UserSearch_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserSearch_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_All")]
        public async Task<IHttpActionResult> UserSearch_All(PageParam pageParam,string Search = "", long UserId = 0)
        {
            var quote = abstractUserSearchServices.UserSearch_All(pageParam,Search ,UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserSearch_Recentsearch API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_Recentsearch")]
        public async Task<IHttpActionResult> UserSearch_Recentsearch(PageParam pageParam, string Search = "", long UserId = 0)
        {
            var quote = abstractUserSearchServices.UserSearch_Recentsearch(pageParam, Search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserSearch_TrendingSearches API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_TrendingSearches")]
        public async Task<IHttpActionResult> UserSearch_TrendingSearches(PageParam pageParam, string Search = "")
        {
            var quote = abstractUserSearchServices.UserSearch_TrendingSearches(pageParam, Search);
            return this.Content((HttpStatusCode)200, quote);
        }


        //UserSearch_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_ById")]
        public async Task<IHttpActionResult> UserSearch_ById(long Id)
        {
            var quote = abstractUserSearchServices.UserSearch_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        //UserSearch_ActInact Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserSearch_ActInact")]
        public async Task<IHttpActionResult> UserSearch_ActInact(long Id)
        {
            var quote = abstractUserSearchServices.UserSearch_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

    }
}
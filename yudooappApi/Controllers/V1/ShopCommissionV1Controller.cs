﻿
using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopCommissionV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopCommissionServices abstractShopCommissionServices;
        #endregion

        #region Cnstr
        public ShopCommissionV1Controller(AbstractShopCommissionServices abstractShopCommissionServices)
        {
            this.abstractShopCommissionServices = abstractShopCommissionServices;
        }
        #endregion

        // ShopCommission_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCommission_Upsert")]
        public async Task<IHttpActionResult> ShopCommission_Upsert(ShopCommission ShopCommission)
        {
            var quote = abstractShopCommissionServices.ShopCommission_Upsert(ShopCommission);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopCommission_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCommission_All")]
        public async Task<IHttpActionResult> ShopCommission_All(PageParam pageParam, string search = "", long ShopOwnerId= 0)
        {
            var quote = abstractShopCommissionServices.ShopCommission_All(pageParam, search, ShopOwnerId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopCommission_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCommission_ById")]
        public async Task<IHttpActionResult> ShopCommission_ById(long Id)
        {
            var quote = abstractShopCommissionServices.ShopCommission_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopCommission_ByShopOwnerId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCommission_ByShopOwnerId")]
        public async Task<IHttpActionResult> ShopCommission_ByShopOwnerId(PageParam pageParam, long ShopOwnerId = 0)
        {
            var quote = abstractShopCommissionServices.ShopCommission_ByShopOwnerId(pageParam, ShopOwnerId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopCommission_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCommission_ActInAct")]
        public async Task<IHttpActionResult> ShopCommission_ActInAct(long Id)
        {
            var quote = abstractShopCommissionServices.ShopCommission_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopCommission_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCommission_Delete")]
        public async Task<IHttpActionResult> ShopCommission_Delete(long Id)
        {
            var quote = abstractShopCommissionServices.ShopCommission_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
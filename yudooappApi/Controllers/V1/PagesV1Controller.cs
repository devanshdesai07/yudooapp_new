﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PagesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPageServices abstractPagesServices;
        #endregion

        #region Cnstr
        public PagesV1Controller(AbstractPageServices abstractPagesServices)
        {
            this.abstractPagesServices = abstractPagesServices;
        }
        #endregion

        // Pages_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Pages_Upsert")]
        public async Task<IHttpActionResult> Pages_Upsert(Page pages)
        {
            var quote = abstractPagesServices.Pages_Upsert(pages);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Pages_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Pages_All")]
        public async Task<IHttpActionResult> Pages_All(PageParam pageParam, string search = "")
        {
            var quote = abstractPagesServices.Pages_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Pages_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Pages_ById")]
        public async Task<IHttpActionResult> Pages_ById(int Id)
        {
            var quote = abstractPagesServices.Pages_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        //Pages_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Pages_Delete")]
        public async Task<IHttpActionResult> Pages_Delete(int Id)
        {
            var quote = abstractPagesServices.Pages_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
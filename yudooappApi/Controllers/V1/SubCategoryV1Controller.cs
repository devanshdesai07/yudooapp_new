﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SubCategoryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractSubCategoryServices abstractSubCategoryServices;
        #endregion

        #region Cnstr
        public SubCategoryV1Controller(AbstractSubCategoryServices abstractSubCategoryServices)
        {
            this.abstractSubCategoryServices = abstractSubCategoryServices;
        }
        #endregion



        // SubCategory_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubCategory_All")]
        public async Task<IHttpActionResult> SubCategory_All(PageParam pageParam, string search = "")
        {
            var quote = abstractSubCategoryServices.SubCategory_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        // SubCategory_LookUpCategoryId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubCategory_LookUpCategoryId")]
        public async Task<IHttpActionResult> SubCategory_LookUpCategoryId(PageParam pageParam, string search = "" , long CategoryId = 0)
        {
            var quote = abstractSubCategoryServices.SubCategory_LookUpCategoryId(pageParam, search , CategoryId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //SubCategory_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SubCategory_ById")]
        public async Task<IHttpActionResult> SubCategory_ById(long Id)
        {
            var quote = abstractSubCategoryServices.SubCategory_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }   
    }



    

}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractProductTypeServices abstractProductTypeServices;
        #endregion

        #region Cnstr
        public ProductTypeV1Controller(AbstractProductTypeServices abstractProductTypeServices)
        {
            this.abstractProductTypeServices = abstractProductTypeServices;
        }
        #endregion

       
        //ProductType_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductType_ById")]
        public async Task<IHttpActionResult> ProductType_ById(long Id)
        {
            var quote = abstractProductTypeServices.ProductType_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //ProductType_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductType_Upsert")]
        public async Task<IHttpActionResult> ProductType_Upsert(ProductType productType)
        {
            var quote = abstractProductTypeServices.ProductType_Upsert(productType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ProductType_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ProductType_All")]
        public async Task<IHttpActionResult> ProductType_All(PageParam pageParam, string search = "",long ParentId = 0)
        {
            AbstractProductType ProductType = new ProductType();
            ProductType.ParentId = ParentId;
            var quote = abstractProductTypeServices.ProductType_All(pageParam, search, ProductType);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}

﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CountryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCountryServices abstractCountryServices;
        #endregion

        #region Cnstr
        public CountryV1Controller(AbstractCountryServices abstractCountryServices)
        {
            this.abstractCountryServices = abstractCountryServices;
        }
        #endregion



        // Country_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Country_All")]
        public async Task<IHttpActionResult> Country_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCountryServices.Country_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
        //Country_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Country_ById")]
        public async Task<IHttpActionResult> Country_ById(long Id)
        {
            var quote = abstractCountryServices.Country_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Country_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Country_Delete")]
        public async Task<IHttpActionResult> Country_Delete(long Id)
        {
            var quote = abstractCountryServices.Country_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
       
    }

}
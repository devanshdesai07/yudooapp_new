﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class VersionV1Controller : AbstractBaseController
    {
        #region Fields
        #endregion

        #region Cnstr
        public VersionV1Controller()
        {
        }
        #endregion

        //Users_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("CheckForUpdate")]
        public async Task<IHttpActionResult> CheckForUpdate(bool IsAndroid)
        {
            SuccessResult<AbstractVersion> SuccessResult = new SuccessResult<AbstractVersion>();
            SuccessResult.Code = 200;
            SuccessResult.Item = new yudooapp.Entities.V1.Version();
            SuccessResult.Item.VersionId = 1;
            SuccessResult.Item.VersionName = "1.0";
            SuccessResult.Item.Message = "New Update Found";

            return this.Content((HttpStatusCode)SuccessResult.Code, SuccessResult);
        }

    }
}

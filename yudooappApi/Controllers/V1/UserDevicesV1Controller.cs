﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserDevicesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserDevicesServices abstractUserDevicesServices;
        #endregion

        #region Cnstr
        public UserDevicesV1Controller(AbstractUserDevicesServices abstractUserDevicesServices)
        {
            this.abstractUserDevicesServices = abstractUserDevicesServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("UserDevices_ByUserId")]
        public async Task<IHttpActionResult> UserDevices_ByUserId(PageParam pageParam, string search = "", long UserId = 0)
        {
            var quote = abstractUserDevicesServices.UserDevices_ByUserId(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
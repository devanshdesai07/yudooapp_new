﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BadRatingSuggestionsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractBadRatingSuggestionsServices abstractBadRatingSuggestionsServices;
        #endregion

        #region Cnstr
        public BadRatingSuggestionsV1Controller(AbstractBadRatingSuggestionsServices abstractBadRatingSuggestionsServices)
        {
            this.abstractBadRatingSuggestionsServices = abstractBadRatingSuggestionsServices;
        }
        #endregion


        // BadRatingSuggestions_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("BadRatingSuggestions_All")]
        public async Task<IHttpActionResult> BadRatingSuggestions_All(PageParam pageParam, string search = "")
        {
            var quote = abstractBadRatingSuggestionsServices.BadRatingSuggestions_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
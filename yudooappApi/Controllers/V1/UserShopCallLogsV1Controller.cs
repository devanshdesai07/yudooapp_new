﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserShopCallLogsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserShopCallLogsServices abstractUserShopCallLogsServices;
        #endregion

        #region Cnstr
        public UserShopCallLogsV1Controller(AbstractUserShopCallLogsServices abstractUserShopCallLogsServices)
        {
            this.abstractUserShopCallLogsServices = abstractUserShopCallLogsServices;
        }
        #endregion

        // UserShopCallLogs_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_Upsert")]
        public async Task<IHttpActionResult> UserShopCallLogs_Upsert(UserShopCallLogs UserShopCallLogs)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_Upsert(UserShopCallLogs);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserShopCallLogs_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_All")]
        public async Task<IHttpActionResult> UserShopCallLogs_All(PageParam pageParam, string search ="", long UserId = 0, long ShopId =0)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_All(pageParam, search, UserId, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserShopCallLogs_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_ById")]
        public async Task<IHttpActionResult> UserShopCallLogs_ById(long Id)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserShopCallLogs_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_ByUserId")]
        public async Task<IHttpActionResult> UserShopCallLogs_ByUserId(PageParam pageParam, long UserId= 0)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserShopCallLogs_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_ByShopId")]
        public async Task<IHttpActionResult> UserShopCallLogs_ByShopId(PageParam pageParam, long ShopId = 0)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserShopCallLogs_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_Delete")]
        public async Task<IHttpActionResult> UserShopCallLogs_Delete(long Id, long DeletedBy)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Request_CallBack API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Request_CallBack")]
        public async Task<IHttpActionResult> Request_CallBack(UserShopCallLogs UserShopCallLogs)
        {
            UserShopCallLogs.CallStatus = 10;
            UserShopCallLogs.WhoInitiated = "User";
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_Upsert(UserShopCallLogs);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Request_CallBack API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Request_CallBack_ShopId")]
        public async Task<IHttpActionResult> Request_CallBack_ShopId(PageParam pageParam, long ShopId = 0)
        {
            var quote = abstractUserShopCallLogsServices.Request_CallBack_ShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserShopCallLogs_CallStatus_Update API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserShopCallLogs_CallStatus_Update")]
        public async Task<IHttpActionResult> UserShopCallLogs_CallStatus_Update(long Id, long CallStatus)
        {
            var quote = abstractUserShopCallLogsServices.UserShopCallLogs_CallStatus_Update(Id, CallStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
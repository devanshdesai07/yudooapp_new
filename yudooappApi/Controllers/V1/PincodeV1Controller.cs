﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Text;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PincodeV1Controller : AbstractBaseController
    {
        //#region Fields
        //private readonly AbstractAddressServices abstractAddressServices;
        //#endregion

        //#region Cnstr
        //public AddressV1Controller(AbstractAddressServices abstractAddressServices)
        //{
        //    this.abstractAddressServices = abstractAddressServices;
        //}
        //#endregion


        //Address_Pincode API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_Pincode")]
        public async Task<IHttpActionResult> Address_Pincode(string Pincode)
        {
            using (var wc = new HttpClient())
            {
                List<PincodeRootArray> pincodeRootArray = new List<PincodeRootArray>();

                wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var path = "https://api.postalpincode.in/pincode/" + Pincode;
                var result = await wc.GetStringAsync(new Uri(path));

                pincodeRootArray = JsonConvert.DeserializeObject<List<PincodeRootArray>>(Convert.ToString(result));

                object AddressResult = "";

                if (pincodeRootArray[0].Status == "Success")
                {
                    AddressResult = pincodeRootArray[0].PostOffice[0];
                }
                else if (pincodeRootArray[0].Status == "Error")
                {
                    AddressResult = pincodeRootArray;
                }
                return this.Content((HttpStatusCode)200, AddressResult);
            }
        }
    }
}
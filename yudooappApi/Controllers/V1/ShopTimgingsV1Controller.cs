﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopTimgingsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopTimgingsServices abstractShopTimgingsServices;
        #endregion

        #region Cnstr
        public ShopTimgingsV1Controller(AbstractShopTimgingsServices abstractShopTimgingsServices)
        {
            this.abstractShopTimgingsServices = abstractShopTimgingsServices;
        }
        #endregion

        // ShopTimgings_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopTimgings_Upsert")]
        public async Task<IHttpActionResult> ShopTimgings_Upsert(ShopTimingJson ShopTimgings)
        {
            var quote = abstractShopTimgingsServices.ShopTimgings_Upsert(ShopTimgings);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopTimgings_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopTimgings_All")]
        public async Task<IHttpActionResult> ShopTimgings_All(PageParam pageParam, string search = "" , long ShopId = 0)
        {
            var quote = abstractShopTimgingsServices.ShopTimgings_All(pageParam, search, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // ShopTimgings_ShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopTimgings_ShopId")]
        public async Task<IHttpActionResult> ShopTimgings_ShopId(PageParam pageParam,  long ShopId = 0)
        {
            var quote = abstractShopTimgingsServices.ShopTimgings_ShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ShopTimgings_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopTimgings_ById")]
        public async Task<IHttpActionResult> ShopTimgings_ById(long Id)
        {
            var quote = abstractShopTimgingsServices.ShopTimgings_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopTimgings_IsClosedIsopen API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopTimgings_IsClosedIsopen")]
        public async Task<IHttpActionResult> ShopTimgings_IsClosedIsopen(long Id)
        {
            var quote = abstractShopTimgingsServices.ShopTimgings_IsClosedIsopen(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //ShopTimgings_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopTimgings_Delete")]
        public async Task<IHttpActionResult> ShopTimgings_Delete(long Id)
        {
            var quote = abstractShopTimgingsServices.ShopTimgings_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
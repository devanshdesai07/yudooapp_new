﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using RestSharp;
using System.Web.Script.Serialization;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class UserCartV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserCartServices abstractUserCartServices;
        private readonly AbstractUserOrderServices abstractUserOrderServices;

        #endregion

        #region Cnstr
        public UserCartV1Controller(AbstractUserCartServices abstractUserCartServices, AbstractUserOrderServices abstractUserOrderServices)
        {
            this.abstractUserCartServices = abstractUserCartServices;
            this.abstractUserOrderServices = abstractUserOrderServices;
        }
        #endregion

        //UserCart_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_Upsert")]
        public async Task<IHttpActionResult> UserCart_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;
            UserCart userCart = new UserCart();
            var httpRequest = HttpContext.Current.Request;
            userCart.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            userCart.UserId = Convert.ToInt64(httpRequest.Params["UserId"]);
            userCart.ShopId = Convert.ToInt64(httpRequest.Params["ShopId"]);
            userCart.UserCartMasterId = Convert.ToInt64(httpRequest.Params["UserCartMasterId"]);
            userCart.Qty = Convert.ToInt64(httpRequest.Params["Qty"]);
            userCart.Price = Convert.ToDecimal(httpRequest.Params["Price"]);
            userCart.Size = Convert.ToString(httpRequest.Params["Size"]);
            userCart.Weight = Convert.ToDecimal(httpRequest.Params["Weight"]);
            userCart.ProductInfo = Convert.ToString(httpRequest.Params["ProductInfo"]);
            userCart.ItemName = Convert.ToString(httpRequest.Params["ItemName"]);
            userCart.VideoCallId = Convert.ToInt64(httpRequest.Params["VideoCallId"]);
            userCart.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
            userCart.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);
            userCart.WeightTypeId = Convert.ToInt64(httpRequest.Params["WeightTypeId"]);
            userCart.Pincode = Convert.ToString(httpRequest.Params["Pincode"]);

            SuccessResult<AbstractUserCart> SuccessResult = new SuccessResult<AbstractUserCart>();


            //if (userCart.Pincode != "")
            //{
            //    using (var wc = new HttpClient())
            //    {
            //        List<PincodeRootArray> pincodeRootArray = new List<PincodeRootArray>();

            //        wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //        var path = "https://api.postalpincode.in/pincode/" + userCart.Pincode;
            //        var AddressrResult = await wc.GetStringAsync(new Uri(path));

            //        object AddressResult = "";
            //        pincodeRootArray = JsonConvert.DeserializeObject<List<PincodeRootArray>>(Convert.ToString(AddressrResult));

            //        if (pincodeRootArray[0].Status == "Success")
            //        {
            //            AddressResult = pincodeRootArray[0].PostOffice[0];
            //            userCart.Country = Convert.ToString(pincodeRootArray[0].PostOffice[0].Country);
            //            userCart.State = Convert.ToString(pincodeRootArray[0].PostOffice[0].State);
            //            userCart.City = Convert.ToString(pincodeRootArray[0].PostOffice[0].District);
            //        }
            //        else if (pincodeRootArray[0].Status == "Error")
            //        {
            //            AddressResult = pincodeRootArray;

            //            SuccessResult.Code = 400;
            //            SuccessResult.Message = "Pincode is invalid";
            //            return this.Content((HttpStatusCode)SuccessResult.Code, SuccessResult);
            //        }
            //    }
            //}

            
            ////string videoPath = "";
            if (httpRequest.Files.Count > 0)
            {
                string basepath = "";
                string filename = "";

                var myFile = httpRequest.Files[0];
                string basePath = "ProductImage/" + userCart.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                userCart.ProductImage = basePath + fileName;

                string bfName = HttpContext.Current.Server.MapPath("~/" + basePath + fileName);

                abstractUserCartServices.S3FileUpload(bfName, userCart.ProductImage);
            }
            //if (httpRequest.Files.Count > 0)
            //{
            //    var myFile = httpRequest.Files[0];
            //    string basePath = "ProductImage/" + userCart.Id + "/";
            //    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
            //    if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
            //    {
            //        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
            //    }
            //    myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
            //    userCart.ProductImage = basePath + fileName;
            //}
            var quote = abstractUserCartServices.UserCart_Upsert(userCart);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("AddUserCart")]
        public async Task<IHttpActionResult> AddUserCart(AddCartJson addCartJson)
        {
            var cartData = JsonConvert.DeserializeObject<List<PersonalUserCart>>(addCartJson.cartJson);
            //var quote = abstractUserCartServices.UserCartDeleteByVideoCallId(cartData[0].VideoCallId);
            SuccessResult<AbstractPersonalUserCart> result = new SuccessResult<AbstractPersonalUserCart>(); 
            foreach (var item in cartData)
            {
                PersonalUserCart personalUserCart = new PersonalUserCart();
                personalUserCart.Id = item.Id;
                personalUserCart.UserId = item.UserId;
                personalUserCart.ShopId = item.ShopId;
                personalUserCart.Qty = item.Qty;
                personalUserCart.Weight = item.Weight;
                personalUserCart.WeightTypeId = item.WeightTypeId;
                personalUserCart.ProductImage = item.ProductImage;
                personalUserCart.Price = item.Price;
                personalUserCart.Size = item.Size;
                personalUserCart.ProductInfo = item.ProductInfo;
                personalUserCart.ItemName = item.ItemName;
                personalUserCart.CreatedBy = item.CreatedBy;
                personalUserCart.UpdatedBy = item.UpdatedBy;
                personalUserCart.VideoCallId = item.VideoCallId;
                personalUserCart.UserCartMasterId = item.UserCartMasterId;
                personalUserCart.Pincode = item.Pincode;

                SuccessResult<AbstractUserCart> SuccessResult = new SuccessResult<AbstractUserCart>();

                //if (personalUserCart.Pincode != "")
                //{
                //    using (var wc = new HttpClient())
                //    {
                //        List<PincodeRootArray> pincodeRootArray = new List<PincodeRootArray>();

                //        wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //        var path = "https://api.postalpincode.in/pincode/" + personalUserCart.Pincode;
                //        var AddressrResult = await wc.GetStringAsync(new Uri(path));

                //        object AddressResult = "";
                //        pincodeRootArray = JsonConvert.DeserializeObject<List<PincodeRootArray>>(Convert.ToString(AddressrResult));

                //        if (pincodeRootArray[0].Status == "Success")
                //        {
                //            AddressResult = pincodeRootArray[0].PostOffice[0];
                //            item.Country = Convert.ToString(pincodeRootArray[0].PostOffice[0].Country);
                //            item.State = Convert.ToString(pincodeRootArray[0].PostOffice[0].State);
                //            item.City = Convert.ToString(pincodeRootArray[0].PostOffice[0].District);
                //        }
                //        else if (pincodeRootArray[0].Status == "Error")
                //        {
                //            AddressResult = pincodeRootArray;

                //            SuccessResult.Code = 400;
                //            SuccessResult.Message = "Pincode is invalid";
                //            return this.Content((HttpStatusCode)SuccessResult.Code, SuccessResult);
                //        }
                //    }
                //}

                result = abstractUserCartServices.AddUserCart(item);
            }
            return this.Content((HttpStatusCode)result.Code, result);
        }

        //UserCart_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_ById")]
        public async Task<IHttpActionResult> UserCart_ById(long Id)
        {
            var quote = abstractUserCartServices.UserCart_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ////UserOrder_OrderDetails API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("UserCart_Details")]
        //public async Task<IHttpActionResult> UserCart_Details(long Id)
        //{
        //    var quote = abstractUserCartServices.UserCart_Details(Id);

        //    if (!string.IsNullOrEmpty(Convert.ToString(quote.Item.ProductDetails)))
        //        quote.Item.ProductDetails = JsonConvert.DeserializeObject(Convert.ToString(quote.Item.ProductDetails));

        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        //UserCart_ByUserId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_All")]
        public async Task<IHttpActionResult> UserCart_All(PageParam pageParam, string Search = "", long UserId = 0, long ShopId = 0)
        {
            AbstractUserCart UserCart = new UserCart();
            var quote = abstractUserCartServices.UserCart_All(pageParam, Search, UserId, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserCart_ByUserId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_ByUserId")]
        public async Task<IHttpActionResult> UserCart_ByUserId(PageParam pageParam, long UserId = 0)
        {
            AbstractUserCart UserCart = new UserCart();
            var quote = abstractUserCartServices.UserCart_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserCart_ByShopId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_ByShopId")]
        public async Task<IHttpActionResult> UserCart_ByShopId(PageParam pageParam, long ShopId = 0)
        {
            AbstractUserCart UserCart = new UserCart();
            var quote = abstractUserCartServices.UserCart_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserCart_ByVideoCallId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_ByVideoCallId")]
        public async Task<IHttpActionResult> UserCart_ByVideoCallId(PageParam pageParam, long VideoCallId = 0)
        {
            AbstractUserCart UserCart = new UserCart();
            var quote = abstractUserCartServices.UserCart_ByVideoCallId(pageParam, VideoCallId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserCart_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_Delete")]
        public async Task<IHttpActionResult> UserCart_Delete(long Id)
        {
            var quote = abstractUserCartServices.UserCart_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //UserCart_IsVisibleToCustomer Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_IsSendToCustomer")]
        public async Task<IHttpActionResult> UserCart_IsSendToCustomer(PageParam pageParam, string Search = "", long VideoCallId = 0)
        {
            var quote = abstractUserCartServices.UserCart_IsSendToCustomer(pageParam, Search, VideoCallId);

            return this.Content((HttpStatusCode)200, quote);
        }

        // RazUserOrder_Upsert_paymentLink API
        [System.Web.Http.HttpPost]
        [InheritedRoute("RazUserOrder_Upsert_paymentLink")]
        public async Task<IHttpActionResult> RazUserOrder_Upsert_paymentLink(long Id = 0, string PaymentLink = "",string RazorPayOrderId = "")
        {
            var quote = abstractUserOrderServices.UserOrder_Upsert_paymentLink(Id, PaymentLink,RazorPayOrderId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserCart_IsSendToOrder Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCart_ConfirmOrder")]
        public async Task<IHttpActionResult> UserCart_ConfirmOrder(long VideoCallId = 0, decimal ShippingChargeFromAPI = 0, decimal CODChargefromAPI = 0, long IsOnline = 0,string Address="",string UserName="")
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var quote = abstractUserCartServices.UserCart_ConfirmOrder(VideoCallId, ShippingChargeFromAPI, CODChargefromAPI, IsOnline, Address,UserName);

            RazRootResponce myDeserializedClass = null;

            RazRootReq root = new RazRootReq();
            root.customer = new RazCustomer();
            root.notify = new RazNotify();
            root.notes = new RazNotes();

            root.amount = Convert.ToDecimal(quote.Item.AmountPaid + quote.Item.DeliveryCharge + quote.Item.PaymentCharge) * 100;
            root.amount = (Math.Round(root.amount / root.amount * root.amount, 2));
            root.currency = "INR";
            root.accept_partial = false;
            //root.expire_by = (int)DateTime.UtcNow.Subtract(new DateTime().AddMonths(1)).TotalSeconds;
            root.reference_id = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss") + "ORD" + quote.Item.Id;
            root.description = quote.Item.Id.ToString();
            root.customer.name = quote.Item.FirstName;
            root.customer.contact = "+91" + Convert.ToString(quote.Item.MobileNumber);
            root.customer.email = quote.Item.Email;
            root.customer.mobile = Convert.ToString(quote.Item.MobileNumber);
            root.notify.sms = false;
            root.notify.email = false;
            root.reminder_enable = true;
            root.notes.policy_name = "Test policy";
            //root.callback_url = Configurations.ClientURL + "api/userOrder/UserOrder_PaymentResponce/";
            root.callback_method = "get";
            using (var wc = new HttpClient())
            {
                //wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                wc.DefaultRequestHeaders.Add("x-api-key", Configurations.RazorPayKey);

                wc.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Configurations.RazorPayToken);

                var path = "https://api.razorpay.com/v1/payment_links";

                var content = new StringContent(JsonConvert.SerializeObject(root), Encoding.UTF8, "application/json");
                try {
                    var result = await wc.PostAsync(new Uri(path), content);
                    string resultContent = await result.Content.ReadAsStringAsync();
                    myDeserializedClass = JsonConvert.DeserializeObject<RazRootResponce>(resultContent);

                }
                catch (Exception ex)
                {

                }


                var roId = "";
                if (IsOnline == 1)
                {

                    var opath = "https://api.razorpay.com/v1/orders";

                    ORoot oRoot = new ORoot();
                    oRoot.amount = Convert.ToInt32(quote.Item.AmountPaid + quote.Item.DeliveryCharge + quote.Item.PaymentCharge) * 100;
                    oRoot.currency = "INR";
                    oRoot.receipt = ConvertTo.String(quote.Item.Id);

                    var ocontent = new StringContent(JsonConvert.SerializeObject(oRoot), Encoding.UTF8, "application/json");
                    try
                    {
                        var oresult = await wc.PostAsync(new Uri(opath), ocontent);
                        string oresultContent = await oresult.Content.ReadAsStringAsync();
                        var reso = JsonConvert.DeserializeObject<OResRoot>(oresultContent);
                        roId = reso.id;
                        quote.Item.RazorPayOrderId = roId;
                        //var opath1 = "https://api.razorpay.com/v1/orders/"+ reso.id;

                        //try
                        //{
                        //    var resp = await wc.GetStringAsync(new Uri(opath1));
                        //    dynamic codRes = JsonConvert.DeserializeObject<dynamic>(resp);

                        //}
                        //catch (Exception ex)
                        //{

                        //}
                    }
                    catch (Exception ex)
                    {

                    }
                    
                  
                }

                if (myDeserializedClass != null && myDeserializedClass.amount > 0)
                {
                    RazUserOrder_Upsert_paymentLink(quote.Item.Id, myDeserializedClass.short_url, roId);
                }


            }

            //string response = string.Empty;
            //string serverKey = "AAAA6tsjjhM:APA91bEbAdQr_VHZ5pHjtTUhspCHe_bN2GTQfYXbgu4skFl7n2_jGcAhw2A8Rox1qAf5UyFW7eSpVmREt-9VcWe_MENAnNSU9ZUDFw7WUzxFt06kcsB2p0mtor1mAp4IRQSspn0QnLNK";
            ////string senderId = "448087173152";
            //string deviceId = "cnXMPeweRiebmzf_OHXFe1:APA91bFZBcf4CgVs0nCStpYU0kemWLL3fb-xFp6hDUQtpTXNEzoZn2hb9TqfbcrecWoMi2fOOvFXoIUncreinNOTTHwE9vL8zLY9iLOaWKNUcI2QYjYvst_zlUXHBXI9R22xtC9-GS7o";

            //WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

            //tRequest.Method = "post";
            //tRequest.ContentType = "application/json";
            //var data = new
            //{
            //    to = deviceId,
            //    notification = new
            //    {
            //        body = "Your order places successfully",//"Your job Status has been changed",
            //        title = "yudoo",//"Fitting job has been completed.",
            //        sound = "Enabled"
            //    }
            //};

            //var serializer = new JavaScriptSerializer();
            //var json = serializer.Serialize(data);
            //Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            //tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            ////tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            //tRequest.ContentLength = byteArray.Length;

            //using (Stream dataStream = tRequest.GetRequestStream())
            //{
            //    dataStream.Write(byteArray, 0, byteArray.Length);
            //    using (WebResponse tResponse = tRequest.GetResponse())
            //    {
            //        using (Stream dataStreamResponse = tResponse.GetResponseStream())
            //        {
            //            using (StreamReader tReader = new StreamReader(dataStreamResponse))
            //            {
            //                String sResponseFromServer = tReader.ReadToEnd();
            //                response = sResponseFromServer;
            //            }
            //        }
            //    }
            //}

            return this.Content((HttpStatusCode)200, quote);
        }


        public class OResRoot
        {
            public string id { get; set; }
            public string entity { get; set; }
            public int amount { get; set; }
            public int amount_paid { get; set; }
            public int amount_due { get; set; }
            public string currency { get; set; }
            public string receipt { get; set; }
            public object offer_id { get; set; }
            public string status { get; set; }
            public int attempts { get; set; }
            public List<object> notes { get; set; }
            public int created_at { get; set; }
        }

        public class ONotes
        {
            public string notes_key_1 { get; set; }
            public string notes_key_2 { get; set; }
        }

        public class ORoot
        {
            public int amount { get; set; }
            public string currency { get; set; }
            public string receipt { get; set; }
            public ONotes notes { get; set; }
        }


        public class AddCartJson
        {
            public string cartJson { get; set; }
        }

        public class RazCustomer
        {
            public string name { get; set; }
            public string contact { get; set; }
            public string email { get; set; }
            public string mobile { get; set; }
        }
        public class RazNotify
        {
            public bool sms { get; set; }
            public bool email { get; set; }
        }
        public class RazNotes
        {
            public string policy_name { get; set; }
        }
        public class RazRootReq
        {
            public decimal amount { get; set; }
            public string currency { get; set; }
            public bool accept_partial { get; set; }
            public int expire_by { get; set; }
            public string reference_id { get; set; }
            public string description { get; set; }
            public RazCustomer customer { get; set; }
            public RazNotify notify { get; set; }
            public bool reminder_enable { get; set; }
            public RazNotes notes { get; set; }
            public string callback_url { get; set; }
            public string callback_method { get; set; }
        }
        public class RazRootResponce
        {
            public bool accept_partial { get; set; }
            public int amount { get; set; }
            public int amount_paid { get; set; }
            public string callback_method { get; set; }
            public string callback_url { get; set; }
            public int cancelled_at { get; set; }
            public int created_at { get; set; }
            public string currency { get; set; }
            public RazCustomer customer { get; set; }
            public string description { get; set; }
            public int expire_by { get; set; }
            public int expired_at { get; set; }
            public int first_min_partial_amount { get; set; }
            public string id { get; set; }
            public RazNotes notes { get; set; }
            public RazNotify notify { get; set; }
            public object payments { get; set; }
            public string reference_id { get; set; }
            public bool reminder_enable { get; set; }
            public List<object> reminders { get; set; }
            public string short_url { get; set; }
            public string status { get; set; }
            public int updated_at { get; set; }
            public bool upi_link { get; set; }
            public string user_id { get; set; }
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StateV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractStateServices abstractStateServices;
        #endregion

        #region Cnstr
        public StateV1Controller(AbstractStateServices abstractStateServices)
        {
            this.abstractStateServices = abstractStateServices;
        }
        #endregion



        // State_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("State_All")]
        public async Task<IHttpActionResult> State_All(PageParam pageParam, string search = "")
        {
            var quote = abstractStateServices.State_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
       
        //State_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("State_ById")]
        public async Task<IHttpActionResult> State_ById(long Id)
        {
            var quote = abstractStateServices.State_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //State_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("State_Delete")]
        public async Task<IHttpActionResult> State_Delete(long Id)
        {
            var quote = abstractStateServices.State_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //State_ByCountryId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("State_ByCountryId")]
        public async Task<IHttpActionResult> State_ByCountryId(PageParam pageParam, long CountryId)
        {
            var quote = abstractStateServices.State_ByCountryId(pageParam, CountryId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GoodRatingSuggestionsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractGoodRatingSuggestionsServices abstractGoodRatingSuggestionsServices;
        #endregion

        #region Cnstr
        public GoodRatingSuggestionsV1Controller(AbstractGoodRatingSuggestionsServices abstractGoodRatingSuggestionsServices)
        {
            this.abstractGoodRatingSuggestionsServices = abstractGoodRatingSuggestionsServices;
        }
        #endregion


        // GoodRatingSuggestions_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("GoodRatingSuggestions_All")]
        public async Task<IHttpActionResult> GoodRatingSuggestions_All(PageParam pageParam, string search = "")
        {
            var quote = abstractGoodRatingSuggestionsServices.GoodRatingSuggestions_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
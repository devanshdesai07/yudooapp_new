﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopRatingsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopRatingsServices abstractShopRatingsServices;
        #endregion

        #region Cnstr
        public ShopRatingsV1Controller(AbstractShopRatingsServices abstractShopRatingsServices)
        {
            this.abstractShopRatingsServices = abstractShopRatingsServices;
        }
        #endregion

        // ShopRatings_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopRatings_Upsert")]
        public async Task<IHttpActionResult> ShopRatings_Upsert(ShopRatings ShopRatings)
        {
            var quote = abstractShopRatingsServices.ShopRatings_Upsert(ShopRatings);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopRatings_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopRatings_All")]
        public async Task<IHttpActionResult> ShopRatings_All(PageParam pageParam, string search = "" , long ShopId = 0 , long UserId = 0)
        {
            var quote = abstractShopRatingsServices.ShopRatings_All(pageParam, search , ShopId , UserId);
            return this.Content((HttpStatusCode)200, quote);
        }


        // ShopRatings_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopRatings_ByShopId")]
        public async Task<IHttpActionResult> ShopRatings_ByShopId(PageParam pageParam, string search = "", long ShopId = 0)
        {
            var quote = abstractShopRatingsServices.ShopRatings_ByShopId(pageParam, search, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // ShopRatings_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopRatings_ByUserId")]
        public async Task<IHttpActionResult> ShopRatings_ByUserId(PageParam pageParam, string search = "", long UserId = 0)
        {
            var quote = abstractShopRatingsServices.ShopRatings_ByUserId(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }


        //ShopRatings_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopRatings_ById")]
        public async Task<IHttpActionResult> ShopRatings_ById(long Id)
        {
            var quote = abstractShopRatingsServices.ShopRatings_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //ShopRatings_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopRatings_Delete")]
        public async Task<IHttpActionResult> ShopRatings_Delete(long Id)
        {
            var quote = abstractShopRatingsServices.ShopRatings_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
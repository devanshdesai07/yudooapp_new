﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerCallsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerCallsServices abstractCustomerCallsServices;
        #endregion

        #region Cnstr
        public CustomerCallsV1Controller(AbstractCustomerCallsServices abstractCustomerCallsServices)
        {
            this.abstractCustomerCallsServices = abstractCustomerCallsServices;
        }
        #endregion

        // CustomerCalls_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerCalls_Upsert")]
        public async Task<IHttpActionResult> CustomerCalls_Upsert(CustomerCalls CustomerCalls)
        {
            var quote = abstractCustomerCallsServices.CustomerCalls_Upsert(CustomerCalls);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // CustomerCalls_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerCalls_All")]
        public async Task<IHttpActionResult> CustomerCalls_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCustomerCallsServices.CustomerCalls_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //CustomerCalls_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerCalls_ById")]
        public async Task<IHttpActionResult> CustomerCalls_ById(long Id)
        {
            var quote = abstractCustomerCallsServices.CustomerCalls_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrderParcelV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractOrderParcelServices abstractOrderParcelServices;
        #endregion

        #region Cnstr
        public OrderParcelV1Controller(AbstractOrderParcelServices abstractOrderParcelServices)
        {
            this.abstractOrderParcelServices = abstractOrderParcelServices;
        }
        #endregion

        //OrderParcel_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OrderParcel_Upsert")]
        public async Task<IHttpActionResult> OrderParcel_Upsert(OrderParcel orderParcel)
        {
            var quote = abstractOrderParcelServices.OrderParcel_Upsert(orderParcel);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //OrderParcel_ByOrderId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OrderParcel_ByOrderId")]
        public async Task<IHttpActionResult> OrderParcel_ByOrderId(long OrderId)
        {
            var quote = abstractOrderParcelServices.OrderParcel_ByOrderId(OrderId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // OrderParcelProduct_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OrderParcelProduct_All")]
        public async Task<IHttpActionResult> OrderParcelProduct_All(PageParam pageParam, string search = "",long OrderId = 0)
        {
            var quote = abstractOrderParcelServices.OrderParcelProduct_All(pageParam, search, OrderId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //OrderParcels_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OrderParcels_Delete")]
        public async Task<IHttpActionResult> OrderParcels_Delete(long Id, long DeletedBy)
        {
            var quote = abstractOrderParcelServices.OrderParcels_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //OrderParcel_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OrderParcel_ById")]
        public async Task<IHttpActionResult> OrderParcel_ById(long Id)
        {
            var quote = abstractOrderParcelServices.OrderParcel_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Check_CreateShipment API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Check_CreateShipment")]
        public async Task<IHttpActionResult> Check_CreateShipment(long Id)
        {
            var quote = abstractOrderParcelServices.Check_CreateShipment(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
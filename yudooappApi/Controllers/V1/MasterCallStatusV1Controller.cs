﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterCallStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterCallStatusServices abstractMasterCallStatusServices;
        #endregion

        #region Cnstr
        public MasterCallStatusV1Controller(AbstractMasterCallStatusServices abstractMasterCallStatusServices)
        {
            this.abstractMasterCallStatusServices = abstractMasterCallStatusServices;
        }
        #endregion

        // Address_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterCallStatus_Upsert")]
        public async Task<IHttpActionResult> MasterCallStatus_Upsert(MasterCallStatus masterCallStatus)
        {
            var quote = abstractMasterCallStatusServices.MasterCallStatus_Upsert(masterCallStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Address_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterCallStatus_All")]
        public async Task<IHttpActionResult> MasterCallStatus_All(PageParam pageParam, string search = "")
        {
            var quote = abstractMasterCallStatusServices.MasterCallStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        

    }
}
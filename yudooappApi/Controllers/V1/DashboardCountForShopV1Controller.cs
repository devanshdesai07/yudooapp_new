﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DashboardCountForShopV1Controller : AbstractBaseController
    {
        private readonly AbstractDashboardCountForShopServices _abstractDashboardCountForShopServices = null;

        public DashboardCountForShopV1Controller(AbstractDashboardCountForShopServices _abstractDashboardCountForShopServices)
        {
            this._abstractDashboardCountForShopServices = _abstractDashboardCountForShopServices;
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DashboardCount_ByShopId")]
        public async Task<IHttpActionResult> DashboardCount_ByShopId(long ShopId,string Period = "Weekly")
        {
            var quote = _abstractDashboardCountForShopServices.DashboardCount_ByShopId(ShopId,Period);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
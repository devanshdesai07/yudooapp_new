﻿    using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUsersServices abstractUsersServices;
        #endregion

        #region Cnstr
        public UsersV1Controller(AbstractUsersServices abstractUsersServices)
        {
            this.abstractUsersServices = abstractUsersServices;
        }
        #endregion


        //Users_SendOTP Api
        //[System.Web.Http.HttpGet]
        //[InheritedRoute("Users_SendOTP")]
        //public async Task<IHttpActionResult> Users_SendOTP(string mobilenumber)
        //{
        //    string otp = mobilenumber == "9924088018" ? "1234" : new Random().Next(0, 9999).ToString("D4");
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", mobilenumber).Replace("#otp#", otp).ToString());
        //    request.Method = "GET";
        //    request.ContentType = "application/json";

        //    try
        //    {
        //        WebResponse webResponse = request.GetResponse();
        //        using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
        //        using (StreamReader responseReader = new StreamReader(webStream))
        //        {
        //            string response = responseReader.ReadToEnd();

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.Out.WriteLine("-----------------");
        //        Console.Out.WriteLine(e.Message);
        //    }

        //    return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);
        //}

        //Users_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ActInAct")]
        public async Task<IHttpActionResult> Users_ActInAct(long Id)
        {
            var quote = abstractUsersServices.Users_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ById")]
        public async Task<IHttpActionResult> Users_ById(long Id)
        {
            var quote = abstractUsersServices.Users_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_IsCallStatus Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_IsCallStatus")]
        public async Task<IHttpActionResult> Users_IsCallStatus(long Id)
        {
            var quote = abstractUsersServices.Users_IsCallStatus(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Caller_Info Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Caller_Info")]
        public async Task<IHttpActionResult> Caller_Info(long ShopId,long UserId)
        {
            var quote = abstractUsersServices.Caller_Info(ShopId,UserId);
            if(quote.Item == null)
            {
                CallerInfo callerInfo = new CallerInfo();
                quote.Item = callerInfo;
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Logout")]
        public async Task<IHttpActionResult> Users_Logout(long Id)
        {
            var quote = abstractUsersServices.Users_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Users_ResetPassword API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_ResetPassword")]
        public async Task<IHttpActionResult> Users_ChangePassword(long Id, string OldPassword, string NewPassword, string ConfirmPassword)
        {
            var quote = abstractUsersServices.Users_ChangePassword(Id, OldPassword, NewPassword, ConfirmPassword);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_Upsert Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Upsert")]
        public async Task<IHttpActionResult> Users_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Users users = new Users();
            var httpRequest = HttpContext.Current.Request;
            users.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            users.FirstName = Convert.ToString(httpRequest.Params["FirstName"]);
            users.LastName = Convert.ToString(httpRequest.Params["LastName"]);
            users.Gender = Convert.ToString(httpRequest.Params["Gender"]);
            users.MobileNumber = Convert.ToInt64(httpRequest.Params["MobileNumber"]);
            users.Email = Convert.ToString(httpRequest.Params["Email"]);
            users.Password = Convert.ToString(httpRequest.Params["Password"]);
            users.DOB = Convert.ToString(httpRequest.Params["DOB"]);
            users.DeviceType = Convert.ToString(httpRequest.Params["DeviceType"]);
            users.UDID = Convert.ToString(httpRequest.Params["UDID"]);
            users.IMEI = Convert.ToString(httpRequest.Params["IMEI"]);
            users.DeviceToken = Convert.ToString(httpRequest.Params["DeviceToken"]);
            users.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
            users.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "UsersProfile/" + users.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                users.ProfileImage = basePath + fileName;
                abstractUsersServices.S3FileUpload(HttpContext.Current.Server.MapPath("~/" + basePath + fileName), users.ProfileImage);
            }

            var quote = abstractUsersServices.Users_Upsert(users);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_VerifyOtp")]
        public async Task<IHttpActionResult> Users_VerifyOtp(string MobileNumber, long Otp,string DeviceType,string UDID,string IMEI,string DeviceToken)
        {
            var quote = abstractUsersServices.CheckUser_Exists(Convert.ToString(MobileNumber));

            if (quote.Code == 400)
            {
                Users users = new Users();
                users.MobileNumber = Convert.ToInt64(MobileNumber);
                var quotes = abstractUsersServices.Users_Upsert(users);
                return this.Content((HttpStatusCode)quote.Code, quotes);
            }
            else
            {
                var quotes = abstractUsersServices.Users_ById(quote.Item.Id);

                //Users users = new Users();
                //users.Id = quotes.Item.Id;
                //users.MobileNumber = Convert.ToInt64(MobileNumber);
                //users.DeviceType = DeviceType;
                //users.UDID = UDID;
                //users.IMEI = IMEI;
                //users.DeviceToken = DeviceToken;

                var quotess = abstractUsersServices.Users_VerifyOtp(Convert.ToInt64(MobileNumber),Otp, DeviceType, UDID, IMEI, DeviceToken) ;

                return this.Content((HttpStatusCode)quote.Code, quotes);
            }

            //var quote1 = abstractUsersServices.Users_VerifyOtp(MobileNumber, Otp);
            //return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_All")]
        public async Task<IHttpActionResult> Users_All(PageParam pageParam, string search = "")
        {
            AbstractUsers Users = new Users();
            var quote = abstractUsersServices.Users_All(pageParam, search, Users);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Users_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_Delete")]
        public async Task<IHttpActionResult> Users_Delete(long Id,long DeletedBy)
        {
            var quote = abstractUsersServices.Users_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ////Users_SendOTP Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_SendOTP")]
        public async Task<IHttpActionResult> Customer_SendOTP(string MobileNumber)
        {
            SuccessResult<AbstractUsers> UsersData = new SuccessResult<AbstractUsers>();

            string otp = new Random().Next(1111, 9999).ToString("D4");
            if (MobileNumber == "1234512345")
            {
                otp = "9999";
            }

            //string otp = MobileNumber.ToString() == "9924088018" ? "1234" : new Random().Next(0, 9999).ToString("D4");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", MobileNumber.ToString()).Replace("#otp#", otp).ToString());
            request.Method = "GET";
            request.ContentType = "application/json";
            
            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            Users users = new Users();
            users.OTP = Convert.ToInt64(otp);
            UsersData.Code = 200;
            UsersData.Message = "OTP Sended";
            UsersData.Item = users;

            return this.Content((HttpStatusCode)HttpStatusCode.OK, UsersData);

            //var quote = abstractUsersServices.Users_SendOTP(MobileNumber, Convert.ToInt32(otp));

            //return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);
        }

        //Users_OnlineOffline Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_OnlineOffline")]
        public async Task<IHttpActionResult> Users_OnlineOffline(long Id, bool IsOnline)
        {
            var quote = abstractUsersServices.Users_OnlineOffline(Id, IsOnline);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //CheckUser_Exists API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CheckUser_Exists")]
        public async Task<IHttpActionResult> CheckUser_Exists(string  MobileNumber)
        {
            var quote = abstractUsersServices.CheckUser_Exists(MobileNumber);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Users_IsCallBackPreference Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Users_IsCallBackPreference")]
        public async Task<IHttpActionResult> Users_IsCallBackPreference(long Id, bool IsCallBackPreference)
        {
            var quote = abstractUsersServices.Users_IsCallBackPreference(Id, IsCallBackPreference);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}

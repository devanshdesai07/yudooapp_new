﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using System.Configuration;
using Newtonsoft.Json;
using GoogleMaps.LocationServices;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShipmentV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShipmentServices abstractShipmentServices;
        #endregion

        #region Cnstr
        public ShipmentV1Controller(AbstractShipmentServices abstractShipmentServices)
        {
            this.abstractShipmentServices = abstractShipmentServices;
        }
        #endregion


        // Shipment_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipment_Upsert")]
        public async Task<IHttpActionResult> Shipment_Upsert(Shipment Shipment)
        {
            var quote = abstractShipmentServices.Shipment_Upsert(Shipment);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Shipment_ByOrderId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipment_ByOrderId")]
        public async Task<IHttpActionResult> Shipment_ByOrderId(long OrderId, PageParam pageParam)
        {
            var quote = abstractShipmentServices.Shipment_ByOrderId(OrderId, pageParam);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Shipment_ByParcelId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipment_ByParcelId")]
        public async Task<IHttpActionResult> Shipment_ByParcelId(long ParcelId, PageParam pageParam)
        {
            var quote = abstractShipmentServices.Shipment_ByParcelId(ParcelId, pageParam);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Shipment_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipment_ById")]
        public async Task<IHttpActionResult> Shipment_ById(long Id)
        {
            var quote = abstractShipmentServices.Shipment_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Shipment_StatusChange API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipment_StatusChange")]
        public async Task<IHttpActionResult> Shipment_StatusChange(long Id)
        {
            var quote = abstractShipmentServices.Shipment_StatusChange(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }   
    

}
﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using yudooapp.APICommon;
using yudooapp.Common;
using yudooapp.Entities.Contract;

namespace yudooappApi.Controllers.V1
{

    public class SignalV1Controller : AbstractBaseController
    {

        // SignalChat API
        [System.Web.Http.HttpPost]
        [InheritedRoute("SignalChat")]
        public async Task<IHttpActionResult> SignalChat(string Session, string Data)
        {

            try
            {
                Signal signal = new Signal();
                signal.session = Session;
                signal.type = "signal:chat";
                signal.data = Data;

                var httpClient = new HttpClient
                {
                    BaseAddress = new Uri("https://video.yudoo.co/")
                };

                httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", "T1BFTlZJRFVBUFA6QXNkY3h6MTIz");

                var requestUrl = $"openvidu/api/signal";


                var req = new HttpRequestMessage(HttpMethod.Post, requestUrl);
                //var content = new JObject(users);

                var json = new JavaScriptSerializer().Serialize(signal);

                // Add body content
                req.Content = new StringContent(
                    json,
                    Encoding.UTF8,
                    "application/json"
                );

                var responseAPi = await httpClient.SendAsync(req);

                var readTask = responseAPi.Content.ReadAsStringAsync().Result;
                //JObject jsonObj = JObject.Parse(readTask);

                SuccessResult<Signal> successResult = Newtonsoft.Json.JsonConvert.DeserializeObject<SuccessResult<Signal>>(readTask);

                if (responseAPi.StatusCode == HttpStatusCode.OK)
                {
                    return this.Content((HttpStatusCode)200, true);
                }
                else
                {
                    return this.Content((HttpStatusCode)400, false);
                }

            }
            catch (Exception error)
            {
                throw;
            }
        }
    }
}
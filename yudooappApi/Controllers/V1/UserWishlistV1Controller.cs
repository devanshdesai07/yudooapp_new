﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using Newtonsoft.Json;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserWishlistV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserWishlistServices abstractUserWishlistServices;
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        #endregion

        #region Cnstr
        public UserWishlistV1Controller(AbstractUserWishlistServices abstractUserWishlistServices, AbstractShopOwnerServices abstractShopOwnerServices)
        {
            this.abstractUserWishlistServices = abstractUserWishlistServices;
            this.abstractShopOwnerServices = abstractShopOwnerServices;
        }
        #endregion


        //UserWishlist_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserWishlist_ById")]
        public async Task<IHttpActionResult> UserWishlist_ById(long Id)
        {
            var quote = abstractUserWishlistServices.UserWishlist_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserWishlist_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserWishlist_All")]
        public async Task<IHttpActionResult> UserWishlist_All(PageParam pageParam, string Search = "", long UserId = 0, long ShopId = 0)
        {
            AbstractUserWishlist UserWishlist = new UserWishlist();
            var quote = abstractUserWishlistServices.UserWishlist_All(pageParam, Search, UserId, ShopId);

            return this.Content((HttpStatusCode)200, quote);
        }

        //UserWishlist_ByUserId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserWishlist_ByUserId")]
        public async Task<IHttpActionResult> UserWishlist_ByUserId(PageParam pageParam, long UserId = 0)
        {
            //AbstractUserWishlist UserWishlist = new UserWishlist();
            //var quote = abstractUserWishlistServices.UserWishlist_ByUserId(pageParam,  UserId);
            

            PagedList<AbstractShopOwner> quote = abstractUserWishlistServices.UserWishlist_ByUserId(pageParam, UserId);

            if (quote != null && quote.Values.Count > 0)
            {
                foreach (var owner in quote.Values)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(owner.Timing)))
                        owner.Timing = JsonConvert.DeserializeObject(Convert.ToString(owner.Timing));

                    owner.Tags = "";
                }
            }

            return this.Content((HttpStatusCode)200, quote);
        }

        //UserWishlist_ByShopId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserWishlist_ByShopId")]
        public async Task<IHttpActionResult> UserWishlist_ByShopId(PageParam pageParam, long ShopId = 0)
        {
            AbstractUserWishlist UserWishlist = new UserWishlist();
            var quote = abstractUserWishlistServices.UserWishlist_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserWishlist_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserWishlist_Upsert")]
        public async Task<IHttpActionResult> UserWishlist_Upsert(UserWishlist userWishlist)
        {
            var quote = abstractUserWishlistServices.UserWishlist_Upsert(userWishlist);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserWishlist_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserWishlist_Delete")]
        public async Task<IHttpActionResult> UserWishlist_Delete(int UserId, int ShopId)
        {
            var quote = abstractUserWishlistServices.UserWishlist_Delete(UserId, ShopId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
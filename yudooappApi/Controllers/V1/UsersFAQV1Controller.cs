﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersFAQV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUsersFAQServices abstractUsersFAQServices;
        #endregion

        #region Cnstr
        public UsersFAQV1Controller(AbstractUsersFAQServices abstractUsersFAQServices)
        {
            this.abstractUsersFAQServices = abstractUsersFAQServices;
        }
        #endregion


        //UsersFAQ_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersFAQ_All")]
        public async Task<IHttpActionResult> UsersFAQ_All(PageParam pageParam, string Search = "")
        {
            AbstractUsersFAQ UsersFAQ = new UsersFAQ();
            var quote = abstractUsersFAQServices.UsersFAQ_All(pageParam, Search);
            return this.Content((HttpStatusCode)200, quote);
        }

        ////UsersFAQ_Delete Api
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("UsersFAQ_Delete")]
        //public async Task<IHttpActionResult> UsersFAQ_Delete(long Id)
        //{
        //    var quote = abstractUsersFAQServices.UsersFAQ_Delete(Id);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        ////UsersFAQ_ActInAct Api
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("UsersFAQ_ActInAct")]
        //public async Task<IHttpActionResult> UsersFAQ_ActInAct(long Id)
        //{
        //    var quote = abstractUsersFAQServices.UsersFAQ_ActInAct(Id);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        //UsersFAQ_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UsersFAQ_ById")]
        public async Task<IHttpActionResult> UsersFAQ_ById(long Id)
        {
            var quote = abstractUsersFAQServices.UsersFAQ_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



        

    }
}
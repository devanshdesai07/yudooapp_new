﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Dynamic;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WeebhokV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserOrderServices abstractUserOrderServices;
        #endregion

        #region Cnstr
        public WeebhokV1Controller(AbstractUserOrderServices abstractUserOrderServices)
        {
            this.abstractUserOrderServices = abstractUserOrderServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("razorpay")]
        public async Task<IHttpActionResult> razorpay(dynamic razorPay)
        {
            long Id = 0, OrderStatus = 0;

            string jsonString = JsonConvert.SerializeObject(razorPay);

            File.AppendAllText(HttpContext.Current.Server.MapPath("~/ErrorLog/razorPay.txt"),
                jsonString + Environment.NewLine);

            try
            {
                if(razorPay.payload.payment_link != null)
                {
                    Id = ConvertTo.Integer(razorPay.payload.payment_link.entity.description);
                    if (razorPay.payload.payment.entity.status == "authorized" || razorPay.payload.payment.entity.status == "captured")
                    {
                        var quote = abstractUserOrderServices.UserOrder_StatusChange(Id, 3);
                    }
                }

                Id = ConvertTo.Integer(razorPay.payload.payment.entity.description);
                if (razorPay.payload.payment.entity.status == "authorized" || razorPay.payload.payment.entity.status == "captured")
                {
                    var quote = abstractUserOrderServices.UserOrder_StatusChange(Id, 3);
                }
            }
            catch(Exception ex)
            {
                Id = ConvertTo.Integer(razorPay.payload.payment.entity.description);
                if (razorPay.payload.payment.entity.status == "authorized" || razorPay.payload.payment.entity.status == "captured")
                {
                    var quote = abstractUserOrderServices.UserOrder_StatusChange(Id, 3);
                }
            }

            SuccessResult<AbstractUserShopCallLogs> successResult = new SuccessResult<AbstractUserShopCallLogs>();
            successResult.Code = 200;
            //var quote = abstractCallBackServices.RazorPay(userShopCallLogs);
            return this.Content((HttpStatusCode)successResult.Code, razorPay);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("vidu")]
        public async Task<IHttpActionResult> vidu(dynamic dynamicObject)
        {
            long Id = 0, OrderStatus = 0;
            string jsonString = JsonConvert.SerializeObject(dynamicObject);
            //TextWriter txt = new StreamWriter(HttpContext.Current.Server.MapPath("~/ErrorLog/ErrorLog2.txt"));
            //txt.Write(jsonString);
            //txt.Close();
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/ErrorLog/ErrorLog2.txt"),
                   jsonString + Environment.NewLine);
            //if(dynamicObject !=null && dynamicObject.@event != null)
            //{
            //    if(dynamicObject.@event == "webrtcConnectionCreated")
            //    {
            //        //dynamicObject.sessionId
            //    }
            //}
            SuccessResult<AbstractUserShopCallLogs> successResult = new SuccessResult<AbstractUserShopCallLogs>();
            successResult.Code = 200;
            //var quote = abstractCallBackServices.RazorPay(userShopCallLogs);
            return this.Content((HttpStatusCode)successResult.Code, dynamicObject);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("shiprocket")]
        public async Task<IHttpActionResult> shiprocket(Shiprocket shiprocket)
        {
            long Id = 0, OrderStatus = 0;
            Id = ConvertTo.Integer(shiprocket.order_id);
            if (shiprocket.shipment_status == "18" || shiprocket.shipment_status == "6"|| shiprocket.shipment_status == "42")
            {
                var quote = abstractUserOrderServices.UserOrder_StatusChangeByShipmentId(Id, 5);
                //var quote = abstractUserOrderServices.UserOrder_StatusChange(Id, 5);
            }
            else if (shiprocket.shipment_status == "7")
            {
                var quote2 = abstractUserOrderServices.UserOrder_StatusChangeByShipmentId(Id, 6);
            }
            else if (shiprocket.shipment_status == "8" || shiprocket.shipment_status == "9")
            {
                var quote3 = abstractUserOrderServices.UserOrder_StatusChangeByShipmentId(Id, 8);
            }

            File.AppendAllText(HttpContext.Current.Server.MapPath("~/ErrorLog/shiprocket.txt"),
                  shiprocket + Environment.NewLine);

            SuccessResult<AbstractUserShopCallLogs> successResult = new SuccessResult<AbstractUserShopCallLogs>();
            successResult.Code = 200;
            return this.Content((HttpStatusCode)successResult.Code, shiprocket);
        }

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class AcquirerData
        {
            public string rrn { get; set; }
        }

        public class Entity
        {
            public string id { get; set; }
            public string entity { get; set; }
            public int amount { get; set; }
            public string currency { get; set; }
            public string status { get; set; }
            public object order_id { get; set; }
            public object invoice_id { get; set; }
            public bool international { get; set; }
            public string method { get; set; }
            public int amount_refunded { get; set; }
            public object refund_status { get; set; }
            public bool captured { get; set; }
            public string description { get; set; }
            public object card_id { get; set; }
            public object bank { get; set; }
            public object wallet { get; set; }
            public string vpa { get; set; }
            public string email { get; set; }
            public string contact { get; set; }
            public List<object> notes { get; set; }
            public object fee { get; set; }
            public object tax { get; set; }
            public object error_code { get; set; }
            public object error_description { get; set; }
            public object error_source { get; set; }
            public object error_step { get; set; }
            public object error_reason { get; set; }
            public AcquirerData acquirer_data { get; set; }
            public int created_at { get; set; }
        }

        public class WeebhokPayment
        {
            public Entity entity { get; set; }
        }

        public class Payload
        {
            public WeebhokPayment payment { get; set; }
        }

        public class WeebhokRoot
        {
            public string entity { get; set; }
            public string account_id { get; set; }
            public string @event { get; set; }
            public List<string> contains { get; set; }
            public Payload payload { get; set; }
            public int created_at { get; set; }
        }


    }


}
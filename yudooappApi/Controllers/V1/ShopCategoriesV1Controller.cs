﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopCategoriesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopCategoriesServices abstractShopCategoriesServices;
        #endregion

        #region Cnstr
        public ShopCategoriesV1Controller(AbstractShopCategoriesServices abstractShopCategoriesServices)
        {
            this.abstractShopCategoriesServices = abstractShopCategoriesServices;
        }
        #endregion

        // ShopCategories_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_Upsert")]
        public async Task<IHttpActionResult> ShopCategories_Upsert(ShopCategories ShopCategories)
        {
            var quote = abstractShopCategoriesServices.ShopCategories_Upsert(ShopCategories);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopCategories_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_All")]
        public async Task<IHttpActionResult> ShopCategories_All(PageParam pageParam, string search = "")
        {
            var quote = abstractShopCategoriesServices.ShopCategories_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopCategories_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_ById")]
        public async Task<IHttpActionResult> ShopCategories_ById(long Id)
        {
            var quote = abstractShopCategoriesServices.ShopCategories_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopCategories_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_ByShopId")]
        public async Task<IHttpActionResult> ShopCategories_ByShopId(PageParam pageParam, long ShopId)
        {
            var quote = abstractShopCategoriesServices.ShopCategories_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopCategories_ByCategoryId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_ByCategoryId")]
        public async Task<IHttpActionResult> ShopCategories_ByCategoryId(PageParam pageParam, long CategoryId)
        {
            var quote = abstractShopCategoriesServices.ShopCategories_ByCategoryId(pageParam, CategoryId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopCategories_BySubCategoryId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_BySubCategoryId")]
        public async Task<IHttpActionResult> ShopCategories_BySubCategoryId(PageParam pageParam, long SubCategoryId)
        {
            var quote = abstractShopCategoriesServices.ShopCategories_BySubCategoryId(pageParam, SubCategoryId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopCategories_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCategories_Delete")]
        public async Task<IHttpActionResult> ShopCategories_Delete(long Id)
        {
            var quote = abstractShopCategoriesServices.ShopCategories_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
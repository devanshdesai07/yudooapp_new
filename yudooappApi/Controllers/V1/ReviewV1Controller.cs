﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReviewV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractReviewServices abstractReviewServices;
        #endregion

        #region Cnstr
        public ReviewV1Controller(AbstractReviewServices abstractReviewServices)
        {
            this.abstractReviewServices = abstractReviewServices;
        }
        #endregion

        //Review_ByCustomerId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Review_ByCustomerId")]
        public async Task<IHttpActionResult> Review_ByCustomerId(PageParam pageParam, string search = "", long CustomerId = 0)
        {
            AbstractReview Review = new Review();
            Review.CustomerId = CustomerId;
            var quote = abstractReviewServices.Review_ByCustomerId(pageParam, search, Review);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Review_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Review_Upsert")]
        public async Task<IHttpActionResult> Review_Upsert(Review review)
        {
            var quote = abstractReviewServices.Review_Upsert(review);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Review_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Review_All")]
        public async Task<IHttpActionResult> Review_All(PageParam pageParam, string search = "")
        {
            AbstractReview Review = new Review();
            var quote = abstractReviewServices.Review_All(pageParam, search, Review);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}

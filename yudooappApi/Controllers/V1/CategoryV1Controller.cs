﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCategoryServices abstractCategoryServices;
        #endregion

        #region Cnstr
        public CategoryV1Controller(AbstractCategoryServices abstractCategoryServices)
        {
            this.abstractCategoryServices = abstractCategoryServices;
        }
        #endregion



        // Category_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Category_All")]
        public async Task<IHttpActionResult> Category_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCategoryServices.Category_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Category_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Category_ById")]
        public async Task<IHttpActionResult> Category_ById(long Id)
        {
            var quote = abstractCategoryServices.Category_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // Banners_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Category_Upsert")]
        public async Task<IHttpActionResult> Category_Upsert(Category category)
        {
            var quote = abstractCategoryServices.Category_Upsert(category);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }



    

}
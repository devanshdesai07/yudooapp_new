﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BannersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractBannersServices abstractBannersServices;
        #endregion

        #region Cnstr
        public BannersV1Controller(AbstractBannersServices abstractBannersServices)
        {
            this.abstractBannersServices = abstractBannersServices;
        }
        #endregion

        // Banners_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Banners_Upsert")]
        public async Task<IHttpActionResult> Banners_Upsert(Banners Banners)
        {
            var quote = abstractBannersServices.Banners_Upsert(Banners);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Banners_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Banners_All")]
        public async Task<IHttpActionResult> Banners_All(PageParam pageParam, string search = "",int IsSpecialist = 0)
        {
            var quote = abstractBannersServices.Banners_All(pageParam, search, IsSpecialist);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Banners_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Banners_ById")]
        public async Task<IHttpActionResult> Banners_ById(long Id)
        {
            var quote = abstractBannersServices.Banners_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Banners_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Banners_ActInAct")]
        public async Task<IHttpActionResult> Banners_ActInAct(long Id)
        {
            var quote = abstractBannersServices.Banners_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Banners_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Banners_Delete")]
        public async Task<IHttpActionResult> Banners_Delete(long Id)
        {
            var quote = abstractBannersServices.Banners_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
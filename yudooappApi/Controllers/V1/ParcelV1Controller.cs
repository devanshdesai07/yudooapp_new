﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using System.Configuration;
using Newtonsoft.Json;
using GoogleMaps.LocationServices;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ParcelV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractParcelServices abstractParcelServices;
        #endregion

        #region Cnstr
        public ParcelV1Controller(AbstractParcelServices abstractParcelServices)
        {
            this.abstractParcelServices = abstractParcelServices;
        }
        #endregion


        // Parcel_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Parcel_Upsert")]
        public async Task<IHttpActionResult> Parcel_Upsert(Parcel parcel)
        {
            var quote = abstractParcelServices.Parcel_Upsert(parcel);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Shipment_ByOrderId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Shipment_ByOrderId")]
        public async Task<IHttpActionResult> Parcel_ByOrderId(long OrderId, PageParam pageParam)
        {
            var quote = abstractParcelServices.Parcel_ByOrderId(OrderId, pageParam);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Parcel_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Parcel_ById")]
        public async Task<IHttpActionResult> Parcel_ById(long Id)
        {
            var quote = abstractParcelServices.Parcel_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Parcel_StatusChange API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Parcel_StatusChange")]
        public async Task<IHttpActionResult> Parcel_StatusChange(long Id)
        {
            var quote = abstractParcelServices.Parcel_StatusChange(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //Parcel_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Parcel_Delete")]
        public async Task<IHttpActionResult> Parcel_Delete(long Id, long DeletedBy)
        {
            var quote = abstractParcelServices.Parcel_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }   
    

}
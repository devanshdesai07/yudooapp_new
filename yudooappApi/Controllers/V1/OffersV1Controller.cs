﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OffersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractOffersServices abstractOffersServices ;
        #endregion

        #region Cnstr
        public OffersV1Controller(AbstractOffersServices abstractOffersServices)
        {
            this.abstractOffersServices = abstractOffersServices;
        }
        #endregion

        // Offers_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_Upsert")]
        public async Task<IHttpActionResult> Offers_Upsert(Offers offers )
        {
            var quote = abstractOffersServices.Offers_Upsert(offers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Offers_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_All")]
        public async Task<IHttpActionResult> Offers_All(PageParam pageParam, string search = "")
        {
            var quote = abstractOffersServices.Offers_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Offers_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_ById")]
        public async Task<IHttpActionResult> Offers_ById(long Id)
        {
            var quote = abstractOffersServices.Offers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Offers_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_ByShopId")]
        public async Task<IHttpActionResult> Offers_ByShopId(PageParam pageParam, long ShopId)
        {
            var quote = abstractOffersServices.Offers_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Offers_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_ActInAct")]
        public async Task<IHttpActionResult> Offers_ActInAct(long Id, bool IsActive)
        {
            var quote = abstractOffersServices.Offers_ActInAct(Id,IsActive);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Offers_All_Active API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_All_Active")]
        public async Task<IHttpActionResult> Offers_All_Active(PageParam pageParam, string search = "")
        {
            var quote = abstractOffersServices.Offers_All_Active(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Offers_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Offers_Delete")]
        public async Task<IHttpActionResult> Offers_Delete(long Id)
        {
            var quote = abstractOffersServices.Offers_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
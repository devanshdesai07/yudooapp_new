﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserCallBackPreferenceTimgingsV1Controller : AbstractBaseController
    {
        private readonly AbstractUserCallBackPreferenceTimgingsServices _abstractUserCallBackPreferenceTimgingsServices = null;
        public UserCallBackPreferenceTimgingsV1Controller(AbstractUserCallBackPreferenceTimgingsServices _abstractUserCallBackPreferenceTimgingsServices)
        {
            this._abstractUserCallBackPreferenceTimgingsServices = _abstractUserCallBackPreferenceTimgingsServices;
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCallBackPreferenceTimgings_Upsert")]
        public async Task<IHttpActionResult> UserCallBackPreferenceTimgings_Upsert(UserCallBackPreferenceTimingJson abstractUserCallBackPreferenceTimingJson)
        {
            var quote = _abstractUserCallBackPreferenceTimgingsServices.UserCallBackPreferenceTimgings_Upsert(abstractUserCallBackPreferenceTimingJson);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCallBackPreferenceTimgings_All")]
        public async Task<IHttpActionResult> UserCallBackPreferenceTimgings_All(PageParam pageParam, string search = "", long UserId = 0)
        {
            var quote = _abstractUserCallBackPreferenceTimgingsServices.UserCallBackPreferenceTimgings_All(pageParam,search,UserId);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCallBackPreferenceTimgings_ById")]
        public async Task<IHttpActionResult> UserCallBackPreferenceTimgings_ById(long Id)
        {
            var quote = _abstractUserCallBackPreferenceTimgingsServices.UserCallBackPreferenceTimgings_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserCallBackPreferenceTimgings_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCallBackPreferenceTimgings_ByUserId")]
        public async Task<IHttpActionResult> UserCallBackPreferenceTimgings_ByUserId(PageParam pageParam, long UserId = 0)
        {
            var quote = _abstractUserCallBackPreferenceTimgingsServices.UserCallBackPreferenceTimgings_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCallBackPreferenceTimgings_Available")]
        public async Task<IHttpActionResult> UserCallBackPreferenceTimgings_Available(long UserId)
        {
            var quote = _abstractUserCallBackPreferenceTimgingsServices.UserCallBackPreferenceTimgings_Available(UserId);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCallBackPreferenceTimgings_Delete")]
        public async Task<IHttpActionResult> UserCallBackPreferenceTimgings_Delete(long Id)
        {
            var quote = _abstractUserCallBackPreferenceTimgingsServices.UserCallBackPreferenceTimgings_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
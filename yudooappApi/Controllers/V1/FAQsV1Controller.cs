﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FAQsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractFAQsServices abstractFAQsServices;
        #endregion

        #region Cnstr
        public FAQsV1Controller(AbstractFAQsServices abstractFAQsServices)
        {
            this.abstractFAQsServices = abstractFAQsServices;
        }
        #endregion

        // FAQs_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FAQs_Upsert")]
        public async Task<IHttpActionResult> FAQs_Upsert(FAQs FAQs)
        {
            var quote = abstractFAQsServices.FAQs_Upsert(FAQs);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // FAQs_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FAQs_All")]
        public async Task<IHttpActionResult> FAQs_All(PageParam pageParam, string search = "")
        {
            var quote = abstractFAQsServices.FAQs_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //FAQs_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FAQs_ById")]
        public async Task<IHttpActionResult> FAQs_ById(long Id)
        {
            var quote = abstractFAQsServices.FAQs_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //FAQs_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("FAQs_ActInAct")]
        public async Task<IHttpActionResult> FAQs_ActInAct(long Id)
        {
            var quote = abstractFAQsServices.FAQs_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //FAQs_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("FAQs_Delete")]
        public async Task<IHttpActionResult> FAQs_Delete(long Id)
        {
            var quote = abstractFAQsServices.FAQs_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
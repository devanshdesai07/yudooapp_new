﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserFeedBackV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserFeedBackServices abstractUserFeedBackServices;
        #endregion

        #region Cnstr
        public UserFeedBackV1Controller(AbstractUserFeedBackServices abstractUserFeedBackServices)
        {
            this.abstractUserFeedBackServices = abstractUserFeedBackServices;
        }
        #endregion

        // UserFeedBack_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserFeedBack_Upsert")]
        public async Task<IHttpActionResult> UserFeedBack_Upsert(UserFeedBack UserFeedBack)
        {
            var quote = abstractUserFeedBackServices.UserFeedBack_Upsert(UserFeedBack);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

       
    }
}
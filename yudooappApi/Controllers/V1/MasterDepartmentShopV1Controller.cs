﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterDepartmentShopV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterDepartmentShopServices abstractMasterDepartmentShopServices;
        #endregion

        #region Cnstr
        public MasterDepartmentShopV1Controller(AbstractMasterDepartmentShopServices abstractMasterDepartmentShopServices)
        {
            this.abstractMasterDepartmentShopServices = abstractMasterDepartmentShopServices;
        }
        #endregion


        //MasterDepartmentShop_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterDepartmentShop_ById")]
        public async Task<IHttpActionResult> MasterDepartmentShop_ById(long Id)
        {
            var quote = abstractMasterDepartmentShopServices.MasterDepartmentShop_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //MasterDepartmentShop_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterDepartmentShop_All")]
        public async Task<IHttpActionResult> MasterDepartmentShop_All(PageParam pageParam, string Search = "")
        {
            AbstractMasterDepartmentShop MasterDepartmentShop = new MasterDepartmentShop();
            var quote = abstractMasterDepartmentShopServices.MasterDepartmentShop_All(pageParam, Search);
            return this.Content((HttpStatusCode)200, quote);
        }

        // MasterDepartmentShop_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterDepartmentShop_Upsert")]
        public async Task<IHttpActionResult> MasterDepartmentShop_Upsert(MasterDepartmentShop masterDepartmentShop)
        {
            var quote = abstractMasterDepartmentShopServices.MasterDepartmentShop_Upsert(masterDepartmentShop);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //MasterDepartmentShop_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterDepartmentShop_Delete")]
        public async Task<IHttpActionResult> MasterDepartmentShop_Delete(long Id)
        {
            var quote = abstractMasterDepartmentShopServices.MasterDepartmentShop_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
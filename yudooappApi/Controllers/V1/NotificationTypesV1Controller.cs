﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificationTypesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractNotificationTypesServices abstractNotificationTypesServices;
        #endregion

        #region Cnstr
        public NotificationTypesV1Controller(AbstractNotificationTypesServices abstractNotificationTypesServices)
        {
            this.abstractNotificationTypesServices = abstractNotificationTypesServices;
        }
        #endregion

        // NotificationTypes_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("NotificationTypes_Upsert")]
        public async Task<IHttpActionResult> NotificationTypes_Upsert(NotificationTypes NotificationTypes)
        {
            var quote = abstractNotificationTypesServices.NotificationTypes_Upsert(NotificationTypes);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // NotificationTypes_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("NotificationTypes_All")]
        public async Task<IHttpActionResult> NotificationTypes_All(PageParam pageParam, string search = "")
        {
            var quote = abstractNotificationTypesServices.NotificationTypes_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //NotificationTypes_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("NotificationTypes_ById")]
        public async Task<IHttpActionResult> NotificationTypes_ById(long Id)
        {
            var quote = abstractNotificationTypesServices.NotificationTypes_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //NotificationTypes_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("NotificationTypes_Delete")]
        public async Task<IHttpActionResult> NotificationTypes_Delete(long Id)
        {
            var quote = abstractNotificationTypesServices.NotificationTypes_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopCallingNotificationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopCallingNotificationServices abstractShopCallingNotificationServices;
        #endregion

        #region Cnstr
        public ShopCallingNotificationV1Controller(AbstractShopCallingNotificationServices abstractShopCallingNotificationServices)
        {
            this.abstractShopCallingNotificationServices = abstractShopCallingNotificationServices;
        }
        #endregion

        // ShopCallingNotification_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopCallingNotification_Upsert")]
        public async Task<IHttpActionResult> ShopCallingNotification_Upsert(ShopCallingNotification ShopCallingNotification)
        {
            var quote = abstractShopCallingNotificationServices.ShopCallingNotification_Upsert(ShopCallingNotification);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

       
    }
}
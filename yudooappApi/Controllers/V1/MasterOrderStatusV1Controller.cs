﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterOrderStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterOrderStatusServices abstractMasterOrderStatusServices;
        #endregion

        #region Cnstr
        public MasterOrderStatusV1Controller(AbstractMasterOrderStatusServices abstractMasterOrderStatusServices)
        {
            this.abstractMasterOrderStatusServices = abstractMasterOrderStatusServices;
        }
        #endregion

        // MasterOrderStatus_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterOrderStatus_Upsert")]
        public async Task<IHttpActionResult> MasterOrderStatus_Upsert(MasterOrderStatus MasterOrderStatus)
        {
            var quote = abstractMasterOrderStatusServices.MasterOrderStatus_Upsert(MasterOrderStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // MasterOrderStatus_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterOrderStatus_All")]
        public async Task<IHttpActionResult> MasterOrderStatus_All(PageParam pageParam, string search = "")
        {
            var quote = abstractMasterOrderStatusServices.MasterOrderStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        

    }
}
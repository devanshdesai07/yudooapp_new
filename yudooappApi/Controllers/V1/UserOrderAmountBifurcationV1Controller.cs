﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserOrderAmountBifurcationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserOrderAmountBifurcationServices abstractUserOrderAmountBifurcationServices;
        #endregion

        #region Cnstr
        public UserOrderAmountBifurcationV1Controller(AbstractUserOrderAmountBifurcationServices abstractUserOrderAmountBifurcationServices)
        {
            this.abstractUserOrderAmountBifurcationServices = abstractUserOrderAmountBifurcationServices;
        }
        #endregion


        // UserOrderAmountBifurcation_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderAmountBifurcation_All")]
        public async Task<IHttpActionResult> UserOrderAmountBifurcation_All(PageParam pageParam, string search = "",long OrderId=0, string FromDate = "", string ToDate = "", string type = "",int IsPaid = 0)
        {
            var quote = abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_All(pageParam, search,OrderId, FromDate, ToDate, type,IsPaid);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserOrderAmountBifurcation_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderAmountBifurcation_Upsert")]
        public async Task<IHttpActionResult> UserOrderAmountBifurcation_Upsert(UserOrderAmountBifurcation UserOrderAmountBifurcation)
        {
            var quote = abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_Upsert(UserOrderAmountBifurcation);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserOrderAmountBifurcation_UpdateIsPaid API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderAmountBifurcation_UpdateIsPaid")]
        public async Task<IHttpActionResult> UserOrderAmountBifurcation_UpdateIsPaid(long Id, int IsPaid)
        {
            var quote = abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_UpdateIsPaid(Id, IsPaid);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OfferStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractOfferStatusServices abstractOfferStatusServices;
        #endregion

        #region Cnstr
        public OfferStatusV1Controller(AbstractOfferStatusServices abstractOfferStatusServices)
        {
            this.abstractOfferStatusServices = abstractOfferStatusServices;
        }
        #endregion

        // OfferStatus_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OfferStatus_Upsert")]
        public async Task<IHttpActionResult> OfferStatus_Upsert(OfferStatus OfferStatus)
        {
            var quote = abstractOfferStatusServices.OfferStatus_Upsert(OfferStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // OfferStatus_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OfferStatus_All")]
        public async Task<IHttpActionResult> OfferStatus_All(PageParam pageParam, string search = "")
        {
            var quote = abstractOfferStatusServices.OfferStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //OfferStatus_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OfferStatus_ById")]
        public async Task<IHttpActionResult> OfferStatus_ById(long Id)
        {
            var quote = abstractOfferStatusServices.OfferStatus_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //OfferStatus_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("OfferStatus_Delete")]
        public async Task<IHttpActionResult> OfferStatus_Delete(long Id)
        {
            var quote = abstractOfferStatusServices.OfferStatus_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
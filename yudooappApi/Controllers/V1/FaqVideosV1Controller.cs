﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FaqVideosV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractFaqVideosServices abstractFaqVideosServices;
        #endregion

        #region Cnstr
        public FaqVideosV1Controller(AbstractFaqVideosServices abstractFaqVideosServices)
        {
            this.abstractFaqVideosServices = abstractFaqVideosServices;
        }
        #endregion

        // FaqVideos_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FaqVideos_Upsert")]
        public async Task<IHttpActionResult> FaqVideos_Upsert(FaqVideos FaqVideos)
        {
            var quote = abstractFaqVideosServices.FaqVideo_Upsert(FaqVideos);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // FaqVideos_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FaqVideos_All")]
        public async Task<IHttpActionResult> FaqVideos_All(PageParam pageParam, string search = "")
        {
            var quote = abstractFaqVideosServices.FaqVideo_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //FaqVideos_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("FaqVideos_ById")]
        public async Task<IHttpActionResult> FaqVideos_ById(long Id)
        {
            var quote = abstractFaqVideosServices.FaqVideo_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //FaqVideos_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("FaqVideos_ActInAct")]
        public async Task<IHttpActionResult> FaqVideos_ActInAct(long Id)
        {
            var quote = abstractFaqVideosServices.FaqVideo_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //FaqVideos_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("FaqVideos_Delete")]
        public async Task<IHttpActionResult> FaqVideos_Delete(long Id)
        {
            var quote = abstractFaqVideosServices.FaqVideo_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
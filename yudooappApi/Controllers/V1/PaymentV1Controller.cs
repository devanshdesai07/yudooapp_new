﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPaymentServices abstractPaymentServices;
        #endregion

        #region Cnstr
        public PaymentV1Controller(AbstractPaymentServices abstractPaymentServices)
        {
            this.abstractPaymentServices = abstractPaymentServices;
        }
        #endregion

       
        //Payment_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_ById")]
        public async Task<IHttpActionResult> Payment_ById(long Id)
        {
            var quote = abstractPaymentServices.Payment_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //Payment_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_Upsert")]
        public async Task<IHttpActionResult> Payment_Upsert(Payment payment)
        {
            var quote = abstractPaymentServices.Payment_Upsert(payment);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Payment_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_All")]
        public async Task<IHttpActionResult> Payment_All(PageParam pageParam, string search = "")
        {
            AbstractPayment Payment = new Payment();
            var quote = abstractPaymentServices.Payment_All(pageParam, search, Payment);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}

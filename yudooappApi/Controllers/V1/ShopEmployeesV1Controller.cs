﻿
using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopEmployeesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopEmployeesServices abstractShopEmployeesServices;
        #endregion

        #region Cnstr
        public ShopEmployeesV1Controller(AbstractShopEmployeesServices abstractShopEmployeesServices)
        {
            this.abstractShopEmployeesServices = abstractShopEmployeesServices;
        }
        #endregion

        // ShopEmployees_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_Upsert")]
        public async Task<IHttpActionResult> ShopEmployees_Upsert(ShopEmployees shopEmployees)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_Upsert(shopEmployees);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopEmployees_All
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_All")]
        public async Task<IHttpActionResult> ShopEmployees_All(PageParam pageParam, string Search = "", long ShopId = 0)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_All(pageParam, Search, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopEmployees_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_ById")]
        public async Task<IHttpActionResult> ShopEmployees_ById(long Id)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopEmployees_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_ByShopId")]
        public async Task<IHttpActionResult> ShopEmployees_ByShopId(PageParam pageParam, long ShopId = 0)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopEmployees_ActInact Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_ActInact")]
        public async Task<IHttpActionResult> ShopEmployees_ActInact(long Id)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopEmployees_IsOnline Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_IsOnline")]
        public async Task<IHttpActionResult> ShopEmployees_IsOnline(long Id)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_IsOnline(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopEmployees_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployees_Delete")]
        public async Task<IHttpActionResult> ShopEmployees_Delete(long Id)
        {
            var quote = abstractShopEmployeesServices.ShopEmployees_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
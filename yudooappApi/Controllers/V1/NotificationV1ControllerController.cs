﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractNotificationServices abstractNotificationServices;
        #endregion

        #region Constr
        public NotificationV1Controller(AbstractNotificationServices abstractNotificationServices)
        {
            this.abstractNotificationServices = abstractNotificationServices;
        }
        #endregion

        // Notification_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_Upsert")]
        public async Task<IHttpActionResult> Notification_Upsert(Notification Notification)
        {
            var quote = abstractNotificationServices.Notification_Upsert(Notification);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Notification_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_All")]
        public async Task<IHttpActionResult> Notification_All(PageParam pageParam, string search = "")
        {
            var quote = abstractNotificationServices.Notification_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Notification_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_ById")]
        public async Task<IHttpActionResult> Notification_ById(long Id)
        {
            var quote = abstractNotificationServices.Notification_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Notification_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_ByUserId")]
        public async Task<IHttpActionResult> Notification_ByUserId(PageParam pageParam, long UserId = 0)
        {
            PagedList<AbstractNotification> quote = abstractNotificationServices.Notification_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Notification_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Notification_Delete")]
        public async Task<IHttpActionResult> Notification_Delete(long Id)
        {
            var quote = abstractNotificationServices.Notification_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
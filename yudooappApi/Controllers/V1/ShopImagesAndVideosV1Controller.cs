﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using System.Security.Claims;
using System.Diagnostics;
using WMPLib;


namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopImagesAndVideosV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopImagesAndVideosServices abstractShopImagesAndVideosServices;
        #endregion

        #region Cnstr
        public ShopImagesAndVideosV1Controller(AbstractShopImagesAndVideosServices abstractShopImagesAndVideosServices)
        {
            this.abstractShopImagesAndVideosServices = abstractShopImagesAndVideosServices;
        }
        #endregion

        // ShopImagesAndVideos_Upsert API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("ShopImagesAndVideos_Upsert")]
        //public async Task<IHttpActionResult> ShopImagesAndVideos_Upsert(ShopImagesAndVideos ShopImagesAndVideos)
        //{
        //    var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_Upsert(ShopImagesAndVideos);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_Upsert")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_Upsert() /*ShopImagesAndVideos request*/
        {
            //var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            ShopImagesAndVideos shopImagesAndVideos = new ShopImagesAndVideos();
            var httpRequest = HttpContext.Current.Request;
            shopImagesAndVideos.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            shopImagesAndVideos.ShopId = Convert.ToInt64(httpRequest.Params["ShopId"]);
            shopImagesAndVideos.IsVideo = Convert.ToBoolean(httpRequest.Params["IsVideo"]);
            shopImagesAndVideos.IsCoverImage = Convert.ToBoolean(httpRequest.Params["IsCoverImage"]);
            
            shopImagesAndVideos.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
            shopImagesAndVideos.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);
            shopImagesAndVideos.IsUploadedFromMobile = true;

            var uploadedImageUrls = new List<KeyValuePair<string, string>>();

            string videoPath = "";
            if (httpRequest.Files.Count > 0)
            {
                string basePath = "";
                string fileName = "";
                //int MegaBytes = 1024 * 1024 * 80;
                //int Seconds = 120;

                for (int i = 0; i < httpRequest.Files.Count; i++)
                {
                    var myFile = httpRequest.Files[i];
                    basePath = "MediaUrl/";

                    //Is the file too big to upload?
                    //if (myFile.ContentLength > MegaBytes)
                    //{
                    //    if (shopImagesAndVideos.IsVideo == true && shopImagesAndVideos.IsCoverImage == false)
                    //    {
                    //        return Json("Sorry!! Maximum allowed file size is 80 mb");
                    //    }
                    //}

                    // Check Video Second less then 2 Minutes
                    //WindowsMediaPlayer wmp = new WindowsMediaPlayer();
                    //IWMPMedia mediaInfo = wmp.newMedia(fileName);
                    //double duration = mediaInfo.duration;
                    //if (duration > Seconds)
                    //{
                    //    if (shopImagesAndVideos.IsVideo == true && shopImagesAndVideos.IsCoverImage == false)
                    //    {
                    //        return Json("Please upload video less then 2 minutes");
                    //    }
                    //}


                    fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                    if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                    }
                    myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                    videoPath = basePath + fileName;

                    abstractShopImagesAndVideosServices.S3FileUpload(HttpContext.Current.Server.MapPath("~/" + videoPath),
                        videoPath);

                    uploadedImageUrls.Add(new KeyValuePair<string, string>("MediaURL", videoPath));
                    shopImagesAndVideos.MediaUrl += basePath + fileName + ",";
                }
            }
            else
            {
                shopImagesAndVideos.MediaUrl = Convert.ToString(httpRequest.Params["MediaUrl"]);
            }
            if (shopImagesAndVideos.IsVideo == true && shopImagesAndVideos.IsCoverImage == false)
            {
                // for the sample video upload 
                string thumbnailPath = "ThumbnailImage/31012022104712_Capture.png";
                if (httpRequest.Files.Count > 0)
                {

                    //string thumbImage = generateThumb(videoPath);
                    //string thumbnailPath = HttpContext.Current.Server.MapPath(String.Format("ThumbnailImage/"));
                    //string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss");

                    //string thumbImage = GetThumbnailImage(videoPath, thumbnailPath, newFileName);
                    //if (thumbImage != string.Empty)
                    //{
                    //    imgThumbnail.Visible = true;
                    //    System.Threading.Thread.Sleep(2000);
                    //    imgThumbnail.ImageUrl = String.Format("~/UserAlbums/{0}", newFileName + ".jpg");
                    //}

                    //var myFile = httpRequest.Files[1];
                    //string basePath = "ThumbnailImage/";
                    //string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                    //if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                    //{
                    //    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                    //}
                    //myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                    //shopImagesAndVideos.ThumbnailImage = "ThumbnailImage/01022022050553_muzammil-soorma-KTdzeb28jyo-unsplash.jpg";

                    //abstractShopImagesAndVideosServices.S3FileUpload(HttpContext.Current.Server.MapPath("~/" + thumbImage), shopImagesAndVideos.ThumbnailImage);
                }
            }

                string jsonString = JsonConvert.SerializeObject(uploadedImageUrls);

            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_Upsert(shopImagesAndVideos);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopImagesAndVideos_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_All")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_All(PageParam pageParam, string search = "")
        {
            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopImagesAndVideos_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_ById")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_ById(long Id)
        {
            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopImagesAndVideos_IsVideo Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_IsVideo")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_IsVideo(long Id)
        {
            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_IsVideo(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        // ShopImagesAndVideos_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_ByShopId")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_ByShopId(PageParam pageParam, long ShopId)
        {
            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopImagesAndVideos_IsCoverImage API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_IsCoverImage")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_IsCoverImage(long Id, bool IsVideo)
        {
            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_IsCoverImage(Id, IsVideo);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopImagesAndVideos_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopImagesAndVideos_Delete")]
        public async Task<IHttpActionResult> ShopImagesAndVideos_Delete(long Id)
        {
            var quote = abstractShopImagesAndVideosServices.ShopImagesAndVideos_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //public string GetThumbnailImage(string videoPath, string thumbnailPath, string fileName)
        //{
        //    try
        //    {
        //        string thumb = thumbnailPath + fileName + ".jpg";

        //        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        //        proc.StartInfo.FileName = Convert.ToString(ConfigurationManager.AppSettings["FFMPEGExePath"]);
        //        proc.StartInfo.Arguments = " -i \"" + videoPath + "\" -f image2 -ss 0:0:1 -vframes 1 -s 150x150 -an \"" + thumb + "\"";
        //        //The command which will be executed
        //        proc.StartInfo.UseShellExecute = false;
        //        proc.StartInfo.CreateNoWindow = true;
        //        proc.StartInfo.RedirectStandardOutput = false;
        //        proc.Start();
        //        return thumb;
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}

        //public static string generateThumb(string file)
        //{
        //    string thumb = "";

        //    try
        //    {
        //        FileInfo fi = new FileInfo(HttpContext.Current.Server.MapPath(file));
        //        string filename = Path.GetFileNameWithoutExtension(fi.Name);
        //        Random random = new Random();
        //        int rand = random.Next(1, 9999999);
        //        string newfilename = "/ThumbnailImage/" + filename + "___(" + rand.ToString() + ").jpg";
        //        var processInfo = new ProcessStartInfo();
        //        processInfo.FileName = "\"" + HttpContext.Current.Server.MapPath("/ThumbnailImage/ffmpeg.exe") + "\"";
        //        processInfo.Arguments = string.Format("-ss {0} -i {1} -f image2 -vframes 1 -y {2}", 5, "\"" + HttpContext.Current.Server.MapPath(file) + "\"", "\"" + HttpContext.Current.Server.MapPath(newfilename) + "\"");
        //        processInfo.CreateNoWindow = true;
        //        processInfo.UseShellExecute = false;
        //        using (var process = new Process())
        //        {
        //            process.StartInfo = processInfo;
        //            process.Start();
        //            process.WaitForExit();
        //            thumb = newfilename;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string error = ex.Message;
        //    }

        //    return thumb;
        //}

        
    }

}
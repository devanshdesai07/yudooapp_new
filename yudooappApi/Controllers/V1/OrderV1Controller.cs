﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrderV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractOrderServices abstractOrderServices;
        #endregion

        #region Cnstr
        public OrderV1Controller(AbstractOrderServices abstractOrderServices)
        {
            this.abstractOrderServices = abstractOrderServices;
        }
        #endregion

        // Order_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Order_Upsert")]
        public async Task<IHttpActionResult> Order_Upsert(Order Order)
        {
            var quote = abstractOrderServices.Order_Upsert(Order);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Order_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Order_All")]
        public async Task<IHttpActionResult> Order_All(PageParam pageParam, string search = "")
        {
            var quote = abstractOrderServices.Order_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        // Order_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminOrderDetails_All")]
        public async Task<IHttpActionResult> AdminOrderDetails_All(PageParam pageParam, string search = "", long Id = 0, long MasterOrderStatusId = 0, string PaymentStatus = "", long ShopId = 0, string ShopName = "", long CityId = 0, string CustomerMobileNumber = "", string CustomerName = "", long Amount = 0)
        {
            var quote = abstractOrderServices.AdminOrderDetails_All(pageParam, search, Id, MasterOrderStatusId, PaymentStatus, ShopId, ShopName, CityId, CustomerMobileNumber, CustomerName, Amount);
            return this.Content((HttpStatusCode)200, quote);
        }


        //Order_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Order_ById")]
        public async Task<IHttpActionResult> Order_ById(long Id)
        {
            var quote = abstractOrderServices.Order_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //OrderDetail_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("OrderDetail_ById")]
        public async Task<IHttpActionResult> OrderDetail_ById(long OrderId)
        {
            var quote = abstractOrderServices.OrderDetail_ById(OrderId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserOrder_OrderDetails API
        [System.Web.Http.HttpPost]
        [InheritedRoute("User_OrderDetails")]
        public async Task<IHttpActionResult> User_OrderDetails(long Id)
        {
            var quote = abstractOrderServices.User_OrderDetails(Id);

            if (!string.IsNullOrEmpty(Convert.ToString(quote.Item.ProductDetails)))
                quote.Item.ProductDetails = JsonConvert.DeserializeObject(Convert.ToString(quote.Item.ProductDetails));

            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractBankMasterServices abstractBankMasterServices;
        #endregion

        #region Cnstr
        public BankMasterV1Controller(AbstractBankMasterServices abstractBankMasterServices)
        {
            this.abstractBankMasterServices = abstractBankMasterServices;
        }
        #endregion

        // BankMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("BankMaster_Upsert")]
        public async Task<IHttpActionResult> BankMaster_Upsert(BankMaster BankMaster)
        {
            var quote = abstractBankMasterServices.BankMaster_Upsert(BankMaster);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //BankMaster_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("BankMaster_ById")]
        public async Task<IHttpActionResult> BankMaster_ById(long Id)
        {
            var quote = abstractBankMasterServices.BankMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //BankMaster_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("BankMaster_ByShopId")]
        public async Task<IHttpActionResult> BankMaster_ByShopId(PageParam pageParam, long ShopId)
        {
            var quote = abstractBankMasterServices.BankMaster_ByShopId(pageParam, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //BankMaster_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("BankMaster_ActInAct")]
        public async Task<IHttpActionResult> BankMaster_ActInAct(long Id)
        {
            var quote = abstractBankMasterServices.BankMaster_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using System.Security.Claims;
using System.Diagnostics;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserOrderItemsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserOrderItemsServices abstractUserOrderItemsServices;
        #endregion

        #region Cnstr
        public UserOrderItemsV1Controller(AbstractUserOrderItemsServices abstractUserOrderItemsServices)
        {
            this.abstractUserOrderItemsServices = abstractUserOrderItemsServices;
        }
        #endregion

        //// UserOrderItems_Upsert API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("UserOrderItems_Upsert")]
        //public async Task<IHttpActionResult> UserOrderItems_Upsert(UserOrderItems UserOrderItems)
        //{
        //    var quote = abstractUserOrderItemsServices.UserOrderItems_Upsert(UserOrderItems);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderItems_Upsert")]
        public async Task<IHttpActionResult> UserOrderItems_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            UserOrderItems userOrderItems = new UserOrderItems();
            var httpRequest = HttpContext.Current.Request;
            userOrderItems.Id = Convert.ToInt64(httpRequest.Params["Id"]);
            userOrderItems.OrderId = Convert.ToInt64(httpRequest.Params["OrderId"]);
            userOrderItems.Qty = Convert.ToInt64(httpRequest.Params["Qty"]);
            userOrderItems.Price = Convert.ToDecimal(httpRequest.Params["Price"]);
            userOrderItems.Size = Convert.ToString(httpRequest.Params["Size"]);
            userOrderItems.ItemName = Convert.ToString(httpRequest.Params["ItemName"]);
            userOrderItems.ProductInfo = Convert.ToString(httpRequest.Params["ProductInfo"]);
            userOrderItems.CreatedBy = Convert.ToInt64(httpRequest.Params["CreatedBy"]);
            userOrderItems.UpdatedBy = Convert.ToInt64(httpRequest.Params["UpdatedBy"]);


            //string videoPath = "";
            if (httpRequest.Files.Count > 0)
            {
                string basePath = "";
                string fileName = "";

                var myFile = httpRequest.Files[0];
                basePath = "ProductImage/";
                fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                //videoPath = basePath + fileName;
                userOrderItems.ProductImage = basePath + fileName;
                abstractUserOrderItemsServices.S3FileUpload(HttpContext.Current.Server.MapPath("~/" + basePath + fileName), userOrderItems.ProductImage);
            }
            //if (userOrderItems.IsVideo == true && shopImagesAndVideos.IsCoverImage == false)
            //{
            //    // for the sample video upload 
            //    string productimage = "ProductImage/31012022104712_Capture.png";
            //    if (httpRequest.Files.Count > 0)
            //    {

            //        //string thumbImage = generateThumb(videoPath);
            //        //string thumbnailPath = HttpContext.Current.Server.MapPath(String.Format("ThumbnailImage/"));
            //        //string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss");

            //        //string thumbImage = GetThumbnailImage(videoPath, thumbnailPath, newFileName);
            //        //if (thumbImage != string.Empty)
            //        //{
            //        //    imgThumbnail.Visible = true;
            //        //    System.Threading.Thread.Sleep(2000);
            //        //    imgThumbnail.ImageUrl = String.Format("~/UserAlbums/{0}", newFileName + ".jpg");
            //        //}

            //        //var myFile = httpRequest.Files[1];
            //        //string basePath = "ThumbnailImage/";
            //        //string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
            //        //if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
            //        //{
            //        //    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
            //        //}
            //        //myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
            //        //shopImagesAndVideos.ThumbnailImage = "ThumbnailImage/01022022050553_muzammil-soorma-KTdzeb28jyo-unsplash.jpg";

            //        //abstractShopImagesAndVideosServices.S3FileUpload(HttpContext.Current.Server.MapPath("~/" + thumbImage), shopImagesAndVideos.ThumbnailImage);
            //    }

            //    userOrderItems.ProductImage = productimage;

            //    abstractUserOrderItemsServices.S3FileUpload(HttpContext.Current.Server.MapPath("~/" + productimage), userOrderItems.ProductImage);

            //}


            var quote = abstractUserOrderItemsServices.UserOrderItems_Upsert(userOrderItems);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }








        // UserOrderItems_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderItems_All")]
        public async Task<IHttpActionResult> UserOrderItems_All(PageParam pageParam, string search = "")
        {
            var quote = abstractUserOrderItemsServices.UserOrderItems_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
        // UserOrderItems_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderItems_ByOrderId")]
        public async Task<IHttpActionResult> UserOrderItems_ByOrderId(PageParam pageParam, long OrderId = 0)
        {
            var quote = abstractUserOrderItemsServices.UserOrderItems_ByOrderId(pageParam, OrderId);
            return this.Content((HttpStatusCode)200, quote);
        }
        // UserOrderItems_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderItems_StatusChange")]
        public async Task<IHttpActionResult> UserOrderItems_StatusChange(long Id,long ItemStatusId,long UpdatedBy)
        {
            var quote = abstractUserOrderItemsServices.UserOrderItems_StatusChange(Id,ItemStatusId,UpdatedBy);
            return this.Content((HttpStatusCode)200, quote);
        }
        //UserOrderItems_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderItems_ById")]
        public async Task<IHttpActionResult> UserOrderItems_ById(long Id)
        {
            var quote = abstractUserOrderItemsServices.UserOrderItems_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

       

        //UserOrderItems_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrderItems_Delete")]
        public async Task<IHttpActionResult> UserOrderItems_Delete(long Id, long DeletedBy)
        {
            var quote = abstractUserOrderItemsServices.UserOrderItems_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
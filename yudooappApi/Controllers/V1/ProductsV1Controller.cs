﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractProductsServices abstractProductsServices;
        #endregion

        #region Cnstr
        public ProductsV1Controller(AbstractProductsServices abstractProductsServices)
        {
            this.abstractProductsServices = abstractProductsServices;
        }
        #endregion

       
        //Products_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Products_Delete")]
        public async Task<IHttpActionResult> Products_Delete(long Id)
        {
            var quote = abstractProductsServices.Products_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Products_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Products_ActInAct")]
        public async Task<IHttpActionResult> Products_ActInAct(long Id)
        {
            var quote = abstractProductsServices.Products_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Products_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Products_ById")]
        public async Task<IHttpActionResult> Products_ById(long Id)
        {
            var quote = abstractProductsServices.Products_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //Products_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Products_Upsert")]
        public async Task<IHttpActionResult> Products_Upsert(Products products)
        {
            var quote = abstractProductsServices.Products_Upsert(products);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Products_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Products_All")]
        public async Task<IHttpActionResult> Products_All(PageParam pageParam, string search = "")
        {
            AbstractProducts Products = new Products();
            var quote = abstractProductsServices.Products_All(pageParam, search, Products);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}

﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopKeeperMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopKeeperMasterServices abstractShopKeeperMasterServices;
        #endregion

        #region Cnstr
        public ShopKeeperMasterV1Controller(AbstractShopKeeperMasterServices abstractShopKeeperMasterServices)
        {
            this.abstractShopKeeperMasterServices = abstractShopKeeperMasterServices;
        }
        #endregion
        //ShopKeeperMaster_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopKeeperMaster_ById")]
        public async Task<IHttpActionResult> ShopKeeperMaster_ById(long Id)
        {
            var quote = abstractShopKeeperMasterServices.ShopKeeperMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using System.Configuration;
using Newtonsoft.Json;
using GoogleMaps.LocationServices;
using yudooappApi.Helper;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopOwnerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        #endregion

        #region Cnstr
        public ShopOwnerV1Controller(AbstractShopOwnerServices abstractShopOwnerServices)
        {
            this.abstractShopOwnerServices = abstractShopOwnerServices;
        }
        #endregion


        // ShopOwner_Upsert API
        //https://github.com/sethwebster/GoogleMaps.LocationServices
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_Upsert")]
        public async Task<IHttpActionResult> ShopOwner_Upsert(ShopOwner shopOwner)
        {
            var gls = new GoogleLocationService("AIzaSyABofRnRkpT_4m344ZOsyRnT90uSGF5H6A&amp");
            if (shopOwner.AddressLine1 != null && shopOwner.AddressLine1 != "")
            {
                var latlong = gls.GetLatLongFromAddress(shopOwner.AddressLine1 + " " + shopOwner.AddressLine2);
                shopOwner.GpsLatitude = ConvertTo.String(latlong.Latitude);
                shopOwner.GpsLongitude = ConvertTo.String(latlong.Longitude);
            }

            var quote = abstractShopOwnerServices.ShopOwner_Upsert(shopOwner);

            if(quote.Code == 200)
            {
                if (quote.Item.AddressLine1 != null)
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    CreatePickupLocation createPickupLocation = new CreatePickupLocation();
                    createPickupLocation.pickup_location = ConvertTo.String(quote.Item.Id);
                    createPickupLocation.name = quote.Item.ShopOwnerName;
                    createPickupLocation.phone = quote.Item.MobileNumber;
                    createPickupLocation.address = quote.Item.AddressLine1;
                    createPickupLocation.address_2 = "";
                    createPickupLocation.city = quote.Item.AddressLine2;
                    createPickupLocation.state = quote.Item.LandMark;
                    createPickupLocation.pin_code = ConvertTo.String(quote.Item.PinCode);
                    createPickupLocation.country = "India";
                    createPickupLocation.email = "test@test.com";
                    using (var wc = new HttpClient())
                    {
                        string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

                        SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                        SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                        DataTable dt = new DataTable();
                        sda.Fill(dt);

                        wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                        var getPickupPath = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup";
                        var path = "https://apiv2.shiprocket.in/v1/external/settings/company/addpickup";

                        var getPickResp = await wc.GetStringAsync(new Uri(getPickupPath));

                        GetPickUp getPickUp = new GetPickUp();
                        getPickUp = JsonConvert.DeserializeObject<GetPickUp>(getPickResp);
                        bool containsItem = getPickUp.data.shipping_address.Any(item => item.pickup_location == ConvertTo.String(quote.Item.Id));
                        if (!containsItem)
                        {

                            var s = JsonConvert.SerializeObject(createPickupLocation);
                            var content = new StringContent(JsonConvert.SerializeObject(createPickupLocation), Encoding.UTF8, "application/json");
                            var result = await wc.PostAsync(new Uri(path), content);
                            string resultContent = await result.Content.ReadAsStringAsync();
                            var res = JsonConvert.DeserializeObject<Pickup>(resultContent);

                            var quote3 = abstractShopOwnerServices.AddShopOwnerPickUpId(quote.Item.Id, Convert.ToString(res.pickup_id));
                        }
                            //List<CreateShipmentResData> createShipmentResDatas = new List<CreateShipmentResData>();
                        //createShipmentResDatas.Add(res.data.data[1]);
                        //res.data.data = createShipmentResDatas;

                    }
                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopOwner_UpdateCommision  API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_UpdateCommision")]
        public async Task<IHttpActionResult> ShopOwner_UpdateCommision(long Id, decimal YudooCommission)
        {
            var quote = abstractShopOwnerServices.ShopOwner_UpdateCommision(Id, YudooCommission);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopOwner_ShopEmployeeActInAct  API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_ShopEmployeeActInAct")]
        public async Task<IHttpActionResult> ShopOwner_ShopEmployeeActInAct(bool IsActive,long ShopOwnerId = 0, long UserId = 0)
        {
            var quote = abstractShopOwnerServices.ShopOwner_ShopEmployeeActInAct(IsActive,ShopOwnerId, UserId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopOwner_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_All")]
        public async Task<IHttpActionResult> ShopOwner_All(PageParam pageParam, string search = "", long CityId = 0, long SpecialityId = 0, long UserId = 0,long CuratedListId = 0,long ShopId = 0)
        {
            PagedList<AbstractShopOwner> quote = abstractShopOwnerServices.ShopOwner_All(pageParam, search, CityId, SpecialityId, UserId, CuratedListId, ShopId);

            if (quote != null && quote.Values.Count > 0)
            {
                foreach (var owner in quote.Values)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(owner.Timing)))
                        owner.Timing = JsonConvert.DeserializeObject(Convert.ToString(owner.Timing));

                    owner.Tags = "";

                }
            }

            

            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopOwner_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_ById")]
        public async Task<IHttpActionResult> ShopOwner_ById(long Id, long UserId = 0)
        {

            SuccessResult<AbstractShopOwner> quote = abstractShopOwnerServices.ShopOwner_ById(Id, UserId);

            //Root shopTimeList = new Root();
            //if (!string.IsNullOrEmpty(quote.Item.Timing))
            //{
            //    string _searilzeObj = JsonConvert.SerializeObject(quote, new JsonSerializerSettings() { ContractResolver = new IgnorePropertiesResolver(new[] { "Timing" }) });
            //    var _root = JsonConvert.DeserializeObject<Root>(_searilzeObj);
            //    _root.Timing = JsonConvert.DeserializeObject<List<Timging>>(quote.Item.Timing); //over ride timing property
            //    shopTimeList = _root;
            //}
            //return this.Content((HttpStatusCode)quote.Code, shopTimeList);

            if (quote != null && quote.Item != null)
            {
                var owner = quote.Item;
                if (!string.IsNullOrEmpty(Convert.ToString(owner.Timing)))
                    owner.Timing = JsonConvert.DeserializeObject(Convert.ToString(owner.Timing));
                owner.Tags = "";
            }

            return this.Content((HttpStatusCode)200, quote);
        }
        //ShopOwner_TagsByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_TagsByShopId")]
        public async Task<IHttpActionResult> ShopOwner_TagsByShopId(long ShopId)
        {
            var quote = abstractShopOwnerServices.ShopOwner_TagsByShopId(ShopId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //ShopOwner_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_ActInAct")]
        public async Task<IHttpActionResult> ShopOwner_ActInAct(long Id)
        {
            var quote = abstractShopOwnerServices.ShopOwner_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopOwner_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_Delete")]
        public async Task<IHttpActionResult> ShopOwner_Delete(long Id, long DeletedBy)
        {
            var quote = abstractShopOwnerServices.ShopOwner_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //CheckSeller_Exists API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CheckSeller_Exists")]
        public async Task<IHttpActionResult> CheckSeller_Exists(string ShopOwnerName, string ShopName, string MobileNumber)
        {
            var quote = abstractShopOwnerServices.CheckSeller_Exists(ShopOwnerName, ShopName, MobileNumber);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //CheckSeller_Exists_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CheckSeller_Exists_ById")]
        public async Task<IHttpActionResult> CheckSeller_Exists_ById(long Id)
        {
            var quote = abstractShopOwnerServices.CheckSeller_Exists_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //CheckSeller_Exists_ByMobileNumberAndById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CheckSeller_Exists_ByMobileNumberAndById")]
        public async Task<IHttpActionResult> CheckSeller_Exists_ByMobileNumberAndById(long Id = 0, string Mobilenumber = "", string DeviceToken = "")
        {
            var quote = abstractShopOwnerServices.CheckSeller_Exists_ByMobileNumberAndById(Id, Mobilenumber, DeviceToken);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //ShopOwner_TagsUpdate API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_TagsUpdate")]
        public async Task<IHttpActionResult> ShopOwner_TagsUpdate(long Id, string Tags)
        {
            var quote = abstractShopOwnerServices.ShopOwner_TagsUpdate(Id, Tags);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //ShopOwner_Logout API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_Logout")]
        public async Task<IHttpActionResult> ShopOwner_Logout(long Id)
        {
            var quote = abstractShopOwnerServices.ShopOwner_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpGet]
        [InheritedRoute("GetGSTInfo")]
        public async Task<IHttpActionResult> GetGSTInfo(string gstnumber)
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string apiUrl = Convert.ToString(ConfigurationManager.AppSettings["gstapi"]);
            apiUrl = apiUrl.Replace("#gstnum#", gstnumber);

            GSTInfo message = new GSTInfo();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    message = JsonConvert.DeserializeObject<GSTInfo>(response);

                }
            }
            catch (Exception e)
            {
                return this.Content((HttpStatusCode)HttpStatusCode.BadGateway, e.Message + " "+ e.StackTrace);
            }

            return this.Content((HttpStatusCode)HttpStatusCode.OK, message);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_SendOTP")]
        public async Task<IHttpActionResult> ShopOwner_SendOTP(long Id, string mobilenumber, string DeviceToken)
        {
            string otp = new Random().Next(1111, 9999).ToString("D4");
            // HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", mobilenumber).Replace("#otp#", otp).ToString());
            // request.Method = "GET";
            // request.ContentType = "application/json";

            try
            {
                //WebResponse webResponse = request.GetResponse();
                //using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                //using (StreamReader responseReader = new StreamReader(webStream))
                //{
                //    string response = responseReader.ReadToEnd();
                //}

                if (mobilenumber == "1234512345" && Id == 9630174)
                {
                    otp = "9999";
                }

                string url = HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]);
                url = url.Replace("#mobile#", mobilenumber).Replace("#otp#", otp).ToString();

                HttpClient client = new HttpClient();
                var resp = await client.GetAsync(url,
                    HttpCompletionOption.ResponseHeadersRead);

                if (resp.IsSuccessStatusCode)
                {
                    //string content = await responseTask.Content.ReadAsStringAsync();
                    //string Msg = JsonConvert.DeserializeObject<string>(content);
                    //response = JsonConvert.DeserializeObject<ResponseJsonModel>(Msg);
                }


            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            var quote = abstractShopOwnerServices.CheckSeller_Exists_ByMobileNumberAndById(Id, mobilenumber, DeviceToken);

            if (quote.Item == null)
            {
                quote.Item = new ShopOwner();
                quote.Item.OTP = Convert.ToInt64(otp);
            }
            else
            {
                quote.Item.OTP = Convert.ToInt64(otp);
            }

            return this.Content((HttpStatusCode)quote.Code, quote);

            //if (quote.Code == 200)
            //{
            //    quote.Item.OTP = Convert.ToInt32(otp);
            //    return this.Content((HttpStatusCode)HttpStatusCode.OK, quote);
            //}
            //else
            //{
            //    quote.Item.OTP = Convert.ToInt32(otp);
            //    return this.Content((HttpStatusCode)HttpStatusCode.OK, quote);
            //}
            //return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);

            //var quote = abstractShopOwnerServices.ShopOwner_SendOTP(mobilenumber, Convert.ToInt32(otp));
            //return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //// bogus API
        //[System.Web.Http.HttpGet]
        //[InheritedRoute("Users_SendOTP")]
        //public async Task<IHttpActionResult> Users_SendOTP(string mobilenumber)
        //{
        //    string otp = new Random().Next(1111, 9999).ToString("D4");
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", mobilenumber).Replace("#otp#", otp).ToString());
        //    request.Method = "GET";
        //    request.ContentType = "application/json";

        //    try
        //    {
        //        WebResponse webResponse = request.GetResponse();
        //        using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
        //        using (StreamReader responseReader = new StreamReader(webStream))
        //        {
        //            string response = responseReader.ReadToEnd();

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.Out.WriteLine("-----------------");
        //        Console.Out.WriteLine(e.Message);
        //    }

        //    return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);
        //}

        //Users_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_VerifyOtp")]
        public async Task<IHttpActionResult> ShopOwner_VerifyOtp(string MobileNumber, long Otp)
        {
            var quote = abstractShopOwnerServices.ShopOwner_VerifyOtp(MobileNumber, Otp);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopOwner_Specialities_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_Specialities_All")]
        public async Task<IHttpActionResult> ShopOwner_Specialities_All(PageParam pageParam, long CityId, long SubCategoryId)
        {
            var quote = abstractShopOwnerServices.ShopOwner_Specialities_All(pageParam, CityId, SubCategoryId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // ShopEmployee_ByParentId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopEmployee_ByParentId")]
        public async Task<IHttpActionResult> ShopEmployee_ByParentId(PageParam pageParam, long ParentId)
        {
            var quote = abstractShopOwnerServices.ShopEmployee_ByParentId(pageParam, ParentId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopOwner_OnlineOffline Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_OnlineOffline")]
        public async Task<IHttpActionResult> ShopOwner_OnlineOffline(long Id, bool IsOnline)
        {
            var quote = abstractShopOwnerServices.ShopOwner_OnlineOffline(Id, IsOnline);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopOwner_IsApprovedByAdmin Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_IsApprovedByAdmin")]
        public async Task<IHttpActionResult> ShopOwner_IsApprovedByAdmin(long Id,long Status)
        {
            var quote = abstractShopOwnerServices.ShopOwner_IsApprovedByAdmin(Id,Status);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopOwnerRights_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwnerRights_Upsert")]
        public async Task<IHttpActionResult> ShopOwnerRights_Upsert(ShopOwnerRights shopOwnerRights)
        {
            var quote = abstractShopOwnerServices.ShopOwnerRights_Upsert(shopOwnerRights);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopAudit_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopAudit_Upsert")]
        public async Task<IHttpActionResult> ShopAudit_Upsert(ShopAudit shopAudit)
        {

            var shopData = abstractShopOwnerServices.ShopOwner_ById(shopAudit.Id,0).Item;
            var quote = abstractShopOwnerServices.ShopAudit_Upsert(shopAudit);

            //if (quote.Code == 200)
            //{
            //    if (quote.Item.AddressLine1 != null)
            //    {
            //        ServicePointManager.Expect100Continue = true;
            //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //        CreatePickupLocation createPickupLocation = new CreatePickupLocation();
            //        createPickupLocation.pickup_location = ConvertTo.String(quote.Item.Id);
            //        createPickupLocation.name = quote.Item.ShopOwnerName;
            //        createPickupLocation.phone = shopData.MobileNumber;
            //        createPickupLocation.address = quote.Item.AddressLine1;
            //        createPickupLocation.address_2 = "";
            //        createPickupLocation.city = shopData.AddressLine2;
            //        createPickupLocation.state = shopData.LandMark;
            //        createPickupLocation.pin_code = ConvertTo.String(quote.Item.PinCode);
            //        createPickupLocation.country = "India";
            //        createPickupLocation.email = "test@test.com";
            //        using (var wc = new HttpClient())
            //        {
            //            wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //            wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + Configurations.DeliveryToken);

            //            var getPickupPath = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup";
            //            var path = "https://apiv2.shiprocket.in/v1/external/settings/company/addpickup";

            //            var getPickResp = await wc.GetStringAsync(new Uri(getPickupPath));

            //            GetPickUp getPickUp = new GetPickUp();
            //            getPickUp = JsonConvert.DeserializeObject<GetPickUp>(getPickResp);
            //            bool containsItem = getPickUp.data.shipping_address.Any(item => item.pickup_location == ConvertTo.String(quote.Item.Id));
            //            if (!containsItem)
            //            {

            //                var s = JsonConvert.SerializeObject(createPickupLocation);
            //                var content = new StringContent(JsonConvert.SerializeObject(createPickupLocation), Encoding.UTF8, "application/json");
            //                var result = await wc.PostAsync(new Uri(path), content);
            //                string resultContent = await result.Content.ReadAsStringAsync();
            //                var res = JsonConvert.DeserializeObject<Pickup>(resultContent);

            //                var quote3 = abstractShopOwnerServices.AddShopOwnerPickUpId(shopData.Id, Convert.ToString(res.pickup_id));
            //            }
            //            //List<CreateShipmentResData> createShipmentResDatas = new List<CreateShipmentResData>();
            //            //createShipmentResDatas.Add(res.data.data[1]);
            //            //res.data.data = createShipmentResDatas;

            //        }
            //    }
            //}

            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopAudit_ShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopAudit_ShopId")]
        public async Task<IHttpActionResult> ShopAudit_ShopId(long ShopId)
        {
            var quote = abstractShopOwnerServices.ShopAudit_ShopId(ShopId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopOwnerRights_BySOU  API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwnerRights_BySOU")]
        public async Task<IHttpActionResult> ShopOwnerRights_BySOU(long ShopId, long UserId)
        {
            var quote = abstractShopOwnerServices.ShopOwnerRights_BySOU(ShopId, UserId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // ShopOwner_ByCallLog API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_ByCallLog")]
        public async Task<IHttpActionResult> ShopOwner_ByCallLog(PageParam pageParam, string search = "", long UserId = 0)
        {
            PagedList<AbstractShopOwner> quote = abstractShopOwnerServices.ShopOwner_ByCallLog(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Chunk Upload Code


        [System.Web.Http.HttpPost]
        [InheritedRoute("GetPresginedURLUpload")]
        public RawJsonActionResult GetPresginedURLUpload([FromBody] dynamic policySTring)
        {
            // Declare Var
            string id = string.Empty; string S3Key = string.Empty; string fileURL = string.Empty; string bucketName = string.Empty; string fileName = string.Empty;
            List<Dictionary<string, string>> listObj = new List<Dictionary<string, string>>();


            if (policySTring.headers == null)
            {       // Convert Dynamic object to list of string
                foreach (var item in policySTring.conditions)
                {
                    try
                    {
                        listObj.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(Convert.ToString(item)));
                    }
                    catch (Exception)
                    { }
                }

                // read var from policy
                foreach (var item in listObj)
                {
                    if (item.Keys.ToList()[0].ToString() == "bucket")
                    {
                        bucketName = item.Values.ToList()[0].ToString();
                    }

                    if (item.Keys.ToList()[0].ToString() == "key")
                    {
                        S3Key = item.Values.ToList()[0].ToString();
                        // todo - Update File Success Status in DB where FIleName = Key
                    }

                    if (item.Keys.ToList()[0].ToString() == "x-amz-meta-qqparentuuid")
                    {
                        fileURL = item.Values.ToList()[0].ToString();
                    }

                    if (item.Keys.ToList()[0].ToString() == "x-amz-meta-customid")
                    {
                        id = item.Values.ToList()[0].ToString();
                    }

                    if (item.Keys.ToList()[0].ToString() == "x-amz-meta-qqfilename")
                    {
                        fileName = item.Values.ToList()[0].ToString().Replace("%20(_imgthumb_)", "");
                    }
                }

                // change policy text key
                string policyText = Newtonsoft.Json.JsonConvert.SerializeObject(policySTring);
                // policyText = id == string.Empty ? policyText : policyText.Replace(thumbURL, id + "/" + thumbURL);

                // Add userId in key name - Prepend
                S3Key = bucketName + ".s3.amazonaws.com/" + S3Key;
                fileURL = bucketName + ".s3.amazonaws.com/" + id + "/" + fileURL + fileName.Substring(fileName.LastIndexOf('.'));

                // create Sing
                var sign = getSignatureKey("PhQF81aPJVc5zvQaagGmRfimcpn9accqttcV/ELo", DateTime.UtcNow.ToString("yyyyMMdd"), "ap-south-1", "s3");
                byte[] kSigning = HmacSHA256(Base64Encode(policyText), sign);

                return new RawJsonActionResult("{ \"policy\" : \"" + Base64Encode(policyText)
                 + "\", \"signature\" : \"" + ByteArrayToString(kSigning) + "\"}");
            }
            else
            {

                // create Sing
                string policyText = policySTring.headers;//.Replace("POST\\n/", "POST\\n/touchlynk%2F").Replace("host:s3.amazonaws.com/touchlynk", "host:s3.amazonaws.com");

                string[] splitArray = new string[1]; splitArray[0] = "aws4_request\n";
                string canonicalRequest = policyText.Split(splitArray, StringSplitOptions.None)[1];


                //canonicalRequest = "GET\n/api\nAction=UrlInfo&ResponseGroup=Rank&Url=cnn.com\nhost:awis.us-west-1.amazonaws.com\nx-amz-date:20171113T030840Z\n\nhost;x-amz-date\ne3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

                //string hashedCanonicalRequest12 = stringtoHexString(sha256(canonicalRequest));
                //string hashedCanonicalRequest = ByteArrayToString(Encoding.UTF8.GetBytes(ComputeSha256Hash(canonicalRequest)));

                string hashedCanonicalRequest = ByteArrayToString(GetHash256(canonicalRequest)); //ComputeSha256Hash(canonicalRequest);
                string hashedCanonicalRequestNew = Hash(Encoding.UTF8.GetBytes(canonicalRequest.ToString()));

                string stringToSign = policyText.Replace(canonicalRequest, hashedCanonicalRequest);

                var sign = getSignatureKey("PhQF81aPJVc5zvQaagGmRfimcpn9accqttcV/ELo", DateTime.UtcNow.ToString("yyyyMMdd"), "ap-south-1", "s3");

                byte[] kSigning = HmacSHA256(stringToSign, sign);

                string responseNew = ToHexString(HmacSha256New(sign, stringToSign.ToString()));

                string simple12 = ByteArrayToString(kSigning).ToLower();
                string simple123 = ByteArrayToStringSoapHexBinary(kSigning).ToLower();

                return new RawJsonActionResult("{ \"signature\" : \"" + simple12 + "\"}");

            }
        }

        public class RawJsonActionResult : IHttpActionResult
        {
            private readonly string _jsonString;

            public RawJsonActionResult(string jsonString)
            {
                _jsonString = jsonString;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                var content = new StringContent(_jsonString);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = new HttpResponseMessage(HttpStatusCode.OK) { Content = content };
                return Task.FromResult(response);
            }
        }

        static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName)
        {
            byte[] kSecret = Encoding.UTF8.GetBytes(("AWS4" + key).ToCharArray());

            byte[] kDate = HmacSHA256(dateStamp, kSecret);
            byte[] kRegion = HmacSHA256(regionName, kDate);
            byte[] kService = HmacSHA256(serviceName, kRegion);
            byte[] kSigning = HmacSHA256("aws4_request", kService);

            return kSigning;
        }

        private static byte[] HmacSha256New(byte[] key, string data)
        {
            return new HMACSHA256(key).ComputeHash(Encoding.UTF8.GetBytes(data));
        }

        static byte[] HmacSHA256(String data, byte[] key)
        {
            String algorithm = "HmacSHA256";
            KeyedHashAlgorithm kha = KeyedHashAlgorithm.Create(algorithm);
            kha.Key = key;
            return kha.ComputeHash(Encoding.UTF8.GetBytes(data));
            // return new HMACSHA256(key).ComputeHash(Encoding.UTF8.GetBytes(data));
        }

        public string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static byte[] GetHash256(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.ASCII.GetBytes(inputString));
        }

        public static string Hash(byte[] bytesToHash)
        {
            return ToHexString(SHA256.Create().ComputeHash(bytesToHash));
        }

        private static string ToHexString(IReadOnlyCollection<byte> array)
        {
            var hex = new StringBuilder(array.Count * 2);
            foreach (var b in array)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }

        public static string ByteArrayToStringSoapHexBinary(byte[] ba)
        {
            SoapHexBinary shb = new SoapHexBinary(ba);
            return shb.ToString();
        }
    }



    public class Addr
    {
        public string bnm { get; set; }
        public string st { get; set; }
        public string loc { get; set; }
        public string bno { get; set; }
        public string stcd { get; set; }
        public string dst { get; set; }
        public string city { get; set; }
        public string flno { get; set; }
        public string lt { get; set; }
        public string pncd { get; set; }
        public string lg { get; set; }
    }

    public class Pradr
    {
        public Addr addr { get; set; }
        public string ntr { get; set; }
    }

    public class TaxpayerInfo
    {
        public string stjCd { get; set; }
        public string lgnm { get; set; }
        public string stj { get; set; }
        public string dty { get; set; }
        public List<object> adadr { get; set; }
        public string cxdt { get; set; }
        public List<string> nba { get; set; }
        public string gstin { get; set; }
        public string lstupdt { get; set; }
        public string rgdt { get; set; }
        public string ctb { get; set; }
        public Pradr pradr { get; set; }
        public string tradeNam { get; set; }
        public string sts { get; set; }
        public string ctjCd { get; set; }
        public string ctj { get; set; }
        public string panNo { get; set; }
    }

    public class Compliance
    {
        public object filingFrequency { get; set; }
    }

    public class GSTInfo
    {
        public TaxpayerInfo taxpayerInfo { get; set; }
        public Compliance compliance { get; set; }
        public List<object> filing { get; set; }
    }

}
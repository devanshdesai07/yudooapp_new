﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CityV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCityServices abstractCityServices;
        #endregion

        #region Cnstr
        public CityV1Controller(AbstractCityServices abstractCityServices)
        {
            this.abstractCityServices = abstractCityServices;
        }
        #endregion



        // City_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("City_All")]
        public async Task<IHttpActionResult> City_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCityServices.City_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
        // City_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("City_Upsert")]
        public async Task<IHttpActionResult> City_Upsert(City City)
        {
            var quote = abstractCityServices.City_Upsert(City);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //City_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("City_ById")]
        public async Task<IHttpActionResult> City_ById(long Id)
        {
            var quote = abstractCityServices.City_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //City_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("City_Delete")]
        public async Task<IHttpActionResult> City_Delete(long Id)
        {
            var quote = abstractCityServices.City_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //City_ByStateId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("City_ByStateId")]
        public async Task<IHttpActionResult> City_ByStateId(PageParam pageParam, long StateId)
        {
            var quote = abstractCityServices.City_ByStateId(pageParam, StateId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
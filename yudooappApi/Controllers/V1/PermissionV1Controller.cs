﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PermissionV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPermissionServices abstractPermissionServices;
        #endregion

        #region Cnstr
        public PermissionV1Controller(AbstractPermissionServices abstractPermissionServices)
        {
            this.abstractPermissionServices = abstractPermissionServices;
        }
        #endregion

       

        //Permission_ByCustomerId Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Permission_ByCustomerId")]
        public async Task<IHttpActionResult> Permission_ByCustomerId(PageParam pageParam, string search = "",long CustomerId = 0)
        {
            AbstractPermission Permission = new Permission();
            Permission.CustomerId = CustomerId;
            var quote = abstractPermissionServices.Permission_ByCustomerId(pageParam, search, Permission);
            return this.Content((HttpStatusCode)200, quote);
        }
        //Permission_VerifyEmail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Permission_Upsert")]
        public async Task<IHttpActionResult> Permission_Upsert(Permission permission)
        {
            var quote = abstractPermissionServices.Permission_Upsert(permission);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}

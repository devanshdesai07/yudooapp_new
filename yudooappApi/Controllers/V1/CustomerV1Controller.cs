﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerServices abstractCustomerServices;
        #endregion

        #region Cnstr
        public CustomerV1Controller(AbstractCustomerServices abstractCustomerServices)
        {
            this.abstractCustomerServices = abstractCustomerServices;
        }
        #endregion

        // Customer_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_Upsert")]
        public async Task<IHttpActionResult> Customer_Upsert(Customer Customer)
        {
            var quote = abstractCustomerServices.Customer_Upsert(Customer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Customer_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_All")]
        public async Task<IHttpActionResult> Customer_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCustomerServices.Customer_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Customer_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_ById")]
        public async Task<IHttpActionResult> Customer_ById(long Id)
        {
            var quote = abstractCustomerServices.Customer_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Customer_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_ActInAct")]
        public async Task<IHttpActionResult> Customer_ActInact(long Id)
        {
            var quote = abstractCustomerServices.Customer_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Customer_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_Delete")]
        public async Task<IHttpActionResult> Customer_Delete(long Id, long DeletedBy)
        {
            var quote = abstractCustomerServices.Customer_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
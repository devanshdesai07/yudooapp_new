﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddressV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAddressServices abstractAddressServices;
        #endregion

        #region Cnstr
        public AddressV1Controller(AbstractAddressServices abstractAddressServices)
        {
            this.abstractAddressServices = abstractAddressServices;
        }
        #endregion

        // Address_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_Upsert")]
        public async Task<IHttpActionResult> Address_Upsert(Address Address)
        {
            var quote = abstractAddressServices.Address_Upsert(Address);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Address_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_All")]
        public async Task<IHttpActionResult> Address_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAddressServices.Address_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Address_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_ById")]
        public async Task<IHttpActionResult> Address_ById(long Id)
        {
            var quote = abstractAddressServices.Address_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Address_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_ByUserId")]
        public async Task<IHttpActionResult> Address_ByUserId(PageParam pageParam, long UserId= 0)
        {
            var quote = abstractAddressServices.Address_ByUserId(pageParam, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Address_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_ActInAct")]
        public async Task<IHttpActionResult> Address_ActInAct(long Id)
        {
            var quote = abstractAddressServices.Address_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Address_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Address_Delete")]
        public async Task<IHttpActionResult> Address_Delete(long Id)
        {
            var quote = abstractAddressServices.Address_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
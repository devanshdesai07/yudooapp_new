﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using RestSharp;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserOrderV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserOrderServices abstractUserOrderServices;
        private readonly AbstractUsersServices abstractUsersServices;
        #endregion

        #region Cnstr
        public UserOrderV1Controller(AbstractUserOrderServices abstractUserOrderServices, AbstractUsersServices abstractUsersServices)
        {
            this.abstractUserOrderServices = abstractUserOrderServices;
            this.abstractUsersServices = abstractUsersServices;
        }
        #endregion

        // UserOrder_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_Upsert")]
        public async Task<IHttpActionResult> UserOrder_Upsert(UserOrder UserOrder)
        {
            var quote = abstractUserOrderServices.UserOrder_Upsert(UserOrder);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // UserOrder_Upsert_paymentLink API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_Upsert_paymentLink")]
        public async Task<IHttpActionResult> UserOrder_Upsert_paymentLink(long Id = 0, string PaymentLink ="")
        {
            var quote = abstractUserOrderServices.UserOrder_Upsert_paymentLink(Id, PaymentLink);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserOrder_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_All")]
        public async Task<IHttpActionResult> UserOrder_All(PageParam pageParam, string search = "")
        {
            var quote = abstractUserOrderServices.UserOrder_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserOrder_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_ByShopId")]
        public async Task<IHttpActionResult> UserOrder_ByShopId(PageParam pageParam, string search = "", long Id = 0,string OrderStatus ="")
        {
            var quote = abstractUserOrderServices.UserOrder_ByShopId(pageParam, search, Id, OrderStatus);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserOrder_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_ById")]
        public async Task<IHttpActionResult> UserOrder_ById(long Id)
        {
            var quote = abstractUserOrderServices.UserOrder_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        


        // UserOrder_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_ByUserId")]
        public async Task<IHttpActionResult> UserOrder_ByUserId(PageParam pageParam, long UserId =0,long IsPendingPayment = 0,long IsPlaced = 0)
        {
            var quote = abstractUserOrderServices.UserOrder_ByUserId(pageParam, UserId, IsPendingPayment, IsPlaced);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserOrder_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_Delete")]
        public async Task<IHttpActionResult> UserOrder_Delete(long Id)
        {
            var quote = abstractUserOrderServices.UserOrder_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //UserOrder_StatusChange API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_StatusChange")]
        public async Task<IHttpActionResult> UserOrder_StatusChange(long Id, long OrderStatus)
        {
            var quote = abstractUserOrderServices.UserOrder_StatusChange(Id,OrderStatus);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserOrder_DeliveryRatingUpdate API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_DeliveryRatingUpdate")]
        public async Task<IHttpActionResult> UserOrder_DeliveryRatingUpdate(long Id, long DeliveryRating)
        {
            var quote = abstractUserOrderServices.UserOrder_DeliveryRatingUpdate(Id, DeliveryRating);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserOrder_PaymentOnline Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserOrder_PaymentOnline")]
        public async Task<IHttpActionResult> UserOrder_PaymentOnline(long UserId = 0, long OrderId = 0)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;



            var quote  = abstractUserOrderServices.UserOrder_ById(OrderId);

            var quotes = abstractUsersServices.Users_ById(UserId);

            RootResponce myDeserializedClass = null;

            try
            {
                RootReq root = new RootReq();
                root.customer = new Customer();
                root.notify = new Notify();
                root.notes = new Notes();

                root.amount = Convert.ToInt32(quote.Item.AmountPaid) * 100;
                root.currency = "INR";
                root.accept_partial = false;
                //root.expire_by = (int)DateTime.UtcNow.Subtract(new DateTime().AddMonths(1)).TotalSeconds;
                root.reference_id = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss") + "ORD" + OrderId;
                root.description = "Test payment";
                root.customer.name = quotes.Item.FirstName;
                root.customer.contact = "+91" + Convert.ToString(quotes.Item.MobileNumber);
                root.customer.email = quotes.Item.Email;
                root.customer.mobile = Convert.ToString(quotes.Item.MobileNumber);
                root.notify.sms = false;
                root.notify.email = false;
                root.reminder_enable = true;
                root.notes.policy_name = "Test policy";
                root.callback_url = Configurations.ClientURL + "api/userOrder/UserOrder_PaymentResponce/";
                root.callback_method = "get";
                using (var wc = new HttpClient())
                {
                    //wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    wc.DefaultRequestHeaders.Add("x-api-key", Configurations.RazorPayKey);

                    wc.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic ", Configurations.RazorPayToken);

                    var path = "https://api.razorpay.com/v1/payment_links";

                    var content = new StringContent(JsonConvert.SerializeObject(root), Encoding.UTF8, "application/json");
                    var result = await wc.PostAsync(new Uri(path), content);
                    string resultContent = await result.Content.ReadAsStringAsync();
                    myDeserializedClass = JsonConvert.DeserializeObject<RootResponce>(resultContent);
                    if (myDeserializedClass != null && myDeserializedClass.amount > 0)
                    {
                        UserOrder_Upsert_paymentLink(OrderId, myDeserializedClass.short_url);
                    }
                }

                //var client = new RestClient("https://api.razorpay.com/v1/payment_links");
                //client.Timeout = -1;
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("Authorization", Configurations.RazorPayToken);
                //request.AddHeader("x-api-key", Configurations.RazorPayKey);
                //request.AddHeader("Content-Type", "application/json");
                //var body = JsonConvert.SerializeObject(root);
                //request.AddParameter("application/json", body, ParameterType.RequestBody);
                //IRestResponse response = client.Execute(request);

                //myDeserializedClass = JsonConvert.DeserializeObject<RootResponce>(response.Content);

                //if (myDeserializedClass != null && myDeserializedClass.amount > 0)
                //{
                //    UserOrder_Upsert_paymentLink(OrderId, myDeserializedClass.short_url);
                //}



            }
            catch (Exception ex)
            {

                throw ex;
            }
            return this.Content((HttpStatusCode)200, myDeserializedClass);
        }



        //UserOrder_PaymentResponce API
        [System.Web.Http.HttpGet]
        [InheritedRoute("UserOrder_PaymentResponce")]
        public async Task<IHttpActionResult> UserOrder_PaymentResponce()
        {
            string queryString = HttpContext.Current.Request.QueryString.ToString();
            string decoded = System.Web.HttpUtility.UrlDecode(queryString);
            string razorpay_payment_link_status = Regex.Match(decoded, "razorpay_payment_link_status=([^&]+)").Groups[1].Value;
            string razorpay_payment_link_reference_id = Regex.Match(decoded, "razorpay_payment_link_reference_id=([^&]+)").Groups[1].Value;

            long Id = 0, OrderStatus = 0;
            if (razorpay_payment_link_status != null && razorpay_payment_link_reference_id != null)
            {
                Id = Convert.ToInt64(razorpay_payment_link_reference_id.Replace("ORD",""));
                if (razorpay_payment_link_status == "paid")
                {
                    OrderStatus = 4;
                }
            }
            var quote = abstractUserOrderServices.UserOrder_StatusChange(Id, OrderStatus);

            if (razorpay_payment_link_status == "paid")
            {
                string url = Configurations.BaseUrl+ "Home/PaymentSuccess";

                System.Uri uri = new System.Uri(url);

                return Redirect(uri);
            }
            else
            {
                string url =  Configurations.BaseUrl + "Home/PaymentFaild";

                System.Uri uri = new System.Uri(url);

                return Redirect(uri);
            }
        }


        public class Customer
        {
            public string name { get; set; }
            public string contact { get; set; }
            public string email { get; set; }
            public string mobile { get; set; }
        }
        public class Notify
        {
            public bool sms { get; set; }
            public bool email { get; set; }
        }
        public class Notes
        {
            public string policy_name { get; set; }
        }
        public class RootReq
        {
            public int amount { get; set; }
            public string currency { get; set; }
            public bool accept_partial { get; set; }
            public int expire_by { get; set; }
            public string reference_id { get; set; }
            public string description { get; set; }
            public Customer customer { get; set; }
            public Notify notify { get; set; }
            public bool reminder_enable { get; set; }
            public Notes notes { get; set; }
            public string callback_url { get; set; }
            public string callback_method { get; set; }
        }
        public class RootResponce
        {
            public bool accept_partial { get; set; }
            public int amount { get; set; }
            public int amount_paid { get; set; }
            public string callback_method { get; set; }
            public string callback_url { get; set; }
            public int cancelled_at { get; set; }
            public int created_at { get; set; }
            public string currency { get; set; }
            public Customer customer { get; set; }
            public string description { get; set; }
            public int expire_by { get; set; }
            public int expired_at { get; set; }
            public int first_min_partial_amount { get; set; }
            public string id { get; set; }
            public Notes notes { get; set; }
            public Notify notify { get; set; }
            public object payments { get; set; }
            public string reference_id { get; set; }
            public bool reminder_enable { get; set; }
            public List<object> reminders { get; set; }
            public string short_url { get; set; }
            public string status { get; set; }
            public int updated_at { get; set; }
            public bool upi_link { get; set; }
            public string user_id { get; set; }
        }


    }
}
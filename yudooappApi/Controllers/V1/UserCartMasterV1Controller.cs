﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserCartMasterV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserCartMasterServices abstractUserCartMasterServices;
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        #endregion

        #region Cnstr
        public UserCartMasterV1Controller(AbstractUserCartMasterServices abstractUserCartMasterServices, AbstractShopOwnerServices abstractShopOwnerServices)
        {
            this.abstractUserCartMasterServices = abstractUserCartMasterServices;
            this.abstractShopOwnerServices = abstractShopOwnerServices;
        }
        #endregion

        // UserCartMaster_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCartMaster_Upsert")]
        public async Task<IHttpActionResult> UserCartMaster_Upsert(UserCartMaster userCartMaster)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            SuccessResult<AbstractUserCartMaster> SuccessResult = new SuccessResult<AbstractUserCartMaster>();

            if (userCartMaster.Pincode != "" && userCartMaster.Pincode != null)
            {
                using (var wc = new HttpClient())
                {
                    List<PincodeRootArray> pincodeRootArray = new List<PincodeRootArray>();

                    wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var path = "https://api.postalpincode.in/pincode/" + userCartMaster.Pincode;
                    var AddressrResult = await wc.GetStringAsync(new Uri(path));

                    object AddressResult = "";
                    pincodeRootArray = JsonConvert.DeserializeObject<List<PincodeRootArray>>(Convert.ToString(AddressrResult));

                    if (pincodeRootArray[0].Status == "Success")
                    {
                        userCartMaster.Country = Convert.ToString(pincodeRootArray[0].PostOffice[0].Country);
                        userCartMaster.State = Convert.ToString(pincodeRootArray[0].PostOffice[0].State);
                        userCartMaster.City = Convert.ToString(pincodeRootArray[0].PostOffice[0].District);
                    }
                    else if (pincodeRootArray[0].Status == "Error")
                    {
                        SuccessResult.Code = 400;
                        SuccessResult.Message = "Pincode is invalid";
                        return this.Content((HttpStatusCode)SuccessResult.Code, SuccessResult);
                    }
                }
            }
            else
            {
                SuccessResult.Code = 400;
                SuccessResult.Message = "Pincode is required";
                return this.Content((HttpStatusCode)SuccessResult.Code, SuccessResult);
            }
            //if (userCartMaster.Address != null)
            //{
            //    var shopData = abstractShopOwnerServices.ShopOwner_ById(userCartMaster.ShopId, 0).Item;

               
            //    CreatePickupLocation createPickupLocation = new CreatePickupLocation();
            //    createPickupLocation.pickup_location = ConvertTo.String(userCartMaster.ShopId);
            //    createPickupLocation.name = userCartMaster.UserName;
            //    createPickupLocation.phone = shopData.MobileNumber;
            //    createPickupLocation.address = userCartMaster.FlatNo +""+userCartMaster.Address;
            //    createPickupLocation.address_2 = "";
            //    createPickupLocation.city = userCartMaster.City;
            //    createPickupLocation.state = userCartMaster.State;
            //    createPickupLocation.pin_code = ConvertTo.String(userCartMaster.Pincode);
            //    createPickupLocation.country = "India";
            //    createPickupLocation.email = "test@test.com";
            //    using (var wc = new HttpClient())
            //    {
            //        string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

            //        SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            //        SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            //        DataTable dt = new DataTable();
            //        sda.Fill(dt);

            //        wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //        wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

            //        var getPickupPath = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup";
            //        var path = "https://apiv2.shiprocket.in/v1/external/settings/company/addpickup";

            //        var getPickResp = await wc.GetStringAsync(new Uri(getPickupPath));

            //        GetPickUp getPickUp = new GetPickUp();
            //        getPickUp = JsonConvert.DeserializeObject<GetPickUp>(getPickResp);
            //        bool containsItem = getPickUp.data.shipping_address.Any(item => item.pickup_location == ConvertTo.String(userCartMaster.ShopId));
            //        if (!containsItem)
            //        {

            //            var s = JsonConvert.SerializeObject(createPickupLocation);
            //            var content = new StringContent(JsonConvert.SerializeObject(createPickupLocation), Encoding.UTF8, "application/json");
            //            var result = await wc.PostAsync(new Uri(path), content);
            //            string resultContent = await result.Content.ReadAsStringAsync();
            //            var res = JsonConvert.DeserializeObject<Pickup>(resultContent);

            //            var quote3 = abstractShopOwnerServices.AddShopOwnerPickUpId(shopData.Id, Convert.ToString(res.pickup_id));
            //        }
            //        //List<CreateShipmentResData> createShipmentResDatas = new List<CreateShipmentResData>();
            //        //createShipmentResDatas.Add(res.data.data[1]);
            //        //res.data.data = createShipmentResDatas;

            //    }
            //}

            var quote = abstractUserCartMasterServices.UserCartMaster_Upsert(userCartMaster);

            if (quote.Item.IsNameChange)
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                using (var wc = new HttpClient())
                {
                    wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //wc.DefaultRequestHeaders.Add("Authorization", "Basic " + Configurations.VideoCallToken);

                    var path = "https://api.mixpanel.com/engage#profile-set";
                    MixRoot mixRoot = new MixRoot();
                    MixSet mixSet = new MixSet();
                    mixRoot.Token = Configurations.MixPanelToken;
                    mixRoot.DistinctId = quote.Item.UserId.ToString();
                    mixSet.Name = quote.Item.UserName;
                    mixRoot.Set = mixSet;
                    var content = new StringContent(JsonConvert.SerializeObject(mixRoot), Encoding.UTF8, "application/json");
                    var result = await wc.PostAsync(new Uri(path), content);
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // UserCartMaster_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCartMaster_All")]
        public async Task<IHttpActionResult> UserCartMaster_All(PageParam pageParam, string search = "")
        {
            var quote = abstractUserCartMasterServices.UserCartMaster_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserCartMaster_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCartMaster_ByShopId")]
        public async Task<IHttpActionResult> UserCartMaster_ByShopId(PageParam pageParam, string search = "" , long ShopId = 0)
        {
            var quote = abstractUserCartMasterServices.UserCartMaster_ByShopId(pageParam, search, ShopId);
            return this.Content((HttpStatusCode)200, quote);
        }

        // UserCartMaster_ByUserId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCartMaster_ByUserId")]
        public async Task<IHttpActionResult> UserCartMaster_ByUserId(PageParam pageParam, string search = "" , long UserId = 0)
        {
            var quote = abstractUserCartMasterServices.UserCartMaster_ByUserId(pageParam, search, UserId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //UserCartMaster_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCartMaster_ById")]
        public async Task<IHttpActionResult> UserCartMaster_ById(long Id)
        {
            var quote = abstractUserCartMasterServices.UserCartMaster_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //UserCartMaster_ByVideoCallId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserCartMaster_ByVideoCallId")]
        public async Task<IHttpActionResult> UserCartMaster_ByVideoCallId(long VideoCallId , long UserId,int Type = 0)
        {
            var quote = abstractUserCartMasterServices.UserCartMaster_ByVideoCallId(VideoCallId, UserId, Type);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        ////UserCartMaster_ByVideoCallId API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("UserCartMaster_ByVideoCallId")]
        //public async Task<IHttpActionResult> UserCartMaster_ByVideoCallId(long VideoCallId)
        //{
        //    var quote = abstractUserCartMasterServices.UserCartMaster_ByVideoCallId(VideoCallId);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}
    }

    public class MixRoot
    {
        [JsonProperty("$token")]
        public string Token { get; set; }

        [JsonProperty("$distinct_id")]
        public string DistinctId { get; set; }

        [JsonProperty("$set")]
        public MixSet Set { get; set; }
    }

    public class MixSet
    {
        [JsonProperty("$name")]
        public string Name { get; set; }
    }
}
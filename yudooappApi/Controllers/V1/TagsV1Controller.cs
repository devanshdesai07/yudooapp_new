﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooappApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TagsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTagsServices abstractTagsServices;
        #endregion

        #region Cnstr
        public TagsV1Controller(AbstractTagsServices abstractTagsServices)
        {
            this.abstractTagsServices = abstractTagsServices;
        }
        #endregion

        // Tags_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Tags_Upsert")]
        public async Task<IHttpActionResult> Tags_Upsert(Tags Tags)
        {
            var quote = abstractTagsServices.Tags_Upsert(Tags);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Tags_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Tags_All")]
        public async Task<IHttpActionResult> Tags_All(PageParam pageParam, string search = "")
        {
            var quote = abstractTagsServices.Tags_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Tags_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Tags_ById")]
        public async Task<IHttpActionResult> Tags_ById(long Id)
        {
            var quote = abstractTagsServices.Tags_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Tags_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Tags_Delete")]
        public async Task<IHttpActionResult> Tags_Delete(long Id)
        {
            var quote = abstractTagsServices.Tags_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
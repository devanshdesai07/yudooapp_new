﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdminTypeServices abstractAdminTypeServices;
        #endregion

        #region Cnstr
        public AdminTypeV1Controller(AbstractAdminTypeServices abstractAdminTypeServices)
        {
            this.abstractAdminTypeServices = abstractAdminTypeServices;
        }
        #endregion

        // AdminType_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminType_Upsert")]
        public async Task<IHttpActionResult> AdminType_Upsert(AdminType AdminType)
        {
            var quote = abstractAdminTypeServices.AdminType_Upsert(AdminType);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // AdminType_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminType_All")]
        public async Task<IHttpActionResult> AdminType_All(PageParam pageParam, string search = "")
        {
            var quote = abstractAdminTypeServices.AdminType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //AdminType_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminType_ById")]
        public async Task<IHttpActionResult> AdminType_ById(int Id)
        {
            var quote = abstractAdminTypeServices.AdminType_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //AdminType_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminType_ActInAct")]
        public async Task<IHttpActionResult> AdminType_ActInAct(int Id)
        {
            var quote = abstractAdminTypeServices.AdminType_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //AdminType_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminType_Delete")]
        public async Task<IHttpActionResult> AdminType_Delete(int Id,int DeletdBy)
        {
            var quote = abstractAdminTypeServices.AdminType_Delete(Id, DeletdBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Text;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Data.SqlClient;
using System.Data;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CallBackV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCallBackServices abstractCallBackServices;
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        private readonly AbstractOrderServices abstractOrderServices;
        private readonly AbstractUserOrderServices abstractUserOrderServices;
        private readonly AbstractUserOrderItemsServices abstractUserOrderItemsServices;
        private readonly AbstractOrderParcelServices abstractOrderParcelServices;
        #endregion

        #region Cnstr
        public CallBackV1Controller(AbstractCallBackServices abstractCallBackServices,
            AbstractShopOwnerServices abstractShopOwnerServices, AbstractOrderServices abstractOrderServices,
            AbstractUserOrderItemsServices abstractUserOrderItemsServices,
            AbstractOrderParcelServices abstractOrderParcelServices,
            AbstractUserOrderServices abstractUserOrderServices
        )
        {
            this.abstractCallBackServices = abstractCallBackServices;
            this.abstractShopOwnerServices = abstractShopOwnerServices;
            this.abstractOrderServices = abstractOrderServices;
            this.abstractUserOrderItemsServices = abstractUserOrderItemsServices;
            this.abstractOrderParcelServices = abstractOrderParcelServices;
            this.abstractUserOrderServices = abstractUserOrderServices;
        }
        #endregion

        // CallBack_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CallBack_CallManage")]
        public async Task<IHttpActionResult> CallBack_CallManage(UserShopCallLogs userShopCallLogs)
        {
            var quote = abstractCallBackServices.CallBack_CallManage(userShopCallLogs);

            if (userShopCallLogs.Type == 8)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("https://video.yudoo.co/openvidu/api/sessions/" + userShopCallLogs.VideoCallLink);
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + Configurations.VideoCallToken);
                HttpResponseMessage response = client.DeleteAsync(client.BaseAddress).Result;
            }
            else if (userShopCallLogs.Type == 11)
            {
                HttpClient client = new HttpClient();

                VRecord vRecord = new VRecord();
                vRecord.session = userShopCallLogs.VideoCallLink;

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + Configurations.VideoCallToken);

                var path = "https://video.yudoo.co/openvidu/api/recordings/start";

                var content = new StringContent(JsonConvert.SerializeObject(vRecord), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(new Uri(path), content);
                string resultContent = await result.Content.ReadAsStringAsync();

            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //[System.Web.Http.HttpPost]
        //[InheritedRoute("CallBack_Upsert")]
        //public async Task<IHttpActionResult> CallBack_Upsert(CallBack callBack)
        //{

        //    var quote = abstractCallBackServices.CallBack_Upsert(callBack);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        //// CallBack_All API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("CallBack_All")]
        //public async Task<IHttpActionResult> CallBack_All(PageParam pageParam, string search = "")
        //{
        //    var quote = abstractCallBackServices.CallBack_All(pageParam, search);
        //    return this.Content((HttpStatusCode)200, quote);
        //}

        ////CallBack_ById API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("CallBack_ById")]
        //public async Task<IHttpActionResult> CallBack_ById(long Id)
        //{
        //    var quote = abstractCallBackServices.CallBack_ById(Id);
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}

        [System.Web.Http.HttpPost]
        [InheritedRoute("DeliveryChargeByPostCode")]
        public async Task<IHttpActionResult> DeliveryChargeByPostCode(long ShopId, int DeliveryPostcode, string Weight, int IsCod)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            SuccessResult<AbstractShopOwner> quote = abstractShopOwnerServices.ShopOwner_ById(ShopId, 0);
            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://apiv2.shiprocket.in/v1/external/courier/serviceability?pickup_postcode=" + quote.Item.PinCode + "&delivery_postcode=" + DeliveryPostcode + "&weight=1&cod=0");
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Configurations.DeliveryToken);

            //HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            //var d = await response.Content.ReadAsStringAsync();
            //var quote = abstractCallBackServices.CallBack_ById(Id);

            SuccessResult<Root> quote1 = new SuccessResult<Root>();
            SuccessResult<CodRoot> quote2 = new SuccessResult<CodRoot>();
            SuccessResult<CodRes> quote3 = new SuccessResult<CodRes>();

            using (var wc = new HttpClient())
            {
                string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                var path = "https://apiv2.shiprocket.in/v1/external/courier/serviceability?pickup_postcode=" + quote.Item.PinCode + "&delivery_postcode=" + DeliveryPostcode + "&weight=" + Weight + "&cod=" + IsCod;
                try
                {
                    var resp = await wc.GetStringAsync(new Uri(path));
                    dynamic codRes = JsonConvert.DeserializeObject<dynamic>(resp);
                    return this.Content((HttpStatusCode)200, codRes);

                }
                catch (Exception ex)
                {
                    dynamic codResd = null;
                    return this.Content((HttpStatusCode)200, codResd);

                }
                //if (IsCod == 1)
                //{
                //    //CodRoot codRoot = new CodRoot();
                //    //codRoot = JsonConvert.DeserializeObject<CodRoot>(resp); //JObject.Parse(resp);
                //    dynamic codRes = JsonConvert.DeserializeObject<dynamic>(resp);

                //    //if (codRes !=null && codRes.data != null)
                //    //{
                //    // //  var s =  codRes.GetType().GetProperty("status").GetValue(codRes, null);

                //    //    quote3.Item.status = codRoot.status;
                //    //    quote3.Item.rate = ConvertTo.String(codRes.data.available_courier_companies[0].rate);
                //    //    quote3.Item.cod_charges = ConvertTo.String(codRes.data.available_courier_companies[0].cod_charges);
                //    //    quote3.Item.coverage_charges = ConvertTo.String(codRes.data.available_courier_companies[0].coverage_charges);
                //    //    quote3.Item.freight_charge = ConvertTo.String(codRes.data.available_courier_companies[0].freight_charge);
                //    //    quote3.Item.cod = codRes.data.available_courier_companies[0].cod;
                //    //}


                //    //quote2.Item = codRoot;
                //    //quote2.Code = 200;
                //    //List<CodAvailableCourierCompany> availableCourierCompanies = new List<CodAvailableCourierCompany>();
                //    //availableCourierCompanies.Add(codRoot.data.available_courier_companies[0]);
                //    //codRoot.data.available_courier_companies = availableCourierCompanies;
                //    return this.Content((HttpStatusCode)200, codRes);

                //}
                //else
                //{
                //    Root root = new Root();
                //    root = JsonConvert.DeserializeObject<Root>(resp); //JObject.Parse(resp);
                //    quote1.Item = root;
                //    quote1.Code = 200;
                //    List<AvailableCourierCompany> availableCourierCompanies = new List<AvailableCourierCompany>();
                //    availableCourierCompanies.Add(root.data.available_courier_companies[0]);
                //    root.data.available_courier_companies = availableCourierCompanies;
                //    return this.Content((HttpStatusCode)quote.Code, quote1);

                //}

            }

        }


        //CreateShipment API
        [System.Web.Http.HttpPost]
        [InheritedRoute("CreateShipment")]
        public async Task<IHttpActionResult> CreateShipment(long OrderId = 0)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 99999;

            var isShipment = abstractOrderParcelServices.Check_CreateShipment(OrderId);
            if (isShipment.Code == 200)
            {
                var CheckProductCount = abstractOrderParcelServices.OrderParcelProduct_All(pageParam, "", OrderId);

                var quote = abstractOrderParcelServices.OrderParcelShipment_ByOrderId(pageParam, "", OrderId);
                CreateShipment createShipment = new CreateShipment();
                if (CheckProductCount.Values.Count == 0)
                {
                    for (int i = 0; i < quote.Values.Count; i++)
                    {
                        quote.Values[i].order_id = quote.Values[i].order_id + "_" + DateTime.Now.ToString("ddMMyyyyhhmmssfff");


                        if (quote.Values != null && !string.IsNullOrEmpty(Convert.ToString(quote.Values[i].shipping_customer_name)))
                        {
                            List<OrderItem> orderItem = new List<OrderItem>();
                            OrderItem orderItemObj = new OrderItem();


                            orderItem = JsonConvert.DeserializeObject<List<OrderItem>>(Convert.ToString(quote.Values[i].shipping_customer_name));
                            quote.Values[i].shipping_customer_name = "";
                            quote.Values[i].order_items = orderItem;

                            //foreach (var item in quote.Item.ProductDetails)
                            //{
                            //    orderItemObj.name = item.ItemName;
                            //    orderItemObj.sku = "";
                            //    orderItemObj.units = item.Qty;
                            //    orderItemObj.selling_price = item.Price;
                            //    orderItemObj.discount = "";
                            //    orderItemObj.tax = "";
                            //    orderItemObj.hsn = "";
                            //    orderItem.Add(orderItemObj);
                            //}
                        }



                        //createShipment.order_date = quote2.Item.CreatedDateStr;
                        string orderItemIds = quote.Values[i].shipping_phone;
                        quote.Values[i].shipping_phone = "";
                        //for (int i=0;i< createShipment.order_items.Count;i++)
                        //{
                        //    orderItemIds += createShipment.order_items[i].hsn + ",";
                        //}

                        using (var wc = new HttpClient())
                        {

                            string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

                            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                            DataTable dt = new DataTable();
                            sda.Fill(dt);

                            wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                            var path = "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc";

                            var content = new StringContent(JsonConvert.SerializeObject(quote.Values[i]), Encoding.UTF8, "application/json");
                            var result = await wc.PostAsync(new Uri(path), content);
                            string resultContent = await result.Content.ReadAsStringAsync();
                            var res = JsonConvert.DeserializeObject<CreateShipmentRes>(resultContent);
                            if (res.shipment_id != 0)
                            {
                                var quote4 = abstractUserOrderItemsServices.AddShipmentIdUserOrderItems_ById(orderItemIds, res.shipment_id);

                                GenerateAWBforShipment generateAWBforShipment = new GenerateAWBforShipment();
                                generateAWBforShipment.shipment_id = res.shipment_id.ToString();
                                generateAWBforShipment.courier_id = "";
                                //generateAWBforShipment.shipment_id =ConvertTo.Integer(res.shipment_id);

                                var AWBpath = "https://apiv2.shiprocket.in/v1/external/courier/assign/awb";
                                var AWBcontent = new StringContent(JsonConvert.SerializeObject(generateAWBforShipment), Encoding.UTF8, "application/json");
                                var AWBresult = await wc.PostAsync(new Uri(AWBpath), AWBcontent);
                                string AWBresultContent = await AWBresult.Content.ReadAsStringAsync();


                                //var resError = JsonConvert.DeserializeObject<AWBcontentError>(AWBresultContent);

                                try
                                {
                                    var resError = JsonConvert.DeserializeObject<ARoot>(AWBresultContent);



                                    if (resError.awb_assign_status != 1)
                                    {
                                        var sendGridClient1 = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                                        var sendGridMessage1 = new SendGridMessage();
                                        sendGridMessage1.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "Yudoo");

                                        //sendGridMessage1.AddTo("kartik.soboft@gmail.com");
                                        //sendGridMessage1.PlainTextContent = "Kindly check the shipping panel for order id " + OrderId + " and shipping id " + res.shipment_id + " Order placed";

                                        sendGridMessage1.SetTemplateData(new RequestObj
                                        {
                                            OrderId = ConvertTo.String(quote.Values[i].order_id),
                                            ShippingId = ConvertTo.String(res.shipment_id),
                                            PlacedDate = ConvertTo.String(DateTime.Now)
                                        });

                                        // sendGridMessage1.AddTo("kartik.soboft@gmail.com");
                                        sendGridMessage1.AddTo("viraj.sheth@yudoo.co");
                                        sendGridMessage1.AddTo("gitanj.sheth@yudoo.co");
                                        sendGridMessage1.SetTemplateId("d-d97e988245414480873947b7da1fae69");
                                        var response1 = sendGridClient1.SendEmailAsync(sendGridMessage1).Result;
                                        if (response1.StatusCode == System.Net.HttpStatusCode.Accepted)
                                        {

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {

                                }

                                //var AWBres = JsonConvert.DeserializeObject<generateManifestRes>(AWBresultContent);


                                generateManifest generate = new generateManifest();
                                List<int> termsList = new List<int>();
                                termsList.Add(res.shipment_id);
                                generate.shipment_id = termsList;

                                var pickuppath = "https://apiv2.shiprocket.in/v1/external/courier/generate/pickup";
                                var pickuppathcontent = new StringContent(JsonConvert.SerializeObject(generate), Encoding.UTF8, "application/json");
                                var pickuppathresult = await wc.PostAsync(new Uri(pickuppath), pickuppathcontent);
                                string pickuppathresultContent = await pickuppathresult.Content.ReadAsStringAsync();
                                //var pickuppathres = JsonConvert.DeserializeObject<generateManifestRes>(pickuppathresultContent);

                                var quote5 = abstractUserOrderServices.UserOrder_StatusChange(OrderId, 4);
                            }
                            else
                            {
                                SuccessResult<AbstractOrderParcel> ress = new SuccessResult<AbstractOrderParcel>();
                                ress.Message = "Please try again";
                                ress.Code = 400;
                                return this.Content((HttpStatusCode)400, ress);
                            }
                        }

                    }

                    return this.Content((HttpStatusCode)200, quote);
                }
                else
                {

                    ErrorObject errorObject = new ErrorObject();
                    errorObject.Code = 400;
                    errorObject.Message = "Please select a all parcel";
                    return this.Content((HttpStatusCode)400, errorObject);
                }
            }
            else
            {
                ErrorObject errorObject = new ErrorObject();
                errorObject.Code = 400;
                errorObject.Message = isShipment.Message;
                return this.Content((HttpStatusCode)400, errorObject);
            }
        }

        [System.Web.Http.HttpGet]
        [InheritedRoute("TrackingShipment")]
        public async Task<IHttpActionResult> TrackingShipment(int ShipmentId)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            using (var wc = new HttpClient())
            {
                string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                var path = "https://apiv2.shiprocket.in/v1/external/courier/track/shipment/" + ShipmentId;

                var result = await wc.GetStringAsync(new Uri(path));

                return this.Content((HttpStatusCode)200, result);

            }

        }

        [System.Web.Http.HttpGet]
        [InheritedRoute("SessionLeave")]
        public async Task<IHttpActionResult> SessionLeave(string SessionId)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            using (var wc = new HttpClient())
            {
                wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                wc.DefaultRequestHeaders.Add("Authorization", "Basic " + Configurations.VideoCallToken);

                var path = "https://video.yudoo.co/openvidu/api/sessions/" + SessionId;

                var result = await wc.GetStringAsync(new Uri(path));

                var res = JsonConvert.DeserializeObject<LeaveRoot>(result);

                return this.Content((HttpStatusCode)200, res);
            }

        }

        [System.Web.Http.HttpGet]
        [InheritedRoute("GenerateManifest")]
        public async Task<IHttpActionResult> GenerateManifest(int ShipmentId)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            generateManifest generate = new generateManifest();
            List<int> termsList = new List<int>();
            termsList.Add(ShipmentId);
            generate.shipment_id = termsList;
            SuccessResult<AbstractUserOrderItems> request = new SuccessResult<AbstractUserOrderItems>();
            using (var wc = new HttpClient())
            {


                wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());


                var path = "https://apiv2.shiprocket.in/v1/external/manifests/generate";
                var content = new StringContent(JsonConvert.SerializeObject(generate), Encoding.UTF8, "application/json");
                var result = await wc.PostAsync(new Uri(path), content);
                string resultContent = await result.Content.ReadAsStringAsync();
                var res = JsonConvert.DeserializeObject<generateManifestRes>(resultContent);
                if (res.manifest_url != null && res.manifest_url != "")
                {
                    request = abstractUserOrderItemsServices.ManifestUrl_ShipmentId(res.manifest_url, "", ShipmentId);
                }
                else
                {
                    //return this.Content((HttpStatusCode)400, res);
                }
            }

            using (var wc1 = new HttpClient())
            {
                wc1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                wc1.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                var path = "https://apiv2.shiprocket.in/v1/external/courier/generate/label";
                var content = new StringContent(JsonConvert.SerializeObject(generate), Encoding.UTF8, "application/json");
                var result = await wc1.PostAsync(new Uri(path), content);
                string resultContent = await result.Content.ReadAsStringAsync();
                var res = JsonConvert.DeserializeObject<generateLabelRes>(resultContent);
                if (res.label_url != null && res.label_url != "")
                {
                    request = abstractUserOrderItemsServices.ManifestUrl_ShipmentId("", res.label_url, ShipmentId);
                    //var result = await wc.GetStringAsync(new Uri(path));
                }
                else
                {
                    return this.Content((HttpStatusCode)400, res);
                }
            }
            return this.Content((HttpStatusCode)200, request);

        }


    }


    public class LeaveDefaultRecordingProperties
    {
        public string name { get; set; }
        public bool hasAudio { get; set; }
        public bool hasVideo { get; set; }
        public string outputMode { get; set; }
        public string recordingLayout { get; set; }
        public string resolution { get; set; }
        public int frameRate { get; set; }
        public int shmSize { get; set; }
    }

    public class LeaveFilter
    {
    }

    public class LeaveMediaOptions
    {
        public bool hasAudio { get; set; }
        public bool audioActive { get; set; }
        public bool hasVideo { get; set; }
        public bool videoActive { get; set; }
        public string typeOfVideo { get; set; }
        public int frameRate { get; set; }
        public string videoDimensions { get; set; }
        public LeaveFilter filter { get; set; }
    }

    public class LeavePublisher
    {
        public object createdAt { get; set; }
        public string streamId { get; set; }
        public LeaveMediaOptions mediaOptions { get; set; }
    }

    public class LeaveSubscriber
    {
        public object createdAt { get; set; }
        public string streamId { get; set; }
    }

    public class LeaveContent
    {
        public string id { get; set; }
        public string @object { get; set; }
        public string status { get; set; }
        public string connectionId { get; set; }
        public string sessionId { get; set; }
        public object createdAt { get; set; }
        public object activeAt { get; set; }
        public string location { get; set; }
        public string ip { get; set; }
        public string platform { get; set; }
        public string token { get; set; }
        public string type { get; set; }
        public bool record { get; set; }
        public string role { get; set; }
        public object kurentoOptions { get; set; }
        public object rtspUri { get; set; }
        public object adaptativeBitrate { get; set; }
        public object onlyPlayWithSubscribers { get; set; }
        public object networkCache { get; set; }
        public string serverData { get; set; }
        public string clientData { get; set; }
        public List<LeavePublisher> publishers { get; set; }
        public List<LeaveSubscriber> subscribers { get; set; }
    }

    public class LeaveConnections
    {
        public int numberOfElements { get; set; }
        public List<LeaveContent> content { get; set; }
    }

    public class LeaveRoot
    {
        public string id { get; set; }
        public string @object { get; set; }
        public string sessionId { get; set; }
        public long createdAt { get; set; }
        public string mediaMode { get; set; }
        public string recordingMode { get; set; }
        public LeaveDefaultRecordingProperties defaultRecordingProperties { get; set; }
        public string customSessionId { get; set; }
        public LeaveConnections connections { get; set; }
        public bool recording { get; set; }
        public string forcedVideoCodec { get; set; }
        public bool allowTranscoding { get; set; }
    }


    public class RequestObj
    {
        [JsonProperty("OrderId")]
        public string OrderId { get; set; }

        [JsonProperty("ShippingId")]
        public string ShippingId { get; set; }

        [JsonProperty("PlacedDate")]
        public string PlacedDate { get; set; }
    }

    public class AWBcontentError
    {
        public string message { get; set; }
        public int status_code { get; set; }
    }

    public class VRecord
    {
        public string session { get; set; }
    }

    public class ErrorObject
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }


    public class generateManifest
    {
        public List<int> shipment_id { get; set; }
    }


    public class GenerateAWBforShipment
    {
        public string shipment_id { get; set; }
        public string courier_id { get; set; }
    }

    public class generateManifestRes
    {
        public string status { get; set; }
        public string manifest_url { get; set; }
    }
    public class generateLabelRes
    {
        public int label_created { get; set; }
        public string label_url { get; set; }
        public string response { get; set; }
        public List<object> not_created { get; set; }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);


    public class SuppressionDates
    {
        public string action_on { get; set; }
        public string delay_remark { get; set; }
        public int pickup_delay_by { get; set; }
        public string pickup_delay_to { get; set; }
        public string pickup_delay_days { get; set; }
        public string pickup_delay_from { get; set; }
    }

    public class RecommendedBy
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class AvailableCourierCompany
    {
        public object base_courier_id { get; set; }
        public string courier_type { get; set; }
        public int courier_company_id { get; set; }
        public string courier_name { get; set; }
        public bool is_rto_address_available { get; set; }
        public double rate { get; set; }
        public int is_custom_rate { get; set; }
        public int cod_multiplier { get; set; }
        public int cod_charges { get; set; }
        public double freight_charge { get; set; }
        public double rto_charges { get; set; }
        public int coverage_charges { get; set; }
        public bool is_surface { get; set; }
        public double rating { get; set; }
        public double rto_performance { get; set; }
        public double pickup_performance { get; set; }
        public double delivery_performance { get; set; }
        public int cod { get; set; }
        public string description { get; set; }
        public int mode { get; set; }
        public int blocked { get; set; }
        public object suppression_dates { get; set; }
        public double min_weight { get; set; }
        public int is_international { get; set; }
        public bool is_hyperlocal { get; set; }
        public int entry_tax { get; set; }
        public string cutoff_time { get; set; }
        public int pickup_availability { get; set; }
        public int seconds_left_for_pickup { get; set; }
        public string suppress_text { get; set; }
        public int pickup_supress_hours { get; set; }
        public string suppress_date { get; set; }
        public int supress_hours { get; set; }
        public int etd_hours { get; set; }
        public string etd { get; set; }
        public string estimated_delivery_days { get; set; }
        public double tracking_performance { get; set; }
        public double weight_cases { get; set; }
        public string realtime_tracking { get; set; }
        public string delivery_boy_contact { get; set; }
        public string pod_available { get; set; }
        public string call_before_delivery { get; set; }
        public string rank { get; set; }
        public string cost { get; set; }
        public string edd { get; set; }
        public string base_weight { get; set; }
        public string pickup_priority { get; set; }
        public int qc_courier { get; set; }
        public bool odablock { get; set; }
    }

    public class Data
    {
        public int is_recommendation_enabled { get; set; }
        public RecommendedBy recommended_by { get; set; }
        public object child_courier_id { get; set; }
        public int recommended_courier_company_id { get; set; }
        public int shiprocket_recommended_courier_id { get; set; }
        public List<AvailableCourierCompany> available_courier_companies { get; set; }
        public object recommendation_advance_rule { get; set; }
    }

    public class Root
    {
        public int status { get; set; }
        public Data data { get; set; }
        public List<object> covid_zones { get; set; }
        public int is_latlong { get; set; }
        public List<object> seller_address { get; set; }
        public string currency { get; set; }
        public int dg_courier { get; set; }
        public int eligible_for_insurance { get; set; }
    }


    public class CodAvailableCourierCompany
    {
        public object base_courier_id { get; set; }
        public string courier_type { get; set; }
        public int courier_company_id { get; set; }
        public string courier_name { get; set; }
        public bool is_rto_address_available { get; set; }
        public string rate { get; set; }
        public int is_custom_rate { get; set; }
        public string cod_multiplier { get; set; }
        public string cod_charges { get; set; }
        public string freight_charge { get; set; }
        public string rto_charges { get; set; }
        public int coverage_charges { get; set; }
        public bool is_surface { get; set; }
        public string rating { get; set; }
        public double rto_performance { get; set; }
        public double pickup_performance { get; set; }
        public double delivery_performance { get; set; }
        public int cod { get; set; }
        public string description { get; set; }
        public int mode { get; set; }
        public int blocked { get; set; }
        public List<object> suppression_dates { get; set; }
        public string min_weight { get; set; }
        public int is_international { get; set; }
        public bool is_hyperlocal { get; set; }
        public int entry_tax { get; set; }
        public string cutoff_time { get; set; }
        public int pickup_availability { get; set; }
        public int seconds_left_for_pickup { get; set; }
        public string suppress_text { get; set; }
        public int pickup_supress_hours { get; set; }
        public string suppress_date { get; set; }
        public int supress_hours { get; set; }
        public int etd_hours { get; set; }
        public string etd { get; set; }
        public string estimated_delivery_days { get; set; }
        public double tracking_performance { get; set; }
        public double weight_cases { get; set; }
        public string realtime_tracking { get; set; }
        public string delivery_boy_contact { get; set; }
        public string pod_available { get; set; }
        public string call_before_delivery { get; set; }
        public string rank { get; set; }
        public string cost { get; set; }
        public string edd { get; set; }
        public string base_weight { get; set; }
        public string pickup_priority { get; set; }
        public int qc_courier { get; set; }
        public bool odablock { get; set; }
    }

    public class CodData
    {
        public int is_recommendation_enabled { get; set; }
        public RecommendedBy recommended_by { get; set; }
        public object child_courier_id { get; set; }
        public int recommended_courier_company_id { get; set; }
        public int shiprocket_recommended_courier_id { get; set; }
        public List<CodAvailableCourierCompany> available_courier_companies { get; set; }
        public object recommendation_advance_rule { get; set; }
    }

    public class CodRoot
    {
        public int status { get; set; }
        public CodData data { get; set; }
        public List<object> covid_zones { get; set; }
        public int is_latlong { get; set; }
        public List<object> seller_address { get; set; }
        public string currency { get; set; }
        public int dg_courier { get; set; }
        public int eligible_for_insurance { get; set; }
    }


    public class CodRes
    {
        public int status { get; set; }
        public int cod { get; set; }
        public string rate { get; set; }
        public string cod_charges { get; set; }
        public string freight_charge { get; set; }
        public string rto_charges { get; set; }
        public string coverage_charges { get; set; }
    }

}
﻿using yudooapp.APICommon;
using yudooapp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooapp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace yudooapp_Api.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopOwnerDevicesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopOwnerDevicesServices abstractShopOwnerDevicesServices;
        #endregion

        #region Cnstr
        public ShopOwnerDevicesV1Controller(AbstractShopOwnerDevicesServices abstractShopOwnerDevicesServices)
        {
            this.abstractShopOwnerDevicesServices = abstractShopOwnerDevicesServices;
        }
        #endregion


    
        // ShopOwnerDevices_ByShopId API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwnerDevices_ByShopId")]
        public async Task<IHttpActionResult> ShopOwnerDevices_ByShopId(PageParam pageParam, string search = "", long ShopOwnerId = 0)
        {
            var quote = abstractShopOwnerDevicesServices.ShopOwnerDevices_ByShopId(pageParam, search, ShopOwnerId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
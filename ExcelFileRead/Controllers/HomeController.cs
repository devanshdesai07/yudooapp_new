﻿using Newtonsoft.Json;
using OfficeOpenXml;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcelFileRead.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Upload(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var usersList = new List<Users>();


                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;

                      

                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            var user = new Users();
                            user.Id = workSheet.Cells[rowIterator, 1].Value.ToString();
                            user.Email = workSheet.Cells[rowIterator, 2].Value.ToString();
                            usersList.Add(user);

                            //List<EmailAddress> emails = new List<EmailAddress>();
                            //EmailAddress usersEmails = new EmailAddress();
                            //usersEmails.Email = user.Email;
                            //emails.Add(usersEmails);

                            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                            var sendGridMessage = new SendGridMessage();
                            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "Rushkar");
                            sendGridMessage.ReplyTo = new EmailAddress("marketing@rushkar.com");
                            sendGridMessage.AddTo(user.Email);
                            sendGridMessage.SetTemplateData(new RequestObj
                            {
                                Var1 = ConfigurationManager.AppSettings["SendGridVar1"],
                                Var2 = ConfigurationManager.AppSettings["SendGridVar2"]
                               
                            });
                            sendGridMessage.SetTemplateId("d-c0fbe8a8df55422f893dcdf8b9165429");
                            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                            {

                            }
                        }

                        
                    }
                }
            }
            return View("Index");
        }
    }
}


public class Users
{
    public string Id { get; set; }
    public string Email { get; set; }
}
public class UsersEmails
{
    public string Email { get; set; }
}

public class RequestObj
{
    [JsonProperty("Var1")]
    public string Var1 { get; set; }

    [JsonProperty("Var2")]
    public string Var2 { get; set; }

    
}
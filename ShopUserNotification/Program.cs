﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopUserNotification
{
    class Program
    {
        static void Main(string[] args)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString; //"Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=YudooDev;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnectionString);
            while (true)
            {
                try
                {
                    string q = "SELECT Id,UserShopCallLogsId,ShopId,UserId,Type,CreatedDate from [dbo].[ShopCallingNotification] where Type = 2 and IsSend = 0";
                    SqlDataAdapter ad = new SqlDataAdapter(q, ConnectionString);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                SendNotification(Convert.ToInt64(row["Id"]),Convert.ToInt64(row["UserShopCallLogsId"]), Convert.ToInt64(row["ShopId"]),Convert.ToInt64(row["UserId"]), Convert.ToInt64(row["Type"]) , Convert.ToDateTime(row["CreatedDate"]));
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
            Console.Read();
        }


        public static string SendNotification(long Id,long UserShopCallLogsId, long ShopId,long UserId,long Type , DateTime CreatedDate)
        {
            try
            {
                string ConnectionString = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=YudooDev;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
                SqlConnection conn = new SqlConnection(ConnectionString);


                //Console.WriteLine("Id = {0} , shopId = {1} , UserId = {2}, Type = {3} , CreatedDate = {4}", Id,ShopId , UserId, Type, CreatedDate.AddHours(2));
                if (CreatedDate.AddMinutes(120) <= DateTime.UtcNow)
                {
                    Console.WriteLine("ShopId = {0} , UserId = {1}" , ShopId, UserShopCallLogsId, UserId);

                    SqlCommand cmd = new SqlCommand("ShopCallingNotification_Upsert", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ShopId", ShopId);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Type", 10);
                    cmd.Parameters.AddWithValue("@UserShopCallLogsId", UserShopCallLogsId);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    conn.Close();

                    string UpdateShopCallingData = $"UPDATE ShopCallingNotification set IsSend = 1 where Id = {Id}";
                    SqlCommand UpdateShopCallingDatacmd = new SqlCommand(UpdateShopCallingData, conn);
                    conn.Open();
                    Console.WriteLine("Openning Connection ...");
                    UpdateShopCallingDatacmd.ExecuteNonQuery();
                    conn.Close();
                    Console.WriteLine("Connection successful!");

                }

               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return "1";
        }

    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using yudooapp.Common;

namespace StatusUpdate
{
    class Program
    {
        public static string DevConnStr = ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString;// "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=YudooDev;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        //public static string ProdConnStr = "Data Source=rushkar-db-zau.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=YudooProd;Persist Security Info=True;User ID=sa;Password=*zZ123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        static async Task Main(string[] args)
        {
            SqlConnection devConn = new SqlConnection(DevConnStr);
            //SqlConnection prodConn = new SqlConnection(ProdConnStr);
            try
            {
                string q = "select * from [dbo].[UserOrder]";
                SqlDataAdapter ad = new SqlDataAdapter(q, devConn);
                DataTable dt = new DataTable();
                ad.Fill(dt);

                //ServicePointManager.Expect100Continue = true;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";
                //SqlDataAdapter sda = new SqlDataAdapter(var_sql, devConn);
                //DataTable dt2 = new DataTable();
                //sda.Fill(dt2);


                if (dt.Rows.Count > 0)
                {
                    List<Int64> ShipmentIdArray = new List<Int64>();
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["Id"].ToString() != "")
                        {
                            string getDT = "select distinct ShipmentId from [dbo].[UserOrderItems] where OrderId = " + row["Id"].ToString() + " AND ShipmentId IS NOT NULL";
                            SqlDataAdapter SI = new SqlDataAdapter(getDT, devConn);
                            DataTable ShipmentId = new DataTable();
                            SI.Fill(ShipmentId);
                            ShipmentIdArray = new List<Int64>();

                            if (ShipmentId.Rows.Count > 0)
                            {
                                ShipmentIdArray = ShipmentId.AsEnumerable()
                                            .Select(r => r.Field<Int64>("ShipmentId"))
                                            .ToList();

                                if (ShipmentIdArray.Count > 0)
                                {
                                    await ShiprocketShipmentAsync(ShipmentIdArray);
                                    //Task.Run(() => ShiprocketShipmentAsync(ShipmentIdArray).Wait());

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Error", error);
            }
        }

        static async Task ShiprocketShipmentAsync(List<Int64> ShipmentIdArray)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";
            SqlConnection devConn = new SqlConnection(DevConnStr);
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, devConn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            try
            {
                foreach (var item in ShipmentIdArray)
                {
                    using (var wc = new HttpClient())
                    {
                        wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                        var path = "https://apiv2.shiprocket.in/v1/external/shipments/" + item;

                        var result = await wc.GetStringAsync(new Uri(path));

                        var res = JsonConvert.DeserializeObject<Root>(result);

                        long Id = 0, OrderStatus = 0;
                        Id = ConvertTo.Integer(res.data.id);
                        if (res.data.status == 18 || res.data.status == 6 || res.data.status == 42)
                        {

                            OrderStatus = 5;
                            //string updateQuery = "UPDATE UserOrderItems SET ItemStatusId = 5 where ShipmentId = " + Id;
                            //SqlCommand updateDevCmd = new SqlCommand(updateQuery, devConn);
                            //devConn.Open();
                            //updateDevCmd.ExecuteNonQuery();
                            //devConn.Close();
                        }
                        else if (res.data.status == 7)
                        {
                            OrderStatus = 6;
                            //string updateQuery = "UPDATE UserOrderItems SET ItemStatusId = 6 where ShipmentId = " + Id;
                            //SqlCommand updateDevCmd = new SqlCommand(updateQuery, devConn);
                            //devConn.Open();
                            //updateDevCmd.ExecuteNonQuery();
                            //devConn.Close();
                        }
                        else if (res.data.status == 8 || res.data.status == 9)
                        {
                            OrderStatus = 8;
                            //string updateQuery = "UPDATE UserOrderItems SET ItemStatusId = 8 where ShipmentId = " + Id;
                            //SqlCommand updateDevCmd = new SqlCommand(updateQuery, devConn);
                            //devConn.Open();
                            //updateDevCmd.ExecuteNonQuery();
                            //devConn.Close();
                        }


                        SqlCommand sqlCmd = new SqlCommand("UserOrder_StatusChangeByShipmentId", devConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@Id", SqlDbType.BigInt).Value = Id;
                        sqlCmd.Parameters.AddWithValue("@OrderStatus", SqlDbType.BigInt).Value = OrderStatus;

                        // Execute the command and get the data in a data reader.
                        SqlDataReader reader = sqlCmd.ExecuteReader();

                        devConn.Close();

                        Console.WriteLine("response", res);
                        //return this.Content((HttpStatusCode)200, res);
                    }

                }

            }
            catch (Exception error)
            {
                Console.WriteLine("Error", error);
            }
        }
    }

    public class ResponseOfGetAPI
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public int company_id { get; set; }
        public string created_at { get; set; }
        public string token { get; set; }
    }



    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Charges
    {
        public string zone { get; set; }
        public string cod_charges { get; set; }
        public string applied_weight_amount { get; set; }
        public string freight_charges { get; set; }
        public string applied_weight { get; set; }
        public string charged_weight { get; set; }
        public string charged_weight_amount { get; set; }
        public string charged_weight_amount_rto { get; set; }
        public string applied_weight_amount_rto { get; set; }
    }

    public class CreatedAt
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }

    public class CustomerDetails
    {
        public string psid { get; set; }
        public string phone { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
    }

    public class Data
    {
        public int id { get; set; }
        public int order_id { get; set; }
        public int channel_id { get; set; }
        public int company_id { get; set; }
        public string invoice_no { get; set; }
        public string invoice_date { get; set; }
        public object courier { get; set; }
        public object sr_courier_id { get; set; }
        public object awb { get; set; }
        public object awb_assign_date { get; set; }
        public object pickup_generated_date { get; set; }
        public object pickup_token_number { get; set; }
        public string method { get; set; }
        public string weight { get; set; }
        public string dimensions { get; set; }
        public Charges charges { get; set; }
        public int quantity { get; set; }
        public string cost { get; set; }
        public string tax { get; set; }
        public object cod_charges { get; set; }
        public string total { get; set; }
        public ShippingAddress shipping_address { get; set; }
        public CustomerDetails customer_details { get; set; }
        public int status { get; set; }
        public object shipped_date { get; set; }
        public object delivered_date { get; set; }
        public object returned_date { get; set; }
        public string label_url { get; set; }
        public object manifest_url { get; set; }
        public CreatedAt created_at { get; set; }
        public UpdatedAt updated_at { get; set; }
    }

    public class Root
    {
        public Data data { get; set; }
    }

    public class ShippingAddress
    {
        public string city { get; set; }
        public string state { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string pincode { get; set; }
        public string address_2 { get; set; }
        public object company_name { get; set; }
    }

    public class UpdatedAt
    {
        public string date { get; set; }
        public int timezone_type { get; set; }
        public string timezone { get; set; }
    }




}

﻿using yudooapp.Common;
using yudooappAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using yudooapp.Services.Contract;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
//using yudooapp.Entities.Contract;
//using yudooapp.Services.Contract;
//using yudooapp.Common.Paging;
//using yudooapp.Entities.V1;

namespace yudooappAdmin.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }
        int AdminTypeId = 0;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                bool isAllow = false;
                bool isAccess = false;
                HttpCookie reqCookie = Request.Cookies["AdminLogin"];
                string ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToString();

                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt32(Convert.ToString(reqCookie["Id"]));
                    ProjectSession.AdminTypeId = Convert.ToInt32(Convert.ToString(reqCookie["AdminTypesId"]));
                }

                else
                {
                    ProjectSession.AdminId = 0;
                }

                string var_sql = "select * from Admin where Id = " + ProjectSession.AdminId;

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    AdminTypeId = Convert.ToInt32(dt.Rows[0]["AdminTypesId"]);
                    ProjectSession.AdminTypeId = Convert.ToInt32(Convert.ToString(dt.Rows[0]["AdminTypesId"]));
                    if (ProjectSession.AdminTypeId == 4 || ProjectSession.AdminTypeId == 5 || ProjectSession.AdminTypeId == 6 && Pages.Controllers.AdminAccess.Contains(ControllerName))
                    {
                        isAccess = true;
                    }
                    else if (ProjectSession.AdminTypeId != 4 && ProjectSession.AdminTypeId != 5 && ProjectSession.AdminTypeId != 6 && Pages.Controllers.UserAccess.Contains(ControllerName))
                    {
                        isAccess = true;
                    }

                    ProjectSession.AdminName = dt.Rows[0]["FirstName"].ToString() + ' ' + dt.Rows[0]["LastName"].ToString();
                    ProjectSession.AdminProfile = "https://yudoo.s3.ap-south-1.amazonaws.com/" + dt.Rows[0]["UserImages"].ToString();

                    isAllow = true;
                }
                else
                {
                    isAllow = false;
                }

                if (isAccess == true)
                {
                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    throw new Exception("");
                }
                FillMenu();
                base.OnActionExecuting(filterContext);
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/Home/Index");
            }
        }

        private void FillMenu()
        {
            try
            {
               /* if (AdminTypeId > 0)
                {
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 1;
                    pageParam.Limit = 1000;

                    SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                    PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();

                    string var_sql = " exec UserPermissions_ByAdminId " + AdminTypeId;
                    DataTable dt = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                    sda.Fill(dt);

                    DataView dView = dt.DefaultView;
                    dView.RowFilter = "";
                    dView.RowFilter = " SubPageAction ='Index'";

                    string strHTML = "";
                    var DataList = offerData.Values.Where(x => x.SubPageAction.ToUpper() == "INDEX").ToArray();

                    strHTML = "<li class='navbar-vertical-item'> ";
                    strHTML += "<a href ='/Home/Index' id = 'mnHome' class='navbar-vertical-link'> ";
                    strHTML += "<div class='navbar-vertical-icon'><i class='tio-dashboard-vs-outlined'></i></div> ";
                    strHTML += "Dashboard ";
                    strHTML += "</a> ";
                    strHTML += "</li> ";
                    

                    for (int cnt = 0; cnt < dView.Count; cnt++)
                    {
                        DataRow d_row = dView[cnt].Row;
                        if (Convert.ToString(d_row["PageName"]).ToUpper() == "CUSTOMER" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/Customer/Index' id='mnCustomer' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-user-outlined'></i></div> ";
                            strHTML += "Customer ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        //else if (Convert.ToString(d_row["PageName"]).ToUpper() == "ADMINTYPES" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        //{
                        //    strHTML = "<li class='navbar-vertical-item'> ";
                        //    strHTML += "<a href ='/AdminTypes/Index' id = 'mnAdminTypes' class='navbar-vertical-link'> ";
                        //    strHTML += "<div class='navbar-vertical-icon'><i class='tio-dashboard-vs-outlined'></i></div> ";
                        //    strHTML += "Admin Types ";
                        //    strHTML += "</a> ";
                        //    strHTML += "</li> ";
                        //}
                        //else if (Convert.ToString(d_row["PageName"]).ToUpper() == "USERPERMISSIONS" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        //else if (Convert.ToString(d_row["AdminTypesName"]).ToUpper() == "Global Admin" || Convert.ToString(d_row["AdminTypesName"]).ToUpper() == "Admin" || Convert.ToString(d_row["AdminTypesName"]).ToUpper() == "super admin")
                        //else if (AdminTypeId == 4 || AdminTypeId == 5 || AdminTypeId == 6)
                        //{
                        //    strHTML += "<li class='navbar-vertical-item'> ";
                        //    strHTML += "<a href ='/UserPermissions/Index' id='mnUserPermissions' class='navbar-vertical-link'> ";
                        //    strHTML += "<div class='navbar-vertical-icon'><i class='tio-shop'></i></div> ";
                        //    strHTML += "UserPermissions ";
                        //    strHTML += "</a> ";
                        //    strHTML += "</li> ";
                        //}
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "SHOP" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/Shop/Index' id='mnShop' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-shop'></i></div> ";
                            strHTML += "Shops ";
                            strHTML += "</a> ";
                            strHTML += "</li> ";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "SHOPOVERVIEW" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/ShopOverview/Index' id='mnShopOverview' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-shopping'></i></div> ";
                            strHTML += "Shop Overview";
                            strHTML += "</a> ";
                            strHTML += "</li> ";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "SHOPONBOARDDETAIL" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/ShopOnboardDetail/Index' id='mnShopOnboardDetail' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-devices-1'></i></div> ";
                            strHTML += "Shop Onboard Details";
                            strHTML += "</a> ";
                            strHTML += "</li> ";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "SHOPAPPROVAL" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/ShopApproval/Index' id='mnShopApproval' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-appointment'></i></div> ";
                            strHTML += "Shop Approval";
                            strHTML += "</a> ";
                            strHTML += "</li> ";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "AMOUNTBIFURCATION" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/AmountBifurcation/Index' id='mnAmountBifurcation' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-appointment'></i></div> ";
                            strHTML += "Amount Bifurcation";
                            strHTML += "</a> ";
                            strHTML += "</li> ";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "BANNER" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href ='/Banner/Index' id='mnBanner' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-podcast'></i></div> ";
                            strHTML += "Banner ";
                            strHTML += "</a> ";
                            strHTML += "</li> ";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "OFFER" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/Offer/Index' id = 'mnOffer' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-security'></i></div> ";
                            strHTML += "Offers ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "ORDERS" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/Orders/Index' id = 'mnOrdersIndex' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-shopping-basket-outlined'></i></div> ";
                            strHTML += "Orders ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "ORDERS" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/Orders/OrderDetails' id = 'mnOrdersOrderDetailsorderId' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-receipt-outlined'></i></div> ";
                            strHTML += "Orders ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "VIDEOCALLS" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/VideoCalls/Index' id = 'mnVideoCalls' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-support'></i></div> ";
                            strHTML += "Video calls ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "PAGES" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/Pages/Index' id = 'mnPages' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-route'></i></div> ";
                            strHTML += "Pages ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                      
                    }

                    strHTML += "<li>";
                    strHTML += "<small class='nav-subtitle'> Master Data</small>";
                    strHTML += "</li> ";

                    for (int cnt = 0; cnt < dView.Count; cnt++)
                    {
                        DataRow d_row = dView[cnt].Row;
                        if (Convert.ToString(d_row["PageName"]).ToUpper() == "SHOPKEEPERSUPPORT" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/ShopkeeperSupport/Index' id = 'mnShopkeeperSupport' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-poi-user'></i></div> ";
                            strHTML += "Shopkeeper Support ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "SHOPKEEPERMASTER" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/ShopkeeperMaster/Index' id = 'mnShopkeeperMaster' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-sharpness'></i></div> ";
                            strHTML += "Shopkeeper Master ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "CUSTOMERCATEGORYORDER" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/CustomerCategoryOrder/Index' id = 'mnCustomerCategoryOrder' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-union'></i></div> ";
                            strHTML += " Customer Category Order ";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "CURATEDLIST" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/CuratedList/Index' id = 'mnCuCuratedList' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-table'></i></div> ";
                            strHTML += "Curated List";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "MASTERDATA" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/MasterData/Index' id = 'mnMasterData' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-back-ui'></i></div> ";
                            strHTML += "Back-end Master";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "CUSTOMERMASTER" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/CustomerMaster/Index' id = 'mnCustomerMaster' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-users-switch'></i></div> ";
                            strHTML += "Customer Master";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }

                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "DOWNLOAD" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/Download/Index' id = 'mnDownload' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-download'></i></div> ";
                            strHTML += "Downloads";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "SENDNOTIFICATION" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/SendNotification/Index' id = 'mnSendNotification' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-notifications'></i></div> ";
                            strHTML += "Send Notification Zone";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "ADMINUSERS" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/AdminUsers/Index' id = 'mnAdminUsers' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-user'></i></div> ";
                            strHTML += "Admin Users";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                        else if (Convert.ToString(d_row["PageName"]).ToUpper() == "ADMINTYPES" && Convert.ToString(d_row["Status"]).ToUpper() == "TRUE")
                        {
                            strHTML += "<li class='navbar-vertical-item'> ";
                            strHTML += "<a href = '/AdminTypes/Index' id = 'mnAdminTypes' class='navbar-vertical-link'> ";
                            strHTML += "<div class='navbar-vertical-icon'><i class='tio-user'></i></div> ";
                            strHTML += "Admin Types";
                            strHTML += "</a> ";
                            strHTML += "</li>";
                        }
                    }
                    ViewBag.LeftMenu = strHTML;
                }*/
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
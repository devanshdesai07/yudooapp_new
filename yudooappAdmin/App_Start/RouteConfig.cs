﻿using System.Web.Mvc;
using System.Web.Routing;


namespace yudooappAdmin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Account", action = "SignIn", id = UrlParameter.Optional }
            );
        }
    }
}

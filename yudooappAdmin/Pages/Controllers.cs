﻿using System.Collections.Generic;

namespace yudooappAdmin.Pages
{
    public class Controllers
    {
        public const string Authentication = "Authentication";
        public const string Home = "Home";
        public const string Users = "Users";
        public const string Customer = "Customer";
        public const string SalonOwner = "SalonOwner";
        public const string SalonsData = "SalonsData";
        public const string ChangePassword = "ChangePassword";
        public const string MasterData = "MasterData";
        public const string Pages = "Pages";
        public const string UserPermissions = "UserPermissions";
        public const string AmountBifurcation = "AmountBifurcation";

        //Yudoo Controllers defind
        public const string Orders = "Orders";
        public const string Shop = "Shop";
        public const string Account = "Account";
        public const string ShopOverview = "ShopOverview";
        
        public const string Banner = "Banner";
        public const string Offer = "Offer";
        public const string CuratedList = "CuratedList";

        public const string VideoCalls = "VideoCalls";
        public const string ShopApproval = "ShopApproval";
        public const string ShopOnboardDetail = "ShopOnboardDetail";
        public const string ShopkeeperSupport = "ShopkeeperSupport";
        public const string CuCuratedList = "CuCuratedList";
        public const string CustomerCategoryOrder = "CustomerCategoryOrder";
        public const string ShopkeeperMaster = "ShopkeeperMaster";
        public const string CustomerMaster = "CustomerMaster";
        public const string Download = "Download";
        public const string SendNotification = "SendNotification";
        public const string AdminUsers = "AdminUsers";
        public const string AdminTypes = "AdminTypes";

        public const string Request_CallBack_Admin = "Request_CallBack_Admin";

        public static string[] AdminAccess = { Authentication, Home, Users ,Customer, SalonOwner, ChangePassword, MasterData, Pages, UserPermissions, AmountBifurcation, Orders, Shop, Account, ShopOverview, Banner, Offer, CuratedList, VideoCalls, ShopApproval, ShopOnboardDetail, ShopkeeperSupport, CuCuratedList, CustomerCategoryOrder, ShopkeeperMaster, CustomerMaster, Download, SendNotification, AdminUsers, AdminTypes, Request_CallBack_Admin };
        public static string[] UserAccess = { Authentication, Home, Users, Customer, SalonOwner, ChangePassword, MasterData, Pages, AmountBifurcation, Orders, Shop, Account, ShopOverview, Banner, Offer, CuratedList, VideoCalls, ShopApproval, ShopOnboardDetail, ShopkeeperSupport, CuCuratedList, CustomerCategoryOrder, ShopkeeperMaster, CustomerMaster, Download, SendNotification, AdminUsers, AdminTypes, Request_CallBack_Admin };

    }
}
﻿namespace yudooappAdmin.Pages
{
    public class Actions
    {
        public const string UpdateDisplayOrderNo = "UpdateDisplayOrderNo";
        public const string UpdateDisplayOrder = "UpdateDisplayOrder";
        public const string Manage = "Manage";
        public const string ManageSpeciality = "ManageSpeciality";
        public const string ManageCity = "ManageCity";
        public const string DeleteBanner = "DeleteBanner";
        public const string UpdateStatus = "UpdateStatus";
        public const string GetData = "GetData";
        public const string Signin = "Signin";
        public const string Salons = "Salons";
        public const string Signout = "Signout";
        public const string Index = "Index";
        public const string Delete = "Delete";
        public const string ViewDetails = "ViewDetails";
        public const string ViewAllData = "ViewAllData";
        public const string ViewAllDataSalons = "ViewAllDataSalons";
        public const string ViewAllDataCustomer = "ViewAllDataCustomer";
        public const string ActiveInActive = "ActiveInActive";
        public const string UserGallery = "UserGallery";
        public const string ChangeStatusSalon = "ChangeStatusSalon";
        public const string ManageCustomers = "ManageCustomers";
        public const string ManageEmployeeLeaves = "ManageEmployeeLeaves";
        public const string ViewAllDataEmployees = "ViewAllDataEmployees";
        public const string ViewAllDataEmployeeLeaves = "ViewAllDataEmployeeLeaves";
        public const string ManageEmployeeWorksheet = "ManageEmployeeWorksheet";
        public const string ViewAllDataEmployeeWorkSheet = "ViewAllDataEmployeeWorkSheet";
        public const string ManageEmployees = "ManageEmployees";
        public const string ManageSalonServices = "ManageSalonServices";
        public const string UpdateCategoryDisplayOrder = "UpdateCategoryDisplayOrder";
        public const string ViewAllDataSalonServices = "ViewAllDataSalonServices";
        public const string ChangePassword = "ChangePassword";
        public const string ViewAllDataAppointments = "ViewAllDataAppointments";
        public const string ManageAppointments = "ManageAppointments";
        public const string BindServicesDropdown = "BindServicesDropdown";
        public const string ViewDataCustomerDetails = "ViewDataCustomerDetails";
        public const string ViewAllDataEmployeeLeavecount = "ViewAllDataEmployeeLeavecount";
        public const string GetAppointmentDetaislById = "GetAppointmentDetaislById";
        public const string ManageEmployeeAndCharges = "ManageEmployeeAndCharges";
        public const string ViewAllDataEmployeeAndCharges = "ViewAllDataEmployeeAndCharges";
        public const string ViewAllDataMasterProductType = "ViewAllDataMasterProductType";
        public const string MasterProductType = "MasterProductType";
        public const string ActiveInActiveProductType = "ActiveInActiveProductType";
        public const string DeleteProductType = "DeleteProductType";
        public const string ProductTypeUpsert = "ProductTypeUpsert";
        public const string GetDataProductTypeById = "GetDataProductTypeById";
        public const string OrderDetails = "OrderDetails";
        public const string ManagePage = "ManagePage";
        public const string GetSpecialityData = "GetSpecialityData";
        public const string SaveSpecialityMaster = "SaveSpecialityMaster";
        public const string DeleteSpeciality = "DeleteSpeciality";
        public const string DeleteCity = "DeleteCity";
        public const string GetBadRatingSuggetions = "GetBadRatingSuggetions";
        public const string SaveBadRatingSuggetions = "SaveBadRatingSuggetions";
        public const string GetGoodRatingSuggetions = "GetGoodRatingSuggetions";
        public const string SaveGoodRatingSuggetions = "SaveGoodRatingSuggetions";
        public const string GetTrendingSearch = "GetTrendingSearch";
        public const string SaveTrendingSearch = "SaveTrendingSearch";
        public const string GetUsersFaqs = "GetUsersFaqs";
        public const string SaveUsersFaqs = "SaveUsersFaqs";
        public const string Customer_Orders = "Customer_Orders";

        public const string GetDataProductBrandById = "GetDataProductBrandById";
        public const string ProductBrandUpsert = "ProductBrandUpsert";
        public const string DeleteProductBrand = "DeleteProductBrand";
        public const string ViewAllDataMasterProductBrand = "ViewAllDataMasterProductBrand";
        public const string ActiveInActiveProductBrand = "ActiveInActiveProductBrand";
        public const string MasterProductBrand = "MasterProductBrand";

        public const string ViewAllDataMasterProductWeight = "ViewAllDataMasterProductWeight";
        public const string ActiveInActiveProductWeight = "ActiveInActiveProductWeight";
        public const string DeleteProductWeight = "DeleteProductWeight";
        public const string ProductWeightUpsert = "ProductWeightUpsert";
        public const string GetDataProductweightById = "GetDataProductweightById";
        public const string MasterProductWeight = "MasterProductWeight";
        public const string GetFactorData = "GetFactorData";
        public const string SaveFactor = "SaveFactor";
        public const string DeleteUsersFaq = "DeleteUsersFaq";
        public const string DeleteTrendingSearch = "DeleteTrendingSearch";
        public const string TrendingSearchManage = "TrendingSearchManage";
        public const string DeleteGoodRatingSuggetions = "DeleteGoodRatingSuggetions";
        public const string GoodRatingManage = "GoodRatingManage";
        public const string DeleteBadRatingSuggetions = "DeleteBadRatingSuggetions";
        public const string BadRatingManage = "BadRatingManage";

        public const string ViewAllDataMasterOrderStatus = "ViewAllDataMasterOrderStatus";
        public const string ActiveInActiveOrderStatus = "ActiveInActiveOrderStatus";
        public const string DeleteOrderStatus = "DeleteOrderStatus";
        public const string OrderStatusUpsert = "OrderStatusUpsert";
        public const string GetDataOrderstatusById = "GetDataOrderstatusById";
        public const string MasterOrderStatus = "MasterOrderStatus";
        public const string MasterProductCategory = "MasterProductCategory";
        public const string ViewAllDataMasterProductCategory = "ViewAllDataMasterProductCategory";
        public const string ActiveInActiveProductCategory = "ActiveInActiveProductCategory";
        public const string DeleteProductCategory = "DeleteProductCategory";
        public const string ProductCategoryUpsert = "ProductCategoryUpsert";
        public const string GetDataProductCategoryById = "GetDataProductCategoryById";
        public const string UserOrderItems_StatusChange = "UserOrderItems_StatusChange";

        public const string GetAllUserPermissions = "GetAllUserPermissions";
        public const string UserPermissionsInsert = "UserPermissionsInsert";
        public const string GetAllCategoryMaster = "GetAllCategoryMaster";
        public const string DeleteCategory = "DeleteCategory";
        public const string GetCategoryData = "GetCategoryData";
        public const string SaveCategoryMaster = "SaveCategoryMaster";
        public const string GetAllSpeciality = "GetAllSpeciality";
        public const string GetAllCity = "GetAllCity";
        public const string GetCityData = "GetCityData";
        public const string SaveCity = "SaveCity";
        public const string GetAllCountay = "GetAllCountay";
        public const string GetAllState = "GetAllState";
        public const string TrendingSearchUpdateDisplayOrder = "TrendingSearchUpdateDisplayOrder";
        public const string GoodRatingUpdateDisplayOrder = "GoodRatingUpdateDisplayOrder";
        public const string BadRatingUpdateDisplayOrder = "BadRatingUpdateDisplayOrder";
        public const string SaveRatings = "SaveRatings";

        //yudooapp action method defind
        public const string ViewAllDataOrders = "ViewAllDataOrders";
        public const string ViewAllDataShop = "ViewAllDataShop";
        public const string SignIn = "SignIn";
        public const string AdminSignIn = "AdminSignIn";
        public const string Logout = "Logout";
        public const string ShopOverviewDataGet = "ShopOverviewDataGet";
        public const string PagesEdit = "PagesEdit";
        public const string PagesAdd = "PagesAdd";
        public const string DeletePages = "DeletePages";
        public const string GetAllAmountBifurcation = "GetAllAmountBifurcation";
        public const string PayAdd = "PayAdd";
        public const string PendingHelpSupportCall = "PendingHelpSupportCall";
        public const string PendingHelpSupportCallAction = "PendingHelpSupportCallAction";
        /// <summary>
        /// User right Permissions
        /// </summary>



        public const string GetAllBanners = "GetAllBanners";
        public const string SaveBanner = "SaveBanner";
        public const string GetAllPages = "GetAllPages";
        public const string GetPromoCodeById = "GetPromoCodeById";


        public const string ViewAllDataCallLogs = "ViewAllDataCallLogs";
        public const string OderDetailsDataGet = "OderDetailsDataGet";
        public const string Shop_Orders = "Shop_Orders";
        public const string ShopEmployees_All = "ShopEmployees_All";
        public const string Shop_ActInAct = "Shop_ActInAct";
        public const string ShopOwner_IsRating = "ShopOwner_IsRating";
        public const string ViewAllDataAdminShop = "ViewAllDataAdminShop";
        public const string GetShopDetails = "GetShopDetails";
        public const string ShopTimgings_IsClosedIsopen = "ShopTimgings_IsClosedIsopen";
        public const string Shop_IsApprovedByAdmin = "Shop_IsApprovedByAdmin";

        public const string GetDataUserShopCallLogs = "GetDataUserShopCallLogs";
        public const string GetAllOffers = "GetAllOffers";
        public const string DeleteOffer = "DeleteOffer";
        public const string GetAllActiveOffers = "GetAllActiveOffers";
        public const string Offers_ActInAct = "Offers_ActInAct";
        public const string SendForApproval = "SendForApproval";
        public const string ShopAuditDataInsert = "ShopAuditDataInsert";
        public const string GetShopAuditDetails = "GetShopAuditDetails";
        public const string DeleteShopImagesAndVideos = "DeleteShopImagesAndVideos";
        public const string findShopNameById = "findShopNameById";
        public const string RefreshDataCategory = "RefreshDataCategory";

        public const string UpdateStatusFAQ = "UpdateStatusFAQ";
        public const string GetAllFaqVideos = "GetAllFaqVideos";
        public const string FaqVideosManage = "FaqVideosManage";
        public const string DeleteFaqVideos = "DeleteFaqVideos";
        public const string GetFaqVideos = "GetFaqVideos";
        public const string SaveFaqVideos = "SaveFaqVideos";
        public const string GetAllFaqs = "GetAllFaqs";
        public const string FaqsManage = "FaqsManage";
        public const string DeleteFaqs = "DeleteFaqs";
        public const string GetFaqs = "GetFaqs";
        public const string SaveFaqs = "SaveFaqs";
        public const string DeleteCuratedList = "DeleteCuratedList";
        public const string GetAllCuratedLists = "GetAllCuratedLists";
        public const string SaveCuratedList = "SaveCuratedList";
        public const string GetAllCategory = "GetAllCategory";


        public const string GetAllUsersFaq = "GetAllUsersFaq";
        public const string GetAllTrendingSearch = "GetAllTrendingSearch";
        public const string GetAllGoodRating = "GetAllGoodRating";
        public const string GetAllBadRating = "GetAllBadRating";
        public const string UsersFaqManage = "UsersFaqManage";

        public const string GetAllDownload = "GetAllDownload";
        public const string ExportTable = "ExportTable";


        public const string GetAllAdminUsers = "GetAllAdminUsers";
        public const string SaveAdminUsersList = "SaveAdminUsersList";
        public const string GetAdminUsersData = "GetAdminUsersData";
        public const string UpdateStatusAdminUsers = "UpdateStatusAdminUsers";
        public const string DeleteAdminUsers = "DeleteAdminUsers";

        public const string OfferStatus_ChangeStatus = "OfferStatus_ChangeStatus";



        public const string ForGotPassword = "ForGotPassword";
        public const string ConfrimOTP = "ConfrimOTP";
        public const string ResetPassword = "ResetPassword";
        public const string ResetPSW = "ResetPSW";

        public const string SaveFactorRight = "SaveFactorRight";
        public const string GetFactorRightData = "GetFactorRightData";
        public const string CurrentyOfflineShops_All = "CurrentyOfflineShops_All";
        public const string GetUnansweredcall = "GetUnansweredcall";

        public const string AdminTypeActiveInActive = "AdminTypeActiveInActive";
        public const string DeletedAdminType = "DeletedAdminType";
        public const string CreateAdminType = "CreateAdminType";
        public const string AdminTypeGetData = "AdminTypeGetData";


        public const string CallStart = "CallStart";
        public const string CallEnd = "CallEnd";
        public const string ReciveCall = "ReciveCall";
        public const string UserOrderAmountData = "UserOrderAmountData";
        public const string ReceivePendingVideoCall = "ReceivePendingVideoCall";
        public const string ReceiveVideoCallInAdmin = "ReceiveVideoCallInAdmin";
        public const string ShopDetailsGet = "ShopDetailsGet";
        public const string CallBackManage = "CallBackManage";

        public const string OnSuccess = "OnSuccess";
        public const string UserOrderItemsStatus = "UserOrderItemsStatus";
        public const string MasterShopOwnerAccess = "MasterShopOwnerAccess";

        public const string AdminIsVideoCall = "AdminIsVideoCall";


        public const string Request_CallBack_Admin = "Request_CallBack_Admin";
        public const string UserOrderStatus = "UserOrderStatus";

    }
}
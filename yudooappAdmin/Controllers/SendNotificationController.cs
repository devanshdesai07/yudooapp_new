﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class SendNotificationController : BaseController
    {
        private readonly AbstractNotificationServices _abstractNotificationServices = null;
        public SendNotificationController(AbstractNotificationServices _abstractNotificationServices)
        {
            this._abstractNotificationServices = _abstractNotificationServices;
        }
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult SaveBanner(Notification notification)
        //{

        //    SuccessResult<AbstractNotification> notificationData = new SuccessResult<AbstractNotification>();

        //    try
        //    {
        //        notificationData = _abstractNotificationServices.SendGeneralNotifications(notification);
        //    }
        //    catch (Exception ex)
        //    {
        //        notificationData.Code = 400;
        //        notificationData.Message = ex.Message;
        //    }
        //    notificationData.Item = null;
        //    return Json(notificationData, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public JsonResult SaveBanner(string Description = "", long Type = 0)
        {
            Notification model = new Notification();
            model.Description = Description;
            model.Type = Type;
            var result = _abstractNotificationServices.SendGeneralNotifications(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
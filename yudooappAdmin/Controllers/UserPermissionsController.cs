﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class UserPermissionsController : BaseController
    {

        private readonly AbstractAdminTypeServices abstractAdminServices;
        private readonly AbstractPageServices abstractPageServices;
        private readonly AbstractUserPermissionsServices _AbstractUserPermissionsServicess = null;

        public class FormData
        {
            public int Id { get; set; }

            public bool IsActive { get; set; }

            public int AdminTypeId { get; set; }
        }

        public UserPermissionsController(AbstractAdminTypeServices _abstractAdminServices,
            AbstractUserPermissionsServices _AbstractUserPermissionsServicess, AbstractPageServices _AbstractPageServices)
        {
            this.abstractAdminServices = _abstractAdminServices;
            this.abstractPageServices = _AbstractPageServices;
            this._AbstractUserPermissionsServicess = _AbstractUserPermissionsServicess;
        }
        public ActionResult Index()
        {
            ViewBag.AdminTypeDrp = MasterAdminType();
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;

            return View();
        }


        [HttpPost]
        public JsonResult UserPermissionsInsert(List<FormData> selectValues)
        {

            SuccessResult<AbstractUserPermissions> offerData = new SuccessResult<AbstractUserPermissions>();

            try
            {
                List<AbstractUserPermissions> objUserPermissions = new List<AbstractUserPermissions>();
                foreach (var item in selectValues)
                {
                    objUserPermissions.Add(new UserPermissions()
                    {
                        AdminTypeId = item.AdminTypeId,
                        PageId = item.Id,
                        Status = item.IsActive,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now
                    });
                }


                _AbstractUserPermissionsServicess.UserPermissions_Upsert(objUserPermissions);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }
            offerData.Item = null;
            return Json(offerData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetData(long Id)
        {
            SuccessResult<AbstractUserPermissions> offerData = new SuccessResult<AbstractUserPermissions>();

            try
            {
                offerData = _AbstractUserPermissionsServicess.UserPermissions_ById(Id);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }

            return Json(offerData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllUserPermissions([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int AdminTypeId = 0)
        {
            {               
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _AbstractUserPermissionsServicess.UserPermissions_ByAdminId(pageParam, search, AdminTypeId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                //response = response.Values.Where(x => x.AdminTypeId == 0).ToArray();
                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllActiveUserPermissions([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _AbstractUserPermissionsServicess.UserPermissions_All_Active(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UserPermissions_ActInAct(long Id, bool IsActive)
        {
            SuccessResult<AbstractUserPermissions> offerData = new SuccessResult<AbstractUserPermissions>();

            try
            {
                offerData = _AbstractUserPermissionsServicess.UserPermissions_ActInAct(Id, IsActive);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }
            offerData.Item = null;
            return Json(offerData, JsonRequestBehavior.AllowGet);
        }

        //Admin type dropdown function
        public IList<SelectListItem> MasterAdminType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractAdminServices.AdminType_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }

                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
    }
}
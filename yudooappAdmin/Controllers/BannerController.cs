﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class BannerController : BaseController
    {
        private readonly AbstractBannersServices _abstractBannerServicess = null;

        private readonly AbstractOffersServices _abstractOffersServicess = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractShopOwnerServices _abstractShopOwnerServices = null;
        private readonly AbstractCuratedListServices _abstractCuratedListServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        public BannerController(AbstractBannersServices _abstractBannerServicess,
            AbstractOffersServices _abstractOffersServicess,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractCuratedListServices abstractCuratedListServices,
            AbstractShopOwnerServices abstractShopOwnerServices)
        {
            this._abstractBannerServicess = _abstractBannerServicess;
            this._abstractOffersServicess = _abstractOffersServicess;
            this._userPermissionsServices = userPermissionsServices;
            this._abstractShopOwnerServices = abstractShopOwnerServices;
            this._abstractCuratedListServices = abstractCuratedListServices;
        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }

            ViewBag.MasterCuratedListDrp = MasterCuratedList();
            ViewBag.MasterShopListDrp = MasterShopList();
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));            
            return View();
        }

        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterCuratedList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractCuratedListServices.CuratedList_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.CuratedListName.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> MasterShopList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                var models = _abstractShopOwnerServices.AdminShopOwnerDetails_All(pageParam, "",0,0,"",0,0,0);

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.ShopName.ToString(), Value = Convert.ToString(master.Id) });
                }

                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllBanners([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractBannerServicess.Banners_All(pageParam, search,2);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveBanner(HttpPostedFileBase files, Banners banners)
        {

            SuccessResult<AbstractBanners> bannerData = new SuccessResult<AbstractBanners>();

            try
            {
                if (banners.Id > 0)
                {
                    //"BannerFiles"
                    if (files != null)
                    {
                        string basePath = "BannerFiles/" + banners.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        banners.BannerUrl = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));

                        _abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);

                        //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), basePath + fileName);
                        //_abstractBannerServicess.GeneratePreSignedURL(Configurations.)
                    }
                    bannerData = _abstractBannerServicess.Banners_Upsert(banners);
                }
                
                if (banners.Id == 0)
                {
                    string basePath = "BannerFiles/" + banners.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    banners.BannerUrl = basePath + fileName;
                    _abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), banners.BannerUrl);
                    bannerData = _abstractBannerServicess.Banners_Upsert(banners);
                }

            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateDisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractBanners> bannerData = new SuccessResult<AbstractBanners>();

            try
            {
                bannerData = _abstractBannerServicess.Banners_Update_DisplayOrder(Id, IsUp);
            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetData(long Id)
        {
            SuccessResult<AbstractBanners> bannerData = new SuccessResult<AbstractBanners>();

            try
            {
                bannerData = _abstractBannerServicess.Banners_ById(Id);
            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }

            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateStatus(long Id)
        {
            SuccessResult<AbstractBanners> bannerData = new SuccessResult<AbstractBanners>();

            try
            {
                bannerData = _abstractBannerServicess.Banners_ActInAct(Id);
            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteBanner(long Id)
        {
            SuccessResult<AbstractBanners> bannerData = new SuccessResult<AbstractBanners>();

            try
            {
                bannerData = _abstractBannerServicess.Banners_Delete(Id);
            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Banner", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult GetAllOffers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        //{
        //    {
        //        int totalRecord = 0;
        //        int filteredRecord = 0;

        //        PageParam pageParam = new PageParam();
        //        pageParam.Offset = requestModel.Start;
        //        pageParam.Limit = requestModel.Length;

        //        string search = Convert.ToString(requestModel.Search.Value);
        //        var response = _abstractOffersServicess.Offers_All(pageParam, search);

        //        totalRecord = (int)response.TotalRecords;
        //        filteredRecord = (int)response.TotalRecords;

        //        return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
}
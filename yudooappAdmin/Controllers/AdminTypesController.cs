﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;
using yudooappAdmin.Pages;
using static yudooappAdmin.Infrastructure.Enums;

namespace yudooappAdmin.Controllers
{
    public class AdminTypesController : BaseController
    {
        private readonly AbstractAdminTypeServices _abstractAdminTypeServicess = null;
        private readonly AbstractAdminServices _abstractAdminServices = null;
        public AdminTypesController(AbstractAdminTypeServices _abstractAdminTypeServicess, AbstractAdminServices _abstractAdminServices)
        {
            this._abstractAdminTypeServicess = _abstractAdminTypeServicess;
            this._abstractAdminServices = _abstractAdminServices;
        }
        public ActionResult Index()
        {
            if (TempData["openPopupSuccess"] != null || TempData["openPopupError"] != null)
                ViewBag.openPopupSuc = TempData["openPopupSuccess"];
                ViewBag.openPopupEror = TempData["openPopupError"];

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100;
            ViewBag.AdminType = _abstractAdminTypeServicess.AdminType_All(pageParam, "").Values;
            return View();
        }

        [HttpGet]
        [ActionName(Actions.AdminTypeActiveInActive)]
        public ActionResult AdminTypeActiveInActive(int Id)
        {
            var result = _abstractAdminTypeServicess.AdminType_ActInAct(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.AdminIsVideoCall)]
        public ActionResult AdminIsVideoCall(bool CallStatus)
        {
            var result = _abstractAdminServices.Admin_IsCallAdmin(ProjectSession.AdminId, CallStatus);
            ProjectSession.IsAdminCall = result.Item.IsAdminCall;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeletedAdminType)]
        public ActionResult DeletedAdminType(int Id)
        {
            var result = _abstractAdminTypeServicess.AdminType_Delete(Id , unchecked((int)ProjectSession.AdminId));
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.CreateAdminType)]
        public ActionResult CreateAdminType(AdminType adminType)
        {
            long Id = 0;
            try
            {

                adminType.CreatedBy = unchecked((int)ProjectSession.AdminId);
                adminType.UpdatedBy = unchecked((int)ProjectSession.AdminId);

                SuccessResult<AbstractAdminType> result = new SuccessResult<AbstractAdminType>();
                result = _abstractAdminTypeServicess.AdminType_Upsert(adminType);

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopupSuccess"] = result.Message;
                    return RedirectToAction(Actions.Index, Pages.Controllers.AdminTypes, new { Area = "" });
                }
                else
                {
                    TempData["openPopupError"] = result.Message;
                    return RedirectToAction(Actions.Index, Pages.Controllers.AdminTypes, new { Area = "" });
                }
            }
            catch (Exception)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.AdminTypes, new { Area = ""});
        }


        [HttpGet]
        [ActionName(Actions.AdminTypeGetData)]
        public ActionResult AdminTypeGetData(int Id)
        {
            var result = _abstractAdminTypeServicess.AdminType_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
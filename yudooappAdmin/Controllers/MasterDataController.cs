﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class MasterDataController : BaseController
    {
        private readonly AbstractCategoryServices _abstractCategoryServices = null;
        private readonly AbstractSpecialityServices _abstractSpecialityServicess = null;
        private readonly AbstractCityServices _abstractCityServices = null;
        private readonly AbstractCountryServices _abstractCountryServices = null;
        private readonly AbstractStateServices _abstractStateServices = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractShopShortingValuesServices _abstractShopShortingValuesServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;

        public MasterDataController(AbstractCategoryServices _abstractCategoryServices,
            AbstractSpecialityServices _abstractSpecialityServicess,
            AbstractCityServices _abstractCityServices,
            AbstractCountryServices _abstractCountryServices,
            AbstractStateServices _abstractStateServices,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractShopShortingValuesServices abstractShopShortingValuesServices)
        {
            this._abstractCategoryServices = _abstractCategoryServices;
            this._abstractSpecialityServicess = _abstractSpecialityServicess;
            this._abstractCityServices = _abstractCityServices;
            this._abstractCountryServices = _abstractCountryServices;
            this._abstractStateServices = _abstractStateServices;
            _userPermissionsServices = userPermissionsServices;
            _abstractShopShortingValuesServices = abstractShopShortingValuesServices;
        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            ViewBag.CategoryData = _abstractCategoryServices.Category_All(pageParam, "").Values;
            return View();
        }
        public ActionResult ManageSpeciality(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            ViewBag.SpecialityData = _abstractSpecialityServicess.Speciality_All(pageParam, "").Values;
            return View();
        }
        public ActionResult ManageCity(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            ViewBag.CityData = _abstractCityServices.City_All(pageParam, "").Values;
            return View();
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult GetAllCategoryMaster([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractCategoryServices.Category_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveCategoryMaster(HttpPostedFileBase files, Category category )
        {

            SuccessResult<AbstractCategory> categoryData = new SuccessResult<AbstractCategory>();

            try
            {
                if (category.Id > 0)
                {
                    if(files != null)
                    {
                        string basePath = "CategoryImage/" + category.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }

                        category.ImageUrl = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));

                        _abstractCategoryServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), category.ImageUrl);
                    }
                    categoryData = _abstractCategoryServices.Category_Upsert(category);
                }
                
                if (category.Id == 0)
                {
                    string basePath = "CategoryImage/" + category.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    category.ImageUrl = basePath + fileName;

                    _abstractCategoryServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), category.ImageUrl);
                    categoryData = _abstractCategoryServices.Category_Upsert(category);
                }
                //files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                //categoryData.Item.ImageUrl = basePath + fileName;

                //_abstractCategoryServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), category.ImageUrl);
                //categoryData = _abstractCategoryServices.Category_Upsert(category);
               
            }
            catch (Exception ex)
            {
                categoryData.Code = 400;
                categoryData.Message = ex.Message;
            }
            categoryData.Item = null;
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult GetCategoryData(long Id)
        {
            SuccessResult<AbstractCategory> categoryData = new SuccessResult<AbstractCategory>();

            try
            {
                categoryData = _abstractCategoryServices.Category_ById(Id);
            }
            catch (Exception ex)
            {
                categoryData.Code = 400;
                categoryData.Message = ex.Message;
            }

            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllSpeciality([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractSpecialityServicess.Speciality_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetSpecialityData(long Id)
        {
            SuccessResult<AbstractSpeciality> specialityData = new SuccessResult<AbstractSpeciality>();

            try
            {
                specialityData = _abstractSpecialityServicess.Speciality_ById(Id);
            }
            catch (Exception ex)
            {
                specialityData.Code = 400;
                specialityData.Message = ex.Message;
            }

            return Json(specialityData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveSpecialityMaster( Speciality speciality)
        {

            SuccessResult<AbstractSpeciality> SpecialityData = new SuccessResult<AbstractSpeciality>();

            try
            {
                SpecialityData = _abstractSpecialityServicess.Speciality_Upsert(speciality);
            }
            catch (Exception ex)
            {
                SpecialityData.Code = 400;
                SpecialityData.Message = ex.Message;
            }
            SpecialityData.Item = null;
            return Json(SpecialityData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ShopShortValue(ShopShortingValues shopShortingValues)
        {

            SuccessResult<AbstractShopShortingValues> abstractShopShortingValues = new SuccessResult<AbstractShopShortingValues>();

            try
            {
                abstractShopShortingValues = _abstractShopShortingValuesServices.ShopShortingValues_Upsert(shopShortingValues);
            }
            catch (Exception ex)
            {
                abstractShopShortingValues.Code = 400;
                abstractShopShortingValues.Message = ex.Message;
            }
            return Json(abstractShopShortingValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ShopShortingValues_All(PageParam pageParam)
        {

            PagedList<AbstractShopShortingValues> abstractShopShortingValues = new PagedList<AbstractShopShortingValues>();

            try
            {
                
                abstractShopShortingValues = _abstractShopShortingValuesServices.ShopShortingValues_All(pageParam);
            }
            catch (Exception ex)
            {
                
            }
            return Json(abstractShopShortingValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSpeciality(long Id)
        {
            SuccessResult<AbstractSpeciality> SpecialityData = new SuccessResult<AbstractSpeciality>();

            try
            {
                SpecialityData = _abstractSpecialityServicess.Speciality_Delete(Id, ProjectSession.AdminId);
            }
            catch (Exception ex)
            {
                SpecialityData.Code = 400;
                SpecialityData.Message = ex.Message;
            }
            SpecialityData.Item = null;
            return Json(SpecialityData, JsonRequestBehavior.AllowGet);
        }
    



        [HttpPost]
        public JsonResult DeleteCategory(long Id)
        {
            SuccessResult<AbstractCategory> categoryData = new SuccessResult<AbstractCategory>();

            try
            {
                categoryData = _abstractCategoryServices.Category_Delete(Id);
            }
            catch (Exception ex)
            {
                categoryData.Code = 400;
                categoryData.Message = ex.Message;
            }
            categoryData.Item = null;
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllCity([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractCityServices.City_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllCountay()
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                //string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractCountryServices.Country_All(pageParam, "");

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllState(long CountryId)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var response = _abstractStateServices.State_ByCountryId(pageParam, CountryId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetCityData(long Id)
        {
            SuccessResult<AbstractCity> cityData = new SuccessResult<AbstractCity>();

            try
            {
                cityData = _abstractCityServices.City_ById(Id);
            }
            catch (Exception ex)
            {
                cityData.Code = 400;
                cityData.Message = ex.Message;
            }

            return Json(cityData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCity(City city)
        {

            SuccessResult<AbstractCity> cityData = new SuccessResult<AbstractCity>();

            try
            {
                cityData = _abstractCityServices.City_Upsert(city);
            }
            catch (Exception ex)
            {
                cityData.Code = 400;
                cityData.Message = ex.Message;
            }
            cityData.Item = null;
            return Json(cityData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteCity(long Id)
        {
            SuccessResult<AbstractCity> CityData = new SuccessResult<AbstractCity>();

            try
            {
                CityData = _abstractCityServices.City_Delete(Id);
            }
            catch (Exception ex)
            {
                CityData.Code = 400;
                CityData.Message = ex.Message;
            }
            CityData.Item = null;
            return Json(CityData, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "MasterData", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}
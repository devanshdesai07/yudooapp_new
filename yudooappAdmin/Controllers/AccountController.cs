﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Pages;

namespace yudooappAdmin.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly AbstractAdminServices abstractAdminServices;
        #endregion

        #region Ctor
        public AccountController(AbstractAdminServices abstractAdminServices)
        {
            this.abstractAdminServices = abstractAdminServices;
        }
        #endregion

        #region Methods
        public ActionResult SignIn()
        {
            return View();
        }

        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {

            if (Request.Cookies["AdminLogin"] != null)
            {
                string[] myCookies = Request.Cookies.AllKeys;
                foreach (string cookie in myCookies)
                {
                    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                }
            }
            bool result = abstractAdminServices.Admin_Logout(ProjectSession.AdminId);

            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction(Actions.SignIn, yudooappAdmin.Pages.Controllers.Account, new { Area = "" });
        }

        #region Json Result Post Method 
        [HttpPost]
        public JsonResult AdminSignIn(string username, string password)
        {
            SuccessResult<AbstractAdmin> result = abstractAdminServices.Admin_Login(username, password);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                Session.Clear();
                ProjectSession.AdminId = result.Item.Id;
                ProjectSession.UserId = result.Item.Id;
                ProjectSession.AdminName = result.Item.FirstName + ' ' + result.Item.LastName;
                ProjectSession.AdminTypeId = result.Item.AdminTypesId;
                ProjectSession.AdminProfile= result.Item.UserImages;
                ProjectSession.IsAdminCall = result.Item.IsAdminCall;
                HttpCookie cookie = new HttpCookie("AdminLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("AdminTypesId", result.Item.AdminTypesId.ToString());

                cookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ChangePassword(string oldPassword, string newPassword, string confirmPassword)
        {
            Admin Admin = new Admin();
            Admin.Id = ProjectSession.AdminId;
            Admin.OldPassword = oldPassword;
            Admin.NewPassword = newPassword;
            Admin.ConfirmPassword = confirmPassword;
            SuccessResult<AbstractAdmin> result1 = abstractAdminServices.Admin_ChangePassword(ProjectSession.AdminId, oldPassword, newPassword, confirmPassword);
            return Json(result1, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UnAuthorizedAccess()
        {
            return Json(new { Code = 401,Message= "Unauthorized Access" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult ForGotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForGotPassword(string email)
        {
            string RandomOTP = CommonHelper.OtpGenerate();
            SuccessResult<AbstractAdmin> result = abstractAdminServices.ForGotPassword(email, RandomOTP);
            if(result != null && result.Code == 200)
            {
                EmailHelper.SendOTP(email,RandomOTP,"ForGot Email Password.");
            }
            else
            {
                result.Message = "Something went wrong.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConfrimOTP(string email,string OTP)
        {
            Boolean response = false;
            SuccessResult<AbstractAdmin> result = abstractAdminServices.Admin_ByEmail(email);
            
            if(result != null && result.Code == 200)
            {
                if(result.Item != null)
                {
                    if(result.Item.OTP == OTP)
                    {
                        response = true;
                    }
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ResetPassword(string UserName)
        {
            Session["ResetUserName"] = UserName;
            
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(string email, string Password,string ConfirmPassword)
        {
            SuccessResult<AbstractAdmin> result = abstractAdminServices.ResetPassword(email,Password,ConfirmPassword);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
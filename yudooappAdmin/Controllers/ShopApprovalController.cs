﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Infrastructure;
using System.Linq;

namespace yudooappAdmin.Controllers
{
    public class ShopApprovalController : BaseController
    {
        #region Fields
        private readonly AbstractAdminShopServices abstractAdminShopServices;
        private readonly AbstractShopOwnerServices _abstractShopOwnerServices;
        private readonly AbstractCityServices abstractCityServices;
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        private readonly AbstractShopImagesAndVideosServices abstractShopImagesAndVideosServices;
        private readonly AbstractCategoryServices _abstractCategoryServices;
        private readonly AbstractSubCategoryServices _abstractSubCategoryServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public ShopApprovalController(AbstractAdminShopServices abstractAdminShopServices,
            AbstractCityServices abstractCityServices,
            AbstractShopOwnerServices _abstractShopOwnerServices,
            AbstractCategoryServices _abstractCategoryServices,
            AbstractShopOwnerServices abstractShopOwnerServices,
            AbstractShopImagesAndVideosServices abstractShopImagesAndVideosServices,
            AbstractSubCategoryServices _abstractSubCategoryServices,
            AbstractUserPermissionsServices userPermissionsServices
        )
        {
            this.abstractAdminShopServices = abstractAdminShopServices;
            this.abstractCityServices = abstractCityServices;
            this._abstractCategoryServices = _abstractCategoryServices;
            this._abstractShopOwnerServices = _abstractShopOwnerServices;
            this.abstractShopOwnerServices = abstractShopOwnerServices;
            this.abstractShopImagesAndVideosServices = abstractShopImagesAndVideosServices;
            this._abstractSubCategoryServices = _abstractSubCategoryServices;
            _userPermissionsServices = userPermissionsServices;

        }
        #endregion

        #region Methods

        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            ViewBag.MasterCityDrp = MasterCity();
            ViewBag.Category = BindCategory();
            ViewBag.SubCategory = BindSubCategory();
            return View();
        }
        #endregion

        [HttpPost]
        public JsonResult UpdateShopStatus(long Id,long StatusId)
        {
            SuccessResult<AbstractShopOwner> returnValues = new SuccessResult<AbstractShopOwner>();
            try
            {
                returnValues = _abstractShopOwnerServices.ShopOwner_UpdateStatus(Id,StatusId);
            }
            catch (Exception ex)
            {
                returnValues.Code = 400;
                returnValues.Message = ex.Message;
            }
            returnValues.Item = null;
            return Json(returnValues,JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> BindSubCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractSubCategoryServices.SubCategory_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        //City dropdown function
        public IList<SelectListItem> MasterCity()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractCityServices.City_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ViewAllDataAdminShop([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            long ShopOBStatus = 0,
            long ShopId = 0,
            string ShopName = "",
            string MobileNumber = "",
            long CityId = 0,
            string PrimaryCategory = "",
            string AllCategory = "",
            string DateOfJoining = ""
        )
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminShopServices.ShopApproval_All(pageParam, search , ShopOBStatus,ShopId, ShopName, MobileNumber, CityId, PrimaryCategory, AllCategory, DateOfJoining);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetShopDetails(int Id = 0)
        {
            SuccessResult<AbstractAdminShop> result = abstractAdminShopServices.AdminShop_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteShopImagesAndVideos(string Url, int Type, long ShopId)
        {
            SuccessResult<AbstractShopAudit> result = abstractShopOwnerServices.ShopAudit_DeleteMedia(Url, Type, ShopId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetShopAuditDetails(int ShopId = 0)
        {
            SuccessResult<AbstractShopAudit> result = abstractShopOwnerServices.ShopAudit_ShopId(ShopId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> BindCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractCategoryServices.Category_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "ShopApproval", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
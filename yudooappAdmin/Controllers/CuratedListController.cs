﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class CuratedListController : BaseController
    {
        private readonly AbstractCuratedListServices _abstractCuratedListServicess = null;
        private readonly AbstractOffersServices _abstractOffersServicess = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;

        public CuratedListController(AbstractCuratedListServices _abstractCuratedListServicess, 
            AbstractOffersServices _abstractOffersServicess, 
            AbstractUserPermissionsServices userPermissionsServices)
        {
            this._abstractCuratedListServicess = _abstractCuratedListServicess;
            this._abstractOffersServicess = _abstractOffersServicess;
            _userPermissionsServices = userPermissionsServices;
        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllCuratedLists([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractCuratedListServicess.CuratedList_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveCuratedList( CuratedList curatedLists)
        {

            SuccessResult<AbstractCuratedList> curatedListData = new SuccessResult<AbstractCuratedList>();

            try
            {
                curatedListData = _abstractCuratedListServicess.CuratedList_Upsert(curatedLists);
            }
            catch (Exception ex)
            {
                curatedListData.Code = 400;
                curatedListData.Message = ex.Message;
            }
            curatedListData.Item = null;
            return Json(curatedListData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult GetData(long Id)
        {
            SuccessResult<AbstractCuratedList> curatedListData = new SuccessResult<AbstractCuratedList>();

            try
            {
                curatedListData = _abstractCuratedListServicess.CuratedList_ById(Id);
            }
            catch (Exception ex)
            {
                curatedListData.Code = 400;
                curatedListData.Message = ex.Message;
            }

            return Json(curatedListData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateStatus(long Id)
        {
            SuccessResult<AbstractCuratedList> curatedListData = new SuccessResult<AbstractCuratedList>();

            try
            {
                curatedListData = _abstractCuratedListServicess.CuratedList_ActInAct(Id);
            }
            catch (Exception ex)
            {
                curatedListData.Code = 400;
                curatedListData.Message = ex.Message;
            }
            curatedListData.Item = null;
            return Json(curatedListData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteCuratedList(long Id,long DeletedBy)
        {
            SuccessResult<AbstractCuratedList> curatedListData = new SuccessResult<AbstractCuratedList>();

            try
            {
                curatedListData = _abstractCuratedListServicess.CuratedList_Delete(Id, DeletedBy);
            }
            catch (Exception ex)
            {
                curatedListData.Code = 400;
                curatedListData.Message = ex.Message;
            }
            curatedListData.Item = null;
            return Json(curatedListData, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "CuratedList", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult GetAllOffers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        //{
        //    {
        //        int totalRecord = 0;
        //        int filteredRecord = 0;

        //        PageParam pageParam = new PageParam();
        //        pageParam.Offset = requestModel.Start;
        //        pageParam.Limit = requestModel.Length;

        //        string search = Convert.ToString(requestModel.Search.Value);
        //        var response = _abstractOffersServicess.Offers_All(pageParam, search);

        //        totalRecord = (int)response.TotalRecords;
        //        filteredRecord = (int)response.TotalRecords;

        //        return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
}
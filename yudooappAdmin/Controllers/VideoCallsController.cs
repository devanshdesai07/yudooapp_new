﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Infrastructure;
using System.Linq;
using Newtonsoft.Json;

namespace yudooappAdmin.Controllers
{
    public class VideoCallsController : BaseController
    {
        #region Fields
        private readonly AbstractUserShopCallLogsServices abstractUserShopCallLogsServices;
        private readonly AbstractCityServices abstractCityServices;
        private readonly AbstractMasterOrderStatusServices abstractMasterOrderStatusServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractUsersServices abstractUsersServices;
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        private readonly AbstractCallBackServices abstractCallBackServices;
        private readonly AbstractUserShopCallLogsServices abstractUserShopCallLogsService;


        bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public VideoCallsController(AbstractUserShopCallLogsServices abstractUserShopCallLogsServices,
            AbstractCityServices abstractCityServices,
            AbstractMasterOrderStatusServices abstractMasterOrderStatusServices,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractUsersServices abstractUsersServices,
            AbstractShopOwnerServices abstractShopOwnerServices,
            AbstractCallBackServices abstractCallBackServices,
            AbstractUserShopCallLogsServices abstractUserShopCallLogsService
            )
        {
            this.abstractUserShopCallLogsServices = abstractUserShopCallLogsServices;
            this.abstractCityServices = abstractCityServices;
            this.abstractMasterOrderStatusServices = abstractMasterOrderStatusServices;
            this.abstractUsersServices = abstractUsersServices;
            this.abstractShopOwnerServices = abstractShopOwnerServices;
            this.abstractCallBackServices = abstractCallBackServices;
            this.abstractUserShopCallLogsService = abstractUserShopCallLogsService;
        }
        #endregion

        #region Methods

        public ActionResult Index()
        {
            ViewBag.MasterOrderStatusDrp = MasterOrderStatus();
            ViewBag.MasterCityDrp = MasterCity();
            return View();
        }
        #endregion

        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterOrderStatus()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractMasterOrderStatusServices.MasterOrderStatus_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        //City dropdown function
        public IList<SelectListItem> MasterCity()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractCityServices.City_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ViewAllDataCallLogs([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long Id = 0, long OrderId = 0, string UserMobileNumber = "", long ShopId = 0, string ShopName = "", string MobileNumber = "", string CallDateTime = "")
        {
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractUserShopCallLogsServices.UserShopCallLogsDetails_All(pageParam, search, Id, OrderId, UserMobileNumber, ShopId, ShopName, MobileNumber, CallDateTime);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
        }

        public ActionResult VideoCallPage()
        {
            return View();
        }

        public ActionResult JoinVideoCallPage(int ShopId = 0, string Response = "")
        {
            ViewBag.VideoUserResponse = Response;
            ViewBag.ShopIdBag = ShopId;
            ViewBag.ShopNameDrp = MasterShopName();
            return View();
        }

        [HttpPost]
        public JsonResult ReceiveVideoCallInAdmin(int UserId = 0)
        {
            var response = abstractUsersServices.Users_IsCallStatus(UserId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CallStart(long id, long type)
        {

            SuccessResult<AbstractUserShopCallLogs> abstractUserShopCallLogs = null;

            try
            {
                UserShopCallLogs userShopCallLogs = new UserShopCallLogs();
                userShopCallLogs.Id = 0;
                userShopCallLogs.WhoInitiated = "Admin";
                if (type == 1)
                {
                    userShopCallLogs.UserId = id;
                }
                else if (type == 2)
                {
                    userShopCallLogs.ShopId = id;
                }
                userShopCallLogs.CallDateTime = DateTime.Now;
                userShopCallLogs.CallStatus = 2;
                userShopCallLogs.CallStartTime = DateTime.Now;
                userShopCallLogs.CallEndTime = DateTime.Now;
                userShopCallLogs.CallBackHrs = 0;
                userShopCallLogs.AdminUserId = ProjectSession.AdminId;
                userShopCallLogs.CreatedBy = ProjectSession.AdminId;

                abstractUserShopCallLogs = abstractUserShopCallLogsServices.UserShopCallLogs_Upsert(userShopCallLogs);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(abstractUserShopCallLogs, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CallEnd(long id, long type)
        {

            SuccessResult<AbstractUserShopCallLogs> abstractUserShopCallLogs = null;

            try
            {
                UserShopCallLogs userShopCallLogs = new UserShopCallLogs();
                userShopCallLogs.Id = 0;
                userShopCallLogs.WhoInitiated = "Admin";
                if (type == 1)
                {
                    userShopCallLogs.UserId = id;
                }
                else if (type == 2)
                {
                    userShopCallLogs.ShopId = id;
                }
                //userShopCallLogs.CallDateTime = DateTime.Now.ToString();
                userShopCallLogs.CallStatus = 5;
                //userShopCallLogs.CallStartTime = DateTime.Now;
                userShopCallLogs.CallEndTime = DateTime.Now;
                //userShopCallLogs.CallStartTime = DateTime.Now;
                //userShopCallLogs.CallBackHrs = 0;
                userShopCallLogs.AdminUserId = ProjectSession.AdminId;
                userShopCallLogs.UpdatedBy = ProjectSession.AdminId;

                abstractUserShopCallLogs = abstractUserShopCallLogsServices.UserShopCallLogs_Upsert(userShopCallLogs);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Json(abstractUserShopCallLogs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ReciveCall([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long Id = 0, long OrderId = 0, string UserMobileNumber = "", long ShopId = 0, string ShopName = "", string MobileNumber = "", string CallDateTime = "")
        {

            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractUserShopCallLogsServices.UserShopCallLogsDetails_All(pageParam, search, Id, OrderId, UserMobileNumber, ShopId, ShopName, MobileNumber, CallDateTime);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ReceiveVideoCall()
        {
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0; //requestModel.Start;
                pageParam.Limit = 0; //requestModel.Length;

                var response = abstractUserShopCallLogsServices.UserShopCallLogs_ReceiveVideoCall(pageParam);

                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ReceivePendingVideoCall()
        {
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0; //requestModel.Start;
                pageParam.Limit = 0; //requestModel.Length;
                var response = abstractUserShopCallLogsServices.UserShopCallLogs_PendingCall(pageParam , ProjectSession.AdminId);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "VideoCalls", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    //var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    //if (add.Count() > 0)
                    //{
                    //    Add_UR = true;
                    //    Edit_UR = true;
                    //}
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //ShopOwner shop Details -------------
        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterShopName()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractShopOwnerServices.ShopOwner_All(pageParam, "", 0, 0, 0);

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.ShopName.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        [HttpPost]
        public JsonResult ShopDetailsGet(int ShopId = 0)
        {
            var response = abstractShopOwnerServices.ShopOwner_ById(ShopId, 0);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CallBackManage(int Id = 0,int ShopId = 0,int UserId = 0,string VideoCallLink = "",int Type = 0,int CallStatus = 0,
            string WhoInitiated = "")
        {

            UserShopCallLogs userShopCallLogs = new UserShopCallLogs();
            userShopCallLogs.Id = Id;
            userShopCallLogs.ShopId = ShopId;
            userShopCallLogs.UserId = UserId;
            userShopCallLogs.VideoCallLink = VideoCallLink;
            userShopCallLogs.Type = Type;
            userShopCallLogs.CallStatus = CallStatus;
            userShopCallLogs.WhoInitiated = WhoInitiated;
            userShopCallLogs.AdminId = ProjectSession.AdminId;
            userShopCallLogs.CreatedBy = 0;
            userShopCallLogs.UpdatedBy = 0;


            var response = abstractCallBackServices.CallBack_CallManage(userShopCallLogs);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UserShopCallLogs(int Id = 0, string VideoCallLink = "", int CallBackHrs = 0, int ShopId = 0, int UserId = 0,
           string WhoInitiated = "")
        {

            UserShopCallLogs userShopCallLogs = new UserShopCallLogs();
            userShopCallLogs.Id = 0;
            userShopCallLogs.VideoSessionId = VideoCallLink;
            userShopCallLogs.CallBackHrs = CallBackHrs;
            userShopCallLogs.CallStatus = 2;
            userShopCallLogs.ShopId = ShopId;
            userShopCallLogs.UserId = UserId;
            userShopCallLogs.WhoInitiated = "AdminToSellerAnswer";


            var response = abstractUserShopCallLogsService.UserShopCallLogs_Upsert(userShopCallLogs);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
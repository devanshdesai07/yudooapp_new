﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Infrastructure;
using System.Linq;

namespace yudooappAdmin.Controllers
{
    public class OrdersController : BaseController
    {
        #region Fields
        private readonly AbstractUserOrderServices abstractUserOrderServices;
        private readonly AbstractOrderServices abstractOrderServices;
        private readonly AbstractCityServices abstractCityServices;
        private readonly AbstractMasterOrderStatusServices abstractMasterOrderStatusServices;
        private readonly AbstractUserOrderItemsServices abstractUserOrderItemsServices;
        private readonly AbstractUserShopCallLogsServices abstractUserShopCallLogsServices;
        private readonly AbstractPaymentServices abstractPaymentServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractUserOrderAmountBifurcationServices abstractUserOrderAmountBifurcationServices;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public OrdersController(AbstractUserOrderServices abstractUserOrderServices,
            AbstractOrderServices abstractOrderServices,
            AbstractCityServices abstractCityServices,
            AbstractMasterOrderStatusServices abstractMasterOrderStatusServices,
            AbstractUserOrderItemsServices abstractUserOrderItemsServices,
            AbstractUserShopCallLogsServices abstractUserShopCallLogsServices,
            AbstractPaymentServices abstractPaymentServices,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractUserOrderAmountBifurcationServices abstractUserOrderAmountBifurcationServices
            )
        {
            this.abstractUserOrderServices = abstractUserOrderServices;
            this.abstractOrderServices = abstractOrderServices;
            this.abstractCityServices = abstractCityServices;
            this.abstractMasterOrderStatusServices = abstractMasterOrderStatusServices;
            this.abstractUserOrderItemsServices = abstractUserOrderItemsServices;
            this.abstractUserShopCallLogsServices = abstractUserShopCallLogsServices;
            this.abstractPaymentServices = abstractPaymentServices;
            _userPermissionsServices = userPermissionsServices;
            this.abstractUserOrderAmountBifurcationServices = abstractUserOrderAmountBifurcationServices;
        }
        #endregion

        #region Methods

        public ActionResult Index()
        {
            ViewBag.MasterOrderStatusDrp = MasterOrderStatus();
            ViewBag.MasterCityDrp = MasterCity();
            ViewBag.MasterPaymentDrp = MasterPaymentStatus();
            return View();
        }

        public ActionResult OrderDetails()
        {
            ViewBag.MasterOrderStatusDrp = MasterOrderStatus();
            return View();
        }

        //[HttpPost]
        //public JsonResult UserOrderItems_ByOrderId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int OrderId = 0)
        //{
        //    int totalRecord = 0;
        //    int filteredRecord = 0;
        //    PageParam pageParam = new PageParam();
        //    pageParam.Offset = 0;
        //    pageParam.Limit = 0;

        //    //string search = Convert.ToString(requestModel.Search.Value);
        //    var response = abstractUserOrderItemsServices.UserOrderItems_ByOrderId(pageParam, OrderId);

        //    totalRecord = (int)response.TotalRecords;
        //    filteredRecord = (int)response.TotalRecords;

        //    return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        //}

        public IList<SelectListItem> MasterPaymentStatus()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractPaymentServices.CustomerPaymentStatus_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Status.ToString(), Value = master.Status.ToString() });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        #endregion
        [HttpPost]
        public JsonResult UserOrderItems_ByOrderId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int OrderId = 0)
        {
            int totalRecord = 0;
            int filteredRecord = 0;
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractUserOrderItemsServices.UserOrderItems_ByOrderId(pageParam, OrderId);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetDataUserShopCallLogs([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int OrderId = 0)
        {
            int totalRecord = 0;
            int filteredRecord = 0;
            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractUserShopCallLogsServices.UserShopCallLogs_ByOrderId(pageParam, OrderId);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetDataUserOrderAmountBifurcation([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int OrderId = 0, string FromDate="", string ToDate="", string type="", int ispaid=0)
        {
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            string Search = Convert.ToString(requestModel.Search.Value);
            var result = abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_All(pageParam, Search, OrderId, FromDate, ToDate, type, ispaid);

            totalRecord = (int)result.TotalRecords;
            filteredRecord = (int)result.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, result.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetDataUserOrderAmountBifurcation_byOrderId([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int OrderId = 0)
        {
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            //string Search = Convert.ToString(requestModel.Search.Value);
            var result = abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_ByOrderId(pageParam,  OrderId);

            totalRecord = (int)result.TotalRecords;
            filteredRecord = (int)result.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, result.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }


        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterOrderStatus()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractMasterOrderStatusServices.MasterOrderStatus_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        //City dropdown function
        public IList<SelectListItem> MasterCity()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractCityServices.City_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ViewAllDataOrders([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long OrderStatus = 0,
            string PaymentStatus = "", long CityId = 0, decimal Amount = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string Search = Convert.ToString(requestModel.Search.Value);
                var response = abstractUserOrderServices.OrderDetails_All(pageParam, Search, OrderStatus, PaymentStatus, CityId, Amount);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        //Order Details Action method
        [HttpPost]
        public JsonResult OderDetailsDataGet(int OrderId = 0)
        {
            SuccessResult<AbstractOrderDetails> result = abstractOrderServices.OrderDetail_ById(OrderId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Order Details Action method
        [HttpPost]
        public JsonResult UserOrderItems_StatusChange(long Id = 0, long ItemStatusId = 0, long UpdatedBy = 0)
        {
            SuccessResult<AbstractUserOrderItems> result = abstractUserOrderItemsServices.UserOrderItems_StatusChange(Id, ItemStatusId, UpdatedBy);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveRatings(long Id = 0, long DeliveryRating = 0)
        {
            var result = abstractUserOrderServices.UserOrder_DeliveryRatingUpdate(Id, DeliveryRating);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UserOrderItemsStatus(long Id = 0, long StatusId = 0)
        {
            SuccessResult<AbstractUserOrderItems> result = abstractUserOrderItemsServices.UserOrderItems_Status(Id, StatusId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UserOrderStatus(long Id = 0, long StatusId = 0)
        {
            SuccessResult<AbstractUserOrder> result = abstractUserOrderServices.UserOrder_StatusChange(Id, StatusId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Orders", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var OrderDetails = offerData.Values.Where(x => x.SubPageAction == "OrderDetails" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }

                    //var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    //if (add.Count() > 0)
                    //{
                    //    Add_UR = true;
                    //    Edit_UR = true;
                    //}
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class PagesController : BaseController
    {
        private readonly AbstractPageServices _abstractPageServices = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;

        public PagesController(AbstractPageServices _abstractPageServices,
            AbstractUserPermissionsServices userPermissionsServices)
        {
            this._abstractPageServices = _abstractPageServices;
            _userPermissionsServices = userPermissionsServices;
        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllPages([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractPageServices.Pages_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetPromoCodeById(int Id = 0)
        {
            SuccessResult<AbstractPage> successResult = _abstractPageServices.Pages_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult PagesEdit(Page pages)
        {
            var result = _abstractPageServices.Pages_Upsert(pages);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDisplayOrderNo(long Id, bool IsUp)
        {
            SuccessResult<AbstractPage> pageData = new SuccessResult<AbstractPage>();

            try
            {
                pageData = _abstractPageServices.Pages_DisplayOrderNo(Id, IsUp);
            }
            catch (Exception ex)
            {
                pageData.Code = 400;
                pageData.Message = ex.Message;
            }
            pageData.Item = null;
            return Json(pageData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeletePages(long Id)
        {
            SuccessResult<AbstractPage> pagesData = new SuccessResult<AbstractPage>();

            try
            {
                pagesData = _abstractPageServices.Pages_Delete(Id);
            }
            catch (Exception ex)
            {
                pagesData.Code = 400;
                pagesData.Message = ex.Message;
            }
            pagesData.Item = null;
            return Json(pagesData, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Pages", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }


}
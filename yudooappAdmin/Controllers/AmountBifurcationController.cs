﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class AmountBifurcationController : BaseController
    {
        private readonly AbstractUserOrderAmountBifurcationServices _abstractUserOrderAmountBifurcationServices = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;


        bool view_UR = false, Add_UR = false, Edit_UR = false;

        public AmountBifurcationController(AbstractUserOrderAmountBifurcationServices _abstractUserOrderAmountBifurcationServices,
            AbstractUserPermissionsServices userPermissionsServices)
        {
            this._abstractUserOrderAmountBifurcationServices = _abstractUserOrderAmountBifurcationServices;
            _userPermissionsServices = userPermissionsServices;

        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllAmountBifurcation([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel , 
            long OrderId = 0, string FromDate = "" , string ToDate = "", string Type = "", int ispaid =0)
            {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_All(pageParam,search, OrderId, FromDate,
                    ToDate, Type, ispaid);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult PayAdd(UserOrderAmountBifurcation userOrderAmountBifurcation)
        {
            var result = _abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_UpdateNote(userOrderAmountBifurcation);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UserOrderAmountData(long OrderId = 0, string FromDate = "", string ToDate = "", string Type = "", int ispaid = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 500000;
            var result = _abstractUserOrderAmountBifurcationServices.UserOrderAmountBifurcation_All(pageParam, "", OrderId, FromDate,ToDate, Type, ispaid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Pages", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }


}
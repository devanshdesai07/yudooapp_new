﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using yudooappAdmin.Infrastructure;
using DataTables.Mvc;
using yudooapp.Common.Paging;

namespace yudooappAdmin.Controllers
{
    public class HomeController : BaseController
    {
        #region Fields
        private readonly AbstractAdminShopServices abstractAdminShopServices;
        private readonly AbstractHelpSupportServices abstractHelpSupportServices;
        #endregion

        #region Ctor
        public HomeController(AbstractAdminShopServices abstractAdminShopServices, AbstractHelpSupportServices abstractHelpSupportServices)
        {
            this.abstractAdminShopServices = abstractAdminShopServices;
            this.abstractHelpSupportServices = abstractHelpSupportServices;
        }
        #endregion

        #region Methods
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CurrentyOfflineShops_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminShopServices.CurrentyOfflineShops_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetUnansweredcall([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminShopServices.GetUnansweredcall(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult UnAuthorizedAccess()
        {
            return Json(new { Code = 401,Message= "Unauthorized Access" }, JsonRequestBehavior.AllowGet);
        }

        //PendingHelpSupportCall
        [HttpPost]
        public JsonResult PendingHelpSupportCall()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 99999;
            var response = abstractHelpSupportServices.HelpSupport_All(pageParam,"");
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //User Call Cut
        [HttpPost]
        public JsonResult PendingHelpSupportCallAction(long Id = 0,long StatusId = 0)
        {
            var response = abstractHelpSupportServices.HelpSupport_Action(Id, StatusId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
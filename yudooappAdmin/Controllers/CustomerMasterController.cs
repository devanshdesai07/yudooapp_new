﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class CustomerMasterController : BaseController
    {
        private readonly AbstractCustomerServices _abstractCustomerServices = null;
        private readonly AbstractBannersServices _abstractBannerServicess = null;
        private readonly AbstractOffersServices _abstractOffersServicess = null;
        private readonly AbstractHomePageShopOrderFactorsServices _abstractHomePageShopOrderFactorsServices = null;
        private readonly AbstractUsersFAQServices _abstractUsersFAQServicess = null;
        private readonly AbstractTrendingSearchServices _abstractTrendingSearchServicess = null;
        private readonly AbstractGoodRatingSuggestionsServices _abstractGoodRatingSuggestionsServicess = null;
        private readonly AbstractBadRatingSuggestionsServices _abstractBadRatingSuggestionsServicess = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractShopKeeperMasterServices _abstractShopKeeperMasterServicess = null;
        private readonly AbstractShopOwnerServices abstractShopOwnerServicess = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;

        public CustomerMasterController(AbstractBannersServices _abstractBannerServicess,
            AbstractOffersServices _abstractOffersServicess,
            AbstractHomePageShopOrderFactorsServices _abstractHomePageShopOrderFactorsServices,
            AbstractCustomerServices _abstractCustomerServices,
            AbstractUsersFAQServices _abstractUsersFAQServicess,
            AbstractTrendingSearchServices _abstractTrendingSearchServicess,
            AbstractGoodRatingSuggestionsServices _abstractGoodRatingSuggestionsServicess,
            AbstractBadRatingSuggestionsServices _abstractBadRatingSuggestionsServicess,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractShopKeeperMasterServices _abstractShopKeeperMasterServicess,
            AbstractShopOwnerServices abstractShopOwnerServicess
        )
        {
            this._abstractBannerServicess = _abstractBannerServicess;
            this._abstractOffersServicess = _abstractOffersServicess;
            this._abstractCustomerServices = _abstractCustomerServices;
            this._abstractHomePageShopOrderFactorsServices = _abstractHomePageShopOrderFactorsServices;
            this._abstractUsersFAQServicess = _abstractUsersFAQServicess;
            this._abstractTrendingSearchServicess = _abstractTrendingSearchServicess;
            this._abstractGoodRatingSuggestionsServicess = _abstractGoodRatingSuggestionsServicess;
            this._abstractBadRatingSuggestionsServicess = _abstractBadRatingSuggestionsServicess;
            this._abstractShopKeeperMasterServicess = _abstractShopKeeperMasterServicess;
            _userPermissionsServices = userPermissionsServices;
            this.abstractShopOwnerServicess = abstractShopOwnerServicess;
            
        }



        
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult UsersFaqManage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }
        public ActionResult TrendingSearchManage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }
        public ActionResult GoodRatingManage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }
        public ActionResult BadRatingManage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllUsersFaq([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractUsersFAQServicess.UsersFAQ_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveUsersFaqs(UsersFAQ usersFAQ)
        {

            SuccessResult<AbstractUsersFAQ> usersfaqsData = new SuccessResult<AbstractUsersFAQ>();

            try
            {
                usersfaqsData = _abstractUsersFAQServicess.UsersFAQ_Upsert(usersFAQ);
            }
            catch (Exception ex)
            {
                usersfaqsData.Code = 400;
                usersfaqsData.Message = ex.Message;
            }
            usersfaqsData.Item = null;
            return Json(usersfaqsData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetUsersFaqs(long Id)
        {
            SuccessResult<AbstractUsersFAQ> usersfaqsData = new SuccessResult<AbstractUsersFAQ>();

            try
            {
                usersfaqsData = _abstractUsersFAQServicess.UsersFAQ_ById(Id);
            }
            catch (Exception ex)
            {
                usersfaqsData.Code = 400;
                usersfaqsData.Message = ex.Message;
            }

            return Json(usersfaqsData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateStatus(long Id)
        {
            SuccessResult<AbstractUsersFAQ> usersfaqsData = new SuccessResult<AbstractUsersFAQ>();

            try
            {
                usersfaqsData = _abstractUsersFAQServicess.UsersFAQ_ActInAct(Id);
            }
            catch (Exception ex)
            {
                usersfaqsData.Code = 400;
                usersfaqsData.Message = ex.Message;
            }
            usersfaqsData.Item = null;
            return Json(usersfaqsData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteUsersFaq(long Id)
        {
            SuccessResult<AbstractUsersFAQ> usersfaqData = new SuccessResult<AbstractUsersFAQ>();

            try
            {
                usersfaqData = _abstractUsersFAQServicess.UsersFAQ_Delete(Id);
            }
            catch (Exception ex)
            {
                usersfaqData.Code = 400;
                usersfaqData.Message = ex.Message;
            }
            usersfaqData.Item = null;
            return Json(usersfaqData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllTrendingSearch([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractTrendingSearchServicess.TrendingSearch_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SaveTrendingSearch(TrendingSearch trendingSearch)
        {

            SuccessResult<AbstractTrendingSearch> trendingSearchData = new SuccessResult<AbstractTrendingSearch>();

            try
            {
                trendingSearchData = _abstractTrendingSearchServicess.TrendingSearch_Upsert(trendingSearch);
            }
            catch (Exception ex)
            {
                trendingSearchData.Code = 400;
                trendingSearchData.Message = ex.Message;
            }
            trendingSearchData.Item = null;
            return Json(trendingSearchData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTrendingSearch(long Id)
        {
            SuccessResult<AbstractTrendingSearch> trendingSearchData = new SuccessResult<AbstractTrendingSearch>();

            try
            {
                trendingSearchData = _abstractTrendingSearchServicess.TrendingSearch_ById(Id);
            }
            catch (Exception ex)
            {
                trendingSearchData.Code = 400;
                trendingSearchData.Message = ex.Message;
            }

            return Json(trendingSearchData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteTrendingSearch(long Id)
        {
            SuccessResult<AbstractTrendingSearch> trendingSearchData = new SuccessResult<AbstractTrendingSearch>();

            try
            {
                trendingSearchData = _abstractTrendingSearchServicess.TrendingSearch_Delete(Id);
            }
            catch (Exception ex)
            {
                trendingSearchData.Code = 400;
                trendingSearchData.Message = ex.Message;
            }
            trendingSearchData.Item = null;
            return Json(trendingSearchData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllGoodRating([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractGoodRatingSuggestionsServicess.GoodRatingSuggestions_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllBadRating([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractBadRatingSuggestionsServicess.BadRatingSuggestions_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult SaveGoodRatingSuggetions(GoodRatingSuggestions goodRatingSuggestions)
        {

            SuccessResult<AbstractGoodRatingSuggestions> goodRatingSuggestionsData = new SuccessResult<AbstractGoodRatingSuggestions>();

            try
            {
                goodRatingSuggestionsData = _abstractGoodRatingSuggestionsServicess.GoodRatingSuggestions_Upsert(goodRatingSuggestions);
            }
            catch (Exception ex)
            {
                goodRatingSuggestionsData.Code = 400;
                goodRatingSuggestionsData.Message = ex.Message;
            }
            goodRatingSuggestionsData.Item = null;
            return Json(goodRatingSuggestionsData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetGoodRatingSuggetions(long Id)
        {
            SuccessResult<AbstractGoodRatingSuggestions> goodRatingSuggestionsData = new SuccessResult<AbstractGoodRatingSuggestions>();

            try
            {
                goodRatingSuggestionsData = _abstractGoodRatingSuggestionsServicess.GoodRatingSuggestions_ById(Id);
            }
            catch (Exception ex)
            {
                goodRatingSuggestionsData.Code = 400;
                goodRatingSuggestionsData.Message = ex.Message;
            }

            return Json(goodRatingSuggestionsData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteGoodRatingSuggetions(long Id)
        {
            SuccessResult<AbstractGoodRatingSuggestions> goodRatingSuggestionsData = new SuccessResult<AbstractGoodRatingSuggestions>();

            try
            {
                goodRatingSuggestionsData = _abstractGoodRatingSuggestionsServicess.GoodRatingSuggestions_Delete(Id);
            }
            catch (Exception ex)
            {
                goodRatingSuggestionsData.Code = 400;
                goodRatingSuggestionsData.Message = ex.Message;
            }
            goodRatingSuggestionsData.Item = null;
            return Json(goodRatingSuggestionsData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult SaveBadRatingSuggetions(BadRatingSuggestions badRatingSuggestions)
        {

            SuccessResult<AbstractBadRatingSuggestions> badRatingSuggestionsData = new SuccessResult<AbstractBadRatingSuggestions>();

            try
            {
                badRatingSuggestionsData = _abstractBadRatingSuggestionsServicess.BadRatingSuggestions_Upsert(badRatingSuggestions);
            }
            catch (Exception ex)
            {
                badRatingSuggestionsData.Code = 400;
                badRatingSuggestionsData.Message = ex.Message;
            }
            badRatingSuggestionsData.Item = null;
            return Json(badRatingSuggestionsData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetBadRatingSuggetions(long Id)
        {
            SuccessResult<AbstractBadRatingSuggestions> badRatingSuggestionsData = new SuccessResult<AbstractBadRatingSuggestions>();

            try
            {
                badRatingSuggestionsData = _abstractBadRatingSuggestionsServicess.BadRatingSuggestions_ById(Id);
            }
            catch (Exception ex)
            {
                badRatingSuggestionsData.Code = 400;
                badRatingSuggestionsData.Message = ex.Message;
            }

            return Json(badRatingSuggestionsData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteBadRatingSuggetions(long Id)
        {
            SuccessResult<AbstractBadRatingSuggestions> badRatingSuggestionsData = new SuccessResult<AbstractBadRatingSuggestions>();

            try
            {
                badRatingSuggestionsData = _abstractBadRatingSuggestionsServicess.BadRatingSuggestions_Delete(Id);
            }
            catch (Exception ex)
            {
                badRatingSuggestionsData.Code = 400;
                badRatingSuggestionsData.Message = ex.Message;
            }
            badRatingSuggestionsData.Item = null;
            return Json(badRatingSuggestionsData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult TrendingSearchUpdateDisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractTrendingSearch> trendingData = new SuccessResult<AbstractTrendingSearch>();

            try
            {
                trendingData = _abstractTrendingSearchServicess.TrendingSearch_Update_DisplayOrder(Id, IsUp);
            }
            catch (Exception ex)
            {
                trendingData.Code = 400;
                trendingData.Message = ex.Message;
            }
            trendingData.Item = null;
            return Json(trendingData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GoodRatingUpdateDisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractGoodRatingSuggestions> goodratingData = new SuccessResult<AbstractGoodRatingSuggestions>();

            try
            {
                goodratingData = _abstractGoodRatingSuggestionsServicess.GoodRatingSuggestions_Update_DisplayOrder(Id, IsUp);
            }
            catch (Exception ex)
            {
                goodratingData.Code = 400;
                goodratingData.Message = ex.Message;
            }
            goodratingData.Item = null;
            return Json(goodratingData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult BadRatingUpdateDisplayOrder(long Id, bool IsUp)
        {
            SuccessResult<AbstractBadRatingSuggestions> badratingData = new SuccessResult<AbstractBadRatingSuggestions>();

            try
            {
                badratingData = _abstractBadRatingSuggestionsServicess.BadRatingSuggestions_Update_DisplayOrder(Id, IsUp);
            }
            catch (Exception ex)
            {
                badratingData.Code = 400;
                badratingData.Message = ex.Message;
            }
            badratingData.Item = null;
            return Json(badratingData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetFactorData(int Id)
        {
            SuccessResult<AbstractHomePageShopOrderFactors> factorData = new SuccessResult<AbstractHomePageShopOrderFactors>();

            try
            {
                factorData = _abstractHomePageShopOrderFactorsServices.HomePageShopOrderFactors_ById(Id);
            }
            catch (Exception ex)
            {
                factorData.Code = 400;
                factorData.Message = ex.Message;
            }

            return Json(factorData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetFactorRightData(int Id)
        {
            SuccessResult<AbstractShopKeeperMaster> shopKeeperMasterData = new SuccessResult<AbstractShopKeeperMaster>();

            try
            {
                shopKeeperMasterData = _abstractShopKeeperMasterServicess.ShopKeeperMaster_ById(Id);
            }
            catch (Exception ex)
            {
                shopKeeperMasterData.Code = 400;
                shopKeeperMasterData.Message = ex.Message;
            }

            return Json(shopKeeperMasterData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveFactor(HomePageShopOrderFactors homePageShopOrderFactors)
        {

            SuccessResult<AbstractHomePageShopOrderFactors> factorData = new SuccessResult<AbstractHomePageShopOrderFactors>();

            try
            {
                factorData = _abstractHomePageShopOrderFactorsServices.HomePageShopOrderFactors_Upsert(homePageShopOrderFactors);
            }
            catch (Exception ex)
            {
                factorData.Code = 400;
                factorData.Message = ex.Message;
            }
            factorData.Item = null;
            return Json(factorData, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult SaveFactor(ShopKeeperMaster shopKeeperMaster)
        //{

        //    SuccessResult<AbstractShopKeeperMaster> factorData = new SuccessResult<AbstractShopKeeperMaster>();

        //    try
        //    {
        //        factorData = _abstractShopKeeperMasterServicess.ShopKeeperMaster_Upsert(shopKeeperMaster);
        //    }
        //    catch (Exception ex)
        //    {
        //        factorData.Code = 400;
        //        factorData.Message = ex.Message;
        //    }
        //    factorData.Item = null;
        //    return Json(factorData, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult SaveFactorRight(ShopKeeperMaster shopKeeperMaster)
        {

            SuccessResult<AbstractShopKeeperMaster> shopKeeperMasterData = new SuccessResult<AbstractShopKeeperMaster>();

            try
            {
                shopKeeperMasterData = _abstractShopKeeperMasterServicess.ShopKeeperMaster_Upsert(shopKeeperMaster);
            }
            catch (Exception ex)
            {
                shopKeeperMasterData.Code = 400;
                shopKeeperMasterData.Message = ex.Message;
            }
            shopKeeperMasterData.Item = null;
            return Json(shopKeeperMasterData, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //public JsonResult Shop_ActInAct(long Id = 0)
        //{
        //    SuccessResult<AbstractShopOwner> result = abstractShopOwnerServices.ShopOwner_ActInAct(Id);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult MasterShopOwnerAccess(bool Status)
        {
            bool result = abstractShopOwnerServicess.MasterOnline_Status(Status);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "CustomerMaster", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class AdminUsersController : BaseController
    {
        private readonly AbstractAdminServices _abstractAdminServicess = null;
        private readonly AbstractAdminTypeServices _abstractAdminTypeServicess = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        public AdminUsersController(AbstractAdminServices _abstractAdminServicess, AbstractAdminTypeServices _abstractAdminTypeServicess, AbstractUserPermissionsServices userPermissionsServices)
        {
            this._abstractAdminServicess = _abstractAdminServicess;
            this._abstractAdminTypeServicess = _abstractAdminTypeServicess;
            _userPermissionsServices = userPermissionsServices;
        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }

        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }

            ViewBag.MasterAdminTypeListDrp = MasterAdminTypeList();
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }

        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterAdminTypeList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                
                var models = _abstractAdminTypeServicess.AdminType_All(pageParam,"");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
        [HttpPost]
        public JsonResult GetAdminUsersData(long Id)
        {
            SuccessResult<AbstractAdmin> adminusersData = new SuccessResult<AbstractAdmin>();

            try
            {
                adminusersData = _abstractAdminServicess.Admin_ById(Id);
            }
            catch (Exception ex)
            {
                adminusersData.Code = 400;
                adminusersData.Message = ex.Message;
            }

            return Json(adminusersData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveAdminUsersList(HttpPostedFileBase files, Admin admin)
        {

            SuccessResult<AbstractAdmin> adminusersData = new SuccessResult<AbstractAdmin>();

            try
            {
                if (admin.Id > 0)
                {
                    //"BannerFiles"
                    if (files != null)
                    {
                        string basePath = "UserImages/" + admin.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        admin.UserImages = basePath + fileName;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));

                        _abstractAdminServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), admin.UserImages);

                        //_abstractBannerServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), basePath + fileName);
                        //_abstractBannerServicess.GeneratePreSignedURL(Configurations.)
                    }
                    adminusersData = _abstractAdminServicess.Admin_Upsert(admin);
                }

                if (admin.Id == 0)
                {
                    string basePath = "UserImages/" + admin.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    admin.UserImages = basePath + fileName;
                    _abstractAdminServicess.S3FileUpload(Server.MapPath("~/" + basePath + fileName), admin.UserImages);
                    adminusersData = _abstractAdminServicess.Admin_Upsert(admin);
                }

                //adminusersData = _abstractAdminServicess.Admin_Upsert(admin);
            }
            catch (Exception ex)
            {
                adminusersData.Code = 400;
                adminusersData.Message = ex.Message;
            }
            adminusersData.Item = null;
            return Json(adminusersData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateStatusAdminUsers(long Id)
        {
            SuccessResult<AbstractAdmin> adminusersData = new SuccessResult<AbstractAdmin>();

            try
            {
                adminusersData = _abstractAdminServicess.Admin_ActInAct(Id);
            }
            catch (Exception ex)
            {
                adminusersData.Code = 400;
                adminusersData.Message = ex.Message;
            }
            adminusersData.Item = null;
            return Json(adminusersData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteAdminUsers(long Id)
        {
            SuccessResult<AbstractAdmin> adminusersData = new SuccessResult<AbstractAdmin>();

            try
            {
                adminusersData = _abstractAdminServicess.Admin_Delete(Id);
            }
            catch (Exception ex)
            {
                adminusersData.Code = 400;
                adminusersData.Message = ex.Message;
            }
            adminusersData.Item = null;
            return Json(adminusersData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllAdminUsers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractAdminServicess.Admin_All(pageParam, search);
                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }


        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "AdminUsers", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using DataTables.Mvc;
using System;
using System.Linq;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class CustomerController : BaseController
    {
        #region Fields
        private readonly AbstractCustomerServices abstractCustomerServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractUserOrderItemsServices abstractUserOrderItemsServices;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public CustomerController(AbstractCustomerServices abstractCustomerServices,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractUserOrderItemsServices abstractUserOrderItemsServices)
        {
            this.abstractCustomerServices = abstractCustomerServices;
            _userPermissionsServices = userPermissionsServices;
            this.abstractUserOrderItemsServices = abstractUserOrderItemsServices;

        }
        #endregion

        #region Methods

        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        
        
        public JsonResult UnAuthorizedAccess()
        {
            return Json(new { Code = 401,Message= "Unauthorized Access" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Customer_Overview(long CustomerPhoneNumber = 0,int OrderID = 0)
        {
            var result = abstractCustomerServices.Customer_Overview(CustomerPhoneNumber, OrderID);
            return Json(result);
        }

        //[HttpPost]
        //public JsonResult Customer_Orders([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long MobileNumber = 0, int OrderId = 0)
        //{
        //    int totalRecord = 0;
        //    int filteredRecord = 0;
        //    PageParam pageParam = new PageParam();
        //    pageParam.Offset = requestModel.Start;
        //    pageParam.Limit = requestModel.Length;

        //    //string search = Convert.ToString(requestModel.Search.Value);
        //    var response = abstractCustomerServices.Customer_Orders(pageParam, MobileNumber, OrderId);

        //    totalRecord = (int)response.TotalRecords;
        //    filteredRecord = (int)response.TotalRecords;

        //    return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        //}
        [HttpPost]
        public JsonResult UserOrderItems_ByMobileNumber([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string MobileNumber = "")
        {
            int totalRecord = 0;
            int filteredRecord = 0;
            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            //string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractUserOrderItemsServices.UserOrderItems_ByMobileNumber(pageParam, MobileNumber);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }
        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Customer", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    //var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    //if (add.Count() > 0)
                    //{
                    //    Add_UR = true;
                    //    Edit_UR = true;
                    //}
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using System.IO;
using yudooappAdmin.Infrastructure;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace yudooappAdmin.Controllers
{
    public class ShopOnboardDetailController : BaseController
    {
        #region Fields
        private readonly AbstractAdminShopServices abstractAdminShopServices;
        private readonly AbstractCategoryServices _abstractCategoryServices;
        private readonly AbstractSubCategoryServices _abstractSubCategoryServices;
        private readonly AbstractCityServices abstractCityServices;
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        private readonly AbstractPaymentServices abstractPaymentServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;
        private readonly AbstractShopTimgingsServices _abstractShopTimgingsServices;
        private readonly AbstractStateServices abstractStateServices;
        bool view_UR = false, Add_UR = false, Edit_UR = false;
        private readonly AbstractSpecialityServices abstractSpecialityServices;

        #endregion

        #region Ctor
        public ShopOnboardDetailController(AbstractAdminShopServices abstractAdminShopServices,
            AbstractCategoryServices _abstractCategoryServices,
            AbstractSubCategoryServices _abstractSubCategoryServices,
            AbstractCityServices abstractCityServices,
            AbstractShopOwnerServices abstractShopOwnerServices,
            AbstractPaymentServices abstractPaymentServices,
            AbstractUserPermissionsServices userPermissionsServices,
            AbstractSpecialityServices abstractSpecialityServices,
            AbstractShopTimgingsServices abstractShopTimgingsServices,
            AbstractStateServices abstractStateServices
            )
        {
            this.abstractAdminShopServices = abstractAdminShopServices;
            this._abstractCategoryServices = _abstractCategoryServices;
            this._abstractSubCategoryServices = _abstractSubCategoryServices;
            this.abstractCityServices = abstractCityServices;
            this.abstractShopOwnerServices = abstractShopOwnerServices;
            this.abstractPaymentServices = abstractPaymentServices;
            this.abstractStateServices = abstractStateServices;
            _userPermissionsServices = userPermissionsServices;
            this.abstractSpecialityServices = abstractSpecialityServices;
            this._abstractShopTimgingsServices = abstractShopTimgingsServices;
        }
        #endregion

        #region Methods

        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            ViewBag.MasterCityDrp = MasterCity();
            ViewBag.MasterPaymentDrp = MasterPaymentStatus();
            ViewBag.Category = BindCategory();
            ViewBag.SubCategory = BindSubCategory();
            return View();
        }
        public ActionResult ViewDetails(long Id)
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            if (Id != 0)
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.MasterSpecialitiesListDrp = MasterSpecialitiesList();
            ViewBag.StateDropdown = MasterStateDropDown();
            ViewBag.Id = Id;
            ViewBag.Category = BindCategory();
            ViewBag.SubCategory = BindSubCategory();
            return View();
        }
        #endregion
        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterSpecialitiesList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractSpecialityServices.Speciality_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> MasterStateDropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractStateServices.State_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
        [HttpPost]
        public JsonResult RefreshDataCategory(long Id)
        {

            SuccessResult<AbstractCategory> result = _abstractCategoryServices.Category_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //City dropdown function
        public IList<SelectListItem> MasterCity()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractCityServices.City_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> MasterPaymentStatus()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractPaymentServices.CustomerPaymentStatus_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Status.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
        public IList<SelectListItem> BindCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractCategoryServices.Category_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = master.Id.ToString() });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
        public IList<SelectListItem> BindSubCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractSubCategoryServices.SubCategory_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = master.Id.ToString() });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ViewAllDataAdminShop([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            long ShopOBStatus = 0,
            long ShopId = 0,
            string ShopName = "",
            string MobileNumber = "",
            long CityId = 0,
            string PrimaryCategory = "",
            string AllCategory = "",
            string DateOfJoining = ""
        )
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractAdminShopServices.AdminShop_All(pageParam, search, ShopOBStatus, ShopId, ShopName, MobileNumber, CityId, PrimaryCategory, AllCategory, DateOfJoining);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetShopDetails(int Id = 0)
        {
            SuccessResult<AbstractAdminShop> result = abstractAdminShopServices.AdminShop_ById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SendForApproval(int Shopid = 0)
        {
            SuccessResult<AbstractAdminShop> result = abstractAdminShopServices.SendForApproval(Shopid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //ShopAuditDataInsert insert
        [HttpPost]
        public async Task<JsonResult> ShopAuditDataInsert(long Id = 0,
            long ShopId = 0,
            bool IsSave = false,
            long PrimaryCategoryId = 0,
            string SubCategoryId = "",
            string ProductTag = "",
            string Description = "",
            string Specialities = "",
            long CommissionPercentage = 0,
            string ShopName = "",
            string ShopNameGujarati = "",
            string ShopNameHindi = "",
            string DescriotionHindi = "",
            string DescriptionGujarati = "",
            string ShopOwnerName = "",
            string GSTNo = "",
            string CompanyName = "",
            string PersonalPAN = "",
            string CompanyPAN = "",
            string HiddenVideo = "",
            string HiddenGifVideo = "",
            string HiddenImage = "",
            long StateId = 0,
            long CityId = 0,
            long PinCode = 0,
            string AddressLine1 = "",
            string FlatNumber = "",
            //HttpPostedFileBase files = null,
            HttpPostedFileBase gifVideos = null,
            HttpPostedFileBase[] imageUrlsm = null,
            string ShopTime = null
        )
        {
            ShopAudit model = new ShopAudit();
            model.Id = Id;
            model.ShopId = ShopId;
            model.IsSave = IsSave;
            model.PrimaryCategoryId = PrimaryCategoryId;
            model.SubCategoryId = SubCategoryId;
            model.ProductTag = ProductTag;
            model.Description = Description;
            model.Specialities = Specialities;
            model.CommissionPercentage = CommissionPercentage;
            model.ShopName = ShopName;
            model.ShopNameGujarati = ShopNameGujarati;
            model.ShopNameHindi = ShopNameHindi;
            model.DescriotionHindi = DescriotionHindi;
            model.DescriptionGujarati = DescriptionGujarati;
            model.ShopOwnerName = ShopOwnerName;
            model.GSTNo = GSTNo;
            model.CompanyName = CompanyName;
            model.PersonalPAN = PersonalPAN;
            model.CompanyPAN = CompanyPAN;
            model.UpdatedBy = ProjectSession.AdminId;
            model.StateId = StateId;
            model.CityId = CityId;
            model.PinCode = PinCode;
            model.AddressLine1 = AddressLine1;
            model.FlatNumber = FlatNumber;

            SuccessResult<AbstractShopOwner> CustomeResult = new SuccessResult<AbstractShopOwner>();

            var CountryName = "";
            var StateName = "";
            var CityName = "";

            if (model.PinCode != 0)
            {
                using (var wc = new HttpClient())
                {
                    List<PincodeRootArray> pincodeRootArray = new List<PincodeRootArray>();

                    wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var path = "https://api.postalpincode.in/pincode/" + ConvertTo.String(model.PinCode);
                    //var AddressrResult = await wc.GetStringAsync(new Uri(path));
                    var AddressrResult = wc.GetStringAsync(path).GetAwaiter().GetResult();

                    object AddressResult = "";
                    pincodeRootArray = JsonConvert.DeserializeObject<List<PincodeRootArray>>(Convert.ToString(AddressrResult));

                    if (pincodeRootArray[0].Status == "Success")
                    {
                        CountryName = Convert.ToString(pincodeRootArray[0].PostOffice[0].Country);
                        StateName = Convert.ToString(pincodeRootArray[0].PostOffice[0].State);
                        CityName = Convert.ToString(pincodeRootArray[0].PostOffice[0].District);
                    }
                    else if (pincodeRootArray[0].Status == "Error")
                    {
                        CustomeResult.Code = 400;
                        CustomeResult.Message = "Pincode is invalid";
                    }
                }
            }
            else
            {
                CustomeResult.Code = 400;
                CustomeResult.Message = "Pincode is required";
            }

            var shopData = abstractShopOwnerServices.ShopOwner_ById(model.ShopId, 0).Item;

            if (model.AddressLine1 != null && ((shopData.FlatNumber != model.FlatNumber) || (shopData.AddressLine1 != model.AddressLine1)
                || (shopData.ShopName != model.ShopName)))
            {
                shopData.AddressCount = shopData.AddressCount + 1;

                abstractShopOwnerServices.AddressKey_Update(model.ShopId, "", shopData.AddressCount, 2);

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                CreatePickupLocation createPickupLocation = new CreatePickupLocation();
                createPickupLocation.pickup_location = ConvertTo.String(model.ShopId + "_" + shopData.AddressCount);
                createPickupLocation.name = model.ShopName;
                createPickupLocation.phone = shopData.MobileNumber;
                createPickupLocation.address = model.FlatNumber + " " + model.AddressLine1;
                createPickupLocation.address_2 = "";
                createPickupLocation.city = CityName;
                createPickupLocation.state = StateName;
                createPickupLocation.pin_code = ConvertTo.String(model.PinCode);
                createPickupLocation.country = CountryName;
                createPickupLocation.email = "test@test.com";
                using (var wc = new HttpClient())
                {
                    try
                    {
                        string var_sql = "select DeviceToken from ShipRocketToken where Id = 1";

                        SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                        SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                        DataTable dt = new DataTable();
                        sda.Fill(dt);

                        wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        wc.DefaultRequestHeaders.Add("Authorization", "Bearer " + dt.Rows[0]["DeviceToken"].ToString());

                        var getPickupPath = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup";
                        var path = "https://apiv2.shiprocket.in/v1/external/settings/company/addpickup";

                        var getPickResp = wc.GetStringAsync(getPickupPath).GetAwaiter().GetResult();

                        GetPickUp getPickUp = new GetPickUp();
                        getPickUp = JsonConvert.DeserializeObject<GetPickUp>(getPickResp);
                        bool containsItem = getPickUp.data.shipping_address.Any(item => item.pickup_location == ConvertTo.String(model.ShopId + "_" + shopData.AddressCount));
                        if (!containsItem)
                        {

                            var s = JsonConvert.SerializeObject(createPickupLocation);
                            var content = new StringContent(JsonConvert.SerializeObject(createPickupLocation), Encoding.UTF8, "application/json");

                            var result2 = await wc.PostAsync(new Uri(path), content);
                            string resultContent = await result2.Content.ReadAsStringAsync();
                            var res = JsonConvert.DeserializeObject<Pickup>(resultContent);

                            //var response = await client.PostAsync(url, data);

                            //var result = await response.Content.ReadAsStringAsync();

                            //Console.WriteLine(result);


                            var quote3 = abstractShopOwnerServices.AddShopOwnerPickUpId(shopData.Id, Convert.ToString(res.pickup_id));
                        }
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                    //List<CreateShipmentResData> createShipmentResDatas = new List<CreateShipmentResData>();
                    //createShipmentResDatas.Add(res.data.data[1]);
                    //res.data.data = createShipmentResDatas;

                }
            }

            //if (files != null)
            //{
            //    string basePath = "MediaUrl/";
            //    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
            //    string path = Server.MapPath("~/" + basePath);
            //    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
            //    {
            //        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
            //    }
            //    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
            //    abstractShopOwnerServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), basePath + fileName);
            //    model.FullLengthVideo = basePath + fileName;
            //}
            //else
            //{
            //    model.FullLengthVideo = HiddenVideo;
            //}

            if (gifVideos != null)
            {
                string basePath = "MediaUrl/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(gifVideos.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                gifVideos.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                abstractShopOwnerServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), basePath + fileName);
                model.GifVideo = basePath + fileName;
            }
            else
            {
                model.GifVideo = HiddenGifVideo;
            }

            string ImageCommaStr = "";

            if (imageUrlsm != null)
            {
                for (int i = 0; i < imageUrlsm.Length; i++)
                {
                    if (imageUrlsm[i] != null)
                    {
                        string basePath = "MediaUrl/";
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(imageUrlsm[i].FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        imageUrlsm[i].SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));

                        ImageCommaStr += basePath + fileName + ",";

                        abstractShopOwnerServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), basePath + fileName);
                    }
                }
                model.ImageUrl = HiddenImage + ImageCommaStr;
            }
            else
            {
                model.ImageUrl = HiddenImage;
            }

            var _model = JsonConvert.DeserializeObject<List<ShopTime>>(ShopTime);

            ShopTimingJson objShopTiming = new ShopTimingJson();
            objShopTiming.data = new List<Datum>();
            List<Datum> objlist = new List<Datum>();

            if (_model != null)
            {
                foreach (var item in _model)
                {
                    Datum obj = new Datum();
                    obj.Id = Convert.ToInt32(item.Id);
                    obj.ShopId = Convert.ToInt32(item.ShopId);
                    obj.FromTime = item.FromTime;
                    obj.ToTime = item.ToTime;
                    obj.BreaktimeStart = item.BreakStartTime;
                    obj.BreakTimeEnd = item.BreakEndTime;
                    obj.Day = item.Day;
                    obj.IsClosed = Convert.ToBoolean(item.IsClosed);
                    obj.CreatedBy = Convert.ToString(0);
                    obj.UpdatedBy = Convert.ToString(0);
                    objlist.Add(obj);
                }
            }
            //AbstractShopTimingJson abstractShopTimingJson = null;
            objShopTiming.data.AddRange(objlist);
            _abstractShopTimgingsServices.ShopTimgings_Upsert(objShopTiming);

            object results = "";

            if (CustomeResult.Code == 400)
            {
                results = CustomeResult;
            }
            else
            {
                results = abstractShopOwnerServices.ShopAudit_Upsert(model);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
            //var results = abstractShopOwnerServices.ShopAudit_Upsert(model);

        }
        [HttpPost]
        public JsonResult CityDrp(long StateId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var result = abstractCityServices.City_ByStateId(pageParam, StateId);
            return Json(result.Values, JsonRequestBehavior.AllowGet);
        }
        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "ShopOnboardDetail", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult ShopTimgings_IsClosedIsopen(long Id = 0)
        {
            SuccessResult<AbstractShopTimgings> result = _abstractShopTimgingsServices.ShopTimgings_IsClosedIsopen(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Chunk Upload VIDEO

        [HttpPost]
        public JsonResult OnSuccess(FormCollection collection, long Id = 0)
        {
            string key = collection["Key"];
            if (Id > 0)
            {
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                string var_sql = "insert into ShopChunkVideo (ShopId,FileURL,CreatedDate,CreatedBy,IsDeleted) values (@Id,@Key,@DateTimeUtc,@AdminUserID,0)";
                SqlCommand cmd = new SqlCommand(var_sql, con);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@Key", key);
                cmd.Parameters.AddWithValue("@DateTimeUtc", DateTime.UtcNow);
                cmd.Parameters.AddWithValue("@AdminUserID", ProjectSession.AdminId);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                ViewBag.Id = Id;
            }
            return Json("200", JsonRequestBehavior.AllowGet);
        }

    }
}
﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Infrastructure;
using System.Linq;

namespace yudooappAdmin.Controllers
{
    public class ShopOverviewController : BaseController
    {
        #region Fields
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        private readonly AbstractShopEmployeesServices abstractShopEmployeesServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public ShopOverviewController(AbstractShopOwnerServices abstractShopOwnerServices,
            AbstractShopEmployeesServices abstractShopEmployeesServices,
            AbstractUserPermissionsServices userPermissionsServices
            )
        {
            this.abstractShopOwnerServices = abstractShopOwnerServices;
            this.abstractShopEmployeesServices = abstractShopEmployeesServices;
            _userPermissionsServices = userPermissionsServices;
        }
        #endregion

        #region Methods
        
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ShopOverviewDataGet(int ShopId = 0, string OwnerPhoneNo = "",string FilterBy="")
        {
            string shopName = "";
            SuccessResult<AbstractShopOwner> result = abstractShopOwnerServices.Shop_Overview(ShopId, OwnerPhoneNo, shopName, FilterBy);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Shop_Orders([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string ShopMobileNumber = "", long ShopId = 0)
        {
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            string Search = Convert.ToString(requestModel.Search.Value);
            var response = abstractShopOwnerServices.Shop_Orders(pageParam, Search, ShopId, ShopMobileNumber);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ShopEmployees_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long ShopId = 0)
        {
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            string Search = Convert.ToString(requestModel.Search.Value);
            var response = abstractShopOwnerServices.ShopEmployee_ByParentId(pageParam, ShopId);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult Shop_ActInAct(long Id = 0)
        {
            SuccessResult<AbstractShopOwner> result = abstractShopOwnerServices.ShopOwner_ActInAct(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Shop_IsApprovedByAdmin(long Id = 0,long Status = 0)
        {
            SuccessResult<AbstractShopOwner> result = abstractShopOwnerServices.ShopOwner_IsApprovedByAdmin(Id, Status);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "ShopOverview", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    //var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    //if (add.Count() > 0)
                    //{
                    //    Add_UR = true;
                    //    Edit_UR = true;
                    //}
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion






    }
}
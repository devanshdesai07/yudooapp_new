﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class ShopkeeperSupportController : BaseController
    {
        private readonly AbstractFaqVideosServices _abstractFaqVideosServices = null;
        private readonly AbstractFAQsServices _abstractFAQsServices = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;

        public ShopkeeperSupportController(AbstractFaqVideosServices _abstractFaqVideosServices, 
            AbstractFAQsServices _abstractFAQsServices,
            AbstractUserPermissionsServices userPermissionsServices
            )
        {
            this._abstractFaqVideosServices = _abstractFaqVideosServices;
            this._abstractFAQsServices = _abstractFAQsServices;
            _userPermissionsServices = userPermissionsServices;
        }
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult FaqVideosManage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }


        public ActionResult FaqsManage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.RId = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllFaqVideos([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractFaqVideosServices.FaqVideo_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveFaqVideos(HttpPostedFileBase files, HttpPostedFileBase Imagefiles, FaqVideos faqVideos)
        {

            SuccessResult<AbstractFaqVideos> faqVideosData = new SuccessResult<AbstractFaqVideos>();

            try
            {
                if (faqVideos.Id > 0)
                {
                    if (Imagefiles != null)
                    {
                        string basePath = "FaqFiles/" + faqVideos.Id.ToString();
                        string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(Imagefiles.FileName);
                        string path = Server.MapPath("~/" + basePath);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                        }
                        faqVideos.ThumbnailImage = basePath + fileName;
                        Imagefiles.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));

                        _abstractFaqVideosServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), faqVideos.VideoUrl);
                    } 
                    if (files != null)
                    {
                        string basePath1 = "FaqVideo/" + faqVideos.Id.ToString();
                        string fileName1 = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                        string path1 = Server.MapPath("~/" + basePath1);
                        if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath1))))
                        {
                            Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath1));
                        }
                        faqVideos.VideoUrl = basePath1 + fileName1;
                        files.SaveAs(HttpContext.Server.MapPath("~/" + basePath1 + fileName1));

                        _abstractFaqVideosServices.S3FileUpload(Server.MapPath("~/" + basePath1 + fileName1), faqVideos.VideoUrl);
                    }
                    faqVideosData = _abstractFaqVideosServices.FaqVideo_Upsert(faqVideos);
                    //string basePath = "FaqVideo/" + faqVideos.Id.ToString();
                    //string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    //string path = Server.MapPath("~/" + basePath);
                    //if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    //{
                    //    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    //}
                    //faqVideos.VideoUrl = basePath + fileName;
                    //files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                }
                
                if (faqVideos.Id == 0)
                {
                    string basePath = "FaqFiles/" + faqVideos.Id.ToString();
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                    faqVideos.ThumbnailImage = basePath + fileName;
                    





                    string basePath1 = "FaqVideo/" + faqVideos.Id.ToString();
                    string fileName1 = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(files.FileName);
                    string path1 = Server.MapPath("~/" + basePath);
                    if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath1))))
                    {
                        Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath1));
                    }
                    files.SaveAs(HttpContext.Server.MapPath("~/" + basePath1 + fileName1));
                    faqVideos.VideoUrl = basePath1 + fileName1;



                    _abstractFaqVideosServices.S3FileUpload(Server.MapPath("~/" + basePath + fileName), faqVideos.VideoUrl);
                    faqVideosData = _abstractFaqVideosServices.FaqVideo_Upsert(faqVideos);
                }

            }
            catch (Exception ex)
            {
                faqVideosData.Code = 400;
                faqVideosData.Message = ex.Message;
            }
            faqVideosData.Item = null;
            return Json(faqVideosData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult GetFaqVideos(long Id)
        {
            SuccessResult<AbstractFaqVideos> faqVideosData = new SuccessResult<AbstractFaqVideos>();

            try
            {
                faqVideosData = _abstractFaqVideosServices.FaqVideo_ById(Id);
            }
            catch (Exception ex)
            {
                faqVideosData.Code = 400;
                faqVideosData.Message = ex.Message;
            }

            return Json(faqVideosData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateStatus(long Id)
        {
            SuccessResult<AbstractFaqVideos> faqVideosData = new SuccessResult<AbstractFaqVideos>();

            try
            {
                faqVideosData = _abstractFaqVideosServices.FaqVideo_ActInAct(Id);
            }
            catch (Exception ex)
            {
                faqVideosData.Code = 400;
                faqVideosData.Message = ex.Message;
            }
            faqVideosData.Item = null;
            return Json(faqVideosData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteFaqVideos(long Id)
        {
            SuccessResult<AbstractFaqVideos> faqVideosData = new SuccessResult<AbstractFaqVideos>();

            try
            {
                faqVideosData = _abstractFaqVideosServices.FaqVideo_Delete(Id);
            }
            catch (Exception ex)
            {
                faqVideosData.Code = 400;
                faqVideosData.Message = ex.Message;
            }
            faqVideosData.Item = null;
            return Json(faqVideosData, JsonRequestBehavior.AllowGet);
        }

        // FAQ


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllFaqs([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractFAQsServices.FAQs_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetFaqs(long Id)
        {
            SuccessResult<AbstractFAQs> faqsData = new SuccessResult<AbstractFAQs>();

            try
            {
                faqsData = _abstractFAQsServices.FAQs_ById(Id);
            }
            catch (Exception ex)
            {
                faqsData.Code = 400;
                faqsData.Message = ex.Message;
            }

            return Json(faqsData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveFaqs(FAQs faqs)
        {
            SuccessResult<AbstractFAQs> faqsData = new SuccessResult<AbstractFAQs>();

            try
            {
                if (faqs.Id > 0)
                {
                    faqs.UpdatedBy = ProjectSession.AdminId;
                }
                else
                {
                    faqs.CreatedBy = ProjectSession.AdminId;
                }
                faqsData = _abstractFAQsServices.FAQs_Upsert(faqs);
            }

            catch (Exception ex)
            {
                faqsData.Code = 400;
                faqsData.Message = ex.Message;
            }
            
            return Json(faqsData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFaqs(long Id)
        {
            SuccessResult<AbstractFAQs> faqsData = new SuccessResult<AbstractFAQs>();

            try
            {
                faqsData = _abstractFAQsServices.FAQs_Delete(Id);
            }
            catch (Exception ex)
            {
                faqsData.Code = 400;
                faqsData.Message = ex.Message;
            }
            faqsData.Item = null;
            return Json(faqsData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateStatusFAQ(long Id)
        {
            SuccessResult<AbstractFAQs> faqsData = new SuccessResult<AbstractFAQs>();

            try
            {
                faqsData = _abstractFAQsServices.FAQs_ActInAct(Id);
            }
            catch (Exception ex)
            {
                faqsData.Code = 400;
                faqsData.Message = ex.Message;
            }
            faqsData.Item = null;
            return Json(faqsData, JsonRequestBehavior.AllowGet);
        }
        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "ShopkeeperSupport", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                        Edit_UR = true;
                    }
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
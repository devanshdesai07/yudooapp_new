﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Infrastructure;
using System.Linq;

namespace yudooappAdmin.Controllers
{
    public class ShopController : BaseController
    {
        #region Fields
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        private readonly AbstractCityServices abstractCityServices;
        private readonly AbstractMasterOrderStatusServices abstractMasterOrderStatusServices;
        private readonly AbstractCategoryServices _abstractCategoryServices;
        private readonly AbstractSubCategoryServices _abstractSubCategoryServices;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public ShopController(AbstractShopOwnerServices abstractShopOwnerServices,
            AbstractCityServices abstractCityServices,
            AbstractMasterOrderStatusServices abstractMasterOrderStatusServices,
            AbstractCategoryServices _abstractCategoryServices,
            AbstractSubCategoryServices _abstractSubCategoryServices,
            AbstractUserPermissionsServices userPermissionsServices
        )
        {
            this.abstractShopOwnerServices = abstractShopOwnerServices;
            this.abstractCityServices = abstractCityServices;
            this.abstractMasterOrderStatusServices = abstractMasterOrderStatusServices;
            this._abstractCategoryServices = _abstractCategoryServices;
            this._abstractSubCategoryServices = _abstractSubCategoryServices;
            _userPermissionsServices = userPermissionsServices;

        }
        #endregion

        #region Methods
        
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            ViewBag.MasterOrderStatusDrp = MasterOrderStatus();
            ViewBag.MasterCityDrp = MasterCity();
            ViewBag.Category = BindCategory();
            ViewBag.SubCategory = BindSubCategory();
            return View();
        }
        #endregion

        //MasterOrderStatus dropdown function
        public IList<SelectListItem> MasterOrderStatus()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractMasterOrderStatusServices.MasterOrderStatus_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        [HttpPost]
        public JsonResult GetData(long Id,long UserId)
        {
            SuccessResult<AbstractShopOwner> shopData = new SuccessResult<AbstractShopOwner>();

            try
            {
                shopData = abstractShopOwnerServices.ShopOwner_ById(Id, UserId);
            }
            catch (Exception ex)
            {
                shopData.Code = 400;
                shopData.Message = ex.Message;
            }

            return Json(shopData, JsonRequestBehavior.AllowGet);
        }
        //City dropdown function
        public IList<SelectListItem> MasterCity()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractCityServices.City_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        public IList<SelectListItem> BindCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractCategoryServices.Category_All(pageParam, "");
                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }
        public IList<SelectListItem> BindSubCategory()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = _abstractSubCategoryServices.SubCategory_All(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ViewAllDataShop([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, long CityId = 0, long CategoryId = 0, string AllCategory = "", 
            long ShopRatingsId = 0, long ShopStatus = 0, int? DraftStatus = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                if(DraftStatus == 2)
                {
                    DraftStatus = 0;
                }

                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractShopOwnerServices.AdminShopOwnerDetails_All(pageParam, search , CityId, CategoryId, AllCategory, ShopRatingsId, ShopStatus , DraftStatus);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ShopOwner_IsRating(bool IsRating)
        {
            SuccessResult<AbstractShopOwner> result = abstractShopOwnerServices.ShopOwner_IsRating(IsRating);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Shop", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status == true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    //var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    //if (add.Count() > 0)
                    //{
                    //    Add_UR = true;
                    //    Edit_UR = true;
                    //}
                    //var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    //if (Edit.Count() > 0)
                    //{
                    //    Edit_UR = true;
                    //}
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
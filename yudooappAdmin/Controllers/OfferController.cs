﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yudooapp.Common;
using yudooapp.Common.Paging;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using yudooappAdmin.Infrastructure;

namespace yudooappAdmin.Controllers
{
    public class OfferController : BaseController
    {
        private readonly AbstractBannersServices _abstractBannerServicess = null;
        private readonly AbstractOffersServices _abstractOffersServicess = null;
        private readonly AbstractShopOwnerServices _abstractShopOwnerServices = null;
        private readonly AbstractUserPermissionsServices _userPermissionsServices = null;

        public OfferController(AbstractBannersServices _abstractBannerServicess,
            AbstractOffersServices _abstractOffersServicess,
            AbstractShopOwnerServices _abstractShopOwnerServices, 
            AbstractUserPermissionsServices userPermissionsServices)
        {
            this._abstractBannerServicess = _abstractBannerServicess;
            this._abstractOffersServicess = _abstractOffersServicess;
            this._abstractShopOwnerServices = _abstractShopOwnerServices;
            _userPermissionsServices = userPermissionsServices;
        }
        bool view_UR = false, Add_UR = false, Edit_UR = false;
        public ActionResult Index()
        {
            UserRights();
            if (!view_UR)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }
        public ActionResult Manage(string ri = "MA==")
        {
            UserRights();
            if (ri == "")
            {
                if (!Add_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            else
            {
                if (!Edit_UR)
                {
                    Response.Redirect("/Home/Index");
                }
            }
            ViewBag.Id = ConvertTo.Integer(ConvertTo.Base64Decode(ri));
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            ViewBag.ShopOwnerData = _abstractShopOwnerServices.ShopOwner_All(pageParam, "",0,0,0,0,0).Values;
            return View();
        }

        [HttpPost]
        public JsonResult OfferStatus_ChangeStatus(long Id = 0, long Status = 0)
        {
            SuccessResult<AbstractOffers> result = _abstractOffersServicess.OfferStatus_ChangeStatus(Id, Status);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveBanner(Offers offers)
        {

            SuccessResult<AbstractOffers> offerData = new SuccessResult<AbstractOffers>();

            try
            {
                offerData = _abstractOffersServicess.Offers_Upsert(offers);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }
            offerData.Item = null;
            return Json(offerData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteOffer(long Id)
        {
            SuccessResult<AbstractOffers> offerData = new SuccessResult<AbstractOffers>();

            try
            {
                offerData = _abstractOffersServicess.Offers_Delete(Id);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }
            offerData.Item = null;
            return Json(offerData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetData(long Id)
        {
            SuccessResult<AbstractOffers> offerData = new SuccessResult<AbstractOffers>();

            try
            {
                offerData = _abstractOffersServicess.Offers_ById(Id);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }

            return Json(offerData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult UpdateStatus(long Id)
        {
            SuccessResult<AbstractBanners> bannerData = new SuccessResult<AbstractBanners>();

            try
            {
                bannerData = _abstractBannerServicess.Banners_ActInAct(Id);
            }
            catch (Exception ex)
            {
                bannerData.Code = 400;
                bannerData.Message = ex.Message;
            }
            bannerData.Item = null;
            return Json(bannerData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllOffers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractOffersServicess.Offers_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAllActiveOffers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                var response = _abstractOffersServicess.Offers_All_Active(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Offers_ActInAct(long Id, bool IsActive)
        {
            SuccessResult<AbstractOffers> offerData = new SuccessResult<AbstractOffers>();

            try
            {
                offerData = _abstractOffersServicess.Offers_ActInAct(Id, IsActive);
            }
            catch (Exception ex)
            {
                offerData.Code = 400;
                offerData.Message = ex.Message;
            }
            offerData.Item = null;
            return Json(offerData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult findShopNameById(long Id, long UserId)
        {
            SuccessResult<AbstractShopOwner> abstractShopOwner = new SuccessResult<AbstractShopOwner>();

            try
            {
                abstractShopOwner = _abstractShopOwnerServices.ShopOwner_ById(Id, UserId);
            }
            catch (Exception ex)
            {
                abstractShopOwner.Code = 400;
                abstractShopOwner.Message = ex.Message;
            }

            return Json(abstractShopOwner, JsonRequestBehavior.AllowGet);
        }

        public void UserRights()
        {
            try
            {
                if (ProjectSession.AdminTypeId == 0)
                {
                    return;
                }
                int AdminTypeId = Convert.ToInt32(ProjectSession.AdminTypeId);
                PagedList<AbstractUserPermissions> offerData = new PagedList<AbstractUserPermissions>();
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;

                offerData = _userPermissionsServices.UserPermissions_ByAdminId(pageParam, "Offer", AdminTypeId);
                if (offerData.Values.Count > 0)
                {
                    var view = offerData.Values.Where(x => x.SubPageAction == "Index" && x.Status==true);
                    if (view.Count() > 0)
                    {
                        view_UR = true;
                    }
                    var add = offerData.Values.Where(x => x.SubPageAction == "Manage" && x.Status == true);
                    if (add.Count() > 0)
                    {
                        Add_UR = true;
                    }
                    var Edit = offerData.Values.Where(x => x.SubPageAction == "Edit" && x.Status == true);
                    if (Edit.Count() > 0)
                    {
                        Edit_UR = true;
                    }
                }
                else
                {
                    view_UR = false;
                    Add_UR = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}
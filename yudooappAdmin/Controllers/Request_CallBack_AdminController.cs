﻿using yudooapp.Common;
using yudooapp.Entities.Contract;
using yudooapp.Entities.V1;
using yudooapp.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using yudooapp.Common.Paging;
using System.Collections.Generic;
using yudooappAdmin.Infrastructure;
using System.Linq;
using Newtonsoft.Json;

namespace yudooappAdmin.Controllers
{
    public class Request_CallBack_AdminController : BaseController
    {
        #region Fields
        private readonly AbstractUserShopCallLogsServices abstractUserShopCallLogsServices;



        //bool view_UR = false, Add_UR = false, Edit_UR = false;
        #endregion

        #region Ctor
        public Request_CallBack_AdminController(AbstractUserShopCallLogsServices abstractUserShopCallLogsServices

            )
        {
            this.abstractUserShopCallLogsServices = abstractUserShopCallLogsServices;

        }
        #endregion

        #region Methods

        public ActionResult Index()
        {
            return View();
        }
        #endregion




        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Request_CallBack_Admin([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            int totalRecord = 0;
            int filteredRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = requestModel.Start;
            pageParam.Limit = requestModel.Length;

            string search = Convert.ToString(requestModel.Search.Value);
            var response = abstractUserShopCallLogsServices.Request_CallBack_Admin(pageParam, search);

            totalRecord = (int)response.TotalRecords;
            filteredRecord = (int)response.TotalRecords;

            return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
        }

    }
}